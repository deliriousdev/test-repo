﻿using System;
using System.Collections.Generic;
using System.Configuration;
using PayPal;

namespace StirCrazy.Core.Gateways
{
    public class PayPalGateway : IPayPalGateway
    {
        private readonly string _payPalClientId = ConfigurationManager.AppSettings["PayPalClientId"];
        private readonly string _payPalClientSecret = ConfigurationManager.AppSettings["PayPalClientSecret"];

        private readonly string _payPalSandBoxClientId = ConfigurationManager.AppSettings["PayPalSandBoxClientId"];
        private readonly string _payPalSandBoxClientSecret = ConfigurationManager.AppSettings["PayPalSandBoxClientSecret"];
        private readonly bool _payPalSandBoxMode = Convert.ToBoolean(ConfigurationManager.AppSettings["PayPalSandBoxMode"]);

        private string AccessToken { get; set; }
        public APIContext Api { get; set; }

        public PayPalGateway()
        {
            try
            {
                AccessToken = _payPalSandBoxMode
                                ? new OAuthTokenCredential(_payPalSandBoxClientId, _payPalSandBoxClientSecret, GetConfiguration()).GetAccessToken()
                                : new OAuthTokenCredential(_payPalClientId, _payPalClientSecret, GetConfiguration()).GetAccessToken();

                InitializeContext();
            }
            catch (Exception exception)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }
        }

        private void InitializeContext()
        {
            try
            {
                Api = new APIContext(AccessToken)
                {
                    Config = GetConfiguration()
                };
            }
            catch (Exception exception)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }
        }

        private Dictionary<string, string> GetConfiguration()
        {
            var mode = (_payPalSandBoxMode) ? "sandbox" : "live";
            var configurationMap = new Dictionary<string, string>
            {
                { "mode", mode }
            };

            return configurationMap;
        }
    }
}
