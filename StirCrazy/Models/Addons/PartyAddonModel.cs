﻿using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Models.Addons
{
    public class PartyAddonModel
    {
        public int Id { get; set; }
        public AddonType AddonType { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public bool IsIncluded { get; set; }
        public bool IsSelected { get; set; }
    }
}