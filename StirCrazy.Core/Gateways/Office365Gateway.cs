﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.Exchange.WebServices.Data;
using StirCrazy.Core.Domain.Calendar;

namespace StirCrazy.Core.Gateways
{
    public class Office365Gateway : IOffice365Gateway
    {
        private readonly string _officeServiceEndpoint = ConfigurationManager.AppSettings["OfficeServiceEndpoint"];
        private readonly string _officeSharedMailbox = ConfigurationManager.AppSettings["OfficeSharedMailbox"];
        private readonly string _officeMailboxUsername = ConfigurationManager.AppSettings["OfficeMailboxUsername"];
        private readonly string _officeMailboxPassword = ConfigurationManager.AppSettings["OfficeMailboxPassword"];
        private readonly string _officeMailboxTimezone = ConfigurationManager.AppSettings["OfficeMailboxTimezone"];
        private readonly bool _officeUseAutoDiscovery = Convert.ToBoolean(ConfigurationManager.AppSettings["OfficeUseAutoDiscovery"]);

        public string OperationStatus = "";
        public string SharedMailbox;

        ExchangeService _mExchangeService;

        public Office365Gateway()
        {
            InitializeService();
            SharedMailbox = _officeSharedMailbox;
        }

        public Office365Gateway(string sharedMailbox)
        {
            InitializeService();
            SharedMailbox = sharedMailbox;
        }

        //This method gets the appointments for appointment Start Time and End Time
        public List<CalendarItem> GetAppointments(DateTime start, DateTime end)
        {
            List<CalendarItem> appointments = null;
            try
            {
                if (_mExchangeService == null)
                {
                    OperationStatus = "Exchange service is not initialized.";
                    return null;
                }

                var calendarView = new CalendarView(start, end)
                {
                    MaxItemsReturned = int.MaxValue,
                    PropertySet = new PropertySet(BasePropertySet.FirstClassProperties)
                    {
                        ItemSchema.IsUnmodified
                    }
                };

                //By default FindAppointments doesnt return all First class properties.
                //So we need to load those properties, after Appointments are returned.
                //FindItemsResults<Appointment> firAppointments = m_ExchangeService.FindAppointments(WellKnownFolderName.Calendar, calendarView);

                FindItemsResults<Appointment> firAppointments;

                if (string.IsNullOrEmpty(SharedMailbox))//user would access own mailbox
                {
                    firAppointments = _mExchangeService.FindAppointments(WellKnownFolderName.Calendar, calendarView);
                }
                else//Delegated admin would access this user's mailbox
                {
                    var mailbox = new Mailbox(SharedMailbox);
                    var folderId = new FolderId(WellKnownFolderName.Calendar, mailbox);
                    firAppointments = _mExchangeService.FindAppointments(folderId, calendarView);
                }


                if (firAppointments.TotalCount > 0)
                {
                    var customPropertySet = new PropertySet(BasePropertySet.FirstClassProperties)
                    {
                        RequestedBodyType = BodyType.Text
                    };

                    //If we not set following value, then body would be returned in HTML

                    //Following call would return all FirstClass properties, and body as Text format, for all returned Appointments
                    _mExchangeService.LoadPropertiesForItems(from Item item in firAppointments select item, customPropertySet);

                    appointments = new List<CalendarItem>();

                    foreach (var appointment in firAppointments)
                    {
                        var ci = new CalendarItem();

                        foreach (var appAttendee in appointment.RequiredAttendees.Select(x => new AppointmentAttendee(x.Name, x.Address)))
                        {
                            ci.Attendees.Add(appAttendee);
                        }

                        ci.Organizer = appointment.Organizer.Address;
                        ci.ItemId = appointment.Id.UniqueId;
                        ci.StartTime = appointment.Start;
                        ci.EndTime = appointment.End;
                        ci.Subject = appointment.Subject;
                        ci.Body = appointment.Body.Text;

                        appointments.Add(ci);
                    }
                }
            }
            catch (Exception e)
            {
                OperationStatus = e.Message;
            }

            return appointments;
        }

        //This method gets the appointments for appointment creation Time
        //if DateTime.MinValue is mention in startCreationTime or endCreationTime, then all appointments are returned
        public List<CalendarItem> GetAppointmentsByCreationDate(DateTime startCreationTime, DateTime endCreationTime)
        {
            List<CalendarItem> appointments = null;
            try
            {
                if (_mExchangeService == null)
                {
                    OperationStatus = "Exchange service is not initialized.";
                    return null;
                }

                var searchFilterCol = new SearchFilter.SearchFilterCollection(LogicalOperator.And);

                SearchFilter sf1 = new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeCreated, startCreationTime);
                SearchFilter sf2 = new SearchFilter.IsLessThanOrEqualTo(ItemSchema.DateTimeCreated, endCreationTime);

                searchFilterCol.Add(sf1);
                searchFilterCol.Add(sf2);

                var itemView = new ItemView(1000);
                FindItemsResults<Item> createdItems = null;

                if (string.IsNullOrEmpty(SharedMailbox))//user would access own mailbox
                {
                    if (startCreationTime == DateTime.MinValue || endCreationTime == DateTime.MinValue)
                        createdItems = _mExchangeService.FindItems(WellKnownFolderName.Calendar, itemView);
                    else
                        createdItems = _mExchangeService.FindItems(WellKnownFolderName.Calendar, searchFilterCol, itemView);
                }//Delegated admin would access this user's mailbox
                else
                {

                    var mailbox = new Mailbox(SharedMailbox);
                    var folderId = new FolderId(WellKnownFolderName.Calendar, mailbox);

                    if (startCreationTime == DateTime.MinValue || endCreationTime == DateTime.MinValue)
                        createdItems = _mExchangeService.FindItems(folderId, itemView);
                    else
                        createdItems = _mExchangeService.FindItems(folderId, searchFilterCol, itemView);
                }
                if (createdItems.TotalCount > 0)
                {
                    var customPropertySet = new PropertySet(BasePropertySet.FirstClassProperties)
                    {
                        RequestedBodyType = BodyType.Text
                    };

                    //If we not set following value, then body would be returned in HTML

                    //Following call would return all FirstClass properties, and body as Text format, for all returned Appointments
                    _mExchangeService.LoadPropertiesForItems(from Item item in createdItems select item, customPropertySet);

                    appointments = new List<CalendarItem>();
                    foreach (var item in createdItems)
                    {
                        if (!(item is Appointment)) continue;

                        var appointment = item as Appointment;
                        var ci = new CalendarItem();

                        foreach (var appAttendee in appointment.RequiredAttendees.Select(x => new AppointmentAttendee(x.Name, x.Address)))
                        {
                            ci.Attendees.Add(appAttendee);
                        }

                        ci.Organizer = appointment.Organizer.Address;
                        ci.ItemId = appointment.Id.UniqueId;
                        ci.StartTime = appointment.Start;
                        ci.EndTime = appointment.End;
                        ci.Subject = appointment.Subject;
                        ci.Body = appointment.Body.Text;

                        appointments.Add(ci);
                    }
                }
            }
            catch (Exception e)
            {
                OperationStatus = e.Message;
            }

            return appointments;
        }

        public string CreateAppointment(CalendarItem calendarItem)
        {
            var result = "";

            if (_mExchangeService == null)
            {
                OperationStatus = "Exchange service is not initialized.";
                return result;
            }
            try
            {
                var appointment = new Appointment(_mExchangeService)
                {
                    Subject = calendarItem.Subject,
                    Body = calendarItem.Body,
                    Start = calendarItem.StartTime,
                    End = calendarItem.EndTime,
                    IsAllDayEvent = calendarItem.IsAllDay
                };

                // Set properties on the appointment.
                //Status = m_ExchangeService.TimeZone.ToString();
                // Save the appointment.
                if (string.IsNullOrEmpty(SharedMailbox))//user would access own mailbox
                {
                    appointment.Save(SendInvitationsMode.SendToNone);
                }
                else//Delegated admin would access this user's mailbox
                {
                    var mailbox = new Mailbox(SharedMailbox);
                    var folderId = new FolderId(WellKnownFolderName.Calendar, mailbox);
                    appointment.Save(folderId, SendInvitationsMode.SendToNone);
                }

                result = appointment.Id.UniqueId;
            }
            catch (Exception e)
            {
                OperationStatus = FormatExceptionDetails(e);
            }

            return result;
        }

        public bool UpdateAppointment(CalendarItem calendarItem)
        {
            var result = false;

            try
            {
                var appointment = Appointment.Bind(_mExchangeService, new ItemId(calendarItem.ItemId));
                appointment.Update(ConflictResolutionMode.AutoResolve, SendInvitationsOrCancellationsMode.SendToNone);
                result = true;
            }
            catch (Exception e)
            {
                OperationStatus = FormatExceptionDetails(e);
            }

            return result;
        }

        public bool DeleteAppointment(string itemId)
        {
            var result = false;

            if (_mExchangeService == null)
            {
                OperationStatus = "Exchange service is not initialized.";
                return false;
            }
            try
            {
                var appointment = Appointment.Bind(_mExchangeService, new ItemId(itemId));
                appointment.Delete(DeleteMode.MoveToDeletedItems);
                result = true;
            }
            catch (Exception e)
            {
                OperationStatus = FormatExceptionDetails(e);
            }

            return result;
        }

        private void InitializeService()
        {
            //szAdmin user would have right to acess szTargetMailbox, either through impersonation, or through premission
            if (_mExchangeService != null) return;

            _mExchangeService = new ExchangeService(ExchangeVersion.Exchange2010_SP1, TimeZoneInfo.FindSystemTimeZoneById(_officeMailboxTimezone))
            {
                Timeout = 5 * 60 * 1000
            };

            System.Net.ServicePointManager.ServerCertificateValidationCallback = ValidateRemoteCertificate;

            if (string.IsNullOrEmpty(_officeMailboxUsername) | string.IsNullOrEmpty(_officeMailboxPassword))
            {
                //put logged in user crdentials
                //m_ExchangeService.Credentials = new WebCredentials(System.Net.CredentialCache.DefaultCredentials);
                throw new Exception("User name or password is empty.");
            }
            else
            {
                //User contains the Domain name e.g. Domain\User or user@domain
                _mExchangeService.Credentials = new WebCredentials(_officeMailboxUsername, _officeMailboxPassword);
            }

            //Target mailbox, who is to be impersonated by admin mailbox
            //Normally an admin mailbox is given imperonated right, to access all mailboxes on exchange server
            //For delegation, this wont work
            //if (!string.IsNullOrEmpty(szTargetMailbox))
            //{
            //    ImpersonatedUserId userToImpersonnate = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, szTargetMailbox);
            //    m_ExchangeService.ImpersonatedUserId = userToImpersonnate;
            //}

            if (_officeUseAutoDiscovery)
            {
                _mExchangeService.AutodiscoverUrl(_officeMailboxUsername, AutodiscoverRedirectionCallback);
            }
            else
            {
                var szEwServiceUrl = "https://" + _officeServiceEndpoint + "/ews/exchange.asmx";
                _mExchangeService.Url = new Uri(szEwServiceUrl);
            }
        }
        static bool AutodiscoverRedirectionCallback(string url)
        {
            // Return true if the URL is an HTTPS URL.
            return url.ToLower().StartsWith("https://");
        }
        private static bool ValidateRemoteCertificate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors policyErrors)
        {
            // Logic to determine the validity of the certificate
            return true;
        }
        public static string FormatExceptionDetails(Exception e)
        {
            string szMsg = "";
            try
            {
                szMsg = "Exception occured: " + e.Message;
                if (e.InnerException != null)
                    szMsg += ". IE: " + e.InnerException.Message;
            }
            catch (Exception exception)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }

            return szMsg;
        }
    }
}
