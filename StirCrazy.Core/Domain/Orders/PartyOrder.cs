using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Domain.Orders
{
    public class PartyOrder
    {
        [Key]
        public Guid Id { get; set; }

        public string EventId { get; set; }
        public Guid? PromotionCodeId { get; set; }
        public int PaymentTransactionId { get; set; }
        public int PartyPackageId { get; set; }
        public int RoomBookingId { get; set; }
        public string ParentId { get; set; }
        public int ChildId { get; set; }
        public int AdditionalGuests { get; set; }
        public int PrepaidAdmissions { get; set; }
        public string PartyTheme { get; set; }
        public string CakeTheme { get; set; }
        public string CakeFlavour { get; set; }
        public bool AddPlaceSetting { get; set; }
        public bool HasOwnPlaceSetting { get; set; }
        public bool AddCake { get; set; }
        public bool HasOwnCake { get; set; }
        public bool HasOwnDecor { get; set; }
        public bool HasCupcakes { get; set; }
        public bool HasOwnBalloons { get; set; }
        public bool HasOwnLootBag { get; set; }
        public bool SwapFoodPlatter { get; set; }
        public bool UpgradeLootBags { get; set; }
        public bool UpgradeLatexBalloons { get; set; }
        public string Notes { get; set; }
        public decimal Discount { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string Promotion { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }

        public virtual List<PartyOrderAddon> PartyAddons { get; set; }
        public virtual PaymentTransaction PaymentTransaction { get; set; }
        public virtual ApplicationUser Parent { get; set; }
        public virtual Child Child { get; set; }
        public virtual PartyPackage PartyPackage { get; set; }
        public virtual RoomBooking RoomBooking { get; set; }
        public virtual PromotionCode PromotionCode { get; set; }
    }

    public enum OrderStatus
    {
        [Display(Name = "Created")]
        Created,
        [Display(Name = "Cancelled")]
        Cancelled,
        [Display(Name = "Scheduled")]
        Scheduled,
        [Display(Name = "Paid")]
        Paid,
        [Display(Name = "Refunded")]
        Refunded,
    }
}