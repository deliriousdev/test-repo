﻿/// <autosync enabled="true" />
/// <reference path="bootstrap-formhelpers.min.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery-2.1.1.js" />
/// <reference path="jquery-ui-1.11.1.js" />
/// <reference path="jquery-ui.min-1.11.1.js" />
/// <reference path="jquery.geocomplete.js" />
/// <reference path="jquery.gmap.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquerypp.custom.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="modernizr.custom.17475.js" />
/// <reference path="moment-with-locales.js" />
/// <reference path="moment.js" />
/// <reference path="respond.js" />
/// <reference path="stircrazy.common.js" />
/// <reference path="stircrazy.order-admission.js" />
/// <reference path="stircrazy.order-bakery.js" />
/// <reference path="stircrazy.party-booking.js" />
/// <reference path="stircrazy.party-grabngo.js" />
/// <reference path="stircrazy.room-booking.js" />
/// <reference path="../vendor/datetimepicker/jquery.datetimepicker.js" />
/// <reference path="../vendor/fullcalendar/fullcalendar.js" />
/// <reference path="../vendor/fullcalendar/gcal.js" />
/// <reference path="../vendor/fullcalendar/moment.min.js" />
/// <reference path="../vendor/jquery-ui/jquery-ui.js" />
/// <reference path="../vendor/jquery.bpopup/jquery.bpopup.min.js" />
/// <reference path="../vendor/jquery.flexisel/jquery.flexisel.js" />
/// <reference path="../vendor/jquery.sortable/jquery.sortable.js" />
/// <reference path="../vendor/nivo-lightbox/nivo-lightbox.js" />
/// <reference path="../vendor/nivo-slider/jquery.nivo.slider.js" />
/// <reference path="../vendor/nivo-slider/jquery.nivo.slider.pack.js" />
/// <reference path="../vendor/jquery.marquee/lib/jquery.marquee.js" />
