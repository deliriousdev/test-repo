﻿namespace StirCrazy.Admin.Models.Reporting
{
    public class TotalsModel
    {
        public decimal OrderTotal { get; set; }
        public decimal YearToDate { get; set; }
    }
}