﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using PayPal.Api.Payments;
using StirCrazy.Admin.Models.Order;
using StirCrazy.Admin.Models.Party;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class RoomController : AsyncController
    {
        private readonly IRoomBookingService _roomBookingService;
        private readonly IRoomOrderService _roomOrderService;
        private readonly IPayPalService _payPalService;

        public RoomController(IRoomBookingService roomBookingService,
            IRoomOrderService roomOrderService,
            IPayPalService payPalService)
        {
            _roomBookingService = roomBookingService;
            _roomOrderService = roomOrderService;
            _payPalService = payPalService;
        }

        public ActionResult Index(OrderStatus status = OrderStatus.Created)
        {
            var model = _roomOrderService.GetByStatus(status).Select(x => new RoomOrderModel()
            {
                Id = x.Id,
                RoomBookingId = x.RoomBookingId,
                PayPalTransactionId = x.PaymentTransactionId,
                Notes = x.Notes,
                Total = x.Total,
                DateCreated = x.DateCreated,
                CreatedBy = x.CreatedBy,
                Status = x.Status,
                Customer = x.Customer,
                RoomBooking = new RoomBookingModel()
                {
                    Id = x.RoomBooking.Id,
                    Date = x.RoomBooking.Date,
                    PartyRoom = x.RoomBooking.PartyRoom,
                    PartyRoomId = x.RoomBooking.PartyRoomId,
                    TimeSlot = x.RoomBooking.TimeSlot,
                    TimeSlotId = x.RoomBooking.TimeSlotId
                },
                PaymentTransaction = x.PaymentTransaction

            }).ToList();

            return View(model);
        }

        public ActionResult Availability(DateTime searchDate)
        {
            var result = _roomBookingService.GetAvailability(searchDate);
            return (!result.Any()) ? null : PartialView("_AvailabilityPartial", result);
        }


        public ActionResult Create()
        {
            var model = new RoomOrderModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(RoomOrderModel model)
        {
            if (ModelState.IsValid)
            {
                model.Customer.UserName = model.Customer.Email;

                var roomOrder = new RoomOrder()
                {
                    Customer = model.Customer,
                    RoomBooking = new RoomBooking()
                    {
                        BookingType = BookingType.RoomRental,
                        PartyRoomId = model.RoomBooking.PartyRoomId,
                        TimeSlotId = model.RoomBooking.TimeSlotId,
                        Date = model.RoomBooking.Date,
                        Status = Status.Scheduled
                    },
                    Promotion = (model.Promotion ?? "NONE"),
                    PaymentTransaction = new PaymentTransaction()
                    {
                        Amount = model.Total,
                        PaymentDate = DateTime.Now
                    },
                    Notes = model.Notes,
                    SubTotal = model.SubTotal,
                    Tax = model.Tax,
                    Total = model.Total,
                    Status = OrderStatus.Paid
                };

                roomOrder.PaymentTransaction.TransactionType = model.PaymentTransaction.TransactionType;
                roomOrder.PaymentTransaction.TransactionNumber = model.PaymentTransaction.TransactionNumber ?? "Not Applicable";
                roomOrder.PaymentTransaction.PaymentStatus = "Approved";

                // SAVE DATABASE
                roomOrder.CreatedBy = "Administrator";
                var order = await _roomOrderService.CreateAsync(roomOrder);

                return RedirectToAction("Email", new RouteValueDictionary { { "Id", order.Id } });
            }

            return View(model);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var roomOrder = await _roomOrderService.GetAsync(id);

            if (roomOrder != null)
            {
                var model = new RoomOrderModel()
                {
                    Id = roomOrder.Id,
                    Customer = roomOrder.Customer,
                    RoomBooking = new RoomBookingModel()
                    {
                        PartyRoom = roomOrder.RoomBooking.PartyRoom,
                        TimeSlot = roomOrder.RoomBooking.TimeSlot,
                        Date = roomOrder.RoomBooking.Date
                    },
                    PaymentTransaction = roomOrder.PaymentTransaction,
                    Notes = roomOrder.Notes,
                    SubTotal = roomOrder.SubTotal,
                    Discount = roomOrder.Discount,
                    Tax = roomOrder.Tax,
                    Total = roomOrder.Total,
                    Status = roomOrder.Status,
                    Promotion = (roomOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", roomOrder.PromotionCode.Code, roomOrder.PromotionCode.Description) : "Not Applicable"
                };

                return View(model);
            }

            return HttpNotFound();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Print(Guid id)
        {
            var roomOrder = await _roomOrderService.GetAsync(id);

            if (roomOrder != null)
            {
                var model = new RoomOrderModel()
                {
                    Id = roomOrder.Id,
                    Customer = roomOrder.Customer,
                    RoomBooking = new RoomBookingModel()
                    {
                        PartyRoom = roomOrder.RoomBooking.PartyRoom,
                        TimeSlot = roomOrder.RoomBooking.TimeSlot,
                        Date = roomOrder.RoomBooking.Date
                    },
                    Notes = roomOrder.Notes,
                    PaymentTransaction = roomOrder.PaymentTransaction,
                    Promotion = (roomOrder.PromotionCode != null) ? roomOrder.PromotionCode.Code : "Not Applicable",
                    Total = roomOrder.Total,
                    Status = roomOrder.Status
                };

                return View(model);
            }

            return HttpNotFound();
        }

        [AllowAnonymous]
        public ActionResult Save(Guid id)
        {
            string baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Room/";
            string detailsUrl = baseUri + "Print/" + id;

            byte[] bytesAsPdf = _roomOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Email(Guid id)
        {
            var roomOrder = await _roomOrderService.GetAsync(id);
            if (roomOrder == null) return HttpNotFound();

            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Room/";
            await _roomOrderService.SendEmailAsync(baseUri, roomOrder.Id, roomOrder.Customer.Email); ;

            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        public async Task<ActionResult> Payment(Guid id)
        {
            var payer = Request["PayerID"];
            var roomOrder = await _roomOrderService.GetAsync(id);

            var payment = new Payment
            {
                id = roomOrder.PaymentTransaction.TransactionNumber,
                state = roomOrder.PaymentTransaction.PaymentStatus
            };

            var result = _payPalService.ExecutePayment(payment, payer);
            if (result.state == "approved")
            {
                roomOrder.Status = OrderStatus.Paid;
            }

            roomOrder.PaymentTransaction.PaymentStatus = result.state;
            await _roomOrderService.UpdateAsync(roomOrder);

            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        private string GetApprovalUrl(Payment payment)
        {
            List<Links> links = payment.links;
            return (from lnk in links where lnk.rel.ToLower().Equals("approval_url") select Server.UrlDecode(lnk.href)).FirstOrDefault();
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var roomOrder = await _roomOrderService.GetAsync(id);

            if (roomOrder != null)
            {
                var model = new RoomOrderModel()
                {
                    Id = roomOrder.Id,
                    Customer = roomOrder.Customer,
                    RoomBooking = new RoomBookingModel()
                    {
                        PartyRoom = roomOrder.RoomBooking.PartyRoom,
                        TimeSlot = roomOrder.RoomBooking.TimeSlot,
                        Date = roomOrder.RoomBooking.Date
                    },
                    PaymentTransaction = roomOrder.PaymentTransaction,
                    Notes = roomOrder.Notes,
                    Total = roomOrder.Total,
                    Status = roomOrder.Status
                };

                return View(model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        public async Task<RedirectToRouteResult> Delete(RoomOrderModel model)
        {
            await _roomOrderService.CancelAsync(model.Id);
            return RedirectToAction("Index");
        }

        public async Task<RedirectToRouteResult> Cancelled(Guid id)
        {
            await _roomOrderService.CancelAsync(id);
            return RedirectToAction("Index", "Home");
        }
    }
}