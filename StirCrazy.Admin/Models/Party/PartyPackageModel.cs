﻿using System.Collections.Generic;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Admin.Models.Party
{
    public class PartyPackageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal GuestPrice { get; set; }
        public int Guests { get; set; }
        public int MaxGuests { get; set; }
        public bool HasCake { get; set; }
        public bool HasPlaceSetting { get; set; }
        public List<PackageAddon> PackageAddons { get; set; }
    }
}