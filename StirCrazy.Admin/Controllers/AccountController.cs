﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using StirCrazy.Admin.Models.Account;
using StirCrazy.Core;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Utilities;
using WebGrease.Css.Extensions;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult Index()
        {
            var admins = _accountService.GetUsersByRole("Administrator");
            var staff = _accountService.GetUsersByRole("Staff");
            var model = new List<ApplicationUser>();

            model.AddRange(admins);
            model.AddRange(staff);
            var accounts = model.Distinct();

            return View(accounts.ToList());
        }

        public ActionResult Customers()
        {
            var users = _accountService.GetUsersByRole("Customer");
            var model = new List<ApplicationUser>();

            model.AddRange(users);
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await _accountService.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    await SignInAsync(user, false);
                    return RedirectToLocal(returnUrl);
                }
                ModelState.AddModelError("", "Invalid username or password.");
            }

            // If we got this far, something failed, redisplay form
            return View();
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(new RegisterViewModel()
            {
                UserName = "",
                RoleName = UserRoles.Customer
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            var password = RandomPassword.Generate(8);

            model.Password = password;
            model.ConfirmPassword = password;
            model.UserName = model.Email;

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Address = model.Address,
                    City = model.City,
                    Province = model.Province,
                    PostalCode = model.PostalCode,
                    PhoneNumber = model.PhoneNumber,
                    Email = model.Email
                };

                try
                {
                    var account = _accountService.FindByEmail(model.Email);

                    // New User
                    if (account == null)
                    {
                        IdentityResult result = await _accountService.CreateAsync(user, password);
                        if (result.Succeeded)
                        {
                            _accountService.AddToRole(user.Id, UserRoles.Customer);
                            return RedirectToAction("Index", "Account");
                        }
                        AddErrors(result);
                    }
                    else
                    {
                        account.UserName = model.Email;
                        account.FirstName = model.FirstName;
                        account.LastName = model.LastName;
                        account.Address = model.Address;
                        account.City = model.City;
                        account.PostalCode = model.PostalCode;
                        account.Province = model.Province;
                        account.PhoneNumber = model.PhoneNumber;
                        account.Email = model.Email;

                        await _accountService.UpdateAsync(account);
                        return RedirectToAction("Index", "Account");
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result =
                await
                    _accountService.RemoveLoginAsync(User.Identity.GetUserId(),
                        new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        public ActionResult Manage(string id, ManageMessageId? message)
        {
            var userId = id ?? User.Identity.GetUserId();
            ApplicationUser user = _accountService.FindById(userId);

            if (user != null)
            {
                var model = new ManageUserViewModel
                {
                    UserName = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Address = user.Address,
                    City = user.City,
                    Province = user.Province,
                    PostalCode = user.PostalCode,
                    PhoneNumber = user.PhoneNumber,
                    Email = user.Email
                };

                ViewBag.StatusMessage =
                    message == ManageMessageId.ChangePasswordSuccess
                        ? "Your password has been changed."
                        : message == ManageMessageId.SetPasswordSuccess
                            ? "Your password has been set."
                            : message == ManageMessageId.RemoveLoginSuccess
                                ? "The external login was removed."
                                : message == ManageMessageId.Error
                                    ? "An error has occurred."
                                    : "";

                ViewBag.HasLocalPassword = HasPassword();
                ViewBag.ReturnUrl = Url.Action("Manage");
                return View(model);
            }

            return RedirectToAction("Login");
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            model.UserName = model.Email;

            if (ModelState.IsValid)
            {
                try
                {
                    var account = _accountService.FindById(model.Id);

                    account.UserName = model.Email;
                    account.FirstName = model.FirstName;
                    account.LastName = model.LastName;
                    account.Address = model.Address;
                    account.City = model.City;
                    account.PostalCode = model.PostalCode;
                    account.Province = model.Province;
                    account.PhoneNumber = model.PhoneNumber;
                    account.Email = model.Email;

                    _accountService.Update(account);

                    if (model.OldPassword != null && model.Id == User.Identity.GetUserId())
                    {
                        IdentityResult result =
                            await _accountService.ChangePasswordAsync(model.Id, model.OldPassword, model.NewPassword);

                        if (result.Succeeded)
                        {
                            return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                        }

                        AddErrors(result);
                    }

                    if (model.NewPassword != null && User.IsInRole(UserRoles.Administrator))
                    {
                        IdentityResult result = _accountService.RemovePassword(model.Id);

                        if (result.Succeeded)
                        {
                            await _accountService.AddPasswordAsync(model.Id, model.NewPassword);
                            return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                        }

                        AddErrors(result);
                    }

                    return RedirectToAction("Index", "Account");
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword(ForgotMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ForgotMessageId.ForgotPasswordSuccess
                    ? "A new password has been sent to your e-mail address."
                    : "";
            ViewBag.HasMessage = message.HasValue;
            return View(new ForgotPasswordModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            ViewBag.HasMessage = false;
            if (ModelState.IsValid)
            {
                ApplicationUser user = _accountService.FindByName(model.Email);
                if (user != null)
                {
                    var forgotPassword = new IdentityResult();
                    string password = string.Empty;
                    string url = string.Concat(ApplicationPath.GetSiteRoot(), Url.Action("Manage", "Account"));
                    try
                    {
                        _accountService.RemovePassword(user.Id);
                        password = RandomPassword.Generate();
                        forgotPassword = _accountService.AddPassword(user.Id, password);
                    }
                    catch (Exception)
                    {
                        //forgotPasswordSucceeded = false;
                    }

                    if (forgotPassword.Succeeded)
                    {
                        //_notificationService.ResetPasswordNotification(user, password);
                        return RedirectToAction("ForgotPassword", new { Message = ForgotMessageId.ForgotPasswordSuccess });
                    }
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
                else
                    ModelState.AddModelError("", "Incorrect email address.\nPlease try another one");
            }


            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Roles(string id)
        {
            ApplicationUser user = _accountService.FindById(id);
            var model = new UserRolesModel(user);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Roles(UserRolesModel model)
        {

            ApplicationUser user = _accountService.FindById(model.Id);
            _accountService.ClearUserRoles(user.Id);
            model.Roles
                .Where(x => x.Selected)
                .ForEach(item => _accountService.AddToRole(user.Id, item.RoleName));
            return RedirectToAction("index");
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            string userId = User.Identity.GetUserId();
            ApplicationUser applicationuser = _accountService.FindById(userId);

            if (applicationuser == null)
            {
                return RedirectToAction("Login", "Account");
            }

            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        public enum ForgotMessageId
        {
            ForgotPasswordSuccess,
            Error
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            ClaimsIdentity identity =
                await _accountService.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            ApplicationUser user = _accountService.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }

                throw new NotImplementedException();
                //context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion
    }
}