﻿using System;

namespace StirCrazy.Core.Domain
{
    public class StoreHoliday
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Holiday { get; set; }
        public TimeSpan? OpenTime { get; set; }
        public TimeSpan? ClosedTime { get; set; }
        public bool IsClosed { get; set; }
        public bool AllowScheduling { get; set; }
    }
}