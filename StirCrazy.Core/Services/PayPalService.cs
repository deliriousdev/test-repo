﻿using System;
using System.Collections.Generic;
using System.Linq;
using PayPal.Api.Payments;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Gateways;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services
{
    public class PayPalService : IPayPalService
    {
        private readonly IPayPalGateway _payPalGateway;
        private readonly IRepository<PaymentTransaction> _payPalRepository;

        public PayPalService(IPayPalGateway payPalGateway, IRepository<PaymentTransaction> payPalRepository)
        {
            _payPalGateway = payPalGateway;
            _payPalRepository = payPalRepository;
        }

        public Payment CreatePayment(double total, string orderDescription, string returnUrl, string cancelUrl)
        {
            var subTotal = (total / 1.05);
            var tax = (subTotal * 0.05);

            var details = new Details
            {
                subtotal = string.Format("{0:0.00}", subTotal),
                tax = string.Format("{0:0.00}", tax),
            };

            var amount = new Amount
            {
                currency = "CAD",
                details = details,
                total = string.Format("{0:0.00}", total)
            };

            var payment = new Payment
            {
                transactions = new List<Transaction> 
                                             { 
                                                new Transaction
                                                {
                                                    amount = amount,
                                                    description = orderDescription
                                                } 
                                             },
                intent = "sale",
                payer = new Payer { payment_method = "paypal" },
                redirect_urls = new RedirectUrls
                {
                    cancel_url = cancelUrl,
                    return_url = returnUrl
                }
            };

            var response = payment.Create(_payPalGateway.Api);
            return response;
        }

        public Refund CreateRefund(int transactionId, string refundDescription)
        {
            var transaction = _payPalRepository.Get(transactionId);
            if (transaction == null) return null;

            var responseRefund = new Refund();

            var refundAmount = new Amount
            {
                currency = "CAD",
                total = string.Format("{0:0.00}", transaction.Amount)
            };

            var refund = new Refund()
            {
                amount = refundAmount,
                description = refundDescription
            };

            var sale = new Sale()
            {
                id = transaction.TransactionNumber
            };

            try
            {
                responseRefund = sale.Refund(_payPalGateway.Api, refund);

                transaction.PaymentStatus = "refunded";
                _payPalRepository.Update(transaction, transaction.Id);

                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception("Scheduling Availability Exception: " + transactionId));
            }
            catch (PayPal.Exception.PayPalException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return responseRefund;
        }

        public Payment ExecutePayment(Payment payment, string payerId)
        {
            var pymntExecution = new PaymentExecution
            {
                payer_id = (payerId)
            };

            Payment executedPayment = payment.Execute(_payPalGateway.Api, pymntExecution);
            return executedPayment;
        }

        public PaymentTransaction GetTransaction(int id)
        {
            return _payPalRepository.Get().FirstOrDefault(x => x.Id == id);
        }

        public void AddTransaction(PaymentTransaction paymentTransaction)
        {
            _payPalRepository.Add(paymentTransaction);
        }

        public void UpdateTransaction(PaymentTransaction paymentTransaction)
        {
            _payPalRepository.Update(paymentTransaction, paymentTransaction.Id);
        }

        public void RemoveTransaction(PaymentTransaction paymentTransaction)
        {
            _payPalRepository.Delete(paymentTransaction);
        }

        public Payment UpdatePaymentStatus(Guid id, string paymentId)
        {
            var payment = Payment.Get(_payPalGateway.Api, paymentId);
            return payment;
        }
    }
}
