using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Services.Order
{
    public interface IBakeryOrderService
    {
        IEnumerable<BakeryOrder> Get();
        IEnumerable<BakeryOrder> GetByStatus(OrderStatus status);
        BakeryOrder Get(Guid id);
        Task<BakeryOrder> GetAsync(Guid id);
        Task<BakeryOrder> CreateAsync(BakeryOrder bakeryOrder);
        Task UpdateAsync(BakeryOrder bakeryOrder);
        Task SendEmailAsync(string baseUri, Guid id, string email);
        Task SendOrderNotificationAsync(BakeryOrder bakeryOrder);
        Task UpdateStatusAsync(Guid id, OrderStatus status);
        Task CancelAsync(Guid id);
        Task RemoveAsync(Guid id);
        byte[] GetPdfBytes(string url);
    }
}