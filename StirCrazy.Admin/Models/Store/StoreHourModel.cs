﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Admin.Models.Store
{
    public class StoreHourModel
    {
        public int Id { get; set; }
        [Required]
        public DayOfWeek DayOfWeek { get; set; }
        [Required]
        public string OpenTime { get; set; }
        [Required]
        public string ClosedTime { get; set; }
        [Required]
        public bool IsClosed { get; set; }
    }
}