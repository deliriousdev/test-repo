﻿using System.Web;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Carousel;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Services;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class CarouselController : Controller
    {
        private readonly IMediaService _mediaService;

        public CarouselController(IMediaService mediaService)
        {
            _mediaService = mediaService;
        }

        public ActionResult Index()
        {
            var model = _mediaService.Get(UploadFolder.Carousel);
            return View(model);
        }

        public ActionResult Create()
        {
            var model = new CarouselModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase carouselImage, CarouselModel model)
        {
            var image = new MediaFile
            {
                Id = model.Id,
                Transition = model.Transition ?? "sliceUpDown",
                IsActive = true
            };

            _mediaService.Save(carouselImage, image, Server.MapPath("~/"), UploadFolder.Carousel);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var carousel = _mediaService.Get(id);
            var model = new CarouselModel
            {
                Id = carousel.Id,
                PhysicalPath = carousel.PhysicalPath,
                Filename = carousel.Filename,
                FilePath = carousel.FilePath,
                Transition = carousel.Transition,
                IsActive = !(carousel.IsActive)
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(CarouselModel model)
        {
            if (ModelState.IsValid)
            {
                var carouselImage = new MediaFile()
                {
                    Id = model.Id,
                    PhysicalPath = model.PhysicalPath,
                    FilePath = model.FilePath,
                    Filename = model.Filename,
                    Transition = model.Transition,
                    IsActive = !(model.IsActive)
                };

                _mediaService.Update(carouselImage);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            _mediaService.Remove(id);
            return null;
        }
    }
}