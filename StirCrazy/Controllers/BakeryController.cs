﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using PayPal.Api.Payments;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Party;
using StirCrazy.Models.Addons;
using StirCrazy.Models.Gallery;
using StirCrazy.Models.Order;

namespace StirCrazy.Controllers
{
    public class BakeryController : AsyncController
    {
        private readonly IMediaService _mediaService;
        private readonly IPartyAddonService _partyAddonService;
        private readonly IBakeryOrderService _bakeryOrderService;
        private readonly IPayPalService _payPalService;
        private readonly IPromotionCodeService _promotionCodeService;

        public BakeryController(IBakeryOrderService bakeryOrderService, 
            IPartyAddonService partyAddonService, 
            IPayPalService payPalService, 
            IPromotionCodeService promotionCodeService, 
            IMediaService mediaService)
        {
            _partyAddonService = partyAddonService;
            _payPalService = payPalService;
            _promotionCodeService = promotionCodeService;
            _mediaService = mediaService;
            _bakeryOrderService = bakeryOrderService;
        }

        public ActionResult Index()
        {
            var model = new GalleryModel
            {
                GalleryImages = _mediaService.Get(UploadFolder.Cakes)
            };

            return View(model);
        }

        public ActionResult CakePans()
        {
            return View();
        }

        public ActionResult Create()
        {
            var model = new BakeryModel
            {
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = new List<BakeryOrderAddonModel>(),
                PaymentTransaction = new PaymentTransaction()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(BakeryModel model)
        {
            var bakeryOrder = new BakeryOrder()
            {
                PickUpDate = model.PickUpDate,
                PickUpTime = model.PickUpTime,
                Notes = model.Notes,
                Customer = model.Customer,
                Child = model.Child,
                CakeTheme = model.CakeTheme,
                CakeFlavour = model.CakeFlavour,
                EdibleImage = model.EdibleImage,
                StandardImage = model.StandardImage,
                OrderAddons = new List<BakeryOrderAddon>(),
                PaymentTransaction = new PaymentTransaction()
                {
                    Amount = model.Total,
                    PaymentDate = DateTime.Now
                },
                SubTotal = model.SubTotal,
                Tax = model.Tax,
                Total = model.Total,
                Status = OrderStatus.Created
            };

            if (model.Promotion != null)
            {
                var promoCode = _promotionCodeService.Get(model.Promotion);

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Percentage)
                {
                    bakeryOrder.Discount = (promoCode.Adjustment / 100) * bakeryOrder.Total;
                    bakeryOrder.PromotionCode = promoCode;
                }

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Discount)
                {
                    bakeryOrder.Discount = promoCode.Adjustment;
                    bakeryOrder.PromotionCode = promoCode;
                }
            }

            bakeryOrder.Customer.UserName = model.Customer.Email;
            foreach (var orderAddon in model.OrderAddons)
            {
                if (orderAddon.IsSelected || orderAddon.Quantity > 0)
                {
                    bakeryOrder.OrderAddons.Add(new BakeryOrderAddon()
                    {
                        BakeryOrderId = bakeryOrder.Id,
                        AddonId = orderAddon.Id,
                        Addon = orderAddon.Addon,
                        IsIncluded = orderAddon.IsIncluded,
                        Quantity = orderAddon.Quantity,
                    });
                }
            }

            // BYPASS PAYPAL PAYMENT PROCESSING
            if (model.BypassPaypal)
            {
                bakeryOrder.PaymentTransaction.TransactionType = model.PaymentTransaction.TransactionType;
                bakeryOrder.PaymentTransaction.TransactionNumber = model.PaymentTransaction.TransactionNumber ?? "Not Applicable";
                bakeryOrder.PaymentTransaction.PaymentStatus = "Approved";

                // SAVE DATABASE
                bakeryOrder.CreatedBy = "Administrator";
                bakeryOrder.Status = OrderStatus.Paid;
                var order = await _bakeryOrderService.CreateAsync(bakeryOrder);

                // SEND NOTIFICATION
                try { await _bakeryOrderService.SendOrderNotificationAsync(bakeryOrder); }
                catch (Exception ex) { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                return RedirectToAction("Email", new RouteValueDictionary { { "Id", order.Id } });
            }

            // SAVE DATABASE
            bakeryOrder.CreatedBy = "Customer";
            await _bakeryOrderService.CreateAsync(bakeryOrder);

            // PAYPAL PAYMENT PROCESSING
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Bakery/";
            var returnUrl = baseUri + "Payment/" + bakeryOrder.Id;
            var cancelUrl = baseUri + "Cancelled/" + bakeryOrder.Id;

            return await ProcessPayment(bakeryOrder.Id, (double)bakeryOrder.Total, "Stir Crazy - Bakery Order", returnUrl, cancelUrl);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var bakeryOrder = await _bakeryOrderService.GetAsync(id);
            if (bakeryOrder == null) return HttpNotFound();

            var model = new BakeryModel()
            {
                Id = bakeryOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = bakeryOrder.OrderAddons.Select(x => new BakeryOrderAddonModel()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    Quantity = x.Quantity,
                    IsIncluded = x.IsIncluded
                }).ToList(),
                CakeFlavour = bakeryOrder.CakeFlavour,
                CakeTheme = bakeryOrder.CakeTheme,
                StandardImage = bakeryOrder.StandardImage,
                EdibleImage = bakeryOrder.EdibleImage,
                Notes = bakeryOrder.Notes,
                Customer = bakeryOrder.Customer,
                Child = bakeryOrder.Child,
                PaymentTransaction = bakeryOrder.PaymentTransaction,
                PickUpDate = bakeryOrder.PickUpDate,
                PickUpTime = bakeryOrder.PickUpTime,
                SubTotal = bakeryOrder.SubTotal,
                Tax = bakeryOrder.Tax,
                Discount = bakeryOrder.Discount,
                Total = bakeryOrder.Total,
                Status = bakeryOrder.Status,
                Promotion = (bakeryOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", bakeryOrder.PromotionCode.Code, bakeryOrder.PromotionCode.Description) : ""
            };

            return View(model);
        }

        public async Task<ActionResult> Print(Guid id)
        {
            var bakeryOrder = await _bakeryOrderService.GetAsync(id);
            if (bakeryOrder == null) return HttpNotFound();

            var model = new BakeryModel()
            {
                Id = bakeryOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = bakeryOrder.OrderAddons.Select(x => new BakeryOrderAddonModel()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    Quantity = x.Quantity,
                    IsIncluded = x.IsIncluded
                }).ToList(),
                CakeFlavour = bakeryOrder.CakeFlavour,
                CakeTheme = bakeryOrder.CakeTheme,
                StandardImage = bakeryOrder.StandardImage,
                EdibleImage = bakeryOrder.EdibleImage,
                Notes = bakeryOrder.Notes,
                Customer = bakeryOrder.Customer,
                Child = bakeryOrder.Child,
                PaymentTransaction = bakeryOrder.PaymentTransaction,
                PickUpDate = bakeryOrder.PickUpDate,
                PickUpTime = bakeryOrder.PickUpTime,
                SubTotal = bakeryOrder.SubTotal,
                Tax = bakeryOrder.Tax,
                Discount = bakeryOrder.Discount,
                Total = bakeryOrder.Total,
                Status = bakeryOrder.Status,
                Promotion = (bakeryOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", bakeryOrder.PromotionCode.Code, bakeryOrder.PromotionCode.Description) : ""
            };

            return View(model);
        }

        public ActionResult Save(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Bakery/";
            var detailsUrl = baseUri + "Print/" + id;

            var bytesAsPdf = _bakeryOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        private async Task<RedirectResult> ProcessPayment(Guid id, double amount, string description, string returnUrl, string cancelUrl)
        {
            if (Request.Url != null)
            {
                var payment = _payPalService.CreatePayment(amount, description, returnUrl, cancelUrl);
                if (payment != null)
                {
                    var paymentId = payment.id;
                    var paymentState = payment.state;

                    var bakeryOrder = await _bakeryOrderService.GetAsync(id);

                    if (bakeryOrder != null)
                    {
                        bakeryOrder.PaymentTransaction.Id = bakeryOrder.PaymentTransaction.Id;
                        bakeryOrder.PaymentTransaction.TransactionType = TransactionType.Paypal;
                        bakeryOrder.PaymentTransaction.PaymentDate = DateTime.Now;
                        bakeryOrder.PaymentTransaction.PaymentStatus = paymentState;
                        bakeryOrder.PaymentTransaction.TransactionNumber = paymentId;
                        bakeryOrder.PaymentTransaction.Amount = (decimal)amount;

                        await _bakeryOrderService.UpdateAsync(bakeryOrder);
                    }

                    var redirectUrl = string.Format("{0}{1}", GetApprovalUrl(payment), "&useraction=commit");
                    return new RedirectResult(redirectUrl);
                }
            }

            return null;
        }

        public async Task<RedirectToRouteResult> Payment(Guid id)
        {
            var payer = Request["PayerID"];
            var bakeryOrder = await _bakeryOrderService.GetAsync(id);

            var payment = new Payment
            {
                id = bakeryOrder.PaymentTransaction.TransactionNumber,
                state = bakeryOrder.PaymentTransaction.PaymentStatus
            };

            var result = _payPalService.ExecutePayment(payment, payer);
            if (result.state == "approved")
            {
                bakeryOrder.Status = OrderStatus.Paid;
            }

            bakeryOrder.PaymentTransaction.PaymentStatus = result.state;

            await _bakeryOrderService.UpdateAsync(bakeryOrder);
            await _bakeryOrderService.SendOrderNotificationAsync(bakeryOrder);

            return RedirectToAction("Email", new RouteValueDictionary { { "Id", id } });
        }

        public async Task<ActionResult> Email(Guid id)
        {
            var grabNGoOrder = await _bakeryOrderService.GetAsync(id);
            if (grabNGoOrder == null) return HttpNotFound();

            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Bakery/";
            await _bakeryOrderService.SendEmailAsync(baseUri, grabNGoOrder.Id, grabNGoOrder.Customer.Email);

            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        public async Task<RedirectToRouteResult> Cancelled(Guid id)
        {
            await _bakeryOrderService.RemoveAsync(id);
            return RedirectToAction("Index", "Home");
        }

        private string GetApprovalUrl(Payment payment)
        {
            var links = payment.links;
            return (from lnk in links where lnk.rel.ToLower().Equals("approval_url") select Server.UrlDecode(lnk.href)).FirstOrDefault();
        }
    }
}