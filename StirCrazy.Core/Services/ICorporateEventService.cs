using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Services
{
    public interface ICorporateEventService
    {
        IEnumerable<CorporateEvent> Get();
        IEnumerable<CorporateEvent> GetByStatus(OrderStatus status);
        CorporateEvent Get(Guid id);
        Task<CorporateEvent> Add(CorporateEvent corporateEvent);
        CorporateEvent Update(CorporateEvent corporateEvent);
        byte[] GetPdfBytes(string url);
        Task SendEmail(string baseUri, Guid id, string email);
        Task SendNotificationAsync(Guid id);
        Task CancelAsync(Guid id);
        Task RemoveAsync(Guid id);
    }
}