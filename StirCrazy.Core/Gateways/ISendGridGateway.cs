﻿using System.Threading.Tasks;
using SendGrid;

namespace StirCrazy.Core.Gateways
{
    public interface ISendGridGateway
    {
        Task Send(SendGridMessage message);
    }
}