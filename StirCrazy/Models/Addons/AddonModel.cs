﻿using System.Collections.Generic;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Models.Addons
{
    public class AddonModel
    {
        public int Id { get; set; }
        public AddonType AddonType { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Size { get; set; }
        public int Quantity { get; set; }
        public bool IsSelected { get; set; }
    }

    public class AddonGroupModel
    {
        public List<AddonModel> Addons { get; set; }
        public string Name { get; set; }

        public int Id { get; set; }

        public AddonGroupModel()
        {
            
            Addons = new List<AddonModel>();
        }
    }
}