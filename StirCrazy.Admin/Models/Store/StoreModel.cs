﻿using System.Collections.Generic;

namespace StirCrazy.Admin.Models.Store
{
    public class StoreModel
    {
        public List<StoreHourModel> StoreHours { get; set; }
        public List<StoreHolidayModel> StoreHolidays { get; set; }
    }
}