using System.Data.Entity;
using StirCrazy.Core;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Gateways;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Party;
using StirCrazy.Core.Services.Social;
using StirCrazy.Core.Services.Store;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(StirCrazy.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(StirCrazy.App_Start.NinjectWebCommon), "Stop")]

namespace StirCrazy.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<DbContext>().To<DatabaseContext>().InRequestScope();

            #region Services
            kernel.Bind<IAccountService>().To<AccountService>();
            kernel.Bind<IAnnualPassOrderService>().To<AnnualPassOrderService>();
            kernel.Bind<IAppSettingService>().To<AppSettingService>();
            kernel.Bind<IAnnouncementService>().To<AnnouncementService>();
            kernel.Bind<ICorporateEventService>().To<CorporateEventService>();
            kernel.Bind<ISendGridService>().To<SendGridService>();
            kernel.Bind<IPartyAddonService>().To<PartyAddonService>();
            kernel.Bind<IRoomBookingService>().To<RoomBookingService>();
            kernel.Bind<IPartyPackageService>().To<PartyPackageService>();
            kernel.Bind<IPartyRoomService>().To<PartyRoomService>();
            kernel.Bind<IMediaService>().To<MediaService>();
            kernel.Bind<IRoomOrderService>().To<RoomOrderService>();
            kernel.Bind<IPartyOrderService>().To<PartyOrderService>();
            kernel.Bind<IPromotionCodeService>().To<PromotionCodeService>();
            kernel.Bind<IPayPalService>().To<PayPalService>();
            kernel.Bind<IGrabNGoOrderService>().To<GrabNGoOrderService>();
            kernel.Bind<IBakeryOrderService>().To<BakeryOrderService>();
            kernel.Bind<IReportingService>().To<ReportingService>();
            kernel.Bind<ISocialMediaService>().To<SocialMediaService>();
            kernel.Bind<IStoreService>().To<StoreService>();
            kernel.Bind<IGoogleService>().To<GoogleService>();
            kernel.Bind<IOffice365Service>().To<Office365Service>();
            #endregion

            #region Repositories
            kernel.Bind(typeof(IRepository<>)).To(typeof(Repository<>));
            kernel.Bind<IRepository<ApplicationUser>>().To<Repository<ApplicationUser>>();
            kernel.Bind<IRepository<PackageAddon>>().To<Repository<PackageAddon>>();
            kernel.Bind<IRepository<RoomBooking>>().To<Repository<RoomBooking>>();
            kernel.Bind<IRepository<PartyPackage>>().To<Repository<PartyPackage>>();
            kernel.Bind<IRepository<PartyRoom>>().To<Repository<PartyRoom>>();
            kernel.Bind<IRepository<MediaFile>>().To<Repository<MediaFile>>();
            kernel.Bind<IRepository<MediaGallery>>().To<Repository<MediaGallery>>();
            kernel.Bind<IRepository<TimeSlot>>().To<Repository<TimeSlot>>();
            kernel.Bind<IRepository<Child>>().To<Repository<Child>>();
            kernel.Bind<IRepository<PartyOrder>>().To<Repository<PartyOrder>>();
            #endregion

            #region Gateways
            kernel.Bind<IPayPalGateway>().To<PayPalGateway>();
            kernel.Bind<IGoogleGateway>().To<GoogleGateway>();
            kernel.Bind<IOffice365Gateway>().To<Office365Gateway>();
            kernel.Bind<ISendGridGateway>().To<SendGridGateway>();
            kernel.Bind<IFacebookGateway>().To<FacebookGateway>();
            kernel.Bind<ITwitterGateway>().To<TwitterGateway>();
            #endregion
        }           
    }
}
