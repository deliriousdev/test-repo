﻿using StirCrazy.Core;
using StirCrazy.Core.Domain;

namespace StirCrazy.Models.Party
{
    public class PartyCardModel
    {
        public ApplicationUser Parent { get; set; }
        public Child Child { get; set; }
        public RoomBookingModel RoomBooking { get; set; }
    }
}