﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Calendar;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Core.Services.Order
{
    public class RoomOrderService : IRoomOrderService
    {
        private readonly IRepository<RoomOrder> _roomOrderRepository;
        private readonly IRoomBookingService _roomBookingService;
        private readonly IAccountService _accountService;
        private readonly ISendGridService _sendGridService;
        private readonly IOffice365Service _office365Service;

        public RoomOrderService(IRepository<RoomOrder> roomOrderRepository, IAccountService accountService, ISendGridService sendGridService, IRoomBookingService roomBookingService, IOffice365Service office365Service)
        {
            _roomOrderRepository = roomOrderRepository;
            _accountService = accountService;
            _sendGridService = sendGridService;
            _roomBookingService = roomBookingService;

            _office365Service = new Office365Service("scheduling@stircrazy.ca");
        }

        public IEnumerable<RoomOrder> Get()
        {
            return _roomOrderRepository.Get();
        }

        public IEnumerable<RoomOrder> GetByStatus(OrderStatus status)
        {
            if (status == OrderStatus.Created)
                return _roomOrderRepository.Get().Where(x => x.RoomBooking.Date.Date >= DateTime.Today);

            if (status == OrderStatus.Cancelled)
                return _roomOrderRepository.Get().Where(x => x.Status == OrderStatus.Cancelled);

            return _roomOrderRepository.Get();
        }

        public async Task<RoomOrder> GetAsync(Guid id)
        {
            return await _roomOrderRepository.GetAsync(id);
        }

        public async Task<RoomOrder> CreateAsync(RoomOrder roomOrder)
        {
            var account = await _accountService.FindByEmailAsync(roomOrder.Customer.Email);
            roomOrder.DateCreated = DateTime.Now;

            // Format Data
            roomOrder.Customer.UserName = roomOrder.Customer.Email.ToLower();
            roomOrder.Customer.FirstName = HtmlHelperExtensions.ToTitleCase(roomOrder.Customer.FirstName);
            roomOrder.Customer.LastName = HtmlHelperExtensions.ToTitleCase(roomOrder.Customer.LastName);
            roomOrder.Customer.Address = HtmlHelperExtensions.ToTitleCase(roomOrder.Customer.Address);
            roomOrder.Customer.City = HtmlHelperExtensions.ToTitleCase(roomOrder.Customer.City);
            roomOrder.Customer.PostalCode = roomOrder.Customer.PostalCode.ToUpper();
            roomOrder.Customer.Province = roomOrder.Customer.Province;
            roomOrder.Customer.PhoneNumber = HtmlHelperExtensions.PhoneNumber(roomOrder.Customer.PhoneNumber);
            roomOrder.Customer.Email = roomOrder.Customer.Email.ToLower();

            if (account != null)
            {
                // Update Account Information
                account.UserName = roomOrder.Customer.Email;
                account.FirstName = roomOrder.Customer.FirstName;
                account.LastName = roomOrder.Customer.LastName;
                account.Address = roomOrder.Customer.Address;
                account.City = roomOrder.Customer.City;
                account.PostalCode = roomOrder.Customer.PostalCode;
                account.Province = roomOrder.Customer.Province;
                account.PhoneNumber = roomOrder.Customer.PhoneNumber;
                account.Email = roomOrder.Customer.Email;

                await _accountService.UpdateAsync(account);

                // Clear Customer Data From Room Order
                roomOrder.CustomerId = account.Id;
                roomOrder.Customer = null;
            }

            roomOrder.DateCreated = DateTime.Now;
            roomOrder.CreatedBy = roomOrder.CreatedBy ?? "Customer";

            var order = await _roomOrderRepository.AddAsync(roomOrder);

            // Setup Account Access
            if (account == null)
            {
                _accountService.SetPassword(order.Customer.Id);
                _accountService.AddToRole(order.Customer.Id, UserRoles.Customer);
            }

            return order;
        }

        public async Task UpdateAsync(RoomOrder roomOrder)
        {
            if (roomOrder != null)
            {
                await _roomOrderRepository.UpdateAsync(roomOrder, roomOrder.Id);
            }
        }

        public async Task UpdateStatusAsync(Guid id, OrderStatus status)
        {
            var roomOrder = _roomOrderRepository.Get(id);
            if (roomOrder != null)
            {
                roomOrder.Status = status;
                await _roomOrderRepository.UpdateAsync(roomOrder, roomOrder.Id);
            }
        }

        public async Task SendEmailAsync(string baseUri, Guid id, string email)
        {
            await _sendGridService.SendEmail(baseUri, id, email, ConfirmationType.RoomOrder);
        }

        public byte[] GetPdfBytes(string url)
        {
            return _sendGridService.GetPdfBytes(url);
        }

        public async Task SendNotificationAsync(Guid id)
        {
            var roomOrder = await GetAsync(id);
            if (roomOrder == null) return;

            var customer = _accountService.FindById(roomOrder.CustomerId);
            roomOrder.Customer = customer;

            var email = ConfigurationManager.AppSettings["RoomOrderNotifications"];

            var roomDate = roomOrder.RoomBooking.Date.Date.ToString("ddd MMM dd, yyyy");
            var checkIn = DateTime.Today.Add(roomOrder.RoomBooking.TimeSlot.CheckInTime).ToString("h:mm tt");
            var checkOut = DateTime.Today.Add(roomOrder.RoomBooking.TimeSlot.CheckOutTime).ToString("h:mm tt");
            var roomIn = DateTime.Today.Add(roomOrder.RoomBooking.TimeSlot.StartTime).ToString("h:mm tt");
            var roomOut = DateTime.Today.Add(roomOrder.RoomBooking.TimeSlot.EndTime).ToString("h:mm tt");
            var formattedPhoneNumber = HtmlHelperExtensions.PhoneNumber(roomOrder.Customer.PhoneNumber);
            var promotionCode = (roomOrder.PromotionCode != null) ? roomOrder.PromotionCode.Code : "NONE";

            if (roomOrder.PromotionCode != null)
            {
                promotionCode = string.Format("{0} - {1}", roomOrder.PromotionCode.Code, roomOrder.PromotionCode.Description);
            }

            var details = "<p>"
                          + "<h2>Booking Information</h2>"
                          + "Date: <strong>" + roomDate + "</strong><br />"
                          + "Room: <strong>" + roomOrder.RoomBooking.PartyRoom.Name + "</strong><br />"
                          + "Check-In: <strong>" + checkIn + "</strong><br />"
                          + "Room Time: <strong>" + roomIn + " - " + roomOut + "</strong><br />"
                          + "Check-Out: <strong>" + checkOut + "</strong><br />"
                          + "<br /><h2>Customer Information</h2>"
                          + "Full Name: <strong>" + roomOrder.Customer.FullName + "</strong><br />"
                          + "Address: <strong>" + roomOrder.Customer.FormattedAddress + "</strong><br />"
                          + "Phone: <strong>" + formattedPhoneNumber + "</strong><br />"
                          + "Email: <strong>" + roomOrder.Customer.Email + "</strong><br />"
                          + "<br /><h2>Payment Information</h2>"
                          + "Transaction ID: <strong>" + roomOrder.PaymentTransaction.TransactionNumber +
                          "</strong><br />"
                          + "Amount: <strong>$" + roomOrder.PaymentTransaction.Amount + "</strong><br />"
                          + "Promotion: <strong>" + promotionCode + "</strong><br />"
                          + "Status: <strong>" + roomOrder.PaymentTransaction.PaymentStatus.ToUpper() + "</strong>"
                          + "</p>";

            // SendGrid Email
            var mail = new SendGridMessage
            {
                Subject = "Room Rental Notification",
                Html = details,
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            try { await _sendGridService.Send(mail); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }

            // Outlook Calendar Event
            var pEvent = new CalendarItem()
            {
                Subject = roomOrder.Customer.FullName + " (" + roomOrder.RoomBooking.PartyRoom.Name + " Room Rental" + ")",
                Body = details,
                StartTime = roomOrder.RoomBooking.Date.Add(roomOrder.RoomBooking.TimeSlot.StartTime),
                EndTime = roomOrder.RoomBooking.Date.Add(roomOrder.RoomBooking.TimeSlot.EndTime)
            };

            try
            {
                var itemId = _office365Service.CreateEvent(pEvent.StartTime, pEvent.EndTime, pEvent.Subject, pEvent.Body, pEvent.IsAllDay);
                roomOrder.EventId = itemId;
                _roomOrderRepository.Update(roomOrder, roomOrder.Id);
            }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }

        public async Task RemoveAsync(Guid id)
        {
            var partyOrder = _roomOrderRepository.Get(id);
            if (partyOrder != null) { await _roomOrderRepository.DeleteAsync(partyOrder); }
        }

        public async Task CancelAsync(Guid id)
        {
            var partyOrder = await _roomOrderRepository.GetAsync(id);
            if (partyOrder == null) return;

            partyOrder.Status = OrderStatus.Cancelled;
            await _roomOrderRepository.UpdateAsync(partyOrder, partyOrder.Id);

            var booking = _roomBookingService.Get(partyOrder.RoomBookingId);
            booking.Status = Status.Cancelled;
            _roomBookingService.Update(booking);

            try { _office365Service.DeleteEvent(partyOrder.EventId); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }
    }
}