﻿$(document).ready(function () {

    var now = moment().add('days', 3);
    var dayOfWeek = now.isoWeekday();
    getOrderTotal(dayOfWeek);

    $('#RoomSchedule').datetimepicker({
        datepicker: true,
        format: 'm/d/Y',
        startDate: now.format('YYYY/MM/DD'),
        minDate: now.format('YYYY/MM/DD'),
        endDate: -Infinity,
        autoClose: true,
        defaultSelect: false,
        timepicker: false,
        validateOnBlur: true,
        inline: true,
        onSelectDate: function (dp, $input) {

            var date = moment($input.val());
            dayOfWeek = date.isoWeekday();
            getOrderTotal(dayOfWeek);

            $('#availability').hide();
            $('#no-availability').hide();
            $('#progressbar').show();

            $.ajax({
                type: "POST",
                url: "/Party/Availability",
                data: { searchDate: $input.val() },
                dataType: 'html',
                success: function (data) {

                    if (data == null || data == "") {
                        $('#progressbar').hide();
                        $('#no-availability').show();
                        $('#availability').empty();
                        return;
                    } else {
                        $('#availability').empty();
                        $('#availability').html(data);

                        $('#progressbar').hide();
                        $('#availability').show();
                    }
                }
            });

            var roomDate = date.format("MMM DD, YYYY");
            $('h2#RoomDate').text(roomDate);
        }
    });

    // INTIALIZE
    $('#availability').empty();

    $(document).on('click', '.timeslot', function () {
        var timeslot = $(this).attr("data-timeslot");
        $('#RoomBooking_TimeSlotId').val(timeslot);
    });

    $("#RoomOrder").data('submitted', false);
    $('.submit-btn').show();
    $('.submit-progress').hide();

    $(document).on('click', '#PromotionCodeValidate', function () {
        validatePromotionalCode();
    });

    function validatePromotionalCode() {
        if ($('#Promotion').val() == "" || $('#Promotion').val() == null) return;

        $('#OrderTotal').attr('data-adjustment', "0");
        $('#OrderTotal').attr('data-adjustment-type', "0");
        $('#PromotionDescription').empty();

        if ($('#Promotion').val() == "SFFC@DMIN7477") {
            $('#ManualTransaction').show();
            $('#PaypalTransaction').hide();
            $('#BypassPaypal').val("True");
            $('#BypassPaypal').attr('data-val', "true");
            return;
        }

        $.ajax({
            type: "POST",
            url: "/Home/PromotionCode",
            data: { promoCode: $('#Promotion').val(), promoType: 2 },
            dataType: 'json',
            success: function (data) {

                if (data != null) {
                    $('#PromotionDescription').empty();
                    $('#PromotionDescription').text(data.Description);

                    var total = $('#OrderTotal').val();

                    // Percentage
                    if (data.AdjustmentType == 1) {
                        var discount = (data.Adjustment / 100) * total;
                        total = (total - discount);
                    }

                    // Discount
                    if (data.AdjustmentType == 2) {
                        total = (total - data.Adjustment);
                    }

                    if (total > 0) {
                        $('#OrderTotal').attr('data-adjustment', data.Adjustment);
                        $('#OrderTotal').attr('data-adjustment-type', data.AdjustmentType);
                        getOrderTotal();
                    } else {
                        $('#PromotionDescription').empty();
                        $('#PromotionDescription').text("(!) The promotional code discount exceeds total order amount.");
                    }
                }
            }
        });
    };

    function getOrderTotal(day) {
        var roomBasePrice = 100.00;
        var total = 0;
        var discount = total;

        if (day >= 1 && day <= 4) {
            roomBasePrice = 100.00;
        }

        // Promotion Code
        if ($('#Promotion').val()) {

            var adjustmentAmount = $('#OrderTotal').attr('data-adjustment');
            var adjustmentType = $('#OrderTotal').attr('data-adjustment-type');

            // Percentage
            if (adjustmentType == 1) {
                var adjustment = (adjustmentAmount / 100) * total;
                discount = (total - adjustment);
            }

            // Percentage
            if (adjustmentType == 2) {
                discount = (total - adjustmentAmount);
            }

            if (discount > 0) { total = discount; }
        }

        // Totals
        var subTotal = (roomBasePrice + total).toFixed(2);
        var gstTotal = (subTotal * 0.05).toFixed(2);
        var orderTotal = (subTotal * 1.05).toFixed(2);

        $('#SubTotal').text('$' + subTotal);
        $('#GstTotal').text('$' + gstTotal);
        $('#FinalTotal').text('$' + orderTotal);

        $('#OrderSubTotal').val(subTotal);
        $('#OrderTaxTotal').val(gstTotal);
        $('#OrderTotal').val(orderTotal);
        $('#OrderTotal').val(parseInt(orderTotal));

        console.log("SubTotal: $" + subTotal + " | Total: $" + orderTotal);
    }

    $('#RoomBooking').submit(function() {

        var validationErrors = [];
        var validationSummary = [];

        $('#ValidationSummary').hide();
        $('#ValidationSummary ul').empty();

        /** VALIDATE PARTY ROOM / TIMESLOT **/
        var timeslot = $('.timeslot:checked');
        if (!timeslot.val()) {
            validationErrors.push("Please choose a party room and available timeslot for your party.");
        };

        /** VALIDATE CUSTOMER INFORMATION **/
        $('#CustomerInformation').find('input[type="text"]').each(function () {     
            if (!$.trim($(this).val()).length > 0) {
                validationErrors.push("Please Complete All Required Customer Information Fields.");
            }
        });

        /** VALIDATE EMAIL **/
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        var valid = pattern.test($('#Customer_Email').val());

        if (!valid) {
            validationErrors.push("Invalid Email Address, Please Provide A Valid Email Address.");
        } else {
            if ($('#Customer_Email').val() != $('#Customer_Email_Confirm').val()) {
                validationErrors.push("Email Address Mismatch, Please Ensure You've Confirmed Your Email Address.");
            }
        }

        /** VERIFY PROVINCE **/
        if ($("#Customer_Province option:selected").index() == 0) {
            validationErrors.push("Please Choose Your Province.");
        }

        /** GET VALIDATION SUMMARY **/
        $.each(validationErrors, function(i, e) {
            if ($.inArray(e, validationSummary) == -1) {
                $('#ValidationSummary').find('ul').empty();
                $('#ValidationSummary').find('ul').append('<li>' + e + '</li>');
                validationSummary.push(e);
            }
        });

        if (validationErrors.length) {
            $('#ValidationSummary').show();
            event.preventDefault();
        } else {
            var $form = $('form');

            if ($form.data('submitted') === true) {
                e.preventDefault();
            } else {
                $form.data('submitted', true);
                $('.submit-btn').hide();
                $('.submit-progress').show();
            }
        }
    });
});