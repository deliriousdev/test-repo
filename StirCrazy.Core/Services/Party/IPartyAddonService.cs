﻿using System.Collections.Generic;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Services.Party
{
    public interface IPartyAddonService
    {
        IEnumerable<PartyOrderAddon> Get();
        PartyOrderAddon Get(int id);
        PartyOrderAddon Add(PartyOrderAddon orderAddon);
        PartyOrderAddon Update(PartyOrderAddon orderAddon);

        IEnumerable<Addon> GetAddons();
        IEnumerable<Addon> GetAddons(AddonType addonType);
        Addon GetAddon(int id);
        Addon Add(Addon addon);
        Addon Update(Addon addon);
        void ReIndex(int id, int order);
    }
}