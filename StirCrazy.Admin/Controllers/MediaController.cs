﻿using System;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using StirCrazy.Admin.Models.Upload;
using StirCrazy.Core.Services;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class MediaController : Controller
    {
        private readonly IMediaService _mediaService;

        public MediaController(IMediaService mediaService)
        {
            _mediaService = mediaService;
        }

        public ActionResult Index()
        {
            var model = new UploadModel()
            {
                MediaFiles = _mediaService.Get()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(HttpPostedFileBase[] uploadImage, UploadModel uploadModel)
        {
            foreach (var image in uploadImage)
            {
                if (image != null)
                {
                    _mediaService.Save(image, Server.MapPath("~/"), uploadModel.UploadFolder);
                }
            }

            var model = new UploadModel()
            {
                MediaFiles = _mediaService.Get()
            };

            ViewBag.Location = uploadModel.UploadFolder;

            return View(model);
        }

        [HttpPost]
        public JsonResult UpdateIndex(string jsonRequest)
        {
            dynamic request = JsonConvert.DeserializeObject(jsonRequest);

            foreach (var item in request)
            {
                var id = Convert.ToInt32(item.id);
                var order = Convert.ToInt32(item.order);

                _mediaService.ReIndex(id, order);
            }

            return Json(request);
        }

        [HttpPost]
        public ActionResult Remove(string id)
        {
            _mediaService.Remove(Convert.ToInt16(id));
            return Content("");
        }
    }
}