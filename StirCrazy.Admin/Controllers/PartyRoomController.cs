﻿using System.Linq;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Party;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Services.Party;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class PartyRoomController : Controller
    {
        private readonly IPartyRoomService _partyRoomService;

        public PartyRoomController(IPartyRoomService partyRoomService)
        {
            _partyRoomService = partyRoomService;
        }

        public ActionResult Index()
        {
            var model = _partyRoomService.Get().Select(x => new PartyRoomModel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Capacity = x.Capacity,
                IsAvailable = x.IsAvailable
            }).ToList();

            return View(model);
        }

        public ActionResult Details(int id = 0)
        {
            var partyRoom = _partyRoomService.Get(id);
            var model = new PartyRoomModel()
            {
                Id = partyRoom.Id,
                Name = partyRoom.Name,
                Description = partyRoom.Description,
                Capacity = partyRoom.Capacity,
                IsAvailable = partyRoom.IsAvailable
            };

            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PartyRoomModel model)
        {
            if (ModelState.IsValid)
            {
                var partyRoom = new PartyRoom()
                {
                    Name = model.Name,
                    Description = model.Description,
                    Capacity = model.Capacity,
                    IsAvailable = model.IsAvailable
                };

                _partyRoomService.Add(partyRoom);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var partyRoom = _partyRoomService.Get(id);
            var model = new PartyRoomModel()
            {
                Id = partyRoom.Id,
                Name = partyRoom.Name,
                Description = partyRoom.Description,
                Capacity = partyRoom.Capacity,
                IsAvailable = partyRoom.IsAvailable
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PartyRoomModel model)
        {
            if (ModelState.IsValid)
            {
                var partyRoom = new PartyRoom()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Description = model.Description,
                    Capacity = model.Capacity,
                    IsAvailable = model.IsAvailable
                };

                _partyRoomService.Update(partyRoom);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Delete(int id = 0)
        {
            var partyRoom = _partyRoomService.Get(id);
            var model = new PartyRoomModel()
            {
                Id = partyRoom.Id,
                Name = partyRoom.Name,
                Description = partyRoom.Description,
                Capacity = partyRoom.Capacity,
                IsAvailable = partyRoom.IsAvailable
            };

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id = 0)
        {
            var partyRoom = _partyRoomService.Get(id);
            _partyRoomService.Remove(partyRoom);
            return RedirectToAction("Index");
        }
    }
}
