﻿using System.Collections.Generic;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Domain.Party
{
    public class PartyPackage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Guests { get; set; }
        public int MaxGuests { get; set; }
        public decimal GuestPrice { get; set; }
        public bool HasCake { get; set; }
        public bool HasPlaceSetting { get; set; }
        public virtual List<PackageAddon> PackageAddons { get; set; }
    }
}