﻿using System.Collections.Generic;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services
{
    public class AnnouncementService : IAnnouncementService
    {
        private readonly IRepository<Announcement> _repository;

        public AnnouncementService(IRepository<Announcement> repository)
        {
            _repository = repository;
        }

        public IEnumerable<Announcement> Get()
        {
            return _repository.Get();
        }

        public Announcement Get(int id)
        {
            return _repository.Get(id);
        }

        public Announcement Add(Announcement announcement)
        {
            return _repository.Add(announcement);
        }

        public Announcement Update(Announcement announcement)
        {
            return _repository.Update(announcement, announcement.Id);
        }

        public void Remove(int id)
        {
            var announcement = _repository.Get(id);
            if (announcement != null)
            {
                _repository.Delete(announcement);
            }
        }
    }
}
