﻿using System;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Admin.Models.Addons
{
    public class BakeryOrderAddonModel
    {
        public int Id { get; set; }
        public Guid BakeryOrderId { get; set; }
        public int Quantity { get; set; }
        public bool IsIncluded { get; set; }
        public bool IsSelected { get; set; }
        public virtual Addon Addon { get; set; }
    }
}