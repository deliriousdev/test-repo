﻿using System.Collections.Generic;
using System.Linq;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services.Store
{
    public class StoreService : IStoreService
    {
        private readonly IRepository<StoreHour> _storeHourRepository;
        private readonly IRepository<StoreHoliday> _storeHolidayRepository;

        public StoreService(IRepository<StoreHour> storeHourRepository, IRepository<StoreHoliday> storeHolidayRepository)
        {
            _storeHourRepository = storeHourRepository;
            _storeHolidayRepository = storeHolidayRepository;
        }

        public IEnumerable<StoreHour> GetStoreHours()
        {
            return _storeHourRepository.Get().OrderBy(x => x.Id);
        }

        public StoreHour GetStoreHour(int id)
        {
            return _storeHourRepository.Get(id);
        }

        public StoreHour Add(StoreHour storeHour)
        {
            return _storeHourRepository.Add(storeHour);
        }

        public StoreHour Update(StoreHour storeHour)
        {
            return _storeHourRepository.Update(storeHour, storeHour.Id);
        }

        public void RemoveStoreHour(int id)
        {
            var storeHour = _storeHourRepository.Get(id);
            _storeHourRepository.Delete(storeHour);
        }

        public IEnumerable<StoreHoliday> GetStoreHolidays()
        {
            return _storeHolidayRepository.Get();
        }

        public StoreHoliday GetStoreHoliday(int id)
        {
            return _storeHolidayRepository.Get(id);
        }

        public StoreHoliday Add(StoreHoliday storeHoliday)
        {
            return _storeHolidayRepository.Add(storeHoliday);
        }

        public StoreHoliday Update(StoreHoliday storeHoliday)
        {
            return _storeHolidayRepository.Update(storeHoliday, storeHoliday.Id);
        }

        public void RemoveStoreHoliday(int id)
        {
            var storeHoliday = _storeHolidayRepository.Get(id);
            if (storeHoliday != null) { _storeHolidayRepository.Delete(storeHoliday); }
        }
    }
}
