﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StirCrazy.Startup))]
namespace StirCrazy
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
