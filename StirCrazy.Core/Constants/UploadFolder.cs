﻿using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Constants
{
    public enum UploadFolder
    {
        [Display(Name = "Default")]
        Default = 0,
        [Display(Name = "Cakes")]
        Cakes = 1,
        [Display(Name = "Playground & Facility")]
        Facility = 2,
        [Display(Name = "Concession Counter")]
        ConcessionCounter = 3,
        [Display(Name = "Parent & Tot Lounge")]
        ParentTotLounge = 4,
        [Display(Name = "Party Rooms")]
        PartyRooms = 5,
        [Display(Name = "Carousel")]
        Carousel = 6,
        [Display(Name = "Announcement")]
        Announcement = 7
    }
}
