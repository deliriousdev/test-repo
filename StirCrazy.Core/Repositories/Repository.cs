﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace StirCrazy.Core.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected DatabaseContext Context;

        public Repository(DatabaseContext context)
        {
            Context = context;
        }

        public IEnumerable<T> Get()
        {
            return Context.Set<T>();
        }

        public async Task<List<T>> GetAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public T Get(int id)
        {
            return Context.Set<T>().Find(id);
        }

        public async Task<T> GetAsync(int id)
        {
            return await Context.Set<T>().FindAsync(id);
        }

        public T Get(Guid id)
        {
            return Context.Set<T>().Find(id);
        }

        public async Task<T> GetAsync(Guid id)
        {
            return await Context.Set<T>().FindAsync(id);
        }

        public T Find(Expression<Func<T, bool>> match)
        {
            return Context.Set<T>().SingleOrDefault(match);
        }

        public async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            return await Context.Set<T>().SingleOrDefaultAsync(match);
        }

        public IEnumerable<T> FindAll(Expression<Func<T, bool>> match)
        {
            return Context.Set<T>().Where(match);
        }

        public async Task<List<T>> FindAllAsync(Expression<Func<T, bool>> match)
        {
            return await Context.Set<T>().Where(match).ToListAsync();
        }

        public T Add(T t)
        {
            Context.Set<T>().Add(t);

            try { Context.SaveChanges(); }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(new DataException(string.Format("DbEntityValidation => Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                    }
                }
            }

            return t;
        }

        public async Task<T> AddAsync(T t)
        {
            Context.Set<T>().Add(t);

            try { await Context.SaveChangesAsync(); }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(new DataException(string.Format("DbEntityValidation => Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                    }
                }
            }

            return t;
        }

        public T Update(T updated, int key)
        {
            if (updated == null)
                return null;

            T existing = Context.Set<T>().Find(key);
            if (existing != null)
            {
                Context.Entry(existing).CurrentValues.SetValues(updated);
                try { Context.SaveChanges(); }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            Elmah.ErrorSignal.FromCurrentContext().Raise(new DataException(string.Format("DbEntityValidation => Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                        }
                    }
                }
            }
            return existing;
        }

        public async Task<T> UpdateAsync(T updated, int key)
        {
            if (updated == null)
                return null;

            T existing = await Context.Set<T>().FindAsync(key);
            if (existing != null)
            {
                Context.Entry(existing).CurrentValues.SetValues(updated);
                try { await Context.SaveChangesAsync(); }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            Elmah.ErrorSignal.FromCurrentContext().Raise(new DataException(string.Format("DbEntityValidation => Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                        }
                    }
                }
            }
            return existing;
        }

        public T Update(T updated, Guid key)
        {
            if (updated == null)
                return null;

            T existing = Context.Set<T>().Find(key);
            if (existing != null)
            {
                Context.Entry(existing).CurrentValues.SetValues(updated);
                try { Context.SaveChanges(); }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            Elmah.ErrorSignal.FromCurrentContext().Raise(new DataException(string.Format("DbEntityValidation => Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                        }
                    }
                }
            }
            return existing;
        }

        public async Task<T> UpdateAsync(T updated, Guid key)
        {
            if (updated == null)
                return null;

            T existing = await Context.Set<T>().FindAsync(key);
            if (existing != null)
            {
                Context.Entry(existing).CurrentValues.SetValues(updated);
                try {  await Context.SaveChangesAsync(); }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            Elmah.ErrorSignal.FromCurrentContext().Raise(new DataException(string.Format("DbEntityValidation => Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                        }
                    }
                }
            }
            return existing;
        }

        public void Delete(T t)
        {
            Context.Set<T>().Remove(t);
            Context.SaveChanges();
        }

        public async Task DeleteAsync(T t)
        {
            Context.Set<T>().Remove(t);

            try { await Context.SaveChangesAsync(); }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        Elmah.ErrorSignal.FromCurrentContext().Raise(new DataException(string.Format("DbEntityValidation => Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                    }
                }
            }
        }

        public int Count()
        {
            return Context.Set<T>().Count();
        }

        public async Task<int> CountAsync()
        {
            return await Context.Set<T>().CountAsync();
        }
    }
}
