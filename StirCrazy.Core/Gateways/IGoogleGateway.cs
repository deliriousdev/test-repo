using Google.Apis.Calendar.v3.Data;

namespace StirCrazy.Core.Gateways
{
    public interface IGoogleGateway
    {
        Events GetEvents();
        void InsertEvent(Event calendarEvent);
        void UpdateEvent(Event calendarEvent, string eventId);
        void RemoveEvent(string eventId);
        Event GetEvent(string eventId);
    }
}