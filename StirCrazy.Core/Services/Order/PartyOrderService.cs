﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Calendar;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Core.Services.Order
{
    public class PartyOrderService : IPartyOrderService
    {
        private readonly IRepository<PartyOrder> _partyOrderRepository;
        private readonly IRepository<PartyPackage> _partyPackageRepository;
        private readonly IAccountService _accountService;
        private readonly IRoomBookingService _roomBookingService;
        private readonly ISendGridService _sendGridService;
        private readonly IOffice365Service _office365Service;

        public PartyOrderService(IRepository<PartyOrder> partyOrderRepository,
                            IRepository<PartyPackage> partyPackageRepository,
                            IAccountService accountService,
                            ISendGridService sendGridService,
                            IRoomBookingService roomBookingService,
                            IOffice365Service office365Service)
        {
            _partyOrderRepository = partyOrderRepository;
            _accountService = accountService;
            _partyPackageRepository = partyPackageRepository;
            _sendGridService = sendGridService;
            _roomBookingService = roomBookingService;

            _office365Service = new Office365Service("scheduling@stircrazy.ca");
        }

        public IEnumerable<PartyOrder> Get()
        {
            return _partyOrderRepository.Get().Where(x => x.Status != OrderStatus.Created);
        }

        public IEnumerable<PartyOrder> GetByStatus(OrderStatus status)
        {
            if (status == OrderStatus.Created)
                return _partyOrderRepository.Get().Where(x => x.RoomBooking.Date.Date >= DateTime.Today);

            if (status == OrderStatus.Cancelled)
                return _partyOrderRepository.Get().Where(x => x.Status == OrderStatus.Cancelled);

            return _partyOrderRepository.Get();
        }

        public async Task<PartyOrder> GetAsync(Guid id)
        {
            return await _partyOrderRepository.GetAsync(id);
        }

        public async Task<PartyOrder> CreateAsync(PartyOrder partyOrder)
        {
            var account = await _accountService.FindByEmailAsync(partyOrder.Parent.Email);
            partyOrder.DateCreated = DateTime.Now;

            // Format Data
            partyOrder.Parent.UserName = partyOrder.Parent.Email.ToLower();
            partyOrder.Parent.FirstName = HtmlHelperExtensions.ToTitleCase(partyOrder.Parent.FirstName);
            partyOrder.Parent.LastName = HtmlHelperExtensions.ToTitleCase(partyOrder.Parent.LastName);
            partyOrder.Parent.Address = HtmlHelperExtensions.ToTitleCase(partyOrder.Parent.Address);
            partyOrder.Parent.City = HtmlHelperExtensions.ToTitleCase(partyOrder.Parent.City);
            partyOrder.Parent.PostalCode = partyOrder.Parent.PostalCode.ToUpper();
            partyOrder.Parent.Province = partyOrder.Parent.Province;
            partyOrder.Parent.PhoneNumber = HtmlHelperExtensions.PhoneNumber(partyOrder.Parent.PhoneNumber);
            partyOrder.Parent.Email = partyOrder.Parent.Email.ToLower();

            partyOrder.Child.FirstName = HtmlHelperExtensions.ToTitleCase(partyOrder.Child.FirstName);
            partyOrder.Child.LastName = HtmlHelperExtensions.ToTitleCase(partyOrder.Child.LastName);

            partyOrder.Child.ParentId = partyOrder.Parent.Id;

            if (account != null)
            {
                // Update Account Information
                account.UserName = partyOrder.Parent.Email;
                account.FirstName = partyOrder.Parent.FirstName;
                account.LastName = partyOrder.Parent.LastName;
                account.Address = partyOrder.Parent.Address;
                account.City = partyOrder.Parent.City;
                account.PostalCode = partyOrder.Parent.PostalCode;
                account.Province = partyOrder.Parent.Province;
                account.PhoneNumber = partyOrder.Parent.PhoneNumber;
                account.Email = partyOrder.Parent.Email;

                // Add Child Information
                partyOrder.Child.ParentId = account.Id;
                var child = account.Children.FirstOrDefault(x => x.FullName == partyOrder.Child.FullName);

                if (child != null)
                {
                    child.FirstName = HtmlHelperExtensions.ToTitleCase(partyOrder.Child.FirstName);
                    child.LastName = HtmlHelperExtensions.ToTitleCase(partyOrder.Child.LastName);
                    child.Age = partyOrder.Child.Age;
                    child.Gender = partyOrder.Child.Gender;
                    child.ParentId = account.Id;

                    partyOrder.ChildId = child.Id;
                }

                _accountService.Update(account);

                // Clear Customer Data From Room Order
                partyOrder.ParentId = account.Id;
                partyOrder.Parent = null;
            }

            partyOrder.DateCreated = DateTime.Now;
            partyOrder.CreatedBy = partyOrder.CreatedBy ?? "Customer";

            var order = _partyOrderRepository.Add(partyOrder);

            // Setup Account Access
            if (account == null)
            {
                _accountService.SetPassword(order.Parent.Id);
                _accountService.AddToRole(order.Parent.Id, UserRoles.Customer);
            }
            else
            {
                order.Parent = account;
            }

            if (partyOrder.Child == null)
            {
                
            }

            return order;
        }

        public async Task UpdateAsync(PartyOrder partyOrder)
        {
            if (partyOrder != null)
            {
                await _partyOrderRepository.UpdateAsync(partyOrder, partyOrder.Id);
            }
        }

        public async Task UpdateStatusAsync(Guid id, OrderStatus status)
        {
            var partyOrder = _partyOrderRepository.Get(id);
            if (partyOrder != null)
            {
                partyOrder.Status = status;
                await _partyOrderRepository.UpdateAsync(partyOrder, partyOrder.Id);
            }
        }

        public decimal Calculate(PartyOrder partyOrder)
        {
            var partyPackage = _partyPackageRepository.Get(partyOrder.PartyPackageId);

            if (partyPackage.Id == 3 && partyOrder.RoomBooking.Date.DayOfWeek == DayOfWeek.Friday)
            {
                partyPackage.Price = 119.99m;
            }

            var guestCount = (partyPackage.Guests + partyOrder.AdditionalGuests);
            var guestTotal = (partyOrder.AdditionalGuests * partyPackage.GuestPrice);
            var orderTotal = (partyPackage.Price + guestTotal);

            // Calculate Addons
            orderTotal += partyOrder.PartyAddons.Where(x => !x.IsIncluded).Sum(partyAddon => (partyAddon.Quantity * partyAddon.Addon.Price));

            // Upgrade LootBags ($2.00/Guest Surcharge)
            if (partyOrder.UpgradeLootBags) { orderTotal += (guestCount * 2); }

            // Upgrade Latex Balloons ($2.00/Guest Surcharge)
            if (partyOrder.UpgradeLatexBalloons) { orderTotal += (guestCount - 1); }

            // Bring Own Balloons ($5.00 Surcharge)
            if (partyOrder.HasOwnBalloons && partyPackage.Id != 2) { orderTotal += 5m; }

            // Bring Own Decor ($2.00/Guest Surcharge)
            if (partyOrder.HasOwnDecor && partyPackage.Id == 3) { orderTotal += (guestCount * 2); }

            // Bring Own LootBags ($2.00/Guest Surcharge)
            if (partyOrder.HasOwnLootBag && partyPackage.Id == 3) { orderTotal += (guestCount * 2); }

            // Place Setting ($2.00/Guest Surcharge)
            if (partyOrder.HasOwnPlaceSetting && partyPackage.Id == 3) { orderTotal += (guestCount * 2); }

            // Add Place Setting ($5.00/Guest Surcharge)
            if (partyOrder.AddPlaceSetting && !partyPackage.HasPlaceSetting) { orderTotal += (guestCount * 5); }

            // Add Cake ($45.00)
            if (partyPackage.HasCake && partyOrder.CakeFlavour != null)
            {
                orderTotal += 45m;
            }

            return orderTotal;
        }

        public async Task SendEmailAsync(string baseUri, Guid id, string email)
        {
            await _sendGridService.SendEmail(baseUri, id, email, ConfirmationType.PartyOrder);
        }

        public byte[] GetPdfBytes(string url)
        {
            return _sendGridService.GetPdfBytes(url);
        }

        public async Task SendNotificationAsync(PartyOrder partyOrder)
        {
            var email = ConfigurationManager.AppSettings["PartyOrderNotifications"];

            var partyPackage = await _partyPackageRepository.GetAsync(partyOrder.PartyPackageId);
            partyOrder.PartyPackage = partyPackage;

            var partyDate = partyOrder.RoomBooking.Date.Date.ToString("ddd MMM dd, yyyy");
            var checkIn = DateTime.Today.Add(partyOrder.RoomBooking.TimeSlot.CheckInTime).ToString("h:mm tt");
            var checkOut = DateTime.Today.Add(partyOrder.RoomBooking.TimeSlot.CheckOutTime).ToString("h:mm tt");
            var roomIn = DateTime.Today.Add(partyOrder.RoomBooking.TimeSlot.StartTime).ToString("h:mm tt");
            var roomOut = DateTime.Today.Add(partyOrder.RoomBooking.TimeSlot.EndTime).ToString("h:mm tt");
            var formattedPhoneNumber = HtmlHelperExtensions.PhoneNumber(partyOrder.Parent.PhoneNumber);
            var promotionCode = (partyOrder.PromotionCode != null) ? partyOrder.PromotionCode.Code : "NONE";

            if (partyOrder.PromotionCode != null)
            {
                promotionCode = string.Format("{0} - {1}", partyOrder.PromotionCode.Code, partyOrder.PromotionCode.Description);
            }

            var details = "<p>"
                          + "<h2>Booking Information</h2>"
                          + "Date: <strong>" + partyDate + "</strong><br />"
                          + "Package: <strong>" + partyOrder.PartyPackage.Name + "</strong><br />"
                          + "Room: <strong>" + partyOrder.RoomBooking.PartyRoom.Name + "</strong><br />"
                          + "Check-In: <strong>" + checkIn + "</strong><br />"
                          + "Room Time: <strong>" + roomIn + " - " + roomOut + "</strong><br />"
                          + "Check-Out: <strong>" + checkOut + "</strong><br />"
                          + "<br /><h2>Customer Information</h2>"
                          + "Full Name: <strong>" + partyOrder.Parent.FullName + "</strong><br />"
                          + "Address: <strong>" + partyOrder.Parent.Address + " " + partyOrder.Parent.City + ", " +
                          partyOrder.Parent.Province + " " + partyOrder.Parent.PostalCode + "</strong><br />"
                          + "Phone: <strong>" + formattedPhoneNumber + "</strong><br />"
                          + "Email: <strong>" + partyOrder.Parent.Email + "</strong><br />"
                          + "<br /><h2>Payment Information</h2>"
                          + "Transaction: <strong>" + partyOrder.PaymentTransaction.TransactionNumber +
                          "</strong><br />"
                          + "Amount: <strong>$" + partyOrder.PaymentTransaction.Amount + "</strong><br />"
                          + "Promotion: <strong>" + promotionCode + "</strong><br />"
                          + "Status: <strong>" + partyOrder.PaymentTransaction.PaymentStatus.ToUpper() + "</strong>"
                          + "<br />"
                          + "<br /><h2>Additional Information</h2>"
                          + partyOrder.Notes
                          + "</p>";

            // SendGrid Email
            var mail = new SendGridMessage
            {
                Subject = "Party Order Notification",
                Html = details,
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            try { await _sendGridService.Send(mail); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }

        public async Task CreateOutlookEvent(PartyOrder partyOrder)
        {
            var partyDate = partyOrder.RoomBooking.Date.Date.ToString("ddd MMM dd, yyyy");
            var checkIn = DateTime.Today.Add(partyOrder.RoomBooking.TimeSlot.CheckInTime).ToString("h:mm tt");
            var checkOut = DateTime.Today.Add(partyOrder.RoomBooking.TimeSlot.CheckOutTime).ToString("h:mm tt");
            var roomIn = DateTime.Today.Add(partyOrder.RoomBooking.TimeSlot.StartTime).ToString("h:mm tt");
            var roomOut = DateTime.Today.Add(partyOrder.RoomBooking.TimeSlot.EndTime).ToString("h:mm tt");
            var formattedPhoneNumber = HtmlHelperExtensions.PhoneNumber(partyOrder.Parent.PhoneNumber);
            var promotionCode = (partyOrder.PromotionCode != null) ? partyOrder.PromotionCode.Code : "NONE";

            if (partyOrder.PromotionCode != null)
            {
                promotionCode = string.Format("{0} - {1}", partyOrder.PromotionCode.Code, partyOrder.PromotionCode.Description);
            }

            var details = "<p>"
                          + "<h2>Booking Information</h2>"
                          + "Date: <strong>" + partyDate + "</strong><br />"
                          + "Package: <strong>" + partyOrder.PartyPackage.Name + "</strong><br />"
                          + "Room: <strong>" + partyOrder.RoomBooking.PartyRoom.Name + "</strong><br />"
                          + "Check-In: <strong>" + checkIn + "</strong><br />"
                          + "Room Time: <strong>" + roomIn + " - " + roomOut + "</strong><br />"
                          + "Check-Out: <strong>" + checkOut + "</strong><br />"
                          + "<br /><h2>Customer Information</h2>"
                          + "Full Name: <strong>" + partyOrder.Parent.FullName + "</strong><br />"
                          + "Address: <strong>" + partyOrder.Parent.Address + " " + partyOrder.Parent.City + ", " +
                          partyOrder.Parent.Province + " " + partyOrder.Parent.PostalCode + "</strong><br />"
                          + "Phone: <strong>" + formattedPhoneNumber + "</strong><br />"
                          + "Email: <strong>" + partyOrder.Parent.Email + "</strong><br />"
                          + "<br /><h2>Payment Information</h2>"
                          + "Transaction: <strong>" + partyOrder.PaymentTransaction.TransactionNumber +
                          "</strong><br />"
                          + "Amount: <strong>$" + partyOrder.PaymentTransaction.Amount + "</strong><br />"
                          + "Promotion: <strong>" + promotionCode + "</strong><br />"
                          + "Status: <strong>" + partyOrder.PaymentTransaction.PaymentStatus.ToUpper() + "</strong>"
                          + "<br />"
                          + "<br /><h2>Additional Information</h2>"
                          + partyOrder.Notes
                          + "</p>";

            // Outlook Calendar Event
            var pEvent = new CalendarItem()
            {
                Subject = partyOrder.Parent.FullName + " (" + partyOrder.RoomBooking.PartyRoom.Name + " Room - " + partyOrder.PartyPackage.Name + ")",
                Body = details,
                StartTime = partyOrder.RoomBooking.Date.Add(partyOrder.RoomBooking.TimeSlot.StartTime),
                EndTime = partyOrder.RoomBooking.Date.Add(partyOrder.RoomBooking.TimeSlot.EndTime)
            };

            try
            {
                var itemId = _office365Service.CreateEvent(pEvent.StartTime, pEvent.EndTime, pEvent.Subject, pEvent.Body, pEvent.IsAllDay);
                partyOrder.EventId = itemId;

                await _partyOrderRepository.UpdateAsync(partyOrder, partyOrder.Id);
            }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }

        public async Task RemoveAsync(Guid id)
        {
            var partyOrder = _partyOrderRepository.Get(id);
            if (partyOrder != null) { await _partyOrderRepository.DeleteAsync(partyOrder); }
        }

        public async Task CancelAsync(Guid id)
        {
            var partyOrder = await _partyOrderRepository.GetAsync(id);
            if (partyOrder == null) return;

            partyOrder.Status = OrderStatus.Cancelled;
            await _partyOrderRepository.UpdateAsync(partyOrder, partyOrder.Id);

            var booking = _roomBookingService.Get(partyOrder.RoomBookingId);
            booking.Status = Status.Cancelled;
            _roomBookingService.Update(booking);

            try { _office365Service.DeleteEvent(partyOrder.EventId); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }
    }
}