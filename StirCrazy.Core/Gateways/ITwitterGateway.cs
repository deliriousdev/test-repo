﻿using System.Collections.Generic;
using Tweetinvi.Core.Interfaces;

namespace StirCrazy.Core.Gateways
{
    public interface ITwitterGateway
    {
        List<ITweet> GetTweets();
    }
}