﻿$(document).ready(function() {

    var now = moment().add('days', 3);

    // DATE PICKER CALENDAR
    $('#PartySchedule').datetimepicker({
        datepicker: true,
        format: 'm/d/Y',
        startDate: now.format('YYYY/MM/DD'),
        minDate: now.format('YYYY/MM/DD'),
        defaultSelect: false,
        endDate: -Infinity,
        autoClose: true,
        timepicker: false,
        validateOnBlur: true,
        inline: true,
        onSelectDate: function(dp, $input) {

            $('#availability').hide();
            $('#no-availability').hide();
            $('#progressbar').show();

            var date = moment($input.val());
            var dayOfWeek = date.isoWeekday();
            setPackageBasePrice(dayOfWeek);

            $('#PartySchedule').val($input.val());

            $.ajax({
                type: "POST",
                url: "/Party/Availability",
                data: { searchDate: $input.val() },
                dataType: 'html',
                success: function(data) {

                    if (data == null || data == "") {
                        $('#progressbar').hide();
                        $('#no-availability').show();
                        $('#availability').empty();
                    } else {
                        $('#availability').empty();
                        $('#availability').html(data);

                        $('#progressbar').hide();
                        $('#availability').show();
                    }
                }
            });

            console.log("date: " + date.format("MMM DD, YYYY"));
            var partyDate = date.format("MMM DD, YYYY");
            $('h2#PartyDate').text(partyDate);
        }
    });


    // INTIALIZE
    var initialize = function() {
        getPartyTotal();
        toggleCupcakes();
        toggleFoodPlatterSwap();
        limitFlavourSelection();

        if ($('#RoomBooking_TimeSlotId').val() && $('#PartySchedule').val()) {
            $.ajax({
                type: "POST",
                url: "/Party/Availability",
                data: { searchDate: $('#PartySchedule').val() },
                dataType: 'html',
                success: function(data) {

                    if (data == null || data == "") {
                        $('#progressbar').hide();
                        $('#no-availability').show();
                        $('#availability').empty();
                    } else {
                        $('#availability').empty();
                        $('#availability').html(data);

                        $('#progressbar').hide();
                        $('#availability').show();
                    }
                }
            });
        }

        $("#PartyBooking").data('submitted', false);
        $('#Submit').show();
        $('#SubmitProgress').hide();
    }

    if ($('#Parent_Email').val()) {
        $('#Parent_Email_Confirm').val($('#Parent_Email').val());
    }

    initialize();

    $("#PartyBooking").on({
        focusout: getPartyTotal,
        change: getPartyTotal,
        keyup: getPartyTotal
    });

    // PROMOTION CODE
    $(document).on('click', '#PromotionCodeValidate', function() {
        $('#PackageBasePrice').attr('data-adjustment', "0");
        $('#PackageBasePrice').attr('data-adjustment-type', "0");

        $.ajax({
            type: "POST",
            url: "/Home/PromotionCode",
            data: { promoCode: $('#Promotion').val(), promoType: 1 },
            dataType: 'json',
            success: function(data) {

                if (data != null) {
                    $('#PromotionDescription').empty();
                    $('#PromotionDescription').text(data.Description);

                    if ($('#PackageBasePrice').val() >= data.Adjustment) {
                        $('#PackageBasePrice').attr('data-adjustment', data.Adjustment);
                        $('#PackageBasePrice').attr('data-adjustment-type', data.AdjustmentType);
                        getPartyTotal();
                        return;
                    } else {
                        $('#PromotionDescription').empty();
                        $('#PromotionDescription').text("* Can Not Apply On A Negative Balance *");
                        return;
                    }
                }
            }
        });
    });

    // EVENT LISTENERS
    $(document).on('click', '.timeslot', function() {
        var timeslot = $(this).attr("data-timeslot");
        var roomCapacity = $(this).attr("data-capacity");

        $('#RoomCapacity').val(roomCapacity);
        $('#RoomBooking_TimeSlotId').val(timeslot);

        var adultCapacity = getGuestCapacity();
        var childCapacity = getGuestTotal();

        $('#AdultGuests').text(adultCapacity);
        $('#ChildGuests').text(childCapacity);

        if ($('#PartyPackage').val() == 4) {
            $('#MaxAdmission').val(15); // Michelle: Limit to 15
        }

        setPrepaidAdmissions();
        $('#PartyGuests').show();
    });


    $(document).on('change', '#AdditionalGuests', function() {
        var adultCapacity = getGuestCapacity();
        var childCapacity = getGuestTotal();

        $('#AdultGuests').text(adultCapacity);
        $('#ChildGuests').text(childCapacity);

        setPrepaidAdmissions();

        var additionalGuestsTotal = (parseInt($('#AdditionalGuests').val()) * $('#AdditionalGuests').data('price'));
        var prepaidAdmissionsTotal = (parseInt($('#PrepaidAdmissions').val()) * $('#PrepaidAdmissions').data('price'));

        $('#AdditionalGuestsTotal').empty();
        $('#AdditionalGuestsTotal').text("$" + additionalGuestsTotal.toFixed(2));

        $('#PrePaidAdmissionsTotal').empty();
        $('#PrePaidAdmissionsTotal').text("$" + prepaidAdmissionsTotal.toFixed(2));

        getPartyTotal();
    });

    $(document).on('change', '#PrepaidAdmissions', function () {
        var adultCapacity = getGuestCapacity();
        var childCapacity = getGuestTotal();

        $('#AdultGuests').text(adultCapacity);
        $('#ChildGuests').text(childCapacity);
        $('#PrePaidAdmissions').text(parseInt($('#PrepaidAdmissions').val()));

        var additionalGuestsTotal = (parseInt($('#AdditionalGuests').val()) * $('#AdditionalGuests').data('price'));
        var prepaidAdmissionsTotal = (parseInt($('#PrepaidAdmissions').val()) * $('#PrepaidAdmissions').data('price'));

        getPartyTotal();
        setAdditionalGuests();

        $('#AdditionalGuestsTotal').empty();
        $('#AdditionalGuestsTotal').text("$" + additionalGuestsTotal.toFixed(2));

        $('#PrePaidAdmissionsTotal').empty();
        $('#PrePaidAdmissionsTotal').text("$" + prepaidAdmissionsTotal.toFixed(2));
    });

    $("body .calculated").on({
        focusout: getPartyTotal,
        change: getPartyTotal,
        keyup: getPartyTotal
    });

    //** INPUTS **//
    $('#PartySection :text').focusin(function () {
        $(this).val("");
    });

    $('#PartySection :text').focusout(function () {
        if ($(this).val() == "" || $(this).val() == null) {
            $(this).val("0");
        }
    });

    function setPrepaidAdmissions() {
        var additionalGuests = $("#AdditionalGuests").val();
        var maxAdmissions = $('#MaxAdmission').val();

        var remainingCapacity = maxAdmissions - additionalGuests;
        $("#PrepaidAdmissions").empty();

        var i = 0;
        while (i <= remainingCapacity) {
            $("#PrepaidAdmissions").append("<option value=" + i + ">" + i + "</option>");
            i++;
        }

        $('#PrePaidAdmissions').text(parseInt($('#PrepaidAdmissions').val()));

    };

    function setAdditionalGuests() {
        var prepaidAdmissions = $("#PrepaidAdmissions").val();
        var additionalGuests = $("#AdditionalGuests").val();
        var maxAdmissions = $('#MaxAdmission').val();

        var remainingCapacity = maxAdmissions - prepaidAdmissions;
        $("#AdditionalGuests").empty();

        var i = 0;
        while (i <= remainingCapacity) {
            $("#AdditionalGuests").append("<option value=" + i + ">" + i + "</option>");
            i++;
        }

        $("#AdditionalGuests").val(additionalGuests);
    }

    $(document).on('click', '.cupcakes', function () {
        getCupcakeTotal();
    });

    $(document).on('change', '#PartyTheme', function () {
        ($("#PartyTheme option:selected").index() > 0) ? $('#BalloonTheme').text('"' + $(this).val() + '"') : $('#BalloonTheme').empty();
    });

    $(document).on('change', '#CakeFlavour', function () {
        getPartyTotal();
    });

    $(document).on('click', '#SwapFoodPlatter', function () {
        toggleFoodPlatterSwap();
    });

    $(document).on('click', '#AddPlaceSetting', function () {
        addPlaceSettings();
    });

    $(document).on('click', '#HasCupcakes', function () {
        toggleCupcakes();
    });

    $(document).on('click', '#HasOwnCake', function () {
        toggleCupcakes();
    });

    $(document).on('click', '#HasOwnLootBag', function () {
        hasOwnLootBag();
    });

    $(document).on('click', '#HasOwnDecor', function () {
        hasOwnDecor();
    });

    $(document).on('click', '#HasOwnPlaceSetting', function () {
        hasOwnPlaceSettings();
    });

    $(document).on('click', '#AddPlaceSetting', function () {
        addPlaceSettings();
    });

    $(document).on('click', '#HasOwnBalloons', function () {
        hasOwnBalloons();
    });

    $('.cupcakes').on({
        keyup: getCupcakeTotal,
        change: getCupcakeTotal,
    });

    $(document).on('click', '.cupcake', function () {

        $('.cupcake').each(function () {
            $(this).val("False");
        });

        $(this).val("True");
    });

    $(document).on('click', '.flavours', function () {
        limitFlavourSelection();
    });

    $(document).on('click', '.flavours-included', function () {
        limitFlavourSelection();
    });

    $(document).on('click', '#AddCake', function () {
        addCake();
    });

    // CUPCAKES
    function toggleCupcakes() {

        $("#Cupcakes").hide();
        $("#OwnCakeDisclaimer").hide();
        $("#SwapCupcakes").show();

        // BRING OWN CAKE
        if ($('#HasOwnCake').is(':checked')) {

            $("#Cupcakes").hide();
            $("#SwapCupcakes").hide();
            $("#Cake").hide();
            $("#OwnCakeDisclaimer").show();

            $('#WantCupcakes').removeAttr('checked');

            // Clear Cupcake Data
            //$('.cupcakes').each(function () {
            //    $(this).val("0");
            //});

            // Reset Cake Selection
            $("#CakeFlavour").prop('selectedIndex', 0);
            $('#Cupcakes').find(':radio').each(function () {
                $(this).removeAttr('checked');
            });

            $('.flavours-included').removeAttr('checked', 'checked');
            $('.flavours-included').removeAttr('disabled', 'disabled');
        }

        // INCLUDED CAKE
        if (!$('#HasOwnCake').is(':checked') && $('#PartyPackage').val() != 3 && $('#PartyPackage').val() != 4) {
            $("#Cake").show();
        }

        if (!$('#HasCupcakes').is(':checked')) {
            $('#OwnCake').show();

            $('.flavours-included').removeAttr('checked', 'checked');
            $('.flavours-included').removeAttr('disabled', 'disabled');

            $('#Cupcakes').find(':radio').each(function () {
                $(this).removeAttr('checked');
            });
        }

        // SWAP CAKE FOR CUPCAKES
        if ($('#HasCupcakes').is(':checked')) {

            $("#Cupcakes").show();
            $("#Cake").hide();
            $('#OwnCake').hide();
            $("#OwnCakeDisclaimer").hide();

            $('#HasOwnCake').removeAttr('checked');

            // Reset Cake Selection
            $("#CakeFlavour").prop('selectedIndex', 0);
        }
    }

    function limitFlavourSelection() {
        var selected = $('.flavours:checked').size();

        if (selected >= 2) {
            $('.flavours').not(':checked').each(function () {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            $('.flavours').removeAttr('disabled', 'disabled');
        }

        selected = $('.flavours-included:checked').size();

        if (selected >= 2) {
            $('.flavours-included').not(':checked').each(function () {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            $('.flavours-included').removeAttr('disabled', 'disabled');
        }
    }

    function getCupcakeTotal() {
        var total = 0.00;
        var dozen = 0.00;

        $('.cupcakes').each(function () {
            var quantity = $(this).val();
            var perDozen = $(this).data('price');

            if (quantity.match(/^\d+$/)) {
                total += (perDozen * quantity);
                dozen += +quantity;
            }
        });

        $('#cupcakeTotal').text('$' + total + ' for ' + dozen + ' dozen.');
        return total;
    }

    // ADD CAKE
    function addCake() {

        if ($('#AddCake').is(':checked')) {
            $("#Cake").show();
            $("#OwnCake").hide();
            $("#OwnCakeDisclaimer").hide();
            $('#HasOwnCake').removeAttr('checked');
        } else {
            $("#Cake").hide();
            $("#OwnCake").show();

            $("#CakeFlavour").prop('selectedIndex', 0);
            $("#CakeTheme").prop('selectedIndex', 0);
        }
    }

    // ADD PLACE SETTINGS
    function addPlaceSettings() {

        if ($('#AddPlaceSetting').is(':checked')) {
            $("#HasOwnPlaceSettings").hide();
            $('#HasOwnPlaceSetting').removeAttr('checked');
            $("#PartyTheme").show();
        } else {
            $("#PartyTheme").hide();
            $("#PartyTheme").prop('selectedIndex', 0);
            $("#HasOwnPlaceSettings").show();
        }
    };

    // OWN PLACE SETTINGS
    function hasOwnPlaceSettings() {

        if ($('#HasOwnPlaceSetting').is(':checked')) {
            $("#AddPlaceSettings").hide();
            $('#AddPlaceSettings').removeAttr('checked', 'checked');
            $("#OwnDecorDisclaimer").show();
        } else {
            $("#AddPlaceSettings").show();
        }
    }

    function getSurcharges() {

        var surchargeTotal = 0;
        var guestTotal = (parseInt($('#PartyPackage_Guests').val()) + parseInt($('#AdditionalGuests').val()));

        if ($('#PartyPackage').val() == 4) { guestTotal = (parseInt($('#PrepaidAdmissions').val())); }

        // HAS OWN PLACE SETTINGS
        if (($('#PartyPackage').val() == 3 || $('#PartyPackage').val() == 4)) {
            var surchargeHasOwnPlaceSetting = (guestTotal * 2);

            if ($('#HasOwnPlaceSetting').is(':checked')) {
                surchargeTotal += surchargeHasOwnPlaceSetting;
            }
            $('#HasOwnPlaceSettingTotal').text('$' + surchargeHasOwnPlaceSetting.toFixed(2));

            addPlaceSettings();
            hasOwnPlaceSettings();
        }

        // HAS OWN LOOT BAG
        if (($('#PartyPackage').val() == 3 || $('#PartyPackage').val() == 4)) {
            var surchargeHasOwnLootBag = (guestTotal * 2);
            
            if ($('#HasOwnLootBag').is(':checked')) { surchargeTotal += surchargeHasOwnLootBag; }
            $('#HasOwnLootBagTotal').text('$' + surchargeHasOwnLootBag.toFixed(2));
            hasOwnLootBag();
        }

        // HAS OWN DECORATIONS
        if (($('#PartyPackage').val() == 3 || $('#PartyPackage').val() == 4)) {
            var surchargeHasOwnDecor = (guestTotal * 2);

            if ($('#HasOwnDecor').is(':checked')) { surchargeTotal += surchargeHasOwnDecor; }
            $('#HasOwnDecorTotal').text('$' + surchargeHasOwnDecor.toFixed(2));
            hasOwnDecor();
        }

        // HAS OWN BALLOONS
        if (($('#PartyPackage').val() != 2)) {
            var surchargeHasOwnBalloons = 5;

            if ($('#HasOwnBalloons').is(':checked')) { surchargeTotal += surchargeHasOwnBalloons; }
            $('#HasOwnBalloonsTotal').text('$' + surchargeHasOwnBalloons.toFixed(2));
            hasOwnBalloons();
        }

        return surchargeTotal;
    }

    function getUpgrades() {

        var upgradeTotal = 0;
        var guestTotal = (parseInt($('#PartyPackage_Guests').val()) + parseInt($('#AdditionalGuests').val()));

        if ($('#PartyPackage').val() == 4) { guestTotal = (parseInt($('#PrepaidAdmissions').val())); }

        // ADD PLACE SETTINGS
        var upgradeAddPlaceSettings = (guestTotal * 5);
        if ($('#AddPlaceSetting').is(':checked')) { upgradeTotal += upgradeAddPlaceSettings; }
        $('#AddPlaceSettingTotal').text('$' + upgradeAddPlaceSettings.toFixed(2));

        // ADD CAKE
        if ($('#PartyPackage').val() == 3 || $('#PartyPackage').val() == 4) {

            if ($('#AddCake').is(':checked')) { upgradeTotal += 45; }
            //addCake();
        }

        // UPGRADE LOOT BAGS
        var upgradeLootBags = (guestTotal * 2);
        if ($('#UpgradeLootBags').is(':checked')) { upgradeTotal += upgradeLootBags; }      
        $('#UpgradeLootBagsTotal').text('$' + upgradeLootBags.toFixed(2));


        // UPGRADE BALLOONS
        if ($('#UpgradeLatexBalloons').is(':checked')) { upgradeTotal += (guestTotal - 1); }
        $('#UpgradeLatexBalloonsTotal').text('$' + (guestTotal - 1).toFixed(2));

        return upgradeTotal;
    }

    // LOOT BAGS
    function hasOwnLootBag() {

        $("#OwnLootBagDisclaimer").hide();
        $("#LootBag").show();
        $("#AddonLootBag").show();

        if ($('#HasOwnLootBag').is(':checked')) {
            $('#LootBag').find('.package-includes').removeClass('error-remaining');
            $('#LootBag').find('.package-includes').removeClass('error-excessive');
            $('#LootBag').find('span.package-validate').empty();

            $("#OwnLootBagDisclaimer").show();
            $("#LootBag").hide();
            $("#AddonLootBag").hide();

            // Reset Loot Bag Selection
            $('#LootBag').find('input:text').val("0");
            $('#AddonLootBag').find('input:text').val("0");
            $('#UpgradeLootBags').removeAttr('checked');
        }
    }

    // DECORATIONS
    function hasOwnDecor() {

        if ($('#HasOwnDecor').is(':checked')) {
            $("#OwnDecorDisclaimer").show();
        }
    }

    // BALLOONS
    function hasOwnBalloons() {

        $("#OwnBalloonsDisclaimer").hide();
        $("#IncludedBalloons").show();

        if ($('#HasOwnBalloons').is(':checked')) {
            $("#OwnBalloonsDisclaimer").show();
            $("#IncludedBalloons").hide();
        }
    }

    // FOOD PLATTER SWAP
    function toggleFoodPlatterSwap() {

        $('#PizzaSwap').hide();
        $('#FoodPlatter').show();

        if ($('#SwapFoodPlatter').is(':checked')) {
            $('#PizzaSwap').show();
            $('#FoodPlatter').hide();

            $('#FoodPlatter').find('.calculated').each(function () {
                $(this).val("0");
            });

            return;
        };

        $('#PizzaSwap').find('.calculated').each(function () {
            $(this).val("0");
        });
    }

    // CALCULATIONS
    function getPartyTotal() {
        var total = parseFloat($('#PackageBasePrice').val());

        validateIncludedAddons();

        // Addons
        total += calculateAddons("FoodPlatter");
        total += calculateAddons("FoodPizza");
        total += calculateAddons("Beverage");
        total += calculateAddons("EdibleArrangements");
        total += calculateAddons("LootBag");
        total += calculateAddons("BalloonMylar");
        total += calculateAddons("BalloonDeluxe");
        total += calculateAddons("BalloonLatex");
        total += calculateAddons("BalloonLatexThemed");
        total += calculateAddons("Tattoo");
        total += calculateAddons("AddOns");

        // Additional
        total += getCupcakeTotal();

        // Upgrades
        total += getUpgrades();

        // Surcharges
        total += getSurcharges();

        total += (parseInt($('#AdditionalGuests').val()) * $('#AdditionalGuests').data('price'));
        total += (parseInt($('#PrepaidAdmissions').val()) * $('#PrepaidAdmissions').data('price'));

        // Promotion Code
        if ($('#Promotion').val()) {
            var adjustmentAmount = $('#PackageBasePrice').attr('data-adjustment');
            var adjustmentType = $('#PackageBasePrice').attr('data-adjustment-type');

            // Percentage
            if (adjustmentType == 1) {
                var discount = (adjustmentAmount / 100) * total;
                total = (total - discount);
            }

            // Percentage
            if (adjustmentType == 2) {
                total = (total - adjustmentAmount);
            }
        }

        // Totals
        var subTotal = parseFloat(total).toFixed(2);
        var gstTotal = (subTotal * 0.05).toFixed(2);
        var partyTotal = (subTotal * 1.05).toFixed(2);

        $('#SubTotal').text('$' + subTotal);
        $('#GstTotal').text('$' + gstTotal);
        $('#FinalTotal').text('$' + partyTotal);

        $('#PartySubTotal').val(subTotal);
        $('#PartyTaxTotal').val(gstTotal);
        $('#PartyTotal').val(partyTotal);
        console.log("SubTotal: $" + subTotal + " | Total: $" + partyTotal);
    }

    function getGuestTotal() {
        var guestTotal = (parseInt($('#PartyPackage_Guests').val()) + parseInt($('#AdditionalGuests').val()));
        if ($('#PartyPackage').val() == 4) { guestTotal = (parseInt($('#PrepaidAdmissions').val())); }

        return guestTotal;
    }

    function getGuestCapacity() {
        var guestTotal = (parseInt($('#PartyPackage_Guests').val()) + parseInt($('#AdditionalGuests').val()) + parseInt($('#PrepaidAdmissions').val()));
        return (parseInt($('#RoomCapacity').val()) - guestTotal);
    }

    function setPackageBasePrice(dayOfWeek) {

        var packageBaseRate = $('#PackageBasePrice').val();

        // PACKAGE 3 (OVERRIDE PRICE)
        if ($('#PartyPackage').val() != 3) {
            $('#PackagePrice').text("$" + packageBaseRate);
            return;
        }

        if ($('#PartyPackage').val() == 3) {

            if (dayOfWeek >= 1 && dayOfWeek <= 4) {
                $('#PackageBasePrice').val(99.99);
                packageBaseRate = $('#PackageBasePrice').val();
                $('#PackagePrice').text("$" + packageBaseRate);
                return;
            }

            if (dayOfWeek == 5) {
                $('#PackageBasePrice').val(119.99);
                packageBaseRate = $('#PackageBasePrice').val();
                $('#PackagePrice').text("$" + packageBaseRate);
                return;
            }

            if (dayOfWeek == 6 || dayOfWeek == 7) {
                $('#PackageBasePrice').val(249.99);
                packageBaseRate = $('#PackageBasePrice').val();
                $('#PackagePrice').text("$" + packageBaseRate);
                return;
            }
        }
    }

    //** PACKAGE ADDONS **//
    function validateIncludedAddons() {
        getIncludedAddons("FoodPlatter");
        getIncludedAddons("PizzaSwap");
        getIncludedAddons("Beverage");
        getIncludedAddons("BalloonMylar");
        getIncludedAddons("BalloonDeluxe");
        getIncludedAddons("BalloonLatex");
        getIncludedAddons("BalloonLatexThemed");
        getIncludedAddons("LootBag");

        setGlobalAddons();

        setIncludedAddons("FoodPlatter", true);
        setIncludedAddons("PizzaSwap", true);
        setIncludedAddons("Beverage", true);
        setIncludedAddons("BalloonMylar");
        setIncludedAddons("BalloonDeluxe");
        setIncludedAddons("BalloonLatex");
        setIncludedAddons("BalloonLatexThemed");
        setIncludedAddons("LootBag", true);
    }

    function getIncludedAddons(addonType) {

        var included = 0;
        var guests = getGuestTotal();

        /** ITERATE PACKAGE ADDONS AND SHOW INCLUDED **/
        $('#' + addonType).find('.package-includes').hide();

        $('#PackageAddons').find('li').each(function () {

            if ($(this).data('id') == addonType && $(this).data('guests') <= guests) {
                included += parseInt($(this).data('quantity'));
            }
        });

        /** OVERRIDE PIZZA SWAPS **/
        if (addonType == "PizzaSwap") {
            var platters = $('#FoodPlatter').find('.package-includes').attr('data-included');
            included = (platters * 5);
        }

        /** SET THE DATA CONTAINER VALUES **/
        var container = $('#' + addonType).find('.package-includes');
        container.find('span.package-quantity').empty();
        container.find('span.package-quantity').text(included);
        container.attr('data-included', included);
    }

    function setIncludedAddons(addonType, useValidation) {
        var container = $('#' + addonType).find('.package-includes');
        var included = container.attr('data-included');

        if (included > 0) { $('#' + addonType).find('.package-includes').show(); }
        if (!useValidation) { return; }

        var quantity = 0;
        /** ITERATE PACKAGE ADDONS AND SET REMAINING COUNT **/
        $('#' + addonType).find('.calculated').each(function () {

            if ($(this).val().match(/^\d+$/)) {

                var qty = $(this).val();
                quantity += parseInt(qty);
            }
        });

        /** VALIDATE & CALCULATE REMAINING INCLUDED ADDONS **/
        var remaining = included - quantity;
        container.find('span.package-validate').empty();
        container.removeClass("error-remaining");
        container.removeClass("error-excessive");

        if (remaining > 0) {
            $('#' + addonType).find('.package-includes').find('span.package-validate').text("* " + remaining + " Included Choice(s) Remaining. *");
            container.addClass("error-remaining");
        }

        if (remaining < 0) {
            container.find('span.package-validate').text("* Quantity Can NOT Exceed " + included + " Choice(s). *");
            container.addClass("error-excessive");
        }
    }

    function calculateAddons(addonType) {

        var total = 0.00;

        $('#Addon' + addonType).find('.calculated').each(function () {

            if ($(this).is(':checkbox') && !$(this).is(':checked')) {
                return true;
            }

            if ($(this).val().match(/^\d+$/)) {
                var quantity = ($(this).val() == "") ? $(this).val("0") : $(this).val();
                var price = $(this).data('price');

                if (quantity) {
                    total += parseInt(quantity) * parseFloat(price) || 0;
                }
                return true;
            }
        });

        $('#Addon' + addonType).find('span.additional-total').text('$' + total.toFixed(2));
        return total;
    }

    /** GLOBAL PACKAGE ADDONS **/
    function setGlobalAddons() {

        var guests = getGuestTotal();

        if ($('#PartyPackage').val() == 3) {

            // FOOD PIZZA (Pkg 3)
            $('#FoodPizza').find('.package-includes').attr('data-included', guests);
            $('#FoodPizza').find('span.package-quantity').text(guests);
            setIncludedAddons("FoodPizza", true);
            return;
        }

        // BALLOONS (ONLY PKG 1 & 2)
        $('#BalloonLatex').find('.package-includes').attr('data-included', (guests - 1));
        $('#BalloonLatex').find('span.package-quantity').text((guests - 1));

        // LOOT BAGS (ONLY PKG 1 & 2)
        $('#LootBag').find('.package-includes').attr('data-included', guests);
        $('#LootBag').find('span.package-quantity').text(guests);
    }

    $('#PartyBooking').submit(function () {

        //$.ajax({
        //    type: "POST",
        //    url: "/Party/Post",
        //    data: $('#PartyBooking').serialize(),
        //    success: function(data) {

        //    }
        //});

        //event.preventDefault();
        //return;

        var validationErrors = [];
        var validationSummary = [];
        $('#ValidationSummary').hide();
        $('#ValidationSummary ul').empty();

        /** CALCULATE PARTY TOTAL **/
        getPartyTotal();

        /** ENSURE AT LEAST ONE GUEST **/
        if (getGuestTotal() == 0) {
            validationErrors.push("You need to add at least one guest to your party.");
        }

        /** VALIDATE PARTY ROOM / TIMESLOT **/
        var timeslot = $('.timeslot:checked');
        if (!timeslot.val()) {
            validationErrors.push("Please Choose A Party Room And Available Timeslot For Your Party.");
        };

        /** VALIDATE PARENT / CHILD INFORMATION **/
        $('#CustomerInformation').find(':input').each(function () {
            if (!$.trim($(this).val()).length > 0) {
                validationErrors.push("Please Complete All Required Parent/Child Information Fields.");
            }
        });

        /** VERIFY CHILD GENDER **/
        if ($("#Child_Gender option:selected").index() == 0) {
            validationErrors.push("Please Provide Child Gender.");
        }

        /** VALIDATE EMAIL **/
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        var valid = pattern.test($('#Parent_Email').val());

        if (!valid) {
            validationErrors.push("Invalid Email Address, Please Provide A Valid Email Address.");
        } else {
            if ($('#Parent_Email').val() != $('#Parent_Email_Confirm').val()) {
                validationErrors.push("Email Address Mismatch, Please Ensure You've Confirmed Your Email Address.");
            }
        }

        /** VERIFY PROVINCE **/
        if ($("#Parent_Province option:selected").index() == 0) {
            validationErrors.push("Please Choose Your Province.");
        }

        /** OVERRIDE VALIDATION **/
        if ($('#SwapFoodPlatter').is(':checked')) {
            $('#FoodPlatter').find('.package-includes').removeClass('error-excessive');
            $('#FoodPlatter').find('.package-includes').removeClass('error-remaining');
        } else {
            $('#PizzaSwap').find('.package-includes').removeClass('error-excessive');
            $('#PizzaSwap').find('.package-includes').removeClass('error-remaining');
        };

        if ($('#HasOwnLootBag').is(':checked')) {
            $('#LootBag').find('.package-includes').removeClass('error-excessive');
            $('#LootBag').find('.package-includes').removeClass('error-remaining');
        }

        /** VALIDATE INCLUDED ADDONS QUANTITIES **/
        if ($('body').find('.error-excessive').length) {
            validationErrors.push("One Or More Included Addons Exceed Their Maximum Limit.");
        }

        /** VALIDATE INCLUDED ADDONS REMAINING **/
        if ($('body').find('.error-remaining').length) {
            validationErrors.push("One Or More Included Addons Still Have Remainging Choices.");
        }

        /** VERIFY PARTY & CAKE THEME **/
        if ($('#PartyPackage').val() != 3 && $('#PartyPackage').val() != 4 && $("#PartyTheme option:selected").index() == 0) {
            validationErrors.push("No Party Theme Has Been Choosen Yet.");
        }

        /** VERIFY PARTY & CAKE FLAVOUR **/
        if ($('#PartyPackage').val() == 2 && $("#CakeFlavour option:selected").index() == 0) {

            if (!$('#HasOwnCake').is(':checked') && !$('#HasCupcakes').is(':checked')) {
                validationErrors.push("No Cake Flavour Has Been Choosen Yet.");
            }
        }

        /** VERIFY SWAPPED CAKE / CUPCAKES FLAVOUR SELECTED **/
        if ($('#PartyPackage').val() != 3 && $('#HasCupcakes').is(':checked')) {

            if ($('.cupcake:checked').length == 0) {
                validationErrors.push("No Cupcake Flavour Has Been Choosen Yet.");
            }
        }

        /** VERIFY SWAPPED CAKE / CUPCAKES ICING SELECTED **/
        if ($('#PartyPackage').val() != 3 && $('#HasCupcakes').is(':checked')) {

            if ($('.flavours-included:checked').size() == 0) {
                validationErrors.push("No Cupcake Icing Has Been Choosen Yet.");
            }
        }

        /** GET VALIDATION SUMMARY **/
        $.each(validationErrors, function (i, e) {
            if ($.inArray(e, validationSummary) == -1) {
                $('#ValidationSummary').find('ul').append('<li>' + e + '</li>');

                console.log(e);
                validationSummary.push(e);
            }
        });

        console.log('submitted');
        if (validationErrors.length) {
            $('#ValidationSummary').show();
            event.preventDefault();
        } else {
            var $form = $('form');

            if ($form.data('submitted') === true) {
                e.preventDefault();
            } else {
                $form.data('submitted', true);
                $('#Submit').hide();
                $('#SubmitProgress').show();
            }
        }
    });
});