﻿namespace StirCrazy.Models.Configuration
{
    public class AppSettingsModel
    {
        public string CalendarAddress { get; set; }
        public string CalendarUrl { get; set; }
        public string GoogleServiceAccount { get; set; }
    }
}