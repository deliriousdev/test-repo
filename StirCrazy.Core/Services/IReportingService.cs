﻿using System;
using System.Collections.Generic;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Services
{
    public interface IReportingService
    {
        IEnumerable<AnnualPassOrder> GetAnnualPassOrders(DateTime? startDate, DateTime? endDate);
        IEnumerable<PartyOrder> GetPartyOrders(DateTime? startDate, DateTime? endDate);
        IEnumerable<RoomOrder> GetRoomOrders(DateTime? startDate, DateTime? endDate);
        IEnumerable<BakeryOrder> GetBakeryOrders(DateTime? startDate, DateTime? endDate);
        IEnumerable<GrabNGoOrder> GetGrabNGoOrders(DateTime? startDate, DateTime? endDate);
        IEnumerable<CorporateEvent> GetCorporateEvents(DateTime? startDate, DateTime? endDate);
    }
}