﻿using System.Linq;
using System.Web.Mvc;
using StirCrazy.Core.Services;

namespace StirCrazy.Controllers
{
    public class CalendarController : Controller
    {
        private readonly IOffice365Service _office365Service;

        public CalendarController()
        {
            _office365Service = new Office365Service("events@stircrazy.ca");
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Events()
        {
            var events = _office365Service.GetEvents();

             if (events.Any())
            {
                return Json(events.Select(x => new
                {
                    title = x.Subject,
                    description = x.Body,
                    period = x.StartTime.ToString("MMMMM dd, yyyy h:mm tt") + " - " + x.EndTime.ToString("MMMMM dd, yyyy h:mm tt"),
                    start = x.StartTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    end = x.EndTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    allDay = x.IsAllDay
                }), JsonRequestBehavior.AllowGet);
            }

            return new JsonResult();
        }
    }
}
