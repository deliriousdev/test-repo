﻿using System.Collections.Generic;
using StirCrazy.Core.Domain;
using Tweetinvi.Core.Interfaces;

namespace StirCrazy.Core.Services.Social
{
    public interface ISocialMediaService
    {
        List<ITweet> GetTwitterTweets();
        List<FacebookPost> GetFacebookPosts();
    }
}