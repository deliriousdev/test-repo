﻿$(document).ready(function () {

    //== JQUERY - LEGACY BINDING SUPPORT (.live) ==\\
    jQuery.fn.extend({
        live: function (types, data, fn) {
            jQuery(this.context).on(types, this.selector, data, fn);
            return this;
        }
    });

    // Add Appointment Code
    $("#addon-save").click(function () {

        var addonType = $('#addon-type').find(':selected').val();
        var addonName = $('#addon-type').find(':selected').text();

        var addonQty = $('#addon-quantity').val();
        var addonGuests = $('#addon-requiredGuests').val();
        var id = $('#addon-list').attr('data-id');

        if (addonType != "" && addonQty != "" && addonGuests != "") {

            $('#addon-list').append('<li class="list-group-item">'
                + '<input type="hidden" name="PackageAddons[' + id + '].AddonType" value="' + addonType + '"/>'
                + '<input type="hidden" name="PackageAddons[' + id + '].Quantity" value="' + addonQty + '"/>'
                + '<input type="hidden" name="PackageAddons[' + id + '].RequiredGuests" value="' + addonGuests + '"/>'
                + addonQty + ' FREE | ' + addonName + ' <button type="button" class="btn btn-default btn-sm btn-rounded addon-remove pull-right">Remove</button>'
                + '</li>');

            id++;
            $('#addon-list').attr('data-id', id);
        }
    });

    $(".addon-remove").live("click", function () {
        $(this).parent('li').remove();
        reindex();
    });

    function reindex() {
        $(".list-group-item").each(function (index) {
            var prefix = "PackageAddons[" + index + "]";
            $(this).find("input").each(function () {
                this.name = this.name.replace(/PackageAddons\[\d+\]/, prefix);
            });
        });
    }

});