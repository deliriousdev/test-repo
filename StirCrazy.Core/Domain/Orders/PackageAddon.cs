﻿using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Domain.Orders
{
    public class PackageAddon
    {
        public int Id { get; set; }
        public int PartyPackageId { get; set; }
        public int? Quantity { get; set; }
        public int RequiredGuests { get; set; }
        public AddonType AddonType { get; set; }
    }
}