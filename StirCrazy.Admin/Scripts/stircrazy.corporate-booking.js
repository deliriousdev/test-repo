﻿$(document).ready(function () {

    var now = moment();

    $('#Schedule').datetimepicker({
        datepicker: true,
        format: 'm/d/Y',
        startDate: now.format('YYYY/MM/DD'),
        minDate: now.format('YYYY/MM/DD'),
        endDate: -Infinity,
        autoClose: true,
        timepicker: false,
        validateOnBlur: true,
        inline: true,
        onSelectDate: function (dp, $input) {
            $('#Schedule').val($input.val());
            $('#ScheduleResponse').empty();
        }
    });

    $('#CheckAvailability').click(function() {

        var date = $('#Schedule').val();
        var startTime = $('#StartTime').val();
        var endTime = $('#EndTime').val();

        $('#AvailabilityStatus').html('One Moment Please...');

        $.ajax({
            type: "POST",
            url: "/CorporateEvents/Availability",
            data: { searchDate: date, startTime: startTime, endTime: endTime },
            dataType: 'json',
            success: function (data) {

                $('#ScheduleResponse').empty();

                if (data == false) {
                    $('#AvailabilityStatus').html('<i class="fa fa-times-circle-o" style="color: green;"> Date/Time Is Available!</i>');
                    event.preventDefault();
                } else {
                    $('#AvailabilityStatus').html('<i class="fa fa-check-circle" style="color: red;"> Date/Time Is Not Available!</i>');
                }
            }
        });

    });

    $('#Availability').submit(function(event) {

        var date = $('#Schedule').val();
        var startTime = $('#StartTime').val();
        var endTime = $('#EndTime').val();

        $.ajax({
            type: "POST",
            url: "/CorporateEvents/Availability",
            data: { searchDate: date, startTime: startTime, endTime: endTime },
            dataType: 'json',
            success: function (data) {

                $('#ScheduleResponse').empty();

                if (data != false) {
                    $('#ScheduleResponse').html("<h3>Unfortunately " + date + " could not reserved!</h3>There are either existing bookings already or this date is scheduled on a holiday closure, Please choose another date.");
                    event.preventDefault();
                    return false;
                }
            }
        });
    });
});