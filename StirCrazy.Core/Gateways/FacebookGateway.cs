﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;

namespace StirCrazy.Core.Gateways
{
    public class FacebookGateway : IFacebookGateway
    {
        private static readonly string FacebookPageId = ConfigurationManager.AppSettings["FacebookPageId"];

        public string GetPosts()
        {
            var requestPolicy = new HttpRequestCachePolicy(HttpCacheAgeControl.MaxAge, TimeSpan.FromHours(1));
            var urlFeed = "https://www.facebook.com/feeds/page.php?id=" + FacebookPageId + "&format=rss20";
            var request = (HttpWebRequest)WebRequest.Create(urlFeed);
            request.CachePolicy = requestPolicy;

            request.Method = "GET";
            request.UserAgent = ".NET";

            var response = (HttpWebResponse)request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream == null) return null;

            var reader = new StreamReader(stream, Encoding.UTF8);
            return reader.ReadToEnd();
        }
    }
}
