﻿using System;
using StirCrazy.Core.Domain;

namespace StirCrazy.Admin.Models
{
    public class PromotionModel
    {
        public Guid Id { get; set; }
        public PromotionType PromotionType { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Adjustment { get; set; }
        public AdjustmentType AdjustmentType { get; set; }
        public DateTime Created { get; set; }
        public DateTime Expired { get; set; }
    }
}