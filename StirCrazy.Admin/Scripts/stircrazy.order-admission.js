﻿$(document).ready(function () {

    var now = moment().add('days', 2);

    // DATE PICKER CALENDAR
    $('#PickUpDate').datetimepicker({
        datepicker: true,
        format: 'm/d/Y',
        startDate: now.format('YYYY/MM/DD'),
        minDate: now.format('YYYY/MM/DD'),
        defaultSelect: false,
        endDate: -Infinity,
        autoClose: true,
        timepicker: false,
        validateOnBlur: true,
        inline: true
    });

    $("#AdmissionOrder").data('submitted', false);
    $('#Submit').show();
    $('#SubmitProgress').hide();

    getOrderTotal();

    $("body :input[type='text']").on({
        keyup: getOrderTotal,
        focusout: getOrderTotal,
        change: getOrderTotal
    });

    //** INPUTS **//
    $('body :input[type="text"]').focusin(function () {
        $(this).val("");
    });

    $('body :input[type="text"]').focusout(function () {
        if ($(this).val() == "" || $(this).val() == null) {
            $(this).val("0");
        }
    });

    // PROMOTION CODE
    $(document).on('click', '#PromotionCodeValidate', function () {
        $('#OrderTotal').attr('data-adjustment', "0");
        $('#OrderTotal').attr('data-adjustment-type', "0");

        $.ajax({
            type: "POST",
            url: "/Home/PromotionCode",
            data: { promoCode: $('#Promotion').val(), promoType: 5 },
            dataType: 'json',
            success: function (data) {

                if (data != null) {
                    $('#PromotionDescription').empty();
                    $('#PromotionDescription').text(data.Description);

                    if ($('#OrderTotal').val() >= data.Adjustment) {
                        $('#OrderTotal').attr('data-adjustment', data.Adjustment);
                        $('#OrderTotal').attr('data-adjustment-type', data.AdjustmentType);
                        getOrderTotal();
                        return;
                    } else {
                        $('#PromotionDescription').empty();
                        $('#PromotionDescription').text("* Can Not Apply On A Negative Balance *");
                        return;
                    }
                }
            }
        });
    });

    // CALCULATIONS
    function getOrderTotal() {

        var total = 124.99;

        // Promotion Code
        if ($('#Promotion').val()) {
            var adjustmentAmount = $('#OrderTotal').attr('data-adjustment');
            var adjustmentType = $('#OrderTotal').attr('data-adjustment-type');

            // Percentage
            if (adjustmentType == 1) {
                var discount = (adjustmentAmount / 100) * total;
                total = (total - discount);
            }

            // Percentage
            if (adjustmentType == 2) {
                total = (total - adjustmentAmount);
            }
        }

        // Totals
        var subTotal = parseFloat(total).toFixed(2);
        var gstTotal = (subTotal * 0.05).toFixed(2);
        var orderTotal = (subTotal * 1.05).toFixed(2);

        $('#SubTotal').text('$' + subTotal);
        $('#GstTotal').text('$' + gstTotal);
        $('#FinalTotal').text('$' + orderTotal);

        $('#OrderSubTotal').val(subTotal);
        $('#OrderTaxTotal').val(gstTotal);
        $('#OrderTotal').val(orderTotal);
        console.log("SubTotal: $" + subTotal + " | Total: $" + orderTotal);
    }

    $('#AnnualPassOrder').submit(function () {

        var validationErrors = [];
        var validationSummary = [];

        $('#ValidationSummary').hide();
        $('#ValidationSummary ul').empty();

        /** CALCULATE TOTAL **/
        getOrderTotal();

        /** VALIDATE CUSTOMER INFORMATION **/
        $('#CustomerInformation').find(':input').each(function () {
            if (!$.trim($(this).val()).length > 0) {
                validationErrors.push("Please Complete All Required Customer Information Fields.");
            }
        });

        $('#ChildInformation').find(':input').each(function () {

            if (!$(this).val()) {
                validationErrors.push("Please Complete All Required Child Information Fields.");
            }
        });

        /** VALIDATE EMAIL **/
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        var valid = pattern.test($('#Customer_Email').val());

        if (!valid) {
            validationErrors.push("Invalid Email Address, Please Provide A Valid Email Address.");
        } else {
            if ($('#Customer_Email').val() != $('#Customer_Email_Confirm').val()) {
                validationErrors.push("Email Address Mismatch, Please Ensure You've Confirmed Your Email Address.");
            }
        }

        /** VERIFY PROVINCE **/
        if ($("#Customer_Province option:selected").index() == 0) {
            validationErrors.push("Please Choose Your Province.");
        }

        /** VALIDATE ORDER **/
        if ($('#OrderTotal').val() == "0.00") {
            validationErrors.push("Please Choose At Least One Item Before Placing Your Order.");
        }

        /** GET VALIDATION SUMMARY **/
        $.each(validationErrors, function (i, e) {
            if ($.inArray(e, validationSummary) == -1) {
                $('#ValidationSummary').find('ul').append('<li>' + e + '</li>');

                console.log(e);
                validationSummary.push(e);
            }
        });

        console.log('submitted');
        if (validationErrors.length) {
            $('#ValidationSummary').show();
            event.preventDefault();
        } else {
            var $form = $('form');

            if ($form.data('submitted') === true) {
                e.preventDefault();
            } else {
                $form.data('submitted', true);
                $('#Submit').hide();
                $('#SubmitProgress').show();
            }
        }
    });
});