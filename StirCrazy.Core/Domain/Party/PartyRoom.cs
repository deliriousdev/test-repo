﻿using System.Collections.Generic;

namespace StirCrazy.Core.Domain.Party
{
    public class PartyRoom
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }
        public bool IsAvailable { get; set; }
        public virtual ICollection<RoomBooking> Bookings { get; set; }
    }
}