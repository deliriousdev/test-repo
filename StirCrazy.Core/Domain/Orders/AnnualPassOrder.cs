﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain.Orders
{
    public class AnnualPassOrder
    {
        [Key]
        public Guid Id { get; set; }
        public Guid? PromotionCodeId { get; set; }
        public int PaymentTransactionId { get; set; }
        public DateTime PickUpDate { get; set; }
        public PickUpTime PickUpTime { get; set; }
        public string ParentId { get; set; }
        public int ChildId { get; set; }
        public string Notes { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }
        public decimal Discount { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }

        public virtual ApplicationUser Parent { get; set; }
        public virtual Child Child { get; set; }
        public virtual PromotionCode PromotionCode { get; set; }
        public virtual PaymentTransaction PaymentTransaction { get; set; }
    }
}
