﻿using System.Collections.Generic;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Services.Party
{
    public interface IPartyRoomService
    {
        List<PartyRoom> Get();
        PartyRoom Get(int id);
        PartyRoom Add(PartyRoom partyRoom);
        PartyRoom Update(PartyRoom partyRoom);
        void Remove(PartyRoom partyRoom);
    }
}