﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using PayPal.Api.Payments;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Party;
using StirCrazy.Models.Addons;
using StirCrazy.Models.Gallery;
using StirCrazy.Models.Order;
using StirCrazy.Models.Party;

namespace StirCrazy.Controllers
{
    public class PartyController : AsyncController
    {
        private readonly IMediaService _mediaService;
        private readonly IPartyAddonService _partyAddonService;
        private readonly IPartyPackageService _partyPackageService;
        private readonly IRoomBookingService _roomBookingService;
        private readonly IPartyOrderService _partyOrderService;
        private readonly IPromotionCodeService _promotionCodeService;
        private readonly IPayPalService _payPalService;

        public PartyController(IPartyAddonService partyAddonService,
            IPartyPackageService partyPackageService,
            IRoomBookingService roomBookingService,
            IPartyOrderService partyOrderService,
            IMediaService mediaService,
            IPayPalService payPalService,
            IPromotionCodeService promotionCodeService)
        {
            _partyAddonService = partyAddonService;
            _partyPackageService = partyPackageService;
            _roomBookingService = roomBookingService;
            _partyOrderService = partyOrderService;
            _mediaService = mediaService;
            _payPalService = payPalService;
            _promotionCodeService = promotionCodeService;
        }

        public ActionResult Packages()
        {
            return View();
        }

        public ActionResult Themes()
        {
            var model = new PartyThemeModel
            {
                GalleryImages = _mediaService.Get(UploadFolder.Cakes),
                PartyThemes = _partyAddonService.GetAddons(AddonType.ThemeParty)
            };

            return View(model);
        }

        public ActionResult Addons()
        {
            var model = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
            {
                Id = x.Id,
                AddonType = x.AddonType,
                Name = x.Name,
                Price = x.Price
            }).ToList();

            return View(model);
        }

        public ActionResult Availability(DateTime searchDate)
        {
            var result = _roomBookingService.GetAvailability(searchDate);
            return (!result.Any()) ? null : PartialView("_AvailabilityPartial", result);
        }

        public ActionResult Create(int id = 0)
        {
            var package = _partyPackageService.Get(id);
            if (package == null) return RedirectToAction("Packages");

            var addons = _partyAddonService.GetAddons();

            var model = new PartyOrderModel
            {
                Addons = addons.Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                PartyAddons = new List<PartyAddonModel>(),
                PartyPackage = new PartyPackageModel
                {
                    Id = package.Id,
                    Name = package.Name,
                    Description = package.Description,
                    Price = package.Price,
                    MaxGuests = package.MaxGuests,
                    Guests = package.Guests,
                    GuestPrice = package.GuestPrice,
                    HasCake = package.HasCake,
                    HasPlaceSetting = package.HasPlaceSetting
                },
                PackageAddons = package.PackageAddons.Select(x => new PackageAddonModel
                {
                    AddonType = x.AddonType,
                    Id = x.Id,
                    PackageId = x.PartyPackageId,
                    Quantity = x.Quantity,
                    RequiredGuests = x.RequiredGuests
                }).ToList(),
                AddonGroups = new List<AddonGroupModel>()
            };

            if (model.Addons.Any(x => x.AddonType == AddonType.EdibleArrangement))
            {
                var edibleAddOns = model.Addons.OrderBy(x => x.Id).Where(x => x.AddonType == AddonType.EdibleArrangement);
                var groupCount = 0;

                foreach (var addon in edibleAddOns)
                {
                    if (addon.AddonType == AddonType.EdibleArrangement)
                    {

                        if (addon.Name.Contains("(Small)"))
                        {
                            addon.Size = "Small";
                        }
                        else if (addon.Name.Contains("(Regular)"))
                        {
                            addon.Size = "Regular";
                        }
                        else if (addon.Name.Contains("(Large)"))
                        {
                            addon.Size = "Large";
                        }

                        var addonNoSuffix = addon.Name.Replace(" (Small)", "").Replace(" (Large)", "").Replace(" (Regular)", "");

                        var group = model.AddonGroups.FirstOrDefault(s => s.Name == addonNoSuffix);

                        if (group == null)
                        {
                            groupCount++;
                            group = new AddonGroupModel() { Name = addonNoSuffix, Id = groupCount };
                            model.AddonGroups.Add(group);
                        }

                        group.Addons.Add(addon);
                    }
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Post(PartyOrderModel model)
        {
            var test = model;
            test.HasOwnDecor = false;
            return null;
        }

        [HttpPost]
        public async Task<RedirectResult> Create(PartyOrderModel model)
        {
            var partyOrder = new PartyOrder
            {
                AdditionalGuests = model.AdditionalGuests,
                PrepaidAdmissions = model.PrepaidAdmissions,
                PartyTheme = model.PartyTheme,
                CakeTheme = model.CakeTheme ?? model.PartyTheme,
                CakeFlavour = model.CakeFlavour,
                AddCake = model.AddCake,
                HasOwnCake = model.HasOwnCake,
                HasOwnDecor = model.HasOwnDecor,
                HasCupcakes = model.HasCupcakes,
                AddPlaceSetting = model.AddPlaceSetting,
                HasOwnPlaceSetting = model.HasOwnPlaceSetting,
                HasOwnBalloons = model.HasOwnBalloons,
                HasOwnLootBag = model.HasOwnLootBag,
                SwapFoodPlatter = model.SwapFoodPlatter,
                UpgradeLootBags = model.UpgradeLootBags,
                UpgradeLatexBalloons = model.UpgradeLatexBalloons,
                Parent = model.Parent,
                Child = model.Child,
                PartyPackageId = model.PartyPackage.Id,
                RoomBooking = new RoomBooking
                {
                    Date = model.RoomBooking.Date,
                    BookingType = BookingType.PartyOrder,
                    PartyRoomId = model.RoomBooking.PartyRoomId,
                    TimeSlotId = model.RoomBooking.TimeSlotId,
                    Status = Status.Pending
                },
                PartyAddons = new List<PartyOrderAddon>(),
                PaymentTransaction = new PaymentTransaction()
                {
                    Amount = model.Total,
                    PaymentDate = DateTime.Now
                },
                Notes = model.Notes,
                SubTotal = model.SubTotal,
                Tax = model.Tax,
                Total = model.Total,
                Status = OrderStatus.Created
            };

            if (model.Promotion != null)
            {
                var promoCode = _promotionCodeService.Get(model.Promotion);

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Percentage)
                {
                    partyOrder.Discount = (promoCode.Adjustment / 100) * partyOrder.Total;
                    partyOrder.PromotionCode = promoCode;
                }

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Discount)
                {
                    partyOrder.Discount = promoCode.Adjustment;
                    partyOrder.PromotionCode = promoCode;
                }
            }

            // Reset Cake Settings If Cupcakes Or Own Cake
            if (partyOrder.HasOwnCake || partyOrder.HasCupcakes)
            {
                partyOrder.CakeFlavour = "";
                partyOrder.CakeTheme = "";
            }

            if (partyOrder.PartyTheme == "" && partyOrder.PartyPackage.Id == 3)
            {
                partyOrder.PartyTheme = "No Theme Choosen";
            }

            partyOrder.Parent.UserName = model.Parent.Email;

            foreach (var partyAddon in model.PartyAddons)
            {
                if (partyAddon.IsSelected || partyAddon.Quantity > 0)
                {
                    partyOrder.PartyAddons.Add(new PartyOrderAddon
                    {
                        AddonId = partyAddon.Id,
                        Quantity = partyAddon.Quantity,
                        IsIncluded = partyAddon.IsIncluded,
                    });
                }
            }



            // BYPASS PAYPAL PAYMENT PROCESSING
            if (model.BypassPaypal)
            {
                partyOrder.PaymentTransaction.TransactionType = model.PaymentTransaction.TransactionType;
                partyOrder.PaymentTransaction.TransactionNumber = model.PaymentTransaction.TransactionNumber ?? "Not Applicable";
                partyOrder.PaymentTransaction.PaymentStatus = "Approved";

                // SAVE DATABASE
                partyOrder.CreatedBy = "Administrator";
                partyOrder.Status = OrderStatus.Paid;
                partyOrder.RoomBooking.Status = Status.Scheduled;

                var order = await _partyOrderService.CreateAsync(partyOrder);

                // SEND NOTIFICATION
                try { await _partyOrderService.SendNotificationAsync(partyOrder); }
                catch (Exception ex) { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                var redirectUrl = string.Format("{0}://{1}/Party/Email/{2}", Request.Url.Scheme, Request.Url.Authority, order.Id);
                return new RedirectResult(redirectUrl);
            }

            // SAVE DATABASE
            partyOrder.CreatedBy = "Customer";
            await _partyOrderService.CreateAsync(partyOrder);

            // PAYPAL PAYMENT PROCESSING
            var package = _partyPackageService.Get(partyOrder.PartyPackageId).Name;
            var description = string.Format("{0} - {1} {2}", "Stir Crazy", package, "Party Booking");

            // URL ROUTES
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Party/";
            var returnUrl = baseUri + "Payment/" + partyOrder.Id;
            var cancelUrl = baseUri + "Cancelled/" + partyOrder.Id;

            return await ProcessPayment(partyOrder.Id, (double)partyOrder.Total, description, returnUrl, cancelUrl);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();
            var package = _partyPackageService.Get(partyOrder.PartyPackageId);

            var partyAddons = partyOrder.PartyAddons.Select(o => new PartyAddonModel()
            {
                Id = o.Id,
                AddonType = o.Addon.AddonType,
                Name = o.Addon.Name,
                Price = o.Addon.Price,
                Quantity = o.Quantity,
                IsIncluded = o.IsIncluded
            }).ToList();

            var model = new PartyOrderModel()
            {
                Id = partyOrder.Id,
                AdditionalGuests = partyOrder.AdditionalGuests,
                PrepaidAdmissions = partyOrder.PrepaidAdmissions,
                PartyTheme = (partyOrder.PartyTheme != null)
                    ? string.Format("{0} THEME", partyOrder.PartyTheme.ToUpper()) : "NO THEME",
                AddCake = partyOrder.AddCake,
                CakeTheme = partyOrder.CakeTheme,
                CakeFlavour = partyOrder.CakeFlavour,
                HasOwnCake = partyOrder.HasOwnCake,
                HasOwnDecor = partyOrder.HasOwnDecor,
                HasCupcakes = partyOrder.HasCupcakes,
                SwapFoodPlatter = partyOrder.SwapFoodPlatter,
                UpgradeLootBags = partyOrder.UpgradeLootBags,
                AddPlaceSetting = partyOrder.AddPlaceSetting,
                HasOwnPlaceSetting = partyOrder.HasOwnPlaceSetting,
                HasOwnBalloons = partyOrder.HasOwnBalloons,
                HasOwnLootBag = partyOrder.HasOwnLootBag,
                UpgradeLatexBalloons = partyOrder.UpgradeLatexBalloons,
                Parent = partyOrder.Parent,
                Child = partyOrder.Child,
                PartyPackageId = partyOrder.PartyPackage.Id,
                PartyPackage = new PartyPackageModel()
                {
                    Id = partyOrder.PartyPackage.Id,
                    Name = partyOrder.PartyPackage.Name,
                    Description = partyOrder.PartyPackage.Description,
                    Guests = partyOrder.PartyPackage.Guests,
                    GuestPrice = partyOrder.PartyPackage.GuestPrice,
                    Price = partyOrder.PartyPackage.Price,
                    MaxGuests = partyOrder.PartyPackage.MaxGuests,
                    HasCake = partyOrder.PartyPackage.HasCake,
                    PackageAddons = partyOrder.PartyPackage.PackageAddons
                },
                PackageAddons = package.PackageAddons.Select(x => new PackageAddonModel
                {
                    AddonType = x.AddonType,
                    Id = x.Id,
                    PackageId = x.PartyPackageId,
                    Quantity = x.Quantity,
                    RequiredGuests = x.RequiredGuests
                }).ToList(),
                RoomBooking = new RoomBookingModel
                {
                    Date = partyOrder.RoomBooking.Date,
                    PartyRoomId = partyOrder.RoomBooking.PartyRoomId,
                    PartyRoom = partyOrder.RoomBooking.PartyRoom,
                    TimeSlotId = partyOrder.RoomBooking.TimeSlotId,
                    TimeSlot = partyOrder.RoomBooking.TimeSlot
                },
                Promotion = (partyOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", partyOrder.PromotionCode.Code, partyOrder.PromotionCode.Description) : "NONE",
                PaymentTransaction = partyOrder.PaymentTransaction,
                PartyAddons = partyAddons,
                Notes = partyOrder.Notes,
                Discount = partyOrder.Discount,
                SubTotal = partyOrder.SubTotal,
                Tax = partyOrder.Tax,
                Total = partyOrder.Total,
                Status = partyOrder.Status
            };

            if (partyOrder.PartyPackage.Id == 3)
            {
                decimal price;

                switch (partyOrder.RoomBooking.Date.DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        price = 119.99m;
                        break;
                    case DayOfWeek.Sunday:
                    case DayOfWeek.Saturday:
                        price = 249.99m;
                        break;
                    default:
                        price = partyOrder.PartyPackage.Price;
                        break;
                }

                model.PartyPackage.Price = Convert.ToDecimal(price);
            }

            return View(model);
        }

        public async Task<ActionResult> Print(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            var package = _partyPackageService.Get(partyOrder.PartyPackageId);

            var partyAddons = partyOrder.PartyAddons.Select(o => new PartyAddonModel()
            {
                Id = o.Id,
                AddonType = o.Addon.AddonType,
                Name = o.Addon.Name,
                Price = o.Addon.Price,
                Quantity = o.Quantity,
                IsIncluded = o.IsIncluded
            }).ToList();

            var model = new PartyOrderModel()
            {
                Id = partyOrder.Id,
                AdditionalGuests = partyOrder.AdditionalGuests,
                PrepaidAdmissions = partyOrder.PrepaidAdmissions,
                PartyTheme = (partyOrder.PartyTheme != null)
                    ? string.Format("{0} THEME", partyOrder.PartyTheme.ToUpper()) : "NO THEME",
                AddCake = partyOrder.AddCake,
                CakeTheme = partyOrder.CakeTheme,
                CakeFlavour = partyOrder.CakeFlavour,
                HasOwnCake = partyOrder.HasOwnCake,
                HasOwnDecor = partyOrder.HasOwnDecor,
                HasCupcakes = partyOrder.HasCupcakes,
                SwapFoodPlatter = partyOrder.SwapFoodPlatter,
                UpgradeLootBags = partyOrder.UpgradeLootBags,
                AddPlaceSetting = partyOrder.AddPlaceSetting,
                HasOwnPlaceSetting = partyOrder.HasOwnPlaceSetting,
                HasOwnBalloons = partyOrder.HasOwnBalloons,
                HasOwnLootBag = partyOrder.HasOwnLootBag,
                UpgradeLatexBalloons = partyOrder.UpgradeLatexBalloons,
                Parent = partyOrder.Parent,
                Child = partyOrder.Child,
                PartyPackageId = partyOrder.PartyPackage.Id,
                PartyPackage = new PartyPackageModel()
                {
                    Id = partyOrder.PartyPackage.Id,
                    Name = partyOrder.PartyPackage.Name,
                    Description = partyOrder.PartyPackage.Description,
                    Guests = partyOrder.PartyPackage.Guests,
                    GuestPrice = partyOrder.PartyPackage.GuestPrice,
                    Price = partyOrder.PartyPackage.Price,
                    MaxGuests = partyOrder.PartyPackage.MaxGuests,
                    HasCake = partyOrder.PartyPackage.HasCake,
                    PackageAddons = partyOrder.PartyPackage.PackageAddons
                },
                PackageAddons = package.PackageAddons.Select(x => new PackageAddonModel
                {
                    AddonType = x.AddonType,
                    Id = x.Id,
                    PackageId = x.PartyPackageId,
                    Quantity = x.Quantity,
                    RequiredGuests = x.RequiredGuests
                }).ToList(),
                RoomBooking = new RoomBookingModel
                {
                    Date = partyOrder.RoomBooking.Date,
                    PartyRoomId = partyOrder.RoomBooking.PartyRoomId,
                    PartyRoom = partyOrder.RoomBooking.PartyRoom,
                    TimeSlotId = partyOrder.RoomBooking.TimeSlotId,
                    TimeSlot = partyOrder.RoomBooking.TimeSlot
                },
                Promotion = (partyOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", partyOrder.PromotionCode.Code, partyOrder.PromotionCode.Description) : "NONE",
                PaymentTransaction = partyOrder.PaymentTransaction,
                PartyAddons = partyAddons,
                Notes = partyOrder.Notes,
                Discount = partyOrder.Discount,
                SubTotal = partyOrder.SubTotal,
                Tax = partyOrder.Tax,
                Total = partyOrder.Total,
                Status = partyOrder.Status
            };

            if (partyOrder.PartyPackage.Id == 3)
            {
                decimal price;

                switch (partyOrder.RoomBooking.Date.DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        price = 119.99m;
                        break;
                    case DayOfWeek.Sunday:
                    case DayOfWeek.Saturday:
                        price = 249.99m;
                        break;
                    default:
                        price = partyOrder.PartyPackage.Price;
                        break;
                }

                model.PartyPackage.Price = price;
            }

            return View(model);
        }

        private async Task<RedirectResult> ProcessPayment(Guid id, double amount, string description, string returnUrl, string cancelUrl)
        {
            if (Request.Url != null)
            {
                var payment = _payPalService.CreatePayment(amount, description, returnUrl, cancelUrl);

                if (payment != null)
                {
                    var paymentId = payment.id;
                    var paymentState = payment.state;

                    var partyOrder = await _partyOrderService.GetAsync(id);

                    if (partyOrder != null)
                    {
                        partyOrder.Id = id;
                        partyOrder.PaymentTransaction.Id = partyOrder.PaymentTransaction.Id;
                        partyOrder.PaymentTransaction.PaymentDate = DateTime.Now;
                        partyOrder.PaymentTransaction.PaymentStatus = paymentState;
                        partyOrder.PaymentTransaction.TransactionNumber = paymentId;
                        partyOrder.PaymentTransaction.Amount = (decimal)amount;

                        await _partyOrderService.UpdateAsync(partyOrder);
                    }

                    var redirectUrl = string.Format("{0}{1}", GetApprovalUrl(payment), "&useraction=commit");
                    return new RedirectResult(redirectUrl);
                }
            }

            return null;
        }

        public async Task<ActionResult> Payment(Guid id)
        {
            var payer = Request["PayerID"];
            var partyOrder = await _partyOrderService.GetAsync(id);

            var payment = new Payment
            {
                id = partyOrder.PaymentTransaction.TransactionNumber,
                state = partyOrder.PaymentTransaction.PaymentStatus
            };

            var result = _payPalService.ExecutePayment(payment, payer);
            if (result.state == "approved")
            {
                partyOrder.Id = id;
                partyOrder.RoomBooking.Status = Status.Scheduled;
                partyOrder.Status = OrderStatus.Paid;
            }

            partyOrder.PaymentTransaction.PaymentStatus = result.state;

            var bookings = _roomBookingService.Get();

            if (bookings.Any(
                    x =>
                        x.Date == partyOrder.RoomBooking.Date && x.TimeSlotId == partyOrder.RoomBooking.TimeSlotId &&
                        x.PartyRoomId == partyOrder.RoomBooking.PartyRoomId && x.Status == Status.Scheduled))
            {
                _payPalService.CreateRefund(partyOrder.PaymentTransactionId, "Party Order Refund");
                return RedirectToAction("Error");
            }

            await _partyOrderService.UpdateAsync(partyOrder);
            await _partyOrderService.SendNotificationAsync(partyOrder);

            return RedirectToAction("Email", new RouteValueDictionary { { "Id", id } });
        }

        public async Task<ActionResult> Invites(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);

            if (partyOrder != null)
            {
                var model = new PartyCardModel()
                {
                    Parent = partyOrder.Parent,
                    Child = partyOrder.Child,
                    RoomBooking = new RoomBookingModel
                    {
                        Date = partyOrder.RoomBooking.Date,
                        PartyRoomId = partyOrder.RoomBooking.PartyRoomId,
                        PartyRoom = partyOrder.RoomBooking.PartyRoom,
                        TimeSlotId = partyOrder.RoomBooking.TimeSlotId,
                        TimeSlot = partyOrder.RoomBooking.TimeSlot
                    }
                };

                return View(model);
            }

            return HttpNotFound();
        }

        public async Task<ActionResult> ThankYou(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            var model = new PartyCardModel()
            {
                Parent = partyOrder.Parent,
                Child = partyOrder.Child,
                RoomBooking = new RoomBookingModel
                {
                    Date = partyOrder.RoomBooking.Date,
                    PartyRoomId = partyOrder.RoomBooking.PartyRoomId,
                    PartyRoom = partyOrder.RoomBooking.PartyRoom,
                    TimeSlotId = partyOrder.RoomBooking.TimeSlotId,
                    TimeSlot = partyOrder.RoomBooking.TimeSlot
                }
            };

            return View(model);
        }

        public ActionResult Map()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Cancelled(Guid id)
        {
            _partyOrderService.RemoveAsync(id);
            return RedirectToAction("Packages");
        }

        [AllowAnonymous]
        public ActionResult Save(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Party/";
            var detailsUrl = baseUri + "Print/" + id;

            var bytesAsPdf = _partyOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any()) return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Email(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Party/";

            var partyOrder = await _partyOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            await _partyOrderService.SendEmailAsync(baseUri, id, partyOrder.Parent.Email);
            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        private string GetApprovalUrl(Payment payment)
        {
            List<Links> links = payment.links;
            return (from lnk in links where lnk.rel.ToLower().Equals("approval_url") select Server.UrlDecode(lnk.href)).FirstOrDefault();
        }
    }
}