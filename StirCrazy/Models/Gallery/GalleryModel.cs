﻿using System.Collections.Generic;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Models.Gallery
{
    public class GalleryModel
    {
        public IEnumerable<MediaFile> GalleryImages { get; set; }
    }

    public class PartyThemeModel
    {
        public IEnumerable<MediaFile> GalleryImages { get; set; }
        public IEnumerable<Addon> PartyThemes { get; set; }
    }
}