﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StirCrazy.Admin.Models;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Services;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class AnnouncementController : Controller
    {
        private readonly IAnnouncementService _announcementService;
        private readonly IMediaService _mediaService;


        public AnnouncementController(IAnnouncementService announcementService, IMediaService mediaService)
        {
            _announcementService = announcementService;
            _mediaService = mediaService;
        }

        public ActionResult Index()
        {
            var model = _announcementService.Get().Select(x => new AnnouncementModel()
            {
                Id = x.Id,
                Message = x.Message,
                MediaFileId = x.MediaFileId,
                MediaFile = x.MediaFile,
                StartDate = x.StartDate.ToString(),
                EndDate = x.EndDate.ToString(),
                IsPopup = x.IsPopup,
                IsImage = x.IsImage
            });

            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase announcementImage, AnnouncementModel model)
        {
            if (ModelState.IsValid)
            {
                var startDate = Convert.ToDateTime(model.StartDate);
                var endDate = Convert.ToDateTime(model.EndDate);

                if (startDate.Date < DateTime.Now.Date) { startDate = DateTime.Now; }
                if (endDate.Date < DateTime.Now.Date) { endDate = DateTime.Now; }

                var announcement = new Announcement()
                {
                    Message = model.Message,
                    StartDate = startDate,
                    EndDate = endDate,
                    IsPopup = model.IsPopup,
                };

                if (announcementImage != null)
                {
                    var mediaFile = _mediaService.Save(announcementImage, Server.MapPath("~/"), UploadFolder.Announcement);
                    announcement.MediaFileId = mediaFile;
                    announcement.IsImage = true;
                    announcement.IsPopup = true;
                }

                _announcementService.Add(announcement);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var announcement = _announcementService.Get(id);
            if (announcement == null) { return HttpNotFound(); }

            var model = new AnnouncementModel()
            {
                Id = announcement.Id,
                Message = announcement.Message,
                StartDate = announcement.StartDate.ToShortDateString(),
                EndDate = announcement.EndDate.ToShortDateString(),
                IsPopup = announcement.IsPopup
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AnnouncementModel model)
        {
            if (ModelState.IsValid)
            {
                var announcement = new Announcement()
                {
                    Id = model.Id,
                    Message = model.Message,
                    StartDate = Convert.ToDateTime(model.StartDate),
                    EndDate = Convert.ToDateTime(model.EndDate),
                    IsPopup = model.IsPopup
                };

                _announcementService.Update(announcement);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Details(int id = 0)
        {
            var announcement = _announcementService.Get(id);
            if (announcement == null) { return HttpNotFound(); }

            var model = new AnnouncementModel()
            {
                Id = announcement.Id,
                Message = announcement.Message,
                StartDate = announcement.StartDate.ToString(),
                EndDate = announcement.EndDate.ToString(),
                IsPopup = announcement.IsPopup
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            _announcementService.Remove(id);
            return null;
        }
    }
}
