﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services;

namespace Tests.Unit.StirCrazy.StirCrazy.Core.Services
{
    [TestFixture]
    public class AppSettingServiceTest
    {
        private Mock<IRepository<AppSetting>> _appSettingRepositoryMock;
        private IAppSettingService _appSettingService;

        private Guid _id;

        [SetUp]
        public void SetUp()
        {
            _appSettingRepositoryMock = new Mock<IRepository<AppSetting>>();
            _appSettingService = new AppSettingService(_appSettingRepositoryMock.Object);
            _id = new Guid();
        }

        [Test]
        public void GetShouldReturnListWhenPopulated()
        {
            var appSettingList = new List<AppSetting>();
            
            var appSetting = new AppSetting
            {
                Id = _id,
            };

            appSettingList.Add(appSetting);
            _appSettingRepositoryMock.Setup(x => x.Get()).Returns(appSettingList.AsQueryable());

            var result = _appSettingService.Get();

            _appSettingRepositoryMock.Verify(x => x.Get());
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetShouldReturnAppSettingWhenProvidedWithId()
        {
            var appSetting = new AppSetting
            {
                Id = _id,
            };

            _appSettingRepositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(appSetting);
            var result = _appSettingService.Get(_id);

            _appSettingRepositoryMock.Verify(x => x.Get(It.IsAny<int>()), Times.Never);
        }

        [Test]
        public void GetShouldReturnNullWhenProvidedWithNullId()
        {
            var appSettings = new List<AppSetting>();

            var appSetting = new AppSetting
            {
                Id = _id,
            };

            appSettings.Add(appSetting);

            var result = _appSettingService.Get(new Guid());

            Assert.IsNull(result);
        }

        [Test, Ignore("Broken")]
        public void ShouldUpdateAppSetting()
        {
            var appSetting = new AppSetting
            {
                Id = _id,
                Meta = "New"
            };

            var updatedSetting = new AppSetting()
            {
                Id = _id,
                Meta = "Updated"
            };

            _appSettingRepositoryMock.Setup(x => x.Get(_id)).Returns(appSetting);
            _appSettingRepositoryMock.Setup(x => x.Update(updatedSetting, updatedSetting.Id));
            _appSettingService.Update(appSetting);

            _appSettingRepositoryMock.Verify(x => x.Update(It.IsAny<AppSetting>(), It.IsAny<int>()));
        }
    }
}
