﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain.Orders
{
    public class GrabNGoOrder
    {
        [Key]
        public Guid Id { get; set; }
        public Guid? PromotionCodeId { get; set; }
        public int PaymentTransactionId { get; set; }
        public string CustomerId { get; set; }
        public DateTime PickUpDate { get; set; }
        public PickUpTime PickUpTime { get; set; }
        public string Notes { get; set; }
        public decimal Discount { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string Promotion { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }

        public virtual ApplicationUser Customer { get; set; }
        public virtual List<GrabNGoOrderAddon> OrderAddons { get; set; }
        public virtual PaymentTransaction PaymentTransaction { get; set; }
        public virtual PromotionCode PromotionCode { get; set; }
    }

    public enum PickUpTime
    {
        [Display(Name = "11:00 AM")]
        AM11 = 1,
        [Display(Name = "12:00 PM")]
        PM12 = 2,
        [Display(Name = "1:00 PM")]
        PM01 = 3,
        [Display(Name = "2:00 PM")]
        PM02 = 4,
        [Display(Name = "3:00 PM")]
        PM03 = 5,
        [Display(Name = "4:00 PM")]
        PM04 = 6,
        [Display(Name = "5:00 PM")]
        PM05 = 7
    }
}
