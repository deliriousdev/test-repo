﻿using System;
using System.Collections.Generic;
using StirCrazy.Core.Domain;

namespace StirCrazy.Core.Services
{
    public interface IAppSettingService
    {
        IEnumerable<AppSetting> Get();
        AppSetting Get(Guid id);
        IEnumerable<AppSetting> Get(MetaType metaType);
        AppSetting Get(string meta);
        AppSetting Add(AppSetting appSetting);
        AppSetting Update(AppSetting appSetting);
        void Clear(MetaType type);
        void Remove(Guid id);
    }
}