﻿using System;
using System.Collections.Generic;
using System.Linq;
using StirCrazy.Core.Domain.Calendar;
using StirCrazy.Core.Gateways;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services
{
    public class Office365Service : IOffice365Service
    {
        private readonly IOffice365Gateway _office365Gateway;

        public Office365Service(IOffice365Gateway office365Gateway)
        {
            _office365Gateway = office365Gateway;
        }

        public Office365Service(string sharedMailbox)
        {
            _office365Gateway = new Office365Gateway(sharedMailbox);
        }

        public List<CalendarItem> GetEvents()
        {
            return _office365Gateway.GetAppointments(DateTime.Now.AddMonths(-3), DateTime.Now.AddYears(1));
        }

        public List<CalendarItem> GetEvents(DateTime startDateTime, DateTime endDateTime)
        {
            return _office365Gateway.GetAppointments(startDateTime, endDateTime);
        }

        public string CreateEvent(DateTime? start, DateTime? end, string summary, string description, bool isAllDay)
        {
            var Event = new CalendarItem()
            {
                Body = description,
                Subject = summary,
                StartTime = start ?? DateTime.Now,
                EndTime = end ?? DateTime.Now,
                IsAllDay = isAllDay
            };

            return _office365Gateway.CreateAppointment(Event);
        }

        public void UpdateEvent(string id, DateTime? start, DateTime? end, string summary, string description, bool isAllDay)
        {
            var Event = new CalendarItem()
            {
                ItemId = id,
                Body = description,
                Subject = summary,
                StartTime = start ?? DateTime.Now,
                EndTime = end ?? DateTime.Now,
                IsAllDay = isAllDay
            };

            _office365Gateway.UpdateAppointment(Event);
        }

        public void DeleteEvent(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                _office365Gateway.DeleteAppointment(id);
            }
        }
    }
}