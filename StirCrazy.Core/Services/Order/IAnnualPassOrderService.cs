using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Services.Order
{
    public interface IAnnualPassOrderService
    {
        IEnumerable<AnnualPassOrder> Get();
        Task<AnnualPassOrder> GetAsync(Guid id);
        IEnumerable<AnnualPassOrder> GetByStatus(OrderStatus status);
        Task<AnnualPassOrder> CreateAsync(AnnualPassOrder annualPassOrder);
        Task UpdateAsync(AnnualPassOrder annualPassOrder);
        Task SendEmailAsync(string baseUri, Guid id, string email);
        Task UpdateStatusAsync(Guid id, OrderStatus status);
        Task SendOrderNotificationAsync(AnnualPassOrder annualPassOrder);
        Task CancelAsync(Guid id);
        Task RemoveAsync(Guid id);
        byte[] GetPdfBytes(string url);
    }
}