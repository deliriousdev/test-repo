﻿using System.Collections.Generic;
using StirCrazy.Core.Domain;

namespace StirCrazy.Models.Facility
{
    public class FacilityModel
    {
        public List<MediaFile> PartyRooms { get; set; }
        public List<MediaFile> Facility { get; set; }
        public List<MediaFile> ParentTotLounge { get; set; }
        public List<MediaFile> ConcessionCounter { get; set; }
    }
}