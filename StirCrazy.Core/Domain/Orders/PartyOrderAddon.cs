using System;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Domain.Orders
{
    public class PartyOrderAddon
    {
        public int Id { get; set; }
        public Guid PartyOrderId { get; set; }
        public int AddonId { get; set; }
        public int Quantity { get; set; }
        public bool IsIncluded { get; set; }
        public virtual Addon Addon { get; set; }
    }
}