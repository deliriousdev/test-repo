﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Core.Services.Order
{
    public class AnnualPassOrderService : IAnnualPassOrderService
    {
        private readonly IRepository<AnnualPassOrder> _annualPassOrderRepository;
        private readonly ISendGridService _sendGridService;
        private readonly IAccountService _accountService;

        public AnnualPassOrderService(IRepository<AnnualPassOrder> annualPassOrderRepository, ISendGridService sendGridService, IAccountService accountService)
        {
            _annualPassOrderRepository = annualPassOrderRepository;
            _sendGridService = sendGridService;
            _accountService = accountService;
        }

        public IEnumerable<AnnualPassOrder> Get()
        {
            return _annualPassOrderRepository.Get();
        }

        public async Task<AnnualPassOrder> GetAsync(Guid id)
        {
            return await _annualPassOrderRepository.GetAsync(id);
        }

        public IEnumerable<AnnualPassOrder> GetByStatus(OrderStatus status)
        {
            if (status == OrderStatus.Created)
                return _annualPassOrderRepository.Get().Where(x => x.PickUpDate.Date >= DateTime.Today);

            if (status == OrderStatus.Cancelled)
                return _annualPassOrderRepository.Get().Where(x => x.Status == OrderStatus.Cancelled);

            return _annualPassOrderRepository.Get();
        }

        public async Task<AnnualPassOrder> CreateAsync(AnnualPassOrder annualPassOrder)
        {
            var account = await _accountService.FindByEmailAsync(annualPassOrder.Parent.Email);

            // Format Data
            annualPassOrder.Parent.UserName = annualPassOrder.Parent.Email.ToLower();
            annualPassOrder.Parent.FirstName = HtmlHelperExtensions.ToTitleCase(annualPassOrder.Parent.FirstName);
            annualPassOrder.Parent.LastName = HtmlHelperExtensions.ToTitleCase(annualPassOrder.Parent.LastName);
            annualPassOrder.Parent.Address = HtmlHelperExtensions.ToTitleCase(annualPassOrder.Parent.Address);
            annualPassOrder.Parent.City = HtmlHelperExtensions.ToTitleCase(annualPassOrder.Parent.City);
            annualPassOrder.Parent.PostalCode = annualPassOrder.Parent.PostalCode.ToUpper();
            annualPassOrder.Parent.Province = annualPassOrder.Parent.Province;
            annualPassOrder.Parent.PhoneNumber = HtmlHelperExtensions.PhoneNumber(annualPassOrder.Parent.PhoneNumber);
            annualPassOrder.Parent.Email = annualPassOrder.Parent.Email.ToLower();

            if (account != null)
            {
                // Update Account Information
                account.UserName = annualPassOrder.Parent.Email;
                account.FirstName = annualPassOrder.Parent.FirstName;
                account.LastName = annualPassOrder.Parent.LastName;
                account.Address = annualPassOrder.Parent.Address;
                account.City = annualPassOrder.Parent.City;
                account.PostalCode = annualPassOrder.Parent.PostalCode;
                account.Province = annualPassOrder.Parent.Province;
                account.PhoneNumber = annualPassOrder.Parent.PhoneNumber;
                account.Email = annualPassOrder.Parent.Email;

                annualPassOrder.ParentId = account.Id;
                annualPassOrder.Parent = null;

                await _accountService.UpdateAsync(account);
            }

            annualPassOrder.DateCreated = DateTime.Now;
            annualPassOrder.CreatedBy = annualPassOrder.CreatedBy ?? "Customer";

            var order = await _annualPassOrderRepository.AddAsync(annualPassOrder);

            // Setup Account Access
            if (account == null)
            {
                _accountService.SetPassword(order.Parent.Id);
                _accountService.AddToRole(order.Parent.Id, UserRoles.Customer);
            }

            return order;
        }

        public async Task UpdateAsync(AnnualPassOrder annualPassOrder)
        {
            if (annualPassOrder != null)
            {
                await _annualPassOrderRepository.UpdateAsync(annualPassOrder, annualPassOrder.Id);
            }
        }

        public async Task SendEmailAsync(string baseUri, Guid id, string email)
        {
            await _sendGridService.SendEmail(baseUri, id, email, ConfirmationType.AnnualPassOrder);
        }

        public async Task CancelAsync(Guid id)
        {
            var annualPassOrder = await _annualPassOrderRepository.GetAsync(id);
            if (annualPassOrder != null)
            {
                annualPassOrder.Status = OrderStatus.Cancelled;
                await _annualPassOrderRepository.UpdateAsync(annualPassOrder, annualPassOrder.Id);
            }
        }

        public async Task UpdateStatusAsync(Guid id, OrderStatus status)
        {
            var annualPassOrder = await _annualPassOrderRepository.GetAsync(id);
            if (annualPassOrder != null)
            {
                annualPassOrder.Status = status;
                await _annualPassOrderRepository.UpdateAsync(annualPassOrder, annualPassOrder.Id);
            }
        }

        public byte[] GetPdfBytes(string url)
        {
            return _sendGridService.GetPdfBytes(url);
        }

        public async Task SendOrderNotificationAsync(AnnualPassOrder annualPassOrder)
        {
            if (annualPassOrder == null) return;

            var email = ConfigurationManager.AppSettings["AnnualPassOrderNotifications"];

            var pickupDate = annualPassOrder.PickUpDate.Date.ToString("ddd MMM dd, yyyy");
            var pickupTime = annualPassOrder.PickUpTime.GetDisplayName();
            var formattedPhoneNumber = HtmlHelperExtensions.PhoneNumber(annualPassOrder.Parent.PhoneNumber);
            var promotionCode = (annualPassOrder.PromotionCode != null) ? annualPassOrder.PromotionCode.Code : "NONE";

            if (annualPassOrder.PromotionCode != null)
            {
                promotionCode = string.Format("{0} - {1}", annualPassOrder.PromotionCode.Code, annualPassOrder.PromotionCode.Description);
            }

            var mail = new SendGridMessage
            {
                Subject = "Annual Pass Order Notification",
                Html = "<p>"
                + "<h2>Order Information</h2>"
                + "Pickup Date: <strong>" + pickupDate + "</strong><br />"
                + "Pickup Time: <strong>" + pickupTime + "</strong><br />"
                + "<br /><h2>Customer Information</h2>"
                + "Full Name: <strong>" + annualPassOrder.Parent.FullName + "</strong><br />"
                + "Address: <strong>" + annualPassOrder.Parent.FormattedAddress + "</strong><br />"
                + "Phone: <strong>" + formattedPhoneNumber + "</strong><br />"
                + "Email: <strong>" + annualPassOrder.Parent.Email + "</strong><br />"
                + "<br /><h2>Payment Information</h2>"
                + "Transaction ID: <strong>" + annualPassOrder.PaymentTransaction.TransactionNumber + "</strong><br />"
                + "Amount: <strong>$" + annualPassOrder.PaymentTransaction.Amount + "</strong><br />"
                + "Promotion: <strong>" + promotionCode + "</strong><br />"
                + "Status: <strong>" + annualPassOrder.PaymentTransaction.PaymentStatus.ToUpper() + "</strong>"
                + "</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            try { await _sendGridService.Send(mail); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }

        public async Task RemoveAsync(Guid id)
        {
            var annualPassOrder = await _annualPassOrderRepository.GetAsync(id);
            if (annualPassOrder == null) return;

            annualPassOrder.Status = OrderStatus.Cancelled;
            await _annualPassOrderRepository.UpdateAsync(annualPassOrder, annualPassOrder.Id);
        }
    }
}
