﻿using System.Collections.Generic;
using System.Web;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;

namespace StirCrazy.Core.Services
{
    public interface IMediaService
    {
        MediaFile Get(int id);
        MediaFile Get(string filename);
        List<MediaFile> Get();
        IEnumerable<MediaFile> Get(UploadFolder uploadFolder);
        int Save(HttpPostedFileBase file, string serverPath, UploadFolder uploadFolder);
        void Save(HttpPostedFileBase file, MediaFile media, string serverPath, UploadFolder uploadFolder);
        void Update(MediaFile mediaFile);
        void ReIndex(int id, int order);
        void Remove(int id);
        void Remove(string filePath);
    }
}