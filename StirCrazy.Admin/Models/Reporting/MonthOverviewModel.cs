﻿using System.Collections.Generic;
using StirCrazy.Core.Constants;

namespace StirCrazy.Admin.Models.Reporting
{
    public class MonthOverviewModel
    {
        public List<MonthReportModel> MonthReports { get; set; }
        public Month Month { get; set; }
    }
}