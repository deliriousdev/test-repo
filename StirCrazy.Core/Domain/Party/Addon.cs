﻿using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain.Party
{
    public class Addon
    {
        public int Id { get; set; }
        public int Index { get; set; }
        public AddonType AddonType { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }
    }

    public enum AddonType : byte
    {
        [Display(Name = "Party Theme")]
        ThemeParty = 16,
        [Display(Name = "Cake Flavour")]
        FlavourCake = 0,
        [Display(Name = "Cake Theme")]
        ThemeCake = 5,
        [Display(Name = "Cupcake Flavour")]
        FlavourCupcake = 1,
        [Display(Name = "Mini Cupcake Flavour")]
        FlavourMiniCupcake = 19,
        [Display(Name = "Cupcake Icing")]
        FlavourIcing = 2,
        [Display(Name = "Pizza")]
        FoodPizza = 3,
        [Display(Name = "Food Platter")]
        FoodPlatter = 4,
        [Display(Name = "Beverage")]
        Beverage = 14,
        [Display(Name = "Balloons - 18\" Mylar")]
        BalloonMylar = 6,
        [Display(Name = "Balloons - Mylar (Super Shape)")]
        BalloonDeluxe = 7,
        [Display(Name = "Balloons - Latex (Color)")]
        BalloonLatex = 8,
        [Display(Name = "Balloons - Latex (Themed)")]
        BalloonLatexThemed = 12,
        [Display(Name = "Addons - Standard Loot Bag")]
        LootBag = 9,
        [Display(Name = "Addons - Stir Crazy Loot Bag")]
        LootBagDeluxe = 13,
        [Display(Name = "Addons - Glitter Tattoo")]
        Tattoo = 10,
        [Display(Name = "Addons - Other")]
        AddOns = 11,
        [Display(Name = "Custom - Edible Arrangement")]
        EdibleArrangement = 15,
        [Display(Name = "Cakes - Standard Image")]
        StandardCakeImage = 17,
        [Display(Name = "Cakes - Edible Image")]
        EdibleCakeImage = 18
    }
}
