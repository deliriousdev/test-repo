﻿$(document).ready(function () {

    var now = moment().add('days', 2);

    // DATE PICKER CALENDAR
    $('#PickUpDate').datetimepicker({
        datepicker: true,
        format: 'm/d/Y',
        startDate: now.format('YYYY/MM/DD'),
        minDate: now.format('YYYY/MM/DD'),
        defaultSelect: false,
        endDate: -Infinity,
        autoClose: true,
        timepicker: false,
        validateOnBlur: true,
        inline: true
    });

    $("#AnnualPassOrder").data('submitted', false);
    $('.submit-btn').show();
    $('.submit-progress').hide();

    getOrderTotal();

    $("body :input[type='text']").on({
        keyup: getOrderTotal,
        focusout: getOrderTotal,
        change: getOrderTotal
    });

    $(document).on('click', '#PromotionCodeValidate', function () {
        validatePromotionalCode();
    });

    function validatePromotionalCode() {
        if ($('#Promotion').val() == "" || $('#Promotion').val() == null) return;

        $('#OrderTotal').attr('data-adjustment', "0");
        $('#OrderTotal').attr('data-adjustment-type', "0");
        $('#PromotionDescription').empty();

        if ($('#Promotion').val() == "SFFC@DMIN7477") {
            $('#ManualTransaction').show();
            $('#PaypalTransaction').hide();
            $('#BypassPaypal').val("True");
            $('#BypassPaypal').attr('data-val', "true");
            return;
        }

        $.ajax({
            type: "POST",
            url: "/Home/PromotionCode",
            data: { promoCode: $('#Promotion').val(), promoType: 5 },
            dataType: 'json',
            success: function (data) {

                if (data != null) {
                    $('#PromotionDescription').empty();
                    $('#PromotionDescription').text(data.Description);

                    var total = $('#OrderTotal').val();

                    // Percentage
                    if (data.AdjustmentType == 1) {
                        var discount = (data.Adjustment / 100) * total;
                        total = (total - discount);
                    }

                    // Discount
                    if (data.AdjustmentType == 2) {
                        total = (total - data.Adjustment);
                    }

                    if (total > 0) {
                        $('#OrderTotal').attr('data-adjustment', data.Adjustment);
                        $('#OrderTotal').attr('data-adjustment-type', data.AdjustmentType);
                        getOrderTotal();
                    } else {
                        $('#PromotionDescription').empty();
                        $('#PromotionDescription').text("(!) The promotional code discount exceeds total order amount.");
                    }
                }
            }
        });
    };

    // CALCULATIONS
    function getOrderTotal() {

        var total = 124.99;
        var discount = total;

        // Promotion Code
        if ($('#Promotion').val()) {

            var adjustmentAmount = $('#OrderTotal').attr('data-adjustment');
            var adjustmentType = $('#OrderTotal').attr('data-adjustment-type');

            // Percentage
            if (adjustmentType == 1) {
                var adjustment = (adjustmentAmount / 100) * total;
                discount = (total - adjustment);
            }

            // Percentage
            if (adjustmentType == 2) {
                discount = (total - adjustmentAmount);
            }

            if (discount > 0) { total = discount; }
        }

        // Totals
        var subTotal = parseFloat(total).toFixed(2);
        var gstTotal = (subTotal * 0.05).toFixed(2);
        var orderTotal = (subTotal * 1.05).toFixed(2);

        $('#SubTotal').text('$' + subTotal);
        $('#GstTotal').text('$' + gstTotal);
        $('#FinalTotal').text('$' + orderTotal);

        $('#OrderSubTotal').val(subTotal);
        $('#OrderTaxTotal').val(gstTotal);
        $('#OrderTotal').val(orderTotal);
        console.log("SubTotal: $" + subTotal + " | Total: $" + orderTotal);
    }

    $('#AnnualPassOrder').submit(function () {

        var validationErrors = [];
        var validationSummary = [];

        $('#ValidationSummary').hide();
        $('#ValidationSummary ul').empty();

        /** CALCULATE TOTAL **/
        getOrderTotal();

        /** VALIDATE CUSTOMER INFORMATION **/
        $('#CustomerInformation').find(':input').each(function () {
            if (!$.trim($(this).val()).length > 0) {
                validationErrors.push("Please Complete All Required Customer Information Fields.");
            }
        });

        $('#ChildInformation').find(':input').each(function () {

            if (!$(this).val()) {
                validationErrors.push("Please Complete All Required Child Information Fields.");
            }
        });

        /** VALIDATE EMAIL **/
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        var valid = pattern.test($('#Customer_Email').val());

        if (!valid) {
            validationErrors.push("Invalid Email Address, Please Provide A Valid Email Address.");
        } else {
            if ($('#Customer_Email').val() != $('#Customer_Email_Confirm').val()) {
                validationErrors.push("Email Address Mismatch, Please Ensure You've Confirmed Your Email Address.");
            }
        }

        /** VERIFY PROVINCE **/
        if ($("#Customer_Province option:selected").index() == 0) {
            validationErrors.push("Please Choose Your Province.");
        }

        /** VALIDATE ORDER **/
        if ($('#OrderTotal').val() == "0.00") {
            validationErrors.push("Please Choose At Least One Item Before Placing Your Order.");
        }

        /** GET VALIDATION SUMMARY **/
        $.each(validationErrors, function (i, e) {
            if ($.inArray(e, validationSummary) == -1) {
                $('#ValidationSummary').find('ul').append('<li>' + e + '</li>');

                console.log(e);
                validationSummary.push(e);
            }
        });

        console.log('submitted');
        if (validationErrors.length) {
            $('#ValidationSummary').show();
            event.preventDefault();
        } else {
            var $form = $('form');

            if ($form.data('submitted') === true) {
                e.preventDefault();
            } else {
                $form.data('submitted', true);
                $('.submit-btn').hide();
                $('.submit-progress').show();
            }
        }
    });
});