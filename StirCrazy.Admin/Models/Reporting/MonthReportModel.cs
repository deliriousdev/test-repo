﻿using System.Collections.Generic;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Admin.Models.Reporting
{
    public class MonthReportModel
    {
        public int Month { get; set; }
        public OrderType Type { get; set; }
        public int Orders { get; set; }
        public IList<dynamic> Objects { get; set; }
        public decimal Total { get; set; }
    }
}