﻿using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Admin.Models.Account
{
    public class ForgotPasswordModel
    {
        [Required(ErrorMessage = "Email Address is required")]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

    }
}