using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Gateways;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Utilities;
using Tweetinvi;

namespace StirCrazy.Core.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DatabaseContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // Create Roles
            if (!roleManager.RoleExists("Administrator")) roleManager.Create(new IdentityRole("Administrator"));
            if (!roleManager.RoleExists("Staff")) roleManager.Create(new IdentityRole("Staff"));
            if (!roleManager.RoleExists("Customer")) roleManager.Create(new IdentityRole("Customer"));

            // Admin Accounts
            var admin = new ApplicationUser
            {
                UserName = "michelle",
                FirstName = "Michelle",
                LastName = "Benson",
                Email = "michelle@stircrazy.ca",
                Address = "3355 Sunridge Way NE",
                City = "Calgary",
                Province = "AB",
                PostalCode = "T1Y 7H5",
            };

            IAccountService accountService = new AccountService(context);
            ISendGridService sendGridService = new SendGridService(new SendGridGateway());

            var adminExists = accountService.FindByEmail(admin.UserName);

            if (adminExists == null)
            {
                var adminresult = accountService.Create(admin, "lavendar*69");

                if (adminresult.Succeeded)
                {
                    userManager.AddToRole(admin.Id, UserRoles.Administrator);
                    sendGridService.SendAccountInformation(admin, "lavendar69");
                }
            }

            var admin2 = new ApplicationUser
            {
                UserName = "greg",
                FirstName = "Greg",
                LastName = "Benson",
                Email = "greg@stircrazy.ca",
                Address = "3355 Sunridge Way NE",
                City = "Calgary",
                Province = "AB",
                PostalCode = "T1Y 7H5",
            };

            var admin2Exists = accountService.FindByEmail(admin2.UserName);

            if (admin2Exists == null)
            {
                var admin2Result = accountService.Create(admin2, "benson5");

                if (admin2Result.Succeeded)
                {
                    userManager.AddToRole(admin2.Id, UserRoles.Administrator);
                    sendGridService.SendAccountInformation(admin2, "benson5");
                }
            }

            // Manager Account
            var staffAcct1 = new ApplicationUser
            {
                UserName = "manager",
                FirstName = "Stir Crazy",
                LastName = "Manager",
                Email = "manager@stircrazy.ca",
                Address = "3355 Sunridge Way NE",
                City = "Calgary",
                Province = "AB",
                PostalCode = "T1Y 7H5",
            };

            var staffAcct1Exists = accountService.FindByEmail(staffAcct1.UserName);

            if (staffAcct1Exists == null)
            {
                var staffAcct1Result = accountService.Create(staffAcct1, "manager5");

                if (staffAcct1Result.Succeeded)
                {
                    userManager.AddToRole(staffAcct1.Id, UserRoles.Manager);
                    sendGridService.SendAccountInformation(staffAcct1, "manager5");
                }
            }

            // Staff Account
            var staffAcct2 = new ApplicationUser
            {
                UserName = "staff",
                FirstName = "Stir Crazy",
                LastName = "Staff",
                Email = "partycoordinator@stircrazy.ca",
                Address = "3355 Sunridge Way NE",
                City = "Calgary",
                Province = "AB",
                PostalCode = "T1Y 7H5",
            };

            var staffAcct2Exists = accountService.FindByEmail(staffAcct2.UserName);

            if (staffAcct2Exists == null)
            {
                var staffAcct2Result = accountService.Create(staffAcct2, "stircraz14");

                if (staffAcct2Result.Succeeded)
                {
                    userManager.AddToRole(staffAcct2.Id, UserRoles.Staff);
                    sendGridService.SendAccountInformation(staffAcct2, "stircraz14");
                }
            }

            // Charmine - Staff Account
            var staffAcct3 = new ApplicationUser
            {
                UserName = "serverops",
                FirstName = "Server",
                LastName = "Operations",
                Email = "serverops@thinkmonkey.ca",
                Address = "3355 Sunridge Way NE",
                City = "Calgary",
                Province = "AB",
                PostalCode = "T1Y 7H5",
            };

            var staffAcct3Exists = accountService.FindByEmail(staffAcct3.UserName);

            if (staffAcct3Exists == null)
            {
                var staffAcct3Result = accountService.Create(staffAcct3, "Welcome001");

                if (staffAcct3Result.Succeeded)
                {
                    userManager.AddToRole(staffAcct3.Id, UserRoles.Administrator);
                    sendGridService.SendAccountInformation(staffAcct3, "Welcome001");
                }
            }

            // Custom Deployment
            var deployment = new Deployment(context);
            deployment.Seed();

            base.Seed(context);
        }
    }
}
