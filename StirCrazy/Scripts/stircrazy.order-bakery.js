﻿$(document).ready(function () {

    var now = moment().add('days', 2);

    // DATE PICKER CALENDAR
    $('#PickUpDate').datetimepicker({
        datepicker: true,
        format: 'm/d/Y',
        defaultDate: now.format('YYYY/MM/DD'),
        startDate: now.format('YYYY/MM/DD'),
        minDate: now.format('YYYY/MM/DD'),
        defaultSelect: false,
        endDate: -Infinity,
        autoClose: true,
        timepicker: false,
        validateOnBlur: true,
        inline: true,
        onSelectDate: function(dp, $input) {
            $('#PickUpDate').val($input.val());
        }
    });

    $("#BakeryOrder").data('submitted', false);
    $('.submit-btn').show();
    $('.submit-progress').hide();

    getOrderTotal();

    $("body :input[type='text']").on({
        focusout: getOrderTotal,
        change: getOrderTotal
    });

    $(document).on('click', '.flavours', function () {
        limitFlavourSelection();
    });

    $(document).on('click', '.mini-flavours', function () {
        limitFlavourSelection();
    });

    $(document).on('change', '#CakeFlavour', function () {
        getOrderTotal();
    });

    $(document).on('change', '#CakeTheme', function () {
        getOrderTotal();
    });

    $(document).on('change', '#StandardImage', function () {
        getOrderTotal();
    });

    $(document).on('change', '#EdibleImage', function () {
        getOrderTotal();
    });

    $(document).on('click', '#PromotionCodeValidate', function () {
        validatePromotionalCode();
    });

    function validatePromotionalCode() {
        if ($('#Promotion').val() == "" || $('#Promotion').val() == null) return;

        $('#OrderTotal').attr('data-adjustment', "0");
        $('#OrderTotal').attr('data-adjustment-type', "0");
        $('#PromotionDescription').empty();

        if ($('#Promotion').val() == "SFFC@DMIN7477") {
            $('#ManualTransaction').show();
            $('#PaypalTransaction').hide();
            $('#BypassPaypal').val("True");
            $('#BypassPaypal').attr('data-val', "true");
            return;
        }

        $.ajax({
            type: "POST",
            url: "/Home/PromotionCode",
            data: { promoCode: $('#Promotion').val(), promoType: 4 },
            dataType: 'json',
            success: function (data) {

                if (data != null) {
                    $('#PromotionDescription').empty();
                    $('#PromotionDescription').text(data.Description);

                    var total = $('#OrderTotal').val();

                    // Percentage
                    if (data.AdjustmentType == 1) {
                        var discount = (data.Adjustment / 100) * total;
                        total = (total - discount);
                    }

                    // Discount
                    if (data.AdjustmentType == 2) {
                        total = (total - data.Adjustment);
                    }

                    if (total > 0) {
                        $('#OrderTotal').attr('data-adjustment', data.Adjustment);
                        $('#OrderTotal').attr('data-adjustment-type', data.AdjustmentType);
                        getOrderTotal();
                    } else {
                        $('#PromotionDescription').empty();
                        $('#PromotionDescription').text("(!) The promotional code discount exceeds total order amount.");
                    }
                }
            }
        });
    };

    // LIMIT ICING FLAVOURS
    function limitFlavourSelection() {
        var selected = $('.flavours:checked').size();

        if (selected >= 2) {
            $('.flavours').not(':checked').each(function () {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            $('.flavours').removeAttr('disabled', 'disabled');
        }

        var selectedMini = $('.mini-flavours:checked').size();

        if (selectedMini >= 2) {
            $('.mini-flavours').not(':checked').each(function () {
                $(this).attr('disabled', 'disabled');
            });
        } else {
            $('.mini-flavours').removeAttr('disabled', 'disabled');
        }
    }

    // CUPCAKE TOTAL
    function getCupcakeTotal() {
        var total = 0.00;
        var dozen = 0.00;

        $('.cupcakes').each(function () {
            var quantity = $(this).val();
            var perDozen = $(this).data('price');

            if (quantity.match(/^\d+$/)) {
                total += (perDozen * quantity);
                dozen += +quantity;
            }
        });

        //console.log("perDozen: " + perDozen + " quantity: " + quantity);
        $('#cupcakeTotal').text('$' + total + ' for ' + dozen + ' dozen.');
        return total;
    }

    function getMiniCupcakeTotal() {
        var total = 0.00;
        var dozen = 0.00;

        $('.mini-cupcakes').each(function () {
            var quantity = $(this).val();
            var perDozen = $(this).data('price');

            if (quantity.match(/^\d+$/)) {
                total += (perDozen * quantity);
                dozen += +quantity;
            }
        });

        //console.log("perDozen: " + perDozen + " quantity: " + quantity);
        $('#miniCupcakeTotal').text('$' + total + ' for ' + dozen + ' dozen.');
        return total;
    }

    // CAKE TOTAL
    function getCakeTotal() {

        if ($("#CakeTheme option:selected").index() > 0) {
            $("#CakeFlavour").removeAttr('disabled');
            $('#cakeTotal').text('$' + 45);
            return 45;
        }

        $("#CakeFlavour").attr('disabled', 'disabled');
        $("#CakeFlavour").prop('selectedIndex', 0);

        $('#cakeTotal').text('$' + 0);
        return 0;
    }

    // IMAGES TOTAL
    function getImagesTotal() {

        var total = 0;

        if ($("#StandardImage option:selected").index() > 0) {
            total += 10;
        }

        if ($("#EdibleImage option:selected").index() > 0) {
            total += 15;
        }

        $('#imagesTotal').text('$' + total);
        return total;
    }

    // CALCULATIONS
    function getOrderTotal() {

        var total = 0;
        var discount = total;

        total += getCupcakeTotal();
        total += getMiniCupcakeTotal();
        total += getCakeTotal();
        total += getImagesTotal();

        // Promotion Code
        if ($('#Promotion').val()) {

            var adjustmentAmount = $('#OrderTotal').attr('data-adjustment');
            var adjustmentType = $('#OrderTotal').attr('data-adjustment-type');

            // Percentage
            if (adjustmentType == 1) {
                var adjustment = (adjustmentAmount / 100) * total;
                discount = (total - adjustment);
            }

            // Percentage
            if (adjustmentType == 2) {
                discount = (total - adjustmentAmount);
            }

            if (discount > 0) { total = discount; }
        }

        // Totals
        var subTotal = parseFloat(total).toFixed(2);
        var gstTotal = (subTotal * 0.05).toFixed(2);
        var orderTotal = (subTotal * 1.05).toFixed(2);

        $('#SubTotal').text('$' + subTotal);
        $('#GstTotal').text('$' + gstTotal);
        $('#FinalTotal').text('$' + orderTotal);

        $('#OrderSubTotal').val(subTotal);
        $('#OrderTaxTotal').val(gstTotal);
        $('#OrderTotal').val(orderTotal);
        console.log("SubTotal: $" + subTotal + " | Total: $" + orderTotal);
    }

    $('#BakeryOrder').submit(function () {

        var validationErrors = [];
        var validationSummary = [];

        $('#ValidationSummary').hide();
        $('#ValidationSummary ul').empty();

        /** CALCULATE PARTY TOTAL **/
        getOrderTotal();

        /** VALIDATE CUSTOMER INFORMATION **/
        $('#CustomerInformation').find(':text').each(function () {
            if (!$.trim($(this).val()).length > 0) {
                validationErrors.push("Please Complete All Required Customer Information Fields.");
            }
        });

        /** VALIDATE EMAIL **/
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        var valid = pattern.test($('#Customer_Email').val());

        if (!valid) {
            validationErrors.push("Invalid Email Address, Please Provide A Valid Email Address.");
        } else {
            if ($('#Customer_Email').val() != $('#Customer_Email_Confirm').val()) {
                validationErrors.push("Email Address Mismatch, Please Ensure You've Confirmed Your Email Address.");
            }
        }

        /** VERIFY PROVINCE **/
        if ($("#Customer_Province option:selected").index() == 0) {
            validationErrors.push("Please Choose Your Province.");
        }

        /** VALIDATE ORDER **/
        if ($('#OrderTotal').val() == "0.00") {
            validationErrors.push("Please Choose At Least One Item Before Placing Your Order.");
        }

        /** GET VALIDATION SUMMARY **/
        $.each(validationErrors, function (i, e) {
            if ($.inArray(e, validationSummary) == -1) {
                $('#ValidationSummary').find('ul').append('<li>' + e + '</li>');

                console.log(e);
                validationSummary.push(e);
            }
        });

        console.log('submitted');
        if (validationErrors.length) {
            $('#ValidationSummary').show();
            event.preventDefault();
        } else {
            var $form = $('form');

            if ($form.data('submitted') === true) {
                e.preventDefault();
            } else {
                $form.data('submitted', true);
                $('.submit-btn').hide();
                $('.submit-progress').show();
            }
        }
    });
});