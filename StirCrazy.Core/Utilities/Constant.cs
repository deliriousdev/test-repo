﻿namespace StirCrazy.Core.Utilities
{
    public static class HtmlCssHelper
    {
        public static string RoomColor(int roomId)
        {
            switch (roomId)
            {
                case 1:
                    return "rocknroll-red";

                case 2:
                    return "mellow-yellow";

                case 3:
                    return "bebop-blue";

                case 4:
                    return "outrageous-orange";

                case 5:
                    return "sublime-lime";

                case 6:
                    return "playful-purple";

                default:
                    return null;
            }
        }
    }
}