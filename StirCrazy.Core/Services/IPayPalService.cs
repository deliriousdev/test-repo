﻿using System;
using PayPal.Api.Payments;
using StirCrazy.Core.Domain;

namespace StirCrazy.Core.Services
{
    public interface IPayPalService
    {
        Payment CreatePayment(double total, string orderDescription, string returnUrl, string cancelUrl);
        Refund CreateRefund(int transactionId, string refundDescription);
        Payment ExecutePayment(Payment payment, string payerId);
        PaymentTransaction GetTransaction(int id);
        void AddTransaction(PaymentTransaction paymentTransaction);
        void UpdateTransaction(PaymentTransaction paymentTransaction);
        void RemoveTransaction(PaymentTransaction paymentTransaction);
        Payment UpdatePaymentStatus(Guid id, string paymentId);
    }
}