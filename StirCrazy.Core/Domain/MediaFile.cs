﻿using System.ComponentModel.DataAnnotations;
using StirCrazy.Core.Constants;

namespace StirCrazy.Core.Domain
{
    public class MediaFile
    {
        [Key]
        public int Id { get; set; }
        public int Index { get; set; }
        public UploadFolder UploadFolder { get; set; }
        public string FilePath { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public string PhysicalPath { get; set; }
        public string ThumbnailPhysical { get; set; }
        public string ThumbnailPath { get; set; }
        public string Transition { get; set; }
        public bool IsActive { get; set; }
    }
}
