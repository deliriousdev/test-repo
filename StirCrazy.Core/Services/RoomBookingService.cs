﻿using System;
using System.Collections.Generic;
using System.Linq;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services
{
    public class RoomBookingService : IRoomBookingService
    {
        private readonly IRepository<RoomBooking> _roomBookingRepository;
        private readonly IRepository<TimeSlot> _timeSlotRepository;
        private readonly IRepository<PartyRoom> _roomRepository;
        private readonly IRepository<StoreHour> _storeHoursRepository;
        private readonly IRepository<StoreHoliday> _storeHolidaysRepository;
        private readonly IRepository<CorporateEvent> _corporateEventsRepository;

        public RoomBookingService(IRepository<RoomBooking> roomBookingRepository,
            IRepository<TimeSlot> timeSlotRepository,
            IRepository<PartyRoom> roomRepository,
            IRepository<StoreHour> storeHoursRepository,
            IRepository<StoreHoliday> storeHolidaysRepository, 
            IRepository<CorporateEvent> corporateEventsRepository)
        {
            _roomBookingRepository = roomBookingRepository;
            _timeSlotRepository = timeSlotRepository;
            _roomRepository = roomRepository;
            _storeHoursRepository = storeHoursRepository;
            _storeHolidaysRepository = storeHolidaysRepository;
            _corporateEventsRepository = corporateEventsRepository;
        }

        public IEnumerable<RoomBooking> Get()
        {
            return _roomBookingRepository.Get();
        }

        public RoomBooking Get(int id)
        {
            return _roomBookingRepository.Get(id);
        }

        public IEnumerable<TimeSlot> GetTimeSlots()
        {
            return _timeSlotRepository.Get();
        }

        public RoomBooking Add(RoomBooking roomBooking)
        {
            return _roomBookingRepository.Add(roomBooking);
        }

        public RoomBooking Update(RoomBooking roomBooking)
        {
            return _roomBookingRepository.Update(roomBooking, roomBooking.Id);
        }

        public void Remove(RoomBooking roomBooking)
        {
            _roomBookingRepository.Delete(roomBooking);
        }

        public IEnumerable<Tuple<PartyRoom, TimeSlot>> GetAvailability(DateTime searchDate)
        {
            IList<TimeSlot> timeSlots;
            var availableSlots = new List<Tuple<PartyRoom, TimeSlot>>();

            var rooms = _roomRepository.Get();
            var storeHours = _storeHoursRepository.Get();
            var storeHolidays = _storeHolidaysRepository.Get();
            var corporateEvents = _corporateEventsRepository.Get();

            // Store Hours Or Holidays
            var hours = storeHours as IList<StoreHour> ?? storeHours.ToList();
            var holidays = storeHolidays as IList<StoreHoliday> ?? storeHolidays.ToList();
            var events = corporateEvents as IList<CorporateEvent> ?? corporateEvents.ToList();

            // Store Closed || Corporate Event
            if (hours.Any(x => x.DayOfWeek == searchDate.DayOfWeek && x.IsClosed)) return availableSlots;
            if (holidays.Any(x => x.Date.Date == searchDate.Date && !x.AllowScheduling)) return availableSlots;
            if (events.Any(x => x.Date.Date == searchDate.Date && x.IsAllDay)) return availableSlots;

            // Store Hours
            var storeHour = hours.FirstOrDefault(x => x.DayOfWeek == searchDate.DayOfWeek && !x.IsClosed);
            timeSlots = _timeSlotRepository.Get().Where(timeslot => storeHour != null && (timeslot.CheckInTime >= storeHour.OpenTime && timeslot.CheckOutTime <= storeHour.ClosedTime)).ToList();

            // Store Holidays
            var storeHoliday = holidays.FirstOrDefault(x => x.Date.Date == searchDate.Date && !x.IsClosed);
            if (storeHoliday != null)
            {
                timeSlots = _timeSlotRepository.Get().Where(timeslot => timeslot.CheckInTime >= storeHoliday.OpenTime && timeslot.CheckOutTime <= storeHoliday.ClosedTime).ToList();
            }

            // Corporate Events
            var corporateEvent = events.Where(x => x.Date == searchDate && !x.IsAllDay && x.Status != OrderStatus.Cancelled);
            if (corporateEvent.Any())
            {
                timeSlots = _timeSlotRepository.Get().Where(timeslot => storeHour != null
                    && (storeHour.OpenTime <= timeslot.CheckInTime && storeHour.ClosedTime >= timeslot.EndTime) 
                    && !corporateEvent.Any(corp => timeslot.CheckOutTime > corp.StartTime && timeslot.CheckInTime < corp.EndTime)).ToList();
            }

            foreach (var room in rooms)
            {
                availableSlots.AddRange(
                    timeSlots.Where(timeslot => room.Bookings.All(booking => booking.Date.Date != searchDate.Date || (timeslot.CheckInTime >= booking.TimeSlot.EndTime || timeslot.CheckOutTime <= booking.TimeSlot.StartTime)
                        || booking.Status == Status.Pending))
                    .Select(timeslot => new Tuple<PartyRoom, TimeSlot>(room, timeslot)));
            }

            return availableSlots;
        }

        public bool CheckAvailability(DateTime searchDate, TimeSpan startTime, TimeSpan endTime)
        {
            var corporateEvents = _corporateEventsRepository.Get();
            var events = corporateEvents as IList<CorporateEvent> ?? corporateEvents.ToList();

            if (events.Any(x => x.Date.Date == searchDate.Date && x.IsAllDay)) return false; // All Day Corporate Event

            // Corporate Events
            var corporateEvent = events.Where(x => x.Date == searchDate.Date && !x.IsAllDay && x.Status != OrderStatus.Cancelled);
            if (corporateEvent.Any(timeslot => timeslot.StartTime <= endTime && timeslot.EndTime >= startTime)) return false; // Existing Corporate Event

            var roomBookings = _roomBookingRepository.Get();
            var bookings = roomBookings as IList<RoomBooking> ?? roomBookings.ToList();
            if (bookings.Any(booking => booking.TimeSlot.CheckOutTime >= startTime && booking.TimeSlot.CheckInTime <= endTime && booking.Status == Status.Scheduled)) return false; // Existing Party Booking

            return true; // No Conflicts
        }

        public void Remove(int id)
        {
            var booking = _roomBookingRepository.Get(id);
            _roomBookingRepository.Delete(booking);
        }
    }
}