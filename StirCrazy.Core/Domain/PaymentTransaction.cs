﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain
{
    public class PaymentTransaction
    {
        public int Id { get; set; }
        public TransactionType TransactionType { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        public string TransactionNumber { get; set; }
        public string PaymentStatus { get; set; }
    }

    public enum TransactionType
    {
        [Display(Name = "PayPal")]
        Paypal,
        [Display(Name = "Cash")]
        Cash,
        [Display(Name = "Credit Card")]
        Credit,
        [Display(Name = "Cheque")]
        Cheque
    }
}
