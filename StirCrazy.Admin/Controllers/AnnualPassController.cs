﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using StirCrazy.Admin.Models.Order;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Services.Order;

namespace StirCrazy.Admin.Controllers
{
    public class AnnualPassController : AsyncController
    {
        private readonly IAnnualPassOrderService _annualPassOrderService;

        public AnnualPassController(IAnnualPassOrderService annualPassOrderService)
        {
            _annualPassOrderService = annualPassOrderService;
        }

        public ActionResult Index(OrderStatus status = OrderStatus.Paid)
        {
            var model = _annualPassOrderService.GetByStatus(status).Select(x => new AnnualPassModel()
            {
                Id = x.Id,
                Notes = x.Notes,
                Customer = x.Parent,
                Child = x.Child,
                PaymentTransaction = x.PaymentTransaction,
                PickUpDate = x.PickUpDate,
                PickUpTime = x.PickUpTime,
                Total = x.Total,
                Status = x.Status
            }).ToList();

            return View(model);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var admissionOrder = await _annualPassOrderService.GetAsync(id);
            if (admissionOrder != null)
            {
                var model = new AnnualPassModel()
                {
                    Customer = admissionOrder.Parent,
                    Child = admissionOrder.Child,
                    Notes = admissionOrder.Notes,
                    PickUpDate = admissionOrder.PickUpDate,
                    PickUpTime = admissionOrder.PickUpTime,
                    PaymentTransaction = admissionOrder.PaymentTransaction,
                    SubTotal = admissionOrder.SubTotal,
                    Tax = admissionOrder.Tax,
                    Discount = admissionOrder.Discount,
                    Total = admissionOrder.Total,
                    Status = admissionOrder.Status,
                    Promotion = (admissionOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", admissionOrder.PromotionCode.Code, admissionOrder.PromotionCode.Description) : "NONE"
                };

                return View(model);
            }

            return HttpNotFound();
        }

        public async Task<ActionResult> Print(Guid id)
        {
            var admissionOrder = await _annualPassOrderService.GetAsync(id);
            if (admissionOrder != null)
            {
                var model = new AnnualPassModel()
                {
                    Customer = admissionOrder.Parent,
                    Child = admissionOrder.Child,
                    Notes = admissionOrder.Notes,
                    PickUpDate = admissionOrder.PickUpDate,
                    PickUpTime = admissionOrder.PickUpTime,
                    PaymentTransaction = admissionOrder.PaymentTransaction,
                    SubTotal = admissionOrder.SubTotal,
                    Tax = admissionOrder.Tax,
                    Discount = admissionOrder.Discount,
                    Total = admissionOrder.Total,
                    Status = admissionOrder.Status,
                    Promotion = (admissionOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", admissionOrder.PromotionCode.Code, admissionOrder.PromotionCode.Description) : "NONE"
                };

                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Save(Guid id)
        {
            string baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Admission/";
            string detailsUrl = baseUri + "Print/" + id;

            byte[] bytesAsPdf = _annualPassOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        public async Task<ActionResult> Email(Guid id)
        {
            string baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Admission/";

            var partyOrder = await _annualPassOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            await _annualPassOrderService.SendEmailAsync(baseUri, id, partyOrder.Parent.Email);

            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        [HttpPost]
        public async Task<RedirectToRouteResult> Delete(RoomOrderModel model)
        {
            await _annualPassOrderService.RemoveAsync(model.Id);
            return RedirectToAction("Index");
        }

        public async Task<RedirectToRouteResult> Cancelled(Guid id)
        {
            await _annualPassOrderService.RemoveAsync(id);
            return RedirectToAction("Index", "Home");
        }
    }
}