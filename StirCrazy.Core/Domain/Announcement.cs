﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain
{
    public class Announcement
    {
        [Key]
        public int Id { get; set; }
        public int? MediaFileId { get; set; }
        public string Message { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsPopup { get; set; }
        public bool IsImage { get; set; }
        public virtual MediaFile MediaFile { get; set; }
    }
}
