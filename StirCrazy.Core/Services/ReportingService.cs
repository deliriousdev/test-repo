﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PayPal;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services
{

    public class ReportingService : IReportingService
    {
        private readonly IRepository<AnnualPassOrder> _annualPassOrdeRepository;
        private readonly IRepository<PartyOrder> _partyOrderRepository;
        private readonly IRepository<BakeryOrder> _bakeryOrderRepository;
        private readonly IRepository<RoomOrder> _roomOrderRepository;
        private readonly IRepository<GrabNGoOrder> _grabNGoRepository;
        private readonly IRepository<CorporateEvent> _corporateEventRepository;

        public ReportingService(IRepository<PartyOrder> partyOrderRepository,
            IRepository<BakeryOrder> bakeryOrderRepository,
            IRepository<RoomOrder> roomOrderRepository,
            IRepository<GrabNGoOrder> grabNGoRepository,
            IRepository<CorporateEvent> corporateEventRepository, 
            IRepository<AnnualPassOrder> annualPassOrdeRepository)
        {
            _partyOrderRepository = partyOrderRepository;
            _bakeryOrderRepository = bakeryOrderRepository;
            _roomOrderRepository = roomOrderRepository;
            _grabNGoRepository = grabNGoRepository;
            _corporateEventRepository = corporateEventRepository;
            _annualPassOrdeRepository = annualPassOrdeRepository;
        }

        public IEnumerable<PartyOrder> GetPartyOrders(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null) startDate = DateTime.Now.Date;
            if (endDate == null) endDate = DateTime.MaxValue.Date;

                return
                    _partyOrderRepository.Get()
                        .Where(x => x.RoomBooking.Date.Date >= startDate && x.RoomBooking.Date.Date <= endDate && x.Status == OrderStatus.Paid)
                        .OrderBy(o => o.RoomBooking.Date);
        }

        public IEnumerable<RoomOrder> GetRoomOrders(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null) startDate = DateTime.Now.Date;
            if (endDate == null) endDate = DateTime.MaxValue.Date;

            return
                _roomOrderRepository.Get()
                    .Where(x => x.RoomBooking.Date.Date >= startDate && x.RoomBooking.Date.Date <= endDate && x.Status == OrderStatus.Paid)
                    .OrderBy(o => o.RoomBooking.Date);
        }

        public IEnumerable<BakeryOrder> GetBakeryOrders(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null) startDate = DateTime.Now.Date;
            if (endDate == null) endDate = DateTime.MaxValue.Date;

            return
                _bakeryOrderRepository.Get()
                    .Where(x => x.PickUpDate.Date.Date >= startDate && x.PickUpDate.Date.Date <= endDate && x.Status == OrderStatus.Paid)
                    .OrderBy(o => o.PickUpDate.Date);
        }

        public IEnumerable<AnnualPassOrder> GetAnnualPassOrders(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null) startDate = DateTime.Now.Date;
            if (endDate == null) endDate = DateTime.MaxValue.Date;

            return
                _annualPassOrdeRepository.Get()
                    .Where(x => x.PickUpDate.Date.Date >= startDate && x.PickUpDate.Date.Date <= endDate && x.Status == OrderStatus.Paid)
                    .OrderBy(o => o.PickUpDate.Date);
        }

        public IEnumerable<GrabNGoOrder> GetGrabNGoOrders(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null) startDate = DateTime.Now;
            if (endDate == null) endDate = DateTime.MaxValue;

            return
                _grabNGoRepository.Get()
                    .Where(x => x.PickUpDate.Date.Date >= startDate && x.PickUpDate.Date.Date <= endDate && x.Status == OrderStatus.Paid)
                    .OrderBy(o => o.PickUpDate.Date);
        }

        public IEnumerable<CorporateEvent> GetCorporateEvents(DateTime? startDate, DateTime? endDate)
        {
            if (startDate == null) startDate = DateTime.Now.Date;
            if (endDate == null) endDate = DateTime.MaxValue.Date;

            return
                _corporateEventRepository.Get()
                    .Where(x => x.Date.Date >= startDate && x.Date.Date <= endDate && x.Status == OrderStatus.Paid)
                    .OrderBy(o => o.Date);
        }

        public dynamic BakeryReport(DateTime start, DateTime end)
        {
            var partyOrders = GetPartyOrders(start, end);
            var bakeryOrders = GetBakeryOrders(start, end);

            return new
            {
                PartyOrders = partyOrders,
                BakeryOrders = bakeryOrders
            };
        }
    }
}
