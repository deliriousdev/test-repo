﻿using System;

namespace StirCrazy.Core.Domain.Temp
{
    public class party_bookings
    {
        public int party_bookings_id { get; set; }
        public int? booking_room_id { get; set; }
        public int booking_pkg_id { get; set; }
        public DateTime booking_date { get; set; }
        public DateTime booking_start_time { get; set; }
        public DateTime booking_end_time { get; set; }
        public string booking_status { get; set; }
        public DateTime create_date { get; set; }
        public string pay_status { get; set; }
        public string pay_transction_id { get; set; }
        public int? party_pkg_qty { get; set; }
        public float? net_total { get; set; }
        public float? gst_percentage { get; set; }
        public float? gst_amount { get; set; }
        public bool? room_rental_service { get; set; }
        public float? room_rental_rate { get; set; }
        public int room_id { get; set; }
        public string room_name { get; set; }
        public string room_name_img { get; set; }
        public string room_photo { get; set; }
        public DateTime action_date { get; set; }
        public int? sort_order { get; set; }
        public int? id { get; set; }
        public int? booking_id { get; set; }
        public string bday_child_name { get; set; }
        public string bday_child_gender { get; set; }
        public int? bday_child_age { get; set; }
        public int? no_adults { get; set; }
        public int? no_children { get; set; }
        public int? no_additional_guest { get; set; }
        public int? no_additional_guest_children { get; set; }
        public int? no_additional_guest_adult { get; set; }
        public string booking_note { get; set; }
        public float? add_cupcake_rate { get; set; }
        public float? pay_amount { get; set; }
        public int? pay_booking_id { get; set; }
        public string pay_fname { get; set; }
        public string pay_lname { get; set; }
        public string pay_email { get; set; }
        public string pay_address { get; set; }
        public string pay_city { get; set; }
        public string pay_city_quadrant { get; set; }
        public string pay_state { get; set; }
        public string pa_zipcode { get; set; }
        public string transction_id { get; set; }
        public string pay_phone { get; set; }
    }
}