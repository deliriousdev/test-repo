﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Calendar;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Core.Services
{
    public class CorporateEventService : ICorporateEventService
    {
        private readonly IRepository<CorporateEvent> _corporateBookingRepository;
        private readonly IAccountService _accountService;
        private readonly ISendGridService _sendGridService;
        private readonly IOffice365Service _office365Service;

        public CorporateEventService(IRepository<CorporateEvent> corporateBookingRepository, IAccountService accountService, ISendGridService sendGridService, IOffice365Service office365Service)
        {
            _corporateBookingRepository = corporateBookingRepository;
            _accountService = accountService;
            _sendGridService = sendGridService;

            _office365Service = new Office365Service("scheduling@stircrazy.ca");
        }

        public IEnumerable<CorporateEvent> Get()
        {
            return _corporateBookingRepository.Get();
        }

        public CorporateEvent Get(Guid id)
        {
            var corporateEvent = _corporateBookingRepository.Get(id);
            return corporateEvent;
        }

        public IEnumerable<CorporateEvent> GetByStatus(OrderStatus status)
        {
            if (status == OrderStatus.Created)
                return _corporateBookingRepository.Get().Where(x => x.Date >= DateTime.Today);

            if (status == OrderStatus.Cancelled)
                return _corporateBookingRepository.Get().Where(x => x.Status == OrderStatus.Cancelled);

            return _corporateBookingRepository.Get();
        }

        public async Task<CorporateEvent> Add(CorporateEvent corporateEvent)
        {
            var account = await _accountService.FindByEmailAsync(corporateEvent.Customer.Email);
            corporateEvent.DateCreated = DateTime.Now;

            corporateEvent.Customer.UserName = corporateEvent.Customer.Email.ToLower();
            corporateEvent.Customer.FirstName = HtmlHelperExtensions.ToTitleCase(corporateEvent.Customer.FirstName);
            corporateEvent.Customer.LastName = HtmlHelperExtensions.ToTitleCase(corporateEvent.Customer.LastName);
            corporateEvent.Customer.Address = HtmlHelperExtensions.ToTitleCase(corporateEvent.Customer.Address);
            corporateEvent.Customer.City = HtmlHelperExtensions.ToTitleCase(corporateEvent.Customer.City);
            corporateEvent.Customer.PostalCode = corporateEvent.Customer.PostalCode.ToUpper();
            corporateEvent.Customer.Province = corporateEvent.Customer.Province;
            corporateEvent.Customer.PhoneNumber = HtmlHelperExtensions.PhoneNumber(corporateEvent.Customer.PhoneNumber);
            corporateEvent.Customer.Email = corporateEvent.Customer.Email.ToLower();

            if (account != null)
            {
                account.UserName = corporateEvent.Customer.Email.ToLower();
                account.FirstName = corporateEvent.Customer.FirstName;
                account.LastName = corporateEvent.Customer.LastName;
                account.Address = corporateEvent.Customer.Address;
                account.City = corporateEvent.Customer.City;
                account.PostalCode = corporateEvent.Customer.PostalCode;
                account.Province = corporateEvent.Customer.Province;
                account.PhoneNumber = corporateEvent.Customer.PhoneNumber;
                account.Email = corporateEvent.Customer.Email;

                await _accountService.UpdateAsync(account);

                corporateEvent.CustomerId = account.Id;
                corporateEvent.Customer = null;
            }

            corporateEvent.DateCreated = DateTime.Now;
            corporateEvent.CreatedBy = corporateEvent.CreatedBy ?? "Customer";

            var order = await _corporateBookingRepository.AddAsync(corporateEvent);

            // Setup Account Access
            if (account == null)
            {
                _accountService.SetPassword(order.Customer.Id);
                _accountService.AddToRole(order.Customer.Id, UserRoles.Customer);
            }

            return order;
        }

        public CorporateEvent Update(CorporateEvent corporateEvent)
        {
            return _corporateBookingRepository.Update(corporateEvent, corporateEvent.Id);
        }

        public async Task SendEmail(string baseUri, Guid id, string email)
        {
            await _sendGridService.SendEmail(baseUri, id, email, ConfirmationType.PartyOrder);
        }

        public byte[] GetPdfBytes(string url)
        {
            return _sendGridService.GetPdfBytes(url);
        }

        public async Task SendNotificationAsync(Guid id)
        {
            var corporateEvent = Get(id);
            if (corporateEvent == null) return;

            var email = ConfigurationManager.AppSettings["CorporateEventNotifications"];

            var eventDate = corporateEvent.Date.Date.ToString("ddd MMM dd, yyyy");
            var startTime = DateTime.Today.Add(corporateEvent.StartTime.GetValueOrDefault()).ToString("h:mm tt");
            var endTime = DateTime.Today.Add(corporateEvent.EndTime.GetValueOrDefault()).ToString("h:mm tt");
            var formattedPhoneNumber = HtmlHelperExtensions.PhoneNumber(corporateEvent.Customer.PhoneNumber);

            var mail = new SendGridMessage
            {
                Subject = "Corporate Event Notification",
                Html = "<p>"
                + "<h2>Booking Information</h2>"
                + "Date: <strong>" + eventDate + "</strong><br />"
                + "Time: <strong>" + startTime + " - " + endTime + "</strong><br />"
                + "<br /><h2>Customer Information</h2>"
                + "Name: <strong>" + corporateEvent.Customer.BusinessName + "</strong><br />"
                + "Address: <strong>" + corporateEvent.Customer.Address + " " + corporateEvent.Customer.City + ", " + corporateEvent.Customer.Province + " " + corporateEvent.Customer.PostalCode + "</strong><br />"
                + "Phone: <strong>" + formattedPhoneNumber + "</strong><br />"
                + "Email: <strong>" + corporateEvent.Customer.Email + "</strong><br />"
                + "</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            try { await _sendGridService.Send(mail); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }

            var details = "<p>"
                + "<h2>Booking Information</h2>"
                + "Date: <strong>" + eventDate + "</strong><br />"
                + "Time: <strong>" + startTime + " - " + endTime + "</strong><br />"
                + "<br /><h2>Customer Information</h2>"
                + "Name: <strong>" + corporateEvent.Customer.BusinessName + "</strong><br />"
                + "Address: <strong>" + corporateEvent.Customer.Address + " " + corporateEvent.Customer.City + ", " + corporateEvent.Customer.Province + " " + corporateEvent.Customer.PostalCode + "</strong><br />"
                + "Phone: <strong>" + formattedPhoneNumber + "</strong><br />"
                + "Email: <strong>" + corporateEvent.Customer.Email + "</strong><br />"
                + "</p>";

            var pEvent = new CalendarItem()
            {
                Subject = corporateEvent.Customer.BusinessName + " (Corporate Event)",
                Body = details,
                StartTime = corporateEvent.Date.Add(corporateEvent.StartTime ?? new TimeSpan(0, 0, 0)),
                EndTime = corporateEvent.Date.Add(corporateEvent.EndTime ?? new TimeSpan(0, 0, 0))
            };

            try
            {
                var itemId = _office365Service.CreateEvent(pEvent.StartTime, pEvent.EndTime, pEvent.Subject, pEvent.Body, pEvent.IsAllDay);
                corporateEvent.EventId = itemId;
                _corporateBookingRepository.Update(corporateEvent, corporateEvent.Id);
            }
            catch (Exception exception)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }
        }

        public async Task RemoveAsync(Guid id)
        {
            var corporateEvent = _corporateBookingRepository.Get(id);
            if (corporateEvent != null) { await _corporateBookingRepository.DeleteAsync(corporateEvent); }
        }

        public async Task CancelAsync(Guid id)
        {
            var corporateEvent = await _corporateBookingRepository.GetAsync(id);
            if (corporateEvent == null) return;

            corporateEvent.Status = OrderStatus.Cancelled;
            await _corporateBookingRepository.UpdateAsync(corporateEvent, corporateEvent.Id);

            try { _office365Service.DeleteEvent(corporateEvent.EventId); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }
    }
}