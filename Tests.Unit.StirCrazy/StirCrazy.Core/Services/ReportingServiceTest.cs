﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services;

namespace Tests.Unit.StirCrazy.StirCrazy.Core.Services
{
    [TestFixture]
    public class ReportingServiceTest
    {
        private Mock<IRepository<AnnualPassOrder>> _annuaPassOrderRepositoryMock;
        private Mock<IRepository<BakeryOrder>> _bakeryOrderRepositoryMock;
        private Mock<IRepository<GrabNGoOrder>> _grabNGoOrderRepositoryMock;
        private Mock<IRepository<RoomOrder>> _roomOrderRepositoryMock;
        private Mock<IRepository<CorporateEvent>> _corporateEventRepositoryMock;
        private Mock<IRepository<PartyOrder>> _partyOrderRepositoryMock;

        private IReportingService _reportingService;

        [SetUp]
        public void SetUp()
        {
            _annuaPassOrderRepositoryMock = new Mock<IRepository<AnnualPassOrder>>();
            _bakeryOrderRepositoryMock = new Mock<IRepository<BakeryOrder>>();
            _grabNGoOrderRepositoryMock = new Mock<IRepository<GrabNGoOrder>>();
            _roomOrderRepositoryMock = new Mock<IRepository<RoomOrder>>();
            _partyOrderRepositoryMock = new Mock<IRepository<PartyOrder>>();
            _corporateEventRepositoryMock = new Mock<IRepository<CorporateEvent>>();

            _reportingService = new ReportingService(_partyOrderRepositoryMock.Object, _bakeryOrderRepositoryMock.Object, _roomOrderRepositoryMock.Object,
                                                    _grabNGoOrderRepositoryMock.Object, _corporateEventRepositoryMock.Object, _annuaPassOrderRepositoryMock.Object);
        }
    }
}
