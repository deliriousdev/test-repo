using System.Collections.Generic;
using StirCrazy.Core.Domain;

namespace StirCrazy.Core.Services.Store
{
    public interface IStoreService
    {
        IEnumerable<StoreHour> GetStoreHours();
        StoreHour GetStoreHour(int id);
        StoreHour Add(StoreHour storeHour);
        StoreHour Update(StoreHour storeHour);
        void RemoveStoreHour(int id);
        IEnumerable<StoreHoliday> GetStoreHolidays();
        StoreHoliday GetStoreHoliday(int id);
        StoreHoliday Add(StoreHoliday storeHoliday);
        StoreHoliday Update(StoreHoliday storeHoliday);
        void RemoveStoreHoliday(int id);
    }
}