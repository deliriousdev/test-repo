﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain.Orders
{
    public class RoomOrder
    {
        [Key]
        public Guid Id { get; set; }
        public string EventId { get; set; }
        public Guid? PromotionCodeId { get; set; }
        public int PaymentTransactionId { get; set; }
        public int RoomBookingId { get; set; }
        public string CustomerId { get; set; }
        public string Notes { get; set; }
        public decimal Discount { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string Promotion { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }
        public virtual ApplicationUser Customer { get; set; }
        public virtual PaymentTransaction PaymentTransaction { get; set; }
        public virtual RoomBooking RoomBooking { get; set; }
        public virtual PromotionCode PromotionCode { get; set; }
    }
}
