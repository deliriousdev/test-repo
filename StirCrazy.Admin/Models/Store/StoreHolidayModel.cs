﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Admin.Models.Store
{
    public class StoreHolidayModel
    {
        public int Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public string Holiday { get; set; }
        public string OpenTime { get; set; }
        public string ClosedTime { get; set; }
        public bool IsClosed { get; set; }
        public bool AllowScheduling { get; set; }
    }
}