﻿using System.Linq;
using System.Web.Mvc;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Services;
using StirCrazy.Models.Facility;

namespace StirCrazy.Controllers
{
    public class FacilityController : Controller
    {
        private readonly FacilityModel _model;
        public FacilityController(IMediaService mediaService)
        {
            IMediaService mediaService1 = mediaService;

            var facilityGallery = mediaService1.Get(UploadFolder.Facility).ToList();
            var partyRooms = mediaService1.Get(UploadFolder.PartyRooms).ToList();
            var concessionCounter = mediaService1.Get(UploadFolder.ConcessionCounter).ToList();
            var parentTotLounge = mediaService1.Get(UploadFolder.ParentTotLounge).ToList();

            var model = new FacilityModel()
            {
                Facility = facilityGallery,
                PartyRooms = partyRooms,
                ConcessionCounter = concessionCounter,
                ParentTotLounge = parentTotLounge
            };

            _model = model;
        }

        public ActionResult Index() { return View(_model); }
        public ActionResult ConcessionCounter() { return View(_model); }
        public ActionResult ParentTot() { return View(_model); }
        public ActionResult Rules() { return View(_model); }
    }
}