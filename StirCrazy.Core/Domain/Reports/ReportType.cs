﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StirCrazy.Core.Domain.Reports
{
    public enum ReportType
    {
        BakeryReport,
        OrderReport
    }
}
