﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Core.Services.Order
{
    public class GrabNGoOrderService : IGrabNGoOrderService
    {
        private readonly IRepository<GrabNGoOrder> _grabNGoOrderRepository;
        private readonly IAccountService _accountService;
        private readonly ISendGridService _sendGridService;

        public GrabNGoOrderService(IRepository<GrabNGoOrder> grabNGoOrderRepository, 
            IAccountService accountService, 
            ISendGridService sendGridService)
        {
            _grabNGoOrderRepository = grabNGoOrderRepository;
            _accountService = accountService;
            _sendGridService = sendGridService;
        }

        public IEnumerable<GrabNGoOrder> Get()
        {
            return _grabNGoOrderRepository.Get();
        }

        public async Task<GrabNGoOrder> GetAsync(Guid id)
        {
            return await _grabNGoOrderRepository.GetAsync(id);
        }

        public IEnumerable<GrabNGoOrder> GetByStatus(OrderStatus status)
        {
            if (status == OrderStatus.Created)
                return _grabNGoOrderRepository.Get().Where(x => x.PickUpDate.Date >= DateTime.Today);

            if (status == OrderStatus.Cancelled)
                return _grabNGoOrderRepository.Get().Where(x => x.Status == OrderStatus.Cancelled);

            return _grabNGoOrderRepository.Get();
        }

        public async Task<GrabNGoOrder> CreateAsync(GrabNGoOrder grabNGoOrder)
        {
            var account = await _accountService.FindByEmailAsync(grabNGoOrder.Customer.Email);

            // Format Data
            grabNGoOrder.Customer.UserName = grabNGoOrder.Customer.Email.ToLower();
            grabNGoOrder.Customer.FirstName = HtmlHelperExtensions.ToTitleCase(grabNGoOrder.Customer.FirstName);
            grabNGoOrder.Customer.LastName = HtmlHelperExtensions.ToTitleCase(grabNGoOrder.Customer.LastName);
            grabNGoOrder.Customer.Address = HtmlHelperExtensions.ToTitleCase(grabNGoOrder.Customer.Address);
            grabNGoOrder.Customer.City = HtmlHelperExtensions.ToTitleCase(grabNGoOrder.Customer.City);
            grabNGoOrder.Customer.PostalCode = grabNGoOrder.Customer.PostalCode.ToUpper();
            grabNGoOrder.Customer.Province = grabNGoOrder.Customer.Province;
            grabNGoOrder.Customer.PhoneNumber = HtmlHelperExtensions.PhoneNumber(grabNGoOrder.Customer.PhoneNumber);
            grabNGoOrder.Customer.Email = grabNGoOrder.Customer.Email.ToLower();

            if (account != null)
            {
                // Update Account Information
                account.UserName = grabNGoOrder.Customer.Email;
                account.FirstName = grabNGoOrder.Customer.FirstName;
                account.LastName = grabNGoOrder.Customer.LastName;
                account.Address = grabNGoOrder.Customer.Address;
                account.City = grabNGoOrder.Customer.City;
                account.PostalCode = grabNGoOrder.Customer.PostalCode;
                account.Province = grabNGoOrder.Customer.Province;
                account.PhoneNumber = grabNGoOrder.Customer.PhoneNumber;
                account.Email = grabNGoOrder.Customer.Email;

                grabNGoOrder.CustomerId = account.Id;
                grabNGoOrder.Customer = null;

                await _accountService.UpdateAsync(account);
            }

            grabNGoOrder.DateCreated = DateTime.Now;
            grabNGoOrder.CreatedBy = grabNGoOrder.CreatedBy ?? "Customer";

            var order = await _grabNGoOrderRepository.AddAsync(grabNGoOrder);

            // Setup Account Access
            if (account == null)
            {
                _accountService.SetPassword(order.Customer.Id);
                _accountService.AddToRole(order.Customer.Id, UserRoles.Customer);
            }

            return order;
        }

        public async Task UpdateAsync(GrabNGoOrder grabNGoOrder)
        {
            if (grabNGoOrder != null)
            {
                await _grabNGoOrderRepository.UpdateAsync(grabNGoOrder, grabNGoOrder.Id);
            }
        }

        public async Task SendEmailAsync(string url, Guid id, string email)
        {
            await _sendGridService.SendEmail(url, id, email, ConfirmationType.GrabNGoOrder);
        }

        public async Task CancelAsync(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderRepository.GetAsync(id);
            if (grabNGoOrder != null)
            {
                grabNGoOrder.Status = OrderStatus.Cancelled;
                await _grabNGoOrderRepository.UpdateAsync(grabNGoOrder, grabNGoOrder.Id);
            }
        }

        public async Task UpdateStatusAsync(Guid id, OrderStatus status)
        {
            var grabNGoOrder = await _grabNGoOrderRepository.GetAsync(id);
            if (grabNGoOrder != null)
            {
                grabNGoOrder.Status = status;
                await _grabNGoOrderRepository.UpdateAsync(grabNGoOrder, grabNGoOrder.Id);
            }
        }

        public async Task SendOrderNotificationAsync(GrabNGoOrder grabNGoOrder)
        {
            if (grabNGoOrder == null) return;

            var email = ConfigurationManager.AppSettings["GrabNGoOrderNotifications"];

            var pickupDate = grabNGoOrder.PickUpDate.Date.ToString("ddd MMM dd, yyyy");
            var pickupTime = grabNGoOrder.PickUpTime.GetDisplayName();
            var formattedPhoneNumber = HtmlHelperExtensions.PhoneNumber(grabNGoOrder.Customer.PhoneNumber);
            var promotionCode = (grabNGoOrder.PromotionCode != null) ? grabNGoOrder.PromotionCode.Code : "";

            if (grabNGoOrder.PromotionCode != null)
            {
                promotionCode = string.Format("{0} - {1}", grabNGoOrder.PromotionCode.Code, grabNGoOrder.PromotionCode.Description);
            }

            var mail = new SendGridMessage
            {
                Subject = "Grab N Go Order Notification",
                Html = "<p>"
                + "<h2>Order Information</h2>"
                + "Pickup Date: <strong>" + pickupDate + "</strong><br />"
                + "Pickup Time: <strong>" + pickupTime + "</strong><br />"
                + "<br /><h2>Customer Information</h2>"
                + "Full Name: <strong>" + grabNGoOrder.Customer.FullName + "</strong><br />"
                + "Address: <strong>" + grabNGoOrder.Customer.FormattedAddress + "</strong><br />"
                + "Phone: <strong>" + formattedPhoneNumber + "</strong><br />"
                + "Email: <strong>" + grabNGoOrder.Customer.Email + "</strong><br />"
                + "<br /><h2>Payment Information</h2>"
                + "Transaction ID: <strong>" + grabNGoOrder.PaymentTransaction.TransactionNumber + "</strong><br />"
                + "Amount: <strong>$" + grabNGoOrder.PaymentTransaction.Amount + "</strong><br />"
                + "Promotion: <strong>" + promotionCode + "</strong><br />"
                + "Status: <strong>" + grabNGoOrder.PaymentTransaction.PaymentStatus.ToUpper() + "</strong>"
                + "</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            try { await _sendGridService.Send(mail); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }

        public async Task RemoveAsync(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderRepository.GetAsync(id);
            if (grabNGoOrder == null) return;

            grabNGoOrder.Status = OrderStatus.Cancelled;
            await _grabNGoOrderRepository.UpdateAsync(grabNGoOrder, grabNGoOrder.Id);
        }

        public byte[] GetPdfBytes(string url)
        {
            return _sendGridService.GetPdfBytes(url);
        }
    }
}