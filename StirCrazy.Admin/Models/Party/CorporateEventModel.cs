﻿using System;
using StirCrazy.Core;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Admin.Models.Party
{
    public class CorporateEventModel
    {
        public Guid Id { get; set; }
        public string CustomerId { get; set; }
        public DateTime Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool IsAllDay { get; set; }
        public string Notes { get; set; }
        public Decimal Total { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }
        public ApplicationUser Customer { get; set; }
    }
}