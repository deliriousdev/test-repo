﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StirCrazy.Core.Domain;

namespace StirCrazy.Core
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        public string BusinessName { get; set; }
        public string Address { get; set; }

        public string FormattedAddress
        {
            get { return Address + " " + City + ", " + Province + " " + Province + " " + PostalCode; }
        }

        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public bool EmailConsent { get; set; }
        public virtual ICollection<Child> Children { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}