﻿using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain.Orders
{
    public enum OrderType
    {
        [Display(Name = "Party Orders")]
        Party,
        [Display(Name = "Room Rentals")]
        Room,
        [Display(Name = "Bakery Orders")]
        Bakery,
        [Display(Name = "Grab N Go Orders")]
        GrabNGo,
        [Display(Name = "Corporate Events")]
        CorporateEvent,
        [Display(Name = "Annual Passes")]
        AnnualPass        
    }
}
