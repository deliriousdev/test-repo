﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StirCrazy.Core;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Admin.Models.Account
{
    public class UserRolesModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Fullname { get; set; }

        public List<SelectRoleEditorViewModel> Roles { get; set; }

        public UserRolesModel()
        {
            Roles = new List<SelectRoleEditorViewModel>();
        }

        public UserRolesModel(ApplicationUser user)
            : this()
        {
            Id = user.Id;
            UserName = user.UserName;
            Fullname = user.FullName;

            var database = new DatabaseContext();

            // Add all available roles to the list of EditorViewModels:
            var allRoles = database.Roles;
            foreach (var role in allRoles)
            {
                // An EditorViewModel will be used by Editor Template:
                var rvm = new SelectRoleEditorViewModel(role);
                this.Roles.Add(rvm);
            }
            var rm = new RoleManager<IdentityRole>(
        new RoleStore<IdentityRole>(new DatabaseContext()));



            // Set the Selected property to true for those roles for 
            // which the current user is a member:
            foreach (var userRole in user.Roles)
            {
                var role = rm.Roles.FirstOrDefault(x => x.Id == userRole.RoleId);

                var checkUserRole =
                    Roles.Find(r => r.RoleName == role.Name);
                checkUserRole.Selected = true;
            }
        }

    }

    public class RoleEditorModel
    {
        public RoleEditorModel() { }
        public RoleEditorModel(IdentityRole role)
        {
            RoleName = role.Name;
        }

        public bool Selected { get; set; }

        [Required]
        public string RoleName { get; set; }
    }
}
