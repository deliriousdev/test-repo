﻿using System.Collections.Generic;
using System.Linq;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services.Party
{
    public class PartyAddonService : IPartyAddonService
    {
        private readonly IRepository<Addon> _addonRepository;
        private readonly IRepository<PartyOrderAddon> _partyAddonRepository;

        public PartyAddonService(IRepository<Addon> addonRepository, IRepository<PartyOrderAddon> partyAddonRepository)
        {
            _addonRepository = addonRepository;
            _partyAddonRepository = partyAddonRepository;
        }

        public IEnumerable<PartyOrderAddon> Get()
        {
            return _partyAddonRepository.Get();
        }

        public PartyOrderAddon Get(int id)
        {
            return _partyAddonRepository.Get(id);
        }

        public PartyOrderAddon Add(PartyOrderAddon orderAddon)
        {
            return _partyAddonRepository.Add(orderAddon);
        }

        public PartyOrderAddon Update(PartyOrderAddon orderAddon)
        {
            return _partyAddonRepository.Update(orderAddon, orderAddon.Id);
        }

        public IEnumerable<Addon> GetAddons()
        {
            return _addonRepository.Get().OrderBy(x => x.Index);
        }

        public IEnumerable<Addon> GetAddons(AddonType addonType)
        {
            return _addonRepository.Get().Where(x => x.AddonType == addonType);
        }

        public Addon GetAddon(int id)
        {
            return _addonRepository.Get(id);
        }

        public Addon Add(Addon addon)
        {
            return _addonRepository.Add(addon);
        }

        public Addon Update(Addon addon)
        {
            return _addonRepository.Update(addon, addon.Id);
        }

        public void ReIndex(int id, int order)
        {
            var addon = GetAddon(id);
            if (addon == null) return;

            addon.Index = order;
            _addonRepository.Update(addon, addon.Id);
        }
    }
}
