﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain.Calendar
{
    public class CalendarItem
    {
        public string ItemId { get; set; }
        public string Body { get; set; }
        public string Organizer { get; set; }
        public string Subject { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public List<AppointmentAttendee> Attendees { get; set; }
        public bool IsAllDay { get; set; }
    }
    public class AppointmentAttendee
    {
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public AppointmentAttendee(string szName, string szEmailAddress)
        {
            Name = szName;
            EmailAddress = szEmailAddress;
        }
    }

    public class CalendarEvent
    {
        [Key]
        public Guid Id { get; set; }
        public string ItemId { get; set; }
        public string Body { get; set; }
        public string Organizer { get; set; }
        public string Subject { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsAllDay { get; set; }
    }
}
