﻿namespace StirCrazy.Core.Domain
{
    public enum ConfirmationType
    {
        AnnualPassOrder,
        BakeryOrder,
        PartyOrder,
        RoomOrder,
        GrabNGoOrder
    }
}
