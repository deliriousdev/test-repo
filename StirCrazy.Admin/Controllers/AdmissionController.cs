﻿using System;
using System.Linq;
using System.Web.Mvc;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Services;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class AdmissionController : Controller
    {
        private readonly IAppSettingService _appSettingService;

        public AdmissionController(IAppSettingService appSettingService)
        {
            _appSettingService = appSettingService;
        }

        // GET: Admission
        public ActionResult Index()
        {
            var admissionRates = _appSettingService.Get().Where(x => x.Type == MetaType.AdmissionRate).ToList();
            return View(admissionRates);
        }

        // GET: Admission/Details/5
        public ActionResult Details(Guid id)
        {
            var model = _appSettingService.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: Admission/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admission/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AppSetting model)
        {
            if (ModelState.IsValid)
            {
                model.Type = MetaType.AdmissionRate;
                _appSettingService.Add(model);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Admission/Edit/5
        public ActionResult Edit(Guid id)
        {
            var model = _appSettingService.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Admission/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AppSetting model)
        {
            if (ModelState.IsValid)
            {
                model.Type = MetaType.AdmissionRate;
                _appSettingService.Update(model);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Admission/Delete/5
        public ActionResult Delete(Guid id)
        {
            var model = _appSettingService.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Admission/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            _appSettingService.Remove(id);
            return RedirectToAction("Index");
        }
    }
}
