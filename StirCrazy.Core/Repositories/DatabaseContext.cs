﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Calendar;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Repositories
{
    public class DatabaseContext : IdentityDbContext<ApplicationUser>
    {
        public DatabaseContext()
            : base("name=StirCrazyContext")
        {
            Database.SetInitializer<DatabaseContext>(null);
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
        }

        public IDbSet<AppSetting> AppSettings { get; set; }
        public IDbSet<CalendarEvent> CalendarEvents { get; set; }
        public IDbSet<RoomBooking> PartyBookings { get; set; }
        public IDbSet<PartyPackage> PartyPackages { get; set; }
        public IDbSet<PackageAddon> PackageAddons { get; set; }
        public IDbSet<Addon> Addons { get; set; }
        public IDbSet<AnnualPassOrder> AdmissionOrders { get; set; }
        public IDbSet<StoreHour> StoreHours { get; set; }
        public IDbSet<StoreHoliday> StoreHolidays { get; set; }
        public IDbSet<Announcement> Announcements { get; set; }
        public IDbSet<CorporateEvent> CorporateEvents { get; set; }
        public IDbSet<PartyRoom> PartyRooms { get; set; }
        public IDbSet<PromotionCode> Promotions { get; set; }
        public IDbSet<MediaFile> MediaFiles { get; set; }
        public IDbSet<TimeSlot> TimeSlots { get; set; }
        public IDbSet<PaymentTransaction> PayPalTransactions { get; set; }
        public IDbSet<PartyOrder> PartyOrders { get; set; }
        public IDbSet<GrabNGoOrder> GrabNGoOrders { get; set; }
        public IDbSet<BakeryOrder> BakeryOrders { get; set; }
        public IDbSet<RoomOrder> RoomOrders { get; set; } 
        public IDbSet<Child> Children { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppSetting>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<CalendarEvent>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<PartyOrder>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<PromotionCode>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<RoomOrder>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<GrabNGoOrder>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<BakeryOrder>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<CorporateEvent>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<AnnualPassOrder>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityUser>()
                .ToTable("Users");

            modelBuilder.Entity<ApplicationUser>()
                .ToTable("Users");

            modelBuilder.Entity<IdentityUser>()
            .HasMany(u => u.Roles)
            .WithOptional()
            .HasForeignKey(r => r.UserId);

            modelBuilder.Entity<IdentityUser>()
                        .HasMany(u => u.Logins)
                        .WithOptional()
                        .HasForeignKey(l => l.UserId);

            modelBuilder.Entity<IdentityUser>()
                        .HasMany(u => u.Claims)
                        .WithOptional()
                        .HasForeignKey(c => c.UserId);
        }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }
}

