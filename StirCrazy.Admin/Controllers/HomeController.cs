﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Dashboard;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IReportingService _reportingService;
        private readonly IPromotionCodeService _promotionCodeService;
        private readonly ICorporateEventService _corporateEventService;
        private readonly IRoomOrderService _roomOrderService;
        private readonly ICorporateEventService _corpEventService;
        private readonly IPartyOrderService _partyOrderService;
        private readonly IOffice365Service _office365Service;

        public HomeController(IReportingService reportingService, IPromotionCodeService promotionCodeService, ICorporateEventService corporateEventService, IRoomOrderService roomOrderService, IOffice365Service office365Service, IPartyOrderService partyOrderService, ICorporateEventService corpEventService)
        {
            _reportingService = reportingService;
            _promotionCodeService = promotionCodeService;
            _corporateEventService = corporateEventService;
            _roomOrderService = roomOrderService;
            _partyOrderService = partyOrderService;
            _corpEventService = corpEventService;
            _office365Service = new Office365Service("scheduling@stircrazy.ca");
        }

        public ActionResult Index()
        {
            var model = new OverviewModel()
            {
                BakeryOrders = _reportingService.GetBakeryOrders(DateTime.Now.Date, DateTime.Now.AddYears(2).Date),
                PartyOrders = _reportingService.GetPartyOrders(DateTime.Now.Date, DateTime.Now.AddYears(2).Date),
                GrabNGoOrders = _reportingService.GetGrabNGoOrders(DateTime.Now.Date, DateTime.Now.AddYears(2).Date),
                RoomRentals = _reportingService.GetRoomOrders(DateTime.Now.Date, DateTime.Now.AddYears(2).Date),
                CorporateEvents = _reportingService.GetCorporateEvents(DateTime.Now.Date, DateTime.Now.AddYears(2).Date),
                AnnualPassOrders = _reportingService.GetAnnualPassOrders(DateTime.Now.Date, DateTime.Now.AddYears(2).Date)
            };

            var list = new List<OrderViewModel>();

            list.AddRange(model.AnnualPassOrders.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Type = OrderType.AnnualPass,
                Customer = x.Parent,
                OrderDate = x.PickUpDate.Date,
                ScheduledDate = x.PickUpDate.ToString("MMM dd, yyyy"),
                ScheduledTime = x.PickUpTime.GetDisplayName(),
                Transaction = x.PaymentTransaction,
                Status = x.Status,
                Total = x.Total
            }));

            list.AddRange(model.BakeryOrders.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Type = OrderType.Bakery,
                Customer = x.Customer,
                OrderDate = x.PickUpDate.Date,
                ScheduledDate = x.PickUpDate.ToString("MMM dd, yyyy"),
                ScheduledTime = x.PickUpTime.GetDisplayName(),
                Transaction = x.PaymentTransaction,
                Status = x.Status,
                Total = x.Total
            }));

            list.AddRange(model.GrabNGoOrders.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Type = OrderType.GrabNGo,
                Customer = x.Customer,
                OrderDate = x.PickUpDate.Date,
                ScheduledDate = x.PickUpDate.ToString("MMM dd, yyyy"),
                ScheduledTime = x.PickUpTime.GetDisplayName(),
                Transaction = x.PaymentTransaction,
                Status = x.Status,

                Total = x.Total
            }));

            foreach (var roomRental in model.RoomRentals)
            {
                var roomOrder = new OrderViewModel()
                {
                    Id = roomRental.Id.ToString(),
                    Customer = roomRental.Customer,
                    OrderDate = roomRental.RoomBooking.Date.Date,
                    ScheduledDate = roomRental.RoomBooking.Date.ToString("MMM dd, yyyy"),
                    ScheduledTime = DateTime.Today.Add(roomRental.RoomBooking.TimeSlot.StartTime).ToString("h:mm tt")
                                + " - " + DateTime.Today.Add(roomRental.RoomBooking.TimeSlot.EndTime).ToString("h:mm tt"),
                    Transaction = roomRental.PaymentTransaction,
                    Booking = roomRental.RoomBooking,
                    Status = roomRental.Status,
                    Type = OrderType.Room,
                    Total = roomRental.Total,
                };

                list.Add(roomOrder);
            }

            foreach (var party in model.PartyOrders)
            {
                var partyOrder = new OrderViewModel()
                {
                    Id = party.Id.ToString(),
                    Customer = party.Parent,
                    OrderDate = party.RoomBooking.Date.Date,
                    ScheduledDate = party.RoomBooking.Date.ToString("MMM dd, yyyy"),
                    ScheduledTime = DateTime.Today.Add(party.RoomBooking.TimeSlot.StartTime).ToString("h:mm tt")
                                + " - " + DateTime.Today.Add(party.RoomBooking.TimeSlot.EndTime).ToString("h:mm tt"),
                    Transaction = party.PaymentTransaction,
                    Booking = party.RoomBooking,
                    Package = party.PartyPackage,
                    Status = party.Status,
                    Type = OrderType.Party,
                    Total = party.Total,
                };

                list.Add(partyOrder);
            }

            foreach (var corp in model.CorporateEvents)
            {
                var startTime = DateTime.ParseExact(corp.StartTime.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
                var endTime = DateTime.ParseExact(corp.EndTime.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);

                var partyOrder = new OrderViewModel()
                {
                    Id = corp.Id.ToString(),
                    Customer = corp.Customer,
                    OrderDate = corp.Date.Date,
                    ScheduledDate = corp.Date.ToString("MMM dd, yyyy"),
                    ScheduledTime = startTime.ToString("h:mm tt") + " - " + endTime.ToString("h:mm tt"),
                    Status = corp.Status,
                    Type = OrderType.CorporateEvent,
                    Total = 0.00m,
                };

                list.Add(partyOrder);
            }

            list = new List<OrderViewModel>(list.OrderBy(x => x.ScheduledDate));
            return View(list);
        }

        [HttpPost]
        public JsonResult PromotionCode(string promoCode, PromotionType promoType)
        {
            var response = _promotionCodeService.Get(promoCode.ToUpper(), promoType);

            // INVALID
            if (response == null)
            {
                return Json(new PromotionCode()
                {
                    Description = "* Promotion Code Invalid *"
                }, JsonRequestBehavior.DenyGet);
            }

            // EXPIRED
            if (response.Expired < DateTime.Now)
            {
                return Json(new PromotionCode()
                {
                    Description = "* Promotion Code Has Expired *"
                }, JsonRequestBehavior.DenyGet);
            }

            return Json(response, JsonRequestBehavior.DenyGet);
        }

        //public ActionResult SyncOutlook()
        //{
        //    var outlookEvents = _office365Service.GetEvents().ToList();

        //    foreach (var outlookEvent in outlookEvents)
        //    {
        //        _office365Service.DeleteEvent(outlookEvent.ItemId);
        //    }

        //    var roomOrders = _roomOrderService.Get().Where(x => x.Status == OrderStatus.Paid).ToList();

        //    foreach (var roomOrder in roomOrders)
        //    {
        //        _roomOrderService.SendNotificationAsync(roomOrder.Id);
        //    }

        //    var partyOrders = _partyOrderService.Get().Where(x => x.Status == OrderStatus.Paid).ToList();

        //    foreach (var partyOrder in partyOrders)
        //    {
        //        _partyOrderService.SendNotificationAsync(partyOrder.Id);
        //    }

        //    var corpEvents = _corporateEventService.Get().Where(x => x.Status == OrderStatus.Paid).ToList();

        //    foreach (var corpEvent in corpEvents)
        //    {
        //        _corporateEventService.SendNotificationAsync(corpEvent.Id);
        //    }

        //    return RedirectToAction("Index");
        //}

        //public ActionResult _SharedFooter()
        //{
        //    var model = new TotalsModel()
        //    {
        //        YearToDate = _reportingService.GetYearToDate(),
        //        OrderTotal = _reportingService.GetPartyOrderYTD()
        //    };

        //    return PartialView(model);
        //}
    }
}