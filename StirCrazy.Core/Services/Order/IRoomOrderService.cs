using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Services.Order
{
    public interface IRoomOrderService
    {
        IEnumerable<RoomOrder> Get();
        IEnumerable<RoomOrder> GetByStatus(OrderStatus status);
        Task<RoomOrder> GetAsync(Guid id);
        Task<RoomOrder> CreateAsync(RoomOrder roomOrder);
        Task UpdateAsync(RoomOrder roomOrder);
        Task UpdateStatusAsync(Guid id, OrderStatus status);
        Task SendEmailAsync(string baseUri, Guid id, string email);
        Task SendNotificationAsync(Guid id);
        Task RemoveAsync(Guid id);
        Task CancelAsync(Guid id);
        byte[] GetPdfBytes(string url);
    }
}