﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services.Party
{
    public class PartyRoomService : IPartyRoomService
    {
        private readonly IRepository<PartyRoom> _partyRoomRepository;

        public PartyRoomService(IRepository<PartyRoom> partyRoomRepository)
        {
            _partyRoomRepository = partyRoomRepository;
        }

        public List<PartyRoom> Get()
        {
            return _partyRoomRepository.Get().ToList();
        }

        public PartyRoom Get(int id)
        {
            return _partyRoomRepository.Get(id);
        }

        public PartyRoom Add(PartyRoom partyRoom)
        {
            CultureInfo.InvariantCulture.TextInfo.ToTitleCase(partyRoom.Name);
            return _partyRoomRepository.Add(partyRoom);
        }

        public PartyRoom Update(PartyRoom partyRoom)
        {
            CultureInfo.InvariantCulture.TextInfo.ToTitleCase(partyRoom.Name);
            return _partyRoomRepository.Update(partyRoom, partyRoom.Id);
        }

        public void Remove(PartyRoom partyRoom)
        {
            _partyRoomRepository.Delete(partyRoom);
        }
    }
}
