﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Dashboard;
using StirCrazy.Admin.Models.Reporting;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Reports;
using StirCrazy.Core.Services;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private readonly IReportingService _reportingService;

        public ReportsController(IReportingService reportingService)
        {
            _reportingService = reportingService;
        }

        public ActionResult Generate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Generate(ReportModel model)
        {
            if (model.ReportType == ReportType.BakeryReport)
            {
                var bakeryOrders = _reportingService.GetBakeryOrders(model.StartDate, model.EndDate).ToList();
                var partyOrders = _reportingService.GetPartyOrders(model.StartDate, model.EndDate).ToList();

                return View("Print", new BakeryReportModel
                {
                    BakeryOrders = bakeryOrders,
                    PartyOrders = partyOrders
                });
            }

            return View();
        }

        public ActionResult Index()
        {
            var model = new OverviewModel()
            {
                BakeryOrders = _reportingService.GetBakeryOrders(DateTime.Now, null),
                PartyOrders = _reportingService.GetPartyOrders(DateTime.Now, null),
                GrabNGoOrders = _reportingService.GetGrabNGoOrders(DateTime.Now, null),
                RoomRentals = _reportingService.GetRoomOrders(DateTime.Now, null),
                CorporateEvents = _reportingService.GetCorporateEvents(DateTime.Now, null),
                AnnualPassOrders = _reportingService.GetAnnualPassOrders(DateTime.Now, null)
            };

            var list = new List<OrderViewModel>();

            list.AddRange(model.BakeryOrders.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Type = OrderType.Bakery,
                Customer = x.Customer,
                ScheduledDate = (x.PickUpDate.ToString("MMM dd, yyyy") + " " + x.PickUpTime.GetDisplayName()),
                Transaction = x.PaymentTransaction,
                Status = x.Status,
                Total = x.Total
            }));
            
            list.AddRange(model.AnnualPassOrders.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Type = OrderType.AnnualPass,
                Customer = x.Parent,
                ScheduledDate = (x.PickUpDate.ToString("MMM dd, yyyy") + " " + x.PickUpTime.GetDisplayName()),
                Transaction = x.PaymentTransaction,
                Status = x.Status,
                Total = x.Total
            }));

            list.AddRange(model.GrabNGoOrders.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Type = OrderType.GrabNGo,
                Customer = x.Customer,
                ScheduledDate = x.PickUpDate.ToString("MMM dd, yyyy") + " " + x.PickUpTime.GetDisplayName(),
                Transaction = x.PaymentTransaction,
                Status = x.Status,

                Total = x.Total
            }));

            list.AddRange(model.RoomRentals.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Customer = x.Customer,
                ScheduledDate = (x.RoomBooking.Date.ToString("MMM dd, yyyy") + "<br />"
                            + x.RoomBooking.TimeSlot.StartTime.ToString("h:mm tt")
                            + " - " + x.RoomBooking.TimeSlot.EndTime.ToString("h:mm tt")),
                Transaction = x.PaymentTransaction,
                Booking = x.RoomBooking,
                Status = x.Status,
                Type = OrderType.Room,
                Total = x.Total
            }));

            list.AddRange(model.CorporateEvents.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Customer = x.Customer,
                ScheduledDate = (x.Date.ToString("MMM dd, yyyy") + "<br />"
                            + x.StartTime.ToString()
                            + " - " + x.EndTime.ToString()),
                Status = x.Status,
                Type = OrderType.Room,
                Total = 0.00m
            }));

            list.AddRange(model.PartyOrders.Select(x => new OrderViewModel()
            {
                Id = x.Id.ToString(),
                Customer = x.Parent,
                ScheduledDate = (x.RoomBooking.Date.ToString("MMM dd, yyyy") + "<br />"
                            + x.RoomBooking.TimeSlot.StartTime.ToString("h:mm tt")
                            + " - " + x.RoomBooking.TimeSlot.EndTime.ToString("h:mm tt")),
                Transaction = x.PaymentTransaction,
                Booking = x.RoomBooking,
                Package = x.PartyPackage,
                Status = x.Status,
                Type = OrderType.Party,
                Total = x.Total,
                cssClass = GetCssClass((x.RoomBooking != null) ? x.RoomBooking.PartyRoomId : 0)
            }));

            list = new List<OrderViewModel>(list.OrderBy(x => x.OrderDate));
            return View(list);
        }

        public ActionResult Monthly(int id = 0)
        {
            var month = id;
            if (month == 0) month = DateTime.Now.Month;
            var year = DateTime.Now.Year;

            var firstDay = new DateTime(year, month, 1);
            var lastDay = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);

            var bakeryOrders = _reportingService.GetBakeryOrders(firstDay, lastDay);
            var partyOrders = _reportingService.GetPartyOrders(firstDay, lastDay);
            var grabNGoOrders = _reportingService.GetGrabNGoOrders(firstDay, lastDay);
            var roomRentals = _reportingService.GetRoomOrders(firstDay, lastDay);
            var corporateEvents = _reportingService.GetCorporateEvents(firstDay, lastDay);
            var annualPasses = _reportingService.GetAnnualPassOrders(firstDay, lastDay);

            var reports = new List<MonthReportModel>
            {
                new MonthReportModel()
                {
                    Month = month,
                    Type = OrderType.AnnualPass,
                    Orders = annualPasses.Count(),
                    Total = annualPasses.Sum(x => x.Total)
                },
                new MonthReportModel()
                {
                    Month = month,
                    Type = OrderType.Bakery,
                    Orders = bakeryOrders.Count(),
                    Total = bakeryOrders.Sum(x => x.Total)
                },
                new MonthReportModel()
                {
                    Month = month,
                    Type = OrderType.Party,
                    Orders = partyOrders.Count(),
                    Total = partyOrders.Sum(x => x.Total)
                },
                new MonthReportModel()
                {
                    Month = month,
                    Type = OrderType.GrabNGo,
                    Orders = grabNGoOrders.Count(),
                    Total = grabNGoOrders.Sum(x => x.Total)
                },
                new MonthReportModel()
                {
                    Month = month,
                    Type = OrderType.Room,
                    Orders = roomRentals.Count(),
                    Total = roomRentals.Sum(x => x.Total)
                },
                new MonthReportModel()
                {
                    Month = month,
                    Type = OrderType.CorporateEvent,
                    Orders = corporateEvents.Count(),
                    Total = corporateEvents.Sum(x => x.Total)
                }
            };

            var model = new MonthOverviewModel
            {
                MonthReports = reports, 
                Month = (Month) month
            };

            return View(model);
        }

        public ActionResult Packages(int id = 0)
        {
            var month = id;
            if (month == 0) month = DateTime.Now.Month;
            var year = DateTime.Now.Year;

            var firstDay = new DateTime(year, month, 1);
            var lastDay = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);

            var partyOrders = _reportingService.GetPartyOrders(firstDay, lastDay);

            var model = new PackagesReportModel()
            {
                Month = (Month)month,
                Packages = partyOrders.Select(x => x.PartyPackage).ToList()
            };

            return View(model);
        }

        private string GetCssClass(int roomId)
        {
            switch (roomId)
            {
                case 1:
                    return "rocknroll-red";

                case 2:
                    return "mellow-yellow";

                case 3:
                    return "bebop-blue";

                case 4:
                    return "outrageous-orange";

                case 5:
                    return "sublime-lime";

                case 6:
                    return "playful-purple";
            }

            return null;
        }
    }
}