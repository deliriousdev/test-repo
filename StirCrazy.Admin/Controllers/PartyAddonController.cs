﻿using System;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using StirCrazy.Admin.Models.Addons;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Services.Party;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class PartyAddonController : Controller
    {
        private readonly IPartyAddonService _partyAddonService;

        public PartyAddonController(IPartyAddonService partyAddonService)
        {
            _partyAddonService = partyAddonService;
        }

        public ActionResult Index()
        {
            var model = _partyAddonService.GetAddons().Select(x => new AddonModel()
            {
                Id = x.Id,
                AddonType = x.AddonType,
                Index = x.Index,
                Name = x.Name,
                Price = x.Price,
                IsActive = x.IsActive
            }).ToList();

            return View(model);
        }

        [HttpPost]
        public JsonResult UpdateIndex(string jsonRequest)
        {
            dynamic request = JsonConvert.DeserializeObject(jsonRequest);

            foreach (var item in request)
            {
                var id = Convert.ToInt32(item.id);
                var order = Convert.ToInt32(item.order);

                _partyAddonService.ReIndex(id, order);
            }

            return Json(request);
        }

        public ActionResult Create()
        {
            return View(new AddonModel());
        }

        [HttpPost]
        public ActionResult Create(AddonModel model)
        {
            if (ModelState.IsValid)
            {
                var addon = new Addon()
                {
                    AddonType = model.AddonType,
                    Name = model.Name,
                    Price = model.Price,
                    IsActive = model.IsActive
                };

                _partyAddonService.Add(addon);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var addon = _partyAddonService.GetAddon(id);
            var model = new AddonModel()
            {
                Id = addon.Id,
                Index = addon.Index,
                AddonType = addon.AddonType,
                Name = addon.Name,
                Price = addon.Price,
                IsActive = addon.IsActive
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AddonModel model)
        {
            if (ModelState.IsValid)
            {
                var addon = new Addon()
                {
                    Id = model.Id,
                    Index = model.Index,
                    AddonType = model.AddonType,
                    Name = model.Name,
                    Price = model.Price,
                    IsActive = model.IsActive
                };

                _partyAddonService.Update(addon);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Details(int id = 0)
        {
            var addon = _partyAddonService.GetAddon(id);
            var model = new AddonModel()
            {
                Id = addon.Id,
                Index = addon.Index,
                AddonType = addon.AddonType,
                Name = addon.Name,
                Price = addon.Price,
                IsActive = addon.IsActive
            };

            return View(model);
        }
    }
}
