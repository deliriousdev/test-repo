﻿using System.Collections.Generic;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;

namespace StirCrazy.Admin.Models.Upload
{
    public class UploadModel
    {
        public int Id { get; set; }
        public UploadFolder UploadFolder { get; set; }
        public string FilePath { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public string PhysicalPath { get; set; }

        public List<MediaFile> MediaFiles { get; set; }
    }
}