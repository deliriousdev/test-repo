﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using PayPal.Api.Payments;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Party;
using StirCrazy.Models.Order;
using StirCrazy.Models.Party;

namespace StirCrazy.Controllers
{
    public class RoomController : AsyncController
    {
        private readonly IMediaService _mediaService;
        private readonly IRoomBookingService _roomBookingService;
        private readonly IRoomOrderService _roomOrderService;
        private readonly IPartyRoomService _partyRoomService;
        private readonly IPromotionCodeService _promotionCodeService;
        private readonly IPayPalService _payPalService;

        public RoomController(IRoomBookingService roomBookingService,
            IMediaService mediaService,
            IRoomOrderService roomOrderService,
            IPayPalService payPalService,
            IPartyRoomService partyRoomService,
            IPromotionCodeService promotionCodeService)
        {
            _roomBookingService = roomBookingService;
            _mediaService = mediaService;
            _roomOrderService = roomOrderService;
            _payPalService = payPalService;
            _partyRoomService = partyRoomService;
            _promotionCodeService = promotionCodeService;
        }

        public ActionResult Index()
        {
            var model = _mediaService.Get(UploadFolder.PartyRooms);
            return View(model);
        }

        public ActionResult Availability(DateTime searchDate)
        {
            var result = _roomBookingService.GetAvailability(searchDate);
            return (!result.Any()) ? null : PartialView("_AvailabilityPartial", result);
        }

        public ActionResult Create()
        {
            var model = new RoomOrderModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(RoomOrderModel model)
        {
            if (ModelState.IsValid)
            {
                model.Customer.UserName = model.Customer.Email;

                var roomOrder = new RoomOrder()
                {
                    Customer = model.Customer,
                    RoomBooking = new RoomBooking()
                    {
                        BookingType = BookingType.RoomRental,
                        PartyRoomId = model.RoomBooking.PartyRoomId,
                        TimeSlotId = model.RoomBooking.TimeSlotId,
                        Date = model.RoomBooking.Date,
                        Status = Status.Pending
                    },
                    Promotion = (model.Promotion ?? "- None -"),
                    PaymentTransaction = new PaymentTransaction()
                    {
                        Amount = model.Total,
                        PaymentDate = DateTime.Now
                    },
                    Notes = model.Notes,
                    SubTotal = model.SubTotal,
                    Tax = model.Tax,
                    Total = model.Total,
                    Status = OrderStatus.Created
                };

                if (model.Promotion != null)
                {
                    var promoCode = _promotionCodeService.Get(model.Promotion);

                    if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Percentage)
                    {
                        roomOrder.Discount = (promoCode.Adjustment / 100) * roomOrder.Total;
                        roomOrder.PromotionCode = promoCode;
                    }

                    if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Discount)
                    {
                        roomOrder.Discount = promoCode.Adjustment;
                        roomOrder.PromotionCode = promoCode;
                    }
                }

                // BYPASS PAYPAL PAYMENT PROCESSING
                if (model.BypassPaypal)
                {
                    roomOrder.PaymentTransaction.TransactionType = model.PaymentTransaction.TransactionType;
                    roomOrder.PaymentTransaction.TransactionNumber = model.PaymentTransaction.TransactionNumber ?? "Not Applicable";
                    roomOrder.PaymentTransaction.PaymentStatus = "Approved";

                    // SAVE DATABASE
                    roomOrder.CreatedBy = "Administrator";
                    roomOrder.Status = OrderStatus.Paid;
                    var order = await _roomOrderService.CreateAsync(roomOrder);

                    // SEND NOTIFICATION
                    await _roomOrderService.SendNotificationAsync(order.Id);

                    return RedirectToAction("Email", new RouteValueDictionary { { "Id", order.Id } });
                }

                // SAVE DATABASE
                roomOrder.CreatedBy = "Customer";
                await _roomOrderService.CreateAsync(roomOrder);

                // PAYPAL PAYMENT PROCESSING
                var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Room/";
                var returnUrl = baseUri + "Payment/" + roomOrder.Id;
                var cancelUrl = baseUri + "Cancelled/" + roomOrder.Id;

                var partyRoom = _partyRoomService.Get(roomOrder.RoomBooking.PartyRoomId);
                var description = string.Format("{0} - {1} {2}", "Stir Crazy", partyRoom.Name, "Room Rental");
                return await ProcessPayment(roomOrder.Id, (double)roomOrder.Total, description, returnUrl, cancelUrl);
            }

            return View(model);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var roomOrder = await _roomOrderService.GetAsync(id);
            if (roomOrder == null) return HttpNotFound();

            var model = new RoomOrderModel()
            {
                Id = roomOrder.Id,
                Customer = roomOrder.Customer,
                RoomBooking = new RoomBookingModel()
                {
                    PartyRoom = roomOrder.RoomBooking.PartyRoom,
                    TimeSlot = roomOrder.RoomBooking.TimeSlot,
                    Date = roomOrder.RoomBooking.Date
                },
                PaymentTransaction = roomOrder.PaymentTransaction,
                Notes = roomOrder.Notes,
                SubTotal = roomOrder.SubTotal,
                Discount = roomOrder.Discount,
                Tax = roomOrder.Tax,
                Total = roomOrder.Total,
                Status = roomOrder.Status,
                Promotion = (roomOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", roomOrder.PromotionCode.Code, roomOrder.PromotionCode.Description) : ""
            };

            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> Print(Guid id)
        {
            var roomOrder = await _roomOrderService.GetAsync(id);
            if (roomOrder == null) return HttpNotFound();

            var model = new RoomOrderModel()
            {
                Id = roomOrder.Id,
                Customer = roomOrder.Customer,
                RoomBooking = new RoomBookingModel()
                {
                    PartyRoom = roomOrder.RoomBooking.PartyRoom,
                    TimeSlot = roomOrder.RoomBooking.TimeSlot,
                    Date = roomOrder.RoomBooking.Date
                },
                Notes = roomOrder.Notes,
                PaymentTransaction = roomOrder.PaymentTransaction,
                Promotion = (roomOrder.PromotionCode != null) ? roomOrder.PromotionCode.Code : "",
                Total = roomOrder.Total,
                Status = roomOrder.Status
            };

            return View(model);
        }

        private async Task<RedirectResult> ProcessPayment(Guid id, double amount, string description, string returnUrl, string cancelUrl)
        {
            if (Request.Url != null)
            {
                var payment = _payPalService.CreatePayment(amount, description, returnUrl, cancelUrl);
                if (payment != null)
                {
                    var paymentId = payment.id;
                    var paymentState = payment.state;

                    var roomOrder = await _roomOrderService.GetAsync(id);

                    if (roomOrder != null)
                    {
                        roomOrder.PaymentTransaction.Id = roomOrder.PaymentTransaction.Id;
                        roomOrder.PaymentTransaction.TransactionType = TransactionType.Paypal;
                        roomOrder.PaymentTransaction.PaymentDate = DateTime.Now;
                        roomOrder.PaymentTransaction.PaymentStatus = paymentState;
                        roomOrder.PaymentTransaction.TransactionNumber = paymentId;
                        roomOrder.PaymentTransaction.Amount = (decimal)amount;

                        await _roomOrderService.UpdateAsync(roomOrder);
                    }

                    var redirectUrl = string.Format("{0}{1}", GetApprovalUrl(payment), "&useraction=commit");
                    return new RedirectResult(redirectUrl);
                }
            }

            return null;
        }

        public async Task<ActionResult> Payment(Guid id)
        {
            var payer = Request["PayerID"];
            var roomOrder = await _roomOrderService.GetAsync(id);

            var payment = new Payment
            {
                id = roomOrder.PaymentTransaction.TransactionNumber,
                state = roomOrder.PaymentTransaction.PaymentStatus
            };

            var result = _payPalService.ExecutePayment(payment, payer);
            if (result.state == "approved")
            {
                roomOrder.Status = OrderStatus.Paid;
                roomOrder.RoomBooking.Status = Status.Scheduled;
            }

            roomOrder.PaymentTransaction.PaymentStatus = result.state;

            var bookings = _roomBookingService.Get();

            if (
                bookings.Any(
                    x =>
                        x.Date == roomOrder.RoomBooking.Date && x.TimeSlotId == roomOrder.RoomBooking.TimeSlotId &&
                        x.PartyRoomId == roomOrder.RoomBooking.PartyRoomId && x.Status == Status.Scheduled))
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception("Scheduling Availability Exception: " + roomOrder.Id));
                return RedirectToAction("Error", "Party");
            }

            await _roomOrderService.UpdateAsync(roomOrder);

            try
            {
                var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Room/";

                await _roomOrderService.SendNotificationAsync(roomOrder.Id);
                await _roomOrderService.SendEmailAsync(baseUri, roomOrder.Id, roomOrder.Customer.Email);
            }
            catch (Exception exception)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }

            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        public ActionResult Cancelled(Guid id)
        {
            _roomOrderService.RemoveAsync(id);
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult Save(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Room/";
            var detailsUrl = baseUri + "Print/" + id;

            var bytesAsPdf = _roomOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Email(Guid id)
        {
            var roomOrder = await _roomOrderService.GetAsync(id);
            if (roomOrder == null) return HttpNotFound();

            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Room/";
            await _roomOrderService.SendEmailAsync(baseUri, roomOrder.Id, roomOrder.Customer.Email); ;

            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        private string GetApprovalUrl(Payment payment)
        {
            var links = payment.links;
            return (from lnk in links where lnk.rel.ToLower().Equals("approval_url") select Server.UrlDecode(lnk.href)).FirstOrDefault();
        }
    }
}