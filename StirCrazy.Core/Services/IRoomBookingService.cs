using System;
using System.Collections.Generic;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Services
{
    public interface IRoomBookingService
    {
        IEnumerable<RoomBooking> Get();
        RoomBooking Get(int id);
        IEnumerable<TimeSlot> GetTimeSlots();
        RoomBooking Add(RoomBooking roomBooking);
        RoomBooking Update(RoomBooking roomBooking);
        void Remove(RoomBooking roomBooking);
        void Remove(int id);
        IEnumerable<Tuple<PartyRoom, TimeSlot>> GetAvailability(DateTime searchStart);
        bool CheckAvailability(DateTime searchDate, TimeSpan startTime, TimeSpan endTime);
    }
}