﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using StirCrazy.Core;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Services.Order;

namespace Tests.Unit.StirCrazy.StirCrazy.Core.Services.Order
{
    [TestFixture]
    public class PartyOrderServiceTest
    {
        private Mock<IRepository<PartyOrder>> _partyOrderRepositoryMock;
        private Mock<IRepository<PartyPackage>> _partyPackageRepositoryMock;
        private Mock<IAccountService> _accountServiceMock;
        private Mock<IRoomBookingService> _roomBookingServiceMock;
        private Mock<ISendGridService> _sendGridServiceMock;
        private Mock<IOffice365Service> _office365ServiceMock;

        private IPartyOrderService _partyOrderService;

        [SetUp]
        public void Setup()
        {
            _partyOrderRepositoryMock = new Mock<IRepository<PartyOrder>>();
            _partyPackageRepositoryMock = new Mock<IRepository<PartyPackage>>();
            _sendGridServiceMock = new Mock<ISendGridService>();
            _roomBookingServiceMock = new Mock<IRoomBookingService>();
            _office365ServiceMock = new Mock<IOffice365Service>();

            _accountServiceMock = new Mock<IAccountService>();
            _partyOrderService = new PartyOrderService(_partyOrderRepositoryMock.Object, 
                                                        _partyPackageRepositoryMock.Object, 
                                                        _accountServiceMock.Object, 
                                                        _sendGridServiceMock.Object, 
                                                        _roomBookingServiceMock.Object, 
                                                        _office365ServiceMock.Object);
        }

        [Test]
        public async void PartyOrderServiceShouldAddPartyOrderToRepository()
        {
            var roomBooking = new RoomBooking()
            {
                Date = new DateTime(2014, 08, 22), // FRIDAY!
                PartyRoomId = 2,
                TimeSlotId = 4
            };

            var partyPackage = new PartyPackage()
            {
                Id = 3,
                Name = "Package 3",
                Description = "Party Package 3",
                GuestPrice = 16m,
                Guests = 9,
                HasCake = false,
                HasPlaceSetting = false,
                MaxGuests = 18,
                PackageAddons = new List<PackageAddon>(),
                Price = 99
            };

            var partyOrder = new PartyOrder()
            {
                Id = Guid.NewGuid(),
                PartyPackageId = 3,
                Child = new Child()
                {
                    FirstName = "Child",
                    LastName = "Little",
                    Age = 4,
                    Gender = Gender.Female
                },
                PartyPackage = partyPackage,
                Parent = new ApplicationUser()
                {
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.com",
                    FirstName = "Tester",
                    LastName = "Test",
                    Address = "123 45 Ave",
                    City = "ABCVille",
                    PhoneNumber = "1-888-888-8888",
                    PostalCode = "T3J 0A4",
                    Province = "Alberta"
                },
                AdditionalGuests = 4,
                PartyAddons = new List<PartyOrderAddon>(),
                RoomBooking = roomBooking,
                Total = 363.99m
            };

            _partyPackageRepositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(partyPackage);
            _partyOrderRepositoryMock.Setup(x => x.AddAsync(It.IsAny<PartyOrder>())).Returns(Task.FromResult(partyOrder));
            _accountServiceMock.Setup(x => x.FindByEmail(It.IsAny<string>())).Returns((Func<ApplicationUser>) null);
            _accountServiceMock.Setup(x => x.SetPassword(It.IsAny<string>())).Returns(It.IsAny<IdentityResult>);
            _accountServiceMock.Setup(x => x.AddToRole(It.IsAny<string>(), UserRoles.Customer)).Returns(It.IsAny<IdentityResult>);

            var result = await _partyOrderService.CreateAsync(partyOrder);

            Assert.AreEqual(result, partyOrder);
            _partyOrderRepositoryMock.Verify(x => x.AddAsync(It.IsAny<PartyOrder>()), Times.Once);
        }

        [Test]
        public void PartyPackageOneAShouldCalculateCorrectTotal()
        {
            // PACKAGE 1 ----------------------------------------
            // 3 Additional Guests
            // 7 Additional Food Platter(s)
            // 5 Additional Dozen Cupcake(s)
            // Swap Cake
            // Upgrade Lootbags
            // Bring Own Decorations
            // --------------------------------------------------

            var roomBooking = new RoomBooking()
            {
                Date = new DateTime(2014, 08, 22), // FRIDAY!
                PartyRoomId = 2,
                TimeSlotId = 4
            };

            var partyPackage = new PartyPackage()
            {
                Id = 1,
                Name = "Package 1",
                Description = "Party Package 1",
                GuestPrice = 22.99m,
                Guests = 8,
                HasCake = false,
                HasPlaceSetting = false,
                MaxGuests = 18,
                PackageAddons = new List<PackageAddon>(),
                Price = 339.99m
            };

            var partyOrder = new PartyOrder()
            {
                Id = Guid.NewGuid(),
                PartyPackageId = 1,
                Child = new Child()
                {
                    FirstName = "Child",
                    LastName = "Little",
                    Age = 4,
                    Gender = Gender.Female
                },
                HasCupcakes = true,
                HasOwnDecor = true,
                UpgradeLootBags = true,
                PartyPackage = partyPackage,
                Parent = new ApplicationUser()
                {
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.com",
                    FirstName = "Tester",
                    LastName = "Test",
                    Address = "123 45 Ave",
                    City = "ABCVille",
                    PhoneNumber = "1-888-888-8888",
                    PostalCode = "T3J 0A4",
                    Province = "Alberta"
                },
                AdditionalGuests = 3,
                PartyAddons = new List<PartyOrderAddon>(),
                RoomBooking = roomBooking,
                Total = 740.96m
            };

            partyOrder.PartyPackage.PackageAddons.Add(new PackageAddon()
            {
                AddonType = AddonType.FoodPlatter,
                Quantity = 9,
                RequiredGuests = 9
            });

            partyOrder.PartyPackage.PackageAddons.Add(new PackageAddon()
            {
                AddonType = AddonType.Beverage,
                Quantity = 2,
                RequiredGuests = 9
            });

            // INCLUDED
            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FoodPizza, Price = 5m },
                IsIncluded = true,
                Quantity = 9
            });

            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.Beverage, Price = 3.95m },
                IsIncluded = true,
                Quantity = 2
            });

            // ADDITIONAL
            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FoodPlatter, Price = 30m },
                IsIncluded = false,
                Quantity = 7
            });

            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FlavourCupcake, Price = 20m },
                IsIncluded = false,
                Quantity = 5
            });

            _partyPackageRepositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(partyPackage);

            var total = _partyOrderService.Calculate(partyOrder);

            Assert.AreEqual(total, partyOrder.Total);
        }

        [Test]
        public void PartyPackageTwoAShouldCalculateCorrectTotal()
        {
            // PACKAGE 2 ----------------------------------------
            // 5 Additional Guests
            // 3 Additional Food Platter(s)
            // 4 Additional Dozen Cupcake(s)
            // 4 Additional Stir Crazy Loot Bags
            // Upgrade Loot Bags
            // Upgrade Balloons
            // --------------------------------------------------

            var roomBooking = new RoomBooking()
            {
                Date = new DateTime(2014, 08, 22), // FRIDAY!
                PartyRoomId = 2,
                TimeSlotId = 4
            };

            var partyPackage = new PartyPackage()
            {
                Id = 2,
                Name = "Package 2",
                Description = "Party Package 2",
                GuestPrice = 22.99m,
                Guests = 8,
                HasCake = true,
                HasPlaceSetting = false,
                MaxGuests = 18,
                PackageAddons = new List<PackageAddon>(),
                Price = 389.99m
            };

            var partyOrder = new PartyOrder()
            {
                Id = Guid.NewGuid(),
                PartyPackageId = 3,
                Child = new Child()
                {
                    FirstName = "Child",
                    LastName = "Little",
                    Age = 4,
                    Gender = Gender.Female
                },
                PartyPackage = partyPackage,
                UpgradeLatexBalloons = true,
                UpgradeLootBags = true,
                Parent = new ApplicationUser()
                {
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.com",
                    FirstName = "Tester",
                    LastName = "Test",
                    Address = "123 45 Ave",
                    City = "ABCVille",
                    PhoneNumber = "1-888-888-8888",
                    PostalCode = "T3J 0A4",
                    Province = "Alberta"
                },
                AdditionalGuests = 5,
                PartyAddons = new List<PartyOrderAddon>(),
                RoomBooking = roomBooking,
                Total = 755.90m
            };

            partyOrder.PartyPackage.PackageAddons.Add(new PackageAddon()
            {
                AddonType = AddonType.FoodPizza,
                Quantity = 9,
                RequiredGuests = 9
            });

            partyOrder.PartyPackage.PackageAddons.Add(new PackageAddon()
            {
                AddonType = AddonType.Beverage,
                Quantity = 2,
                RequiredGuests = 9
            });

            // ADDITIONAL
            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FoodPlatter, Price = 30m },
                IsIncluded = false,
                Quantity = 3
            });

            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FoodPizza, Price = 5m },
                IsIncluded = false,
                Quantity = 3
            });

            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FlavourCupcake, Price = 20m },
                IsIncluded = false,
                Quantity = 4
            });

            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.LootBagDeluxe, Price = 6.99m },
                IsIncluded = false,
                Quantity = 4
            });

            _partyPackageRepositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(partyPackage);

            var total = _partyOrderService.Calculate(partyOrder);

            Assert.AreEqual(total, partyOrder.Total);
        }

        [Test]
        public void PartyPackageThreeAShouldCalculateCorrectTotal()
        {
            // PACKAGE 3 ----------------------------------------
            // Booked On A Friday ($119.99)
            // 4 Additional Guests
            // 4 Additional Food Platter(s)
            // 3 Additional Dozen Cupcake(s)
            // --------------------------------------------------

            var roomBooking = new RoomBooking()
            {
                Date = new DateTime(2014, 08, 22), // FRIDAY!
                PartyRoomId = 2,
                TimeSlotId = 4
            };

            var partyPackage = new PartyPackage()
            {
                Id = 3,
                Name = "Package 3",
                Description = "Party Package 3",
                GuestPrice = 16m,
                Guests = 9,
                HasCake = false,
                HasPlaceSetting = false,
                MaxGuests = 18,
                PackageAddons = new List<PackageAddon>(),
                Price = 99
            };

            var partyOrder = new PartyOrder()
            {
                Id = Guid.NewGuid(),
                PartyPackageId = 3,
                Child = new Child()
                {
                    FirstName = "Child",
                    LastName = "Little",
                    Age = 4,
                    Gender = Gender.Female
                },
                PartyPackage = partyPackage,
                Parent = new ApplicationUser()
                {
                    Id = Guid.NewGuid().ToString(),
                    Email = "test@test.com",
                    FirstName = "Tester",
                    LastName = "Test",
                    Address = "123 45 Ave",
                    City = "ABCVille",
                    PhoneNumber = "1-888-888-8888",
                    PostalCode = "T3J 0A4",
                    Province = "Alberta"
                },
                AdditionalGuests = 4,
                PartyAddons = new List<PartyOrderAddon>(),
                RoomBooking = roomBooking,
                Total = 363.99m
            };

            partyOrder.PartyPackage.PackageAddons.Add(new PackageAddon()
            {
                AddonType = AddonType.FoodPizza,
                Quantity = 9,
                RequiredGuests = 9
            });

            partyOrder.PartyPackage.PackageAddons.Add(new PackageAddon()
            {
                AddonType = AddonType.Beverage,
                Quantity = 2,
                RequiredGuests = 9
            });

            // INCLUDED
            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FoodPizza, Price = 5m },
                IsIncluded = true,
                Quantity = 9
            });

            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.Beverage, Price = 3.95m },
                IsIncluded = true,
                Quantity = 2
            });

            // ADDITIONAL
            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FoodPlatter, Price = 30m },
                IsIncluded = false,
                Quantity = 2
            });

            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FoodPlatter, Price = 30m },
                IsIncluded = false,
                Quantity = 2
            });

            partyOrder.PartyAddons.Add(new PartyOrderAddon()
            {
                Addon = new Addon() { AddonType = AddonType.FlavourCupcake, Price = 20m },
                IsIncluded = false,
                Quantity = 3
            });

            _partyPackageRepositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(partyPackage);

            var total = _partyOrderService.Calculate(partyOrder);

            Assert.AreEqual(total, partyOrder.Total);
        }
    }
}
