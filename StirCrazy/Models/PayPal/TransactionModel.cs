﻿using System;

namespace StirCrazy.Models.PayPal
{
    public class TransactionModel
    {
        public Guid Id { get; set; }
        public DateTime PaymentDate { get; set; }
        public string TransactionNumber { get; set; }
        public string PaymentStatus { get; set; }
    }
}