﻿using System;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Models.Party
{
    public class RoomBookingModel
    {
        public BookingType BookingType { get; set; }
        public int PartyRoomId { get; set; }
        public int TimeSlotId { get; set; }
        public DateTime Date { get; set; }
        public virtual PartyRoom PartyRoom { get; set; }
        public virtual TimeSlot TimeSlot { get; set; }
    }
}