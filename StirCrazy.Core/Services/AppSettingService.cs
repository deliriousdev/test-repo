﻿using System;
using System.Collections.Generic;
using System.Linq;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services
{
    public class AppSettingService : IAppSettingService
    {
        private readonly IRepository<AppSetting> _appSettingRepository;

        public AppSettingService(IRepository<AppSetting> appSettingRepository)
        {
            _appSettingRepository = appSettingRepository;
        }

        public IEnumerable<AppSetting> Get()
        {
            return _appSettingRepository.Get();
        }

        public AppSetting Get(Guid id)
        {
            return _appSettingRepository.Get(id);
        }

        public IEnumerable<AppSetting> Get(MetaType metaType)
        {
            return _appSettingRepository.Get().Where(x => x.Type == metaType);
        }

        public AppSetting Get(string meta)
        {
            return _appSettingRepository.Get().FirstOrDefault(x => x.Meta == meta);
        }

        public AppSetting Add(AppSetting appSetting)
        {
            return _appSettingRepository.Add(appSetting);
        }

        public AppSetting Update(AppSetting appSetting)
        {
            return _appSettingRepository.Update(appSetting, appSetting.Id);
        }

        public void Clear(MetaType type)
        {
            var settings = _appSettingRepository.Get().Where(x => x.Type == type).ToList();

            foreach (var userSetting in settings)
            {
                Remove(userSetting.Id);
            }
        }

        public void Remove(Guid id)
        {
            var addon = _appSettingRepository.Get(id);
            _appSettingRepository.Delete(addon);
        }
    }
}
