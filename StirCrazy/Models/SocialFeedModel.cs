﻿namespace StirCrazy.Models
{
    public class SocialFeedModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string PubDate { get; set; }
        public string Author { get; set; }
        public string Link { get; set; }
    }
}