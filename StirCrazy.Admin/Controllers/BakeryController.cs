﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Addons;
using StirCrazy.Admin.Models.Order;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Party;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class BakeryController : AsyncController
    {
        private readonly IBakeryOrderService _bakeryOrderService;
        private readonly IPartyAddonService _partyAddonService;
        private readonly IPromotionCodeService _promotionCodeService;

        public BakeryController(IBakeryOrderService bakeryOrderService, 
            IPartyAddonService partyAddonService, 
            IPromotionCodeService promotionCodeService)
        {
            _bakeryOrderService = bakeryOrderService;
            _partyAddonService = partyAddonService;
            _promotionCodeService = promotionCodeService;
        }

        public ActionResult Index(OrderStatus status = OrderStatus.Paid)
        {
            var model = _bakeryOrderService.GetByStatus(status).Select(x => new BakeryModel()
            {
                Id = x.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(c => new AddonModel()
                {
                    Id = c.Id,
                    AddonType = c.AddonType,
                    Name = c.Name,
                    Price = c.Price
                }).ToList(),
                OrderAddons = x.OrderAddons.Select(y => new BakeryOrderAddonModel()
                {
                    Id = y.Id,
                    Addon = y.Addon,
                    Quantity = y.Quantity
                }).ToList(),
                CakeFlavour = x.CakeFlavour,
                CakeTheme = x.CakeTheme,
                StandardImage = x.StandardImage,
                EdibleImage = x.EdibleImage,
                Notes = x.Notes,
                Customer = x.Customer,
                Child = x.Child,
                PaymentTransaction = x.PaymentTransaction,
                PickUpDate = x.PickUpDate,
                PickUpTime = x.PickUpTime,
                Total = x.Total,
                Status = x.Status

            }).ToList();

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new BakeryModel()
            {
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = new List<BakeryOrderAddonModel>(),
                PaymentTransaction = new PaymentTransaction()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(BakeryModel model)
        {
            var bakeryOrder = new BakeryOrder()
            {
                PickUpDate = model.PickUpDate,
                PickUpTime = model.PickUpTime,
                Notes = model.Notes,
                Customer = model.Customer,
                Child = model.Child,
                CakeTheme = model.CakeTheme,
                CakeFlavour = model.CakeFlavour,
                EdibleImage = model.EdibleImage,
                StandardImage = model.StandardImage,
                OrderAddons = new List<BakeryOrderAddon>(),
                PaymentTransaction = new PaymentTransaction()
                {
                    Amount = model.Total,
                    PaymentDate = DateTime.Now,
                    TransactionType = model.PaymentTransaction.TransactionType,
                    TransactionNumber = model.PaymentTransaction.TransactionNumber ?? "Not Applicable",
                    PaymentStatus = "Paid"
                },
                SubTotal = model.SubTotal,
                Tax = model.Tax,
                Total = model.Total,
                Status = OrderStatus.Created
            };

            if (model.Promotion != null)
            {
                var promoCode = _promotionCodeService.Get(model.Promotion);

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Percentage)
                {
                    bakeryOrder.Discount = (promoCode.Adjustment / 100) * bakeryOrder.Total;
                    bakeryOrder.PromotionCode = promoCode;
                }

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Discount)
                {
                    bakeryOrder.Discount = promoCode.Adjustment;
                    bakeryOrder.PromotionCode = promoCode;
                }
            }

            bakeryOrder.Customer.UserName = model.Customer.Email;
            foreach (var orderAddon in model.OrderAddons)
            {
                if (orderAddon.IsSelected || orderAddon.Quantity > 0)
                {
                    bakeryOrder.OrderAddons.Add(new BakeryOrderAddon()
                    {
                        BakeryOrderId = bakeryOrder.Id,
                        AddonId = orderAddon.Id,
                        Addon = orderAddon.Addon,
                        Quantity = orderAddon.Quantity,
                        IsIncluded = orderAddon.IsIncluded
                    });
                }
            }

            // SAVE DATABASE
            bakeryOrder.CreatedBy = User.Identity.Name;
            await _bakeryOrderService.CreateAsync(bakeryOrder);

            return RedirectToAction("Index");
        }

        public ActionResult Details(Guid id)
        {
            var bakeryOrder = _bakeryOrderService.Get(id);
            if (bakeryOrder == null) return HttpNotFound();

            var model = new BakeryModel()
            {
                Id = bakeryOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = bakeryOrder.OrderAddons.Select(x => new BakeryOrderAddonModel()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    Quantity = x.Quantity,
                    IsIncluded = x.IsIncluded
                }).ToList(),
                CakeFlavour = bakeryOrder.CakeFlavour,
                CakeTheme = bakeryOrder.CakeTheme,
                StandardImage = bakeryOrder.StandardImage,
                EdibleImage = bakeryOrder.EdibleImage,
                Notes = bakeryOrder.Notes,
                Customer = bakeryOrder.Customer,
                Child = bakeryOrder.Child,
                PaymentTransaction = bakeryOrder.PaymentTransaction,
                PickUpDate = bakeryOrder.PickUpDate,
                PickUpTime = bakeryOrder.PickUpTime,
                SubTotal = bakeryOrder.SubTotal,
                Tax = bakeryOrder.Tax,
                Discount = bakeryOrder.Discount,
                Total = bakeryOrder.Total,
                Status = bakeryOrder.Status,
                Promotion = (bakeryOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", bakeryOrder.PromotionCode.Code, bakeryOrder.PromotionCode.Description) : "NONE"
            };

            return View(model);
        }

        public async Task<ActionResult> Print(Guid id)
        {
            var bakeryOrder = await _bakeryOrderService.GetAsync(id);
            if (bakeryOrder == null) return HttpNotFound();

            var model = new BakeryModel()
            {
                Id = bakeryOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = bakeryOrder.OrderAddons.Select(x => new BakeryOrderAddonModel()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    Quantity = x.Quantity,
                    IsIncluded = x.IsIncluded
                }).ToList(),
                CakeFlavour = bakeryOrder.CakeFlavour,
                CakeTheme = bakeryOrder.CakeTheme,
                StandardImage = bakeryOrder.StandardImage,
                EdibleImage = bakeryOrder.EdibleImage,
                Notes = bakeryOrder.Notes,
                Customer = bakeryOrder.Customer,
                Child = bakeryOrder.Child,
                PaymentTransaction = bakeryOrder.PaymentTransaction,
                PickUpDate = bakeryOrder.PickUpDate,
                PickUpTime = bakeryOrder.PickUpTime,
                SubTotal = bakeryOrder.SubTotal,
                Tax = bakeryOrder.Tax,
                Discount = bakeryOrder.Discount,
                Total = bakeryOrder.Total,
                Status = bakeryOrder.Status,
                Promotion = (bakeryOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", bakeryOrder.PromotionCode.Code, bakeryOrder.PromotionCode.Description) : "NONE"
            };

            return View(model);
        }

        public ActionResult Save(Guid id)
        {
            string baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Bakery/";
            string detailsUrl = baseUri + "Print/" + id;

            byte[] bytesAsPdf = _bakeryOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        public async Task<ActionResult> Email(Guid id)
        {
            var bakeryOrder = await _bakeryOrderService.GetAsync(id);
            if (bakeryOrder == null) return HttpNotFound();

            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Bakery/";
            await _bakeryOrderService.SendEmailAsync(baseUri, bakeryOrder.Id, bakeryOrder.Customer.Email);

            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var bakeryOrder = await _bakeryOrderService.GetAsync(id);
            if (bakeryOrder == null) return HttpNotFound();

            var model = new BakeryModel()
            {
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = bakeryOrder.OrderAddons.Select(x => new BakeryOrderAddonModel()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    Quantity = x.Quantity,
                    IsIncluded = x.IsIncluded
                }).ToList(),
                CakeFlavour = bakeryOrder.CakeFlavour,
                CakeTheme = bakeryOrder.CakeTheme,
                StandardImage = bakeryOrder.StandardImage,
                EdibleImage = bakeryOrder.EdibleImage,
                Notes = bakeryOrder.Notes,
                Customer = bakeryOrder.Customer,
                Child = bakeryOrder.Child,
                PaymentTransaction = bakeryOrder.PaymentTransaction,
                PickUpDate = bakeryOrder.PickUpDate,
                PickUpTime = bakeryOrder.PickUpTime,
                SubTotal = bakeryOrder.SubTotal,
                Tax = bakeryOrder.Tax,
                Discount = bakeryOrder.Discount,
                Total = bakeryOrder.Total,
                Status = bakeryOrder.Status,
                Promotion = (bakeryOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", bakeryOrder.PromotionCode.Code, bakeryOrder.PromotionCode.Description) : "NONE"
            };

            return View(model);
        }

        [HttpPost]
        public async Task<RedirectToRouteResult> Delete(RoomOrderModel model)
        {
            await _bakeryOrderService.RemoveAsync(model.Id);
            return RedirectToAction("Index");
        }

        public async Task<RedirectToRouteResult> Cancelled(Guid id)
        {
            await _bakeryOrderService.RemoveAsync(id);
            return RedirectToAction("Index", "Home");
        }
    }
}