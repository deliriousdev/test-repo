﻿using System;
using System.Collections.Generic;
using StirCrazy.Admin.Models.Addons;
using StirCrazy.Admin.Models.Party;
using StirCrazy.Core;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Admin.Models.Order
{
    public class PartyOrderModel
    {
        public Guid Id { get; set; }
        public int PartyPackageId { get; set; }
        public int RoomBookingId { get; set; }
        public ApplicationUser Parent { get; set; }
        public Child Child { get; set; }
        public int AdditionalGuests { get; set; }
        public int PrepaidAdmissions { get; set; }
        public List<PackageAddonModel> PackageAddons { get; set; }
        public List<PartyAddonModel> PartyAddons { get; set; }
        public List<AddonModel> Addons { get; set; }
        public List<AddonGroupModel> AddonGroups { get; set; }
        public string PartyTheme { get; set; }
        public string CakeTheme { get; set; }
        public string CakeFlavour { get; set; }
        public bool AddCake { get; set; }
        public bool AddPlaceSetting { get; set; }
        public bool HasOwnPlaceSetting { get; set; }
        public bool HasOwnCake { get; set; }
        public bool HasOwnDecor { get; set; }
        public bool HasOwnLootBag { get; set; }
        public bool HasOwnBalloons { get; set; }
        public bool HasCupcakes { get; set; }
        public bool UpgradeLootBags { get; set; }
        public bool UpgradeLatexBalloons { get; set; }
        public bool SwapFoodPlatter { get; set; }
        public string Notes { get; set; }
        public decimal Discount { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string Promotion { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }
        public PartyPackageModel PartyPackage { get; set; }
        public RoomBookingModel RoomBooking { get; set; }
        public PaymentTransaction PaymentTransaction { get; set; }
    }
}