﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Migrations
{
    public class Deployment : DbMigrationsConfiguration<DatabaseContext>
    {
        private readonly DatabaseContext _context;

        public Deployment(DatabaseContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            // Create Timeslots (Deprecated)
            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 6,
                CheckInTime = new TimeSpan(10, 0, 0),
                StartTime = new TimeSpan(10, 30, 0),
                EndTime = new TimeSpan(11, 30, 0),
                CheckOutTime = new TimeSpan(12, 0, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 7,
                CheckInTime = new TimeSpan(11, 30, 0),
                StartTime = new TimeSpan(12, 0, 0),
                EndTime = new TimeSpan(13, 0, 0),
                CheckOutTime = new TimeSpan(13, 30, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 8,
                CheckInTime = new TimeSpan(13, 0, 0),
                StartTime = new TimeSpan(13, 30, 0),
                EndTime = new TimeSpan(14, 30, 0),
                CheckOutTime = new TimeSpan(15, 0, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 9,
                CheckInTime = new TimeSpan(14, 30, 0),
                StartTime = new TimeSpan(15, 0, 0),
                EndTime = new TimeSpan(16, 0, 0),
                CheckOutTime = new TimeSpan(16, 30, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 10,
                CheckInTime = new TimeSpan(16, 0, 0),
                StartTime = new TimeSpan(16, 30, 0),
                EndTime = new TimeSpan(17, 30, 0),
                CheckOutTime = new TimeSpan(18, 0, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 11,
                CheckInTime = new TimeSpan(17, 30, 0),
                StartTime = new TimeSpan(18, 0, 0),
                EndTime = new TimeSpan(19, 0, 0),
                CheckOutTime = new TimeSpan(19, 30, 0)
            });

            // Create Timeslots
            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 1,
                CheckInTime = new TimeSpan(10, 0, 0),
                StartTime = new TimeSpan(10, 30, 0),
                EndTime = new TimeSpan(12, 0, 0),
                CheckOutTime = new TimeSpan(12, 30, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 2,
                CheckInTime = new TimeSpan(12, 0, 0),
                StartTime = new TimeSpan(12, 30, 0),
                EndTime = new TimeSpan(14, 0, 0),
                CheckOutTime = new TimeSpan(14, 30, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 3,
                CheckInTime = new TimeSpan(14, 0, 0),
                StartTime = new TimeSpan(14, 30, 0),
                EndTime = new TimeSpan(16, 0, 0),
                CheckOutTime = new TimeSpan(16, 30, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 4,
                CheckInTime = new TimeSpan(16, 0, 0),
                StartTime = new TimeSpan(16, 30, 0),
                EndTime = new TimeSpan(18, 0, 0),
                CheckOutTime = new TimeSpan(18, 30, 0)
            });

            _context.TimeSlots.AddOrUpdate(new TimeSlot
            {
                Id = 5,
                CheckInTime = new TimeSpan(18, 0, 0),
                StartTime = new TimeSpan(18, 30, 0),
                EndTime = new TimeSpan(20, 0, 0),
                CheckOutTime = new TimeSpan(20, 30, 0)
            });

            // Hours Of Operation
            _context.StoreHours.AddOrUpdate(c => c.Id, new StoreHour()
            {
                Id = 1,
                DayOfWeek = DayOfWeek.Monday,
                OpenTime = new TimeSpan(9, 30, 00),
                ClosedTime = new TimeSpan(20, 30, 00),
                IsClosed = false
            });

            _context.StoreHours.AddOrUpdate(c => c.Id, new StoreHour()
            {
                Id = 2,
                DayOfWeek = DayOfWeek.Tuesday,
                OpenTime = new TimeSpan(9, 30, 00),
                ClosedTime = new TimeSpan(20, 30, 00),
                IsClosed = false
            });

            _context.StoreHours.AddOrUpdate(c => c.Id, new StoreHour()
            {
                Id = 3,
                DayOfWeek = DayOfWeek.Wednesday,
                OpenTime = new TimeSpan(9, 30, 00),
                ClosedTime = new TimeSpan(20, 30, 00),
                IsClosed = false
            });

            _context.StoreHours.AddOrUpdate(c => c.Id, new StoreHour()
            {
                Id = 4,
                DayOfWeek = DayOfWeek.Thursday,
                OpenTime = new TimeSpan(9, 30, 00),
                ClosedTime = new TimeSpan(20, 30, 00),
                IsClosed = false
            });

            _context.StoreHours.AddOrUpdate(c => c.Id, new StoreHour()
            {
                Id = 5,
                DayOfWeek = DayOfWeek.Friday,
                OpenTime = new TimeSpan(9, 30, 00),
                ClosedTime = new TimeSpan(20, 30, 00),
                IsClosed = false
            });

            _context.StoreHours.AddOrUpdate(c => c.Id, new StoreHour()
            {
                Id = 6,
                DayOfWeek = DayOfWeek.Saturday,
                OpenTime = new TimeSpan(9, 30, 00),
                ClosedTime = new TimeSpan(20, 30, 00),
                IsClosed = false
            });

            _context.StoreHours.AddOrUpdate(c => c.Id, new StoreHour()
            {
                Id = 7,
                DayOfWeek = DayOfWeek.Sunday,
                OpenTime = new TimeSpan(10, 00, 00),
                ClosedTime = new TimeSpan(18, 00, 00),
                IsClosed = false
            });

            // Party Rooms
            _context.PartyRooms.AddOrUpdate(new PartyRoom()
            {
                Id = 1,
                Name = "Rock 'N' Roll Red",
                Capacity = 25,
                IsAvailable = true
            });

            _context.PartyRooms.AddOrUpdate(new PartyRoom()
            {
                Id = 2,
                Name = "Mellow Yellow",
                Capacity = 25,
                IsAvailable = true
            });

            _context.PartyRooms.AddOrUpdate(new PartyRoom()
            {
                Id = 3,
                Name = "Be Bop Blue",
                Capacity = 25,
                IsAvailable = true
            });

            _context.PartyRooms.AddOrUpdate(new PartyRoom()
            {
                Id = 4,
                Name = "Outrageous Orange",
                Capacity = 25,
                IsAvailable = true
            });

            _context.PartyRooms.AddOrUpdate(new PartyRoom()
            {
                Id = 5,
                Name = "Sublime Lime",
                Capacity = 30,
                IsAvailable = true
            });

            _context.PartyRooms.AddOrUpdate(new PartyRoom()
            {
                Id = 6,
                Name = "Playful Purple",
                Capacity = 25,
                IsAvailable = true
            });

            // PARTY PACKAGE 1
            _context.PartyPackages.AddOrUpdate(new PartyPackage()
            {
                Id = 1,
                Name = "Package 1",
                Description = "Party Package 1",
                Guests = 8,
                MaxGuests = 15,
                Price = 339.99m,
                GuestPrice = 22.99m,
                HasCake = true,
                HasPlaceSetting = true,
                PackageAddons = new List<PackageAddon>()
                {
                    new PackageAddon()
                    {
                        AddonType = AddonType.FoodPlatter,
                        Quantity = 2,
                        RequiredGuests = 8
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.FoodPlatter,
                        Quantity = 1,
                        RequiredGuests = 15
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.LootBag,
                        Quantity = 8,
                        RequiredGuests = 8
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.Beverage,
                        Quantity = 2,
                        RequiredGuests = 8
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.Beverage,
                        Quantity = 1,
                        RequiredGuests = 15
                    }
                }
            });

            // PARTY PACKAGE 2
            _context.PartyPackages.AddOrUpdate(new PartyPackage()
            {
                Id = 2,
                Name = "Package 2",
                Description = "Party Package 2",
                Guests = 8,
                MaxGuests = 15,
                Price = 389.99m,
                GuestPrice = 22.99m,
                HasCake = true,
                HasPlaceSetting = true,
                PackageAddons = new List<PackageAddon>()
                {
                    new PackageAddon()
                    {
                        AddonType = AddonType.FoodPlatter,
                        Quantity = 3,
                        RequiredGuests = 8
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.FoodPlatter,
                        Quantity = 1,
                        RequiredGuests = 15
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.LootBag,
                        Quantity = 8,
                        RequiredGuests = 8
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.BalloonMylar,
                        Quantity = 1,
                        RequiredGuests = 8
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.BalloonLatex,
                        Quantity = 7,
                        RequiredGuests = 8
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.Beverage,
                        Quantity = 2,
                        RequiredGuests = 8
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.Beverage,
                        Quantity = 1,
                        RequiredGuests = 15
                    }
                }
            });

            // PARTY PACKAGE 3
            _context.PartyPackages.AddOrUpdate(new PartyPackage()
            {
                Id = 3,
                Name = "Package 3",
                Description = "Party Package 3",
                Guests = 9,
                MaxGuests = 18,
                Price = 99.99m,
                GuestPrice = 16.00m,
                HasCake = false,
                HasPlaceSetting = false,
                PackageAddons = new List<PackageAddon>()
                {
                    new PackageAddon()
                    {
                        AddonType = AddonType.FoodPizza,
                        Quantity = 9,
                        RequiredGuests = 9
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.Beverage,
                        Quantity = 2,
                        RequiredGuests = 9
                    },
                    new PackageAddon()
                    {
                        AddonType = AddonType.Beverage,
                        Quantity = 1,
                        RequiredGuests = 18
                    }
                }
            });

            // BUILD YOUR OWN
            _context.PartyPackages.AddOrUpdate(new PartyPackage()
            {
                Id = 4,
                Name = "Build Your Own Party Package",
                Description = "Build Your Own Party Template",
                Guests = 0,
                MaxGuests = 18,
                Price = 100m,
                GuestPrice = 9.49m,
                HasCake = false,
                HasPlaceSetting = false
            });
        }
    }
}
