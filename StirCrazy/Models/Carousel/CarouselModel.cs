﻿using System.Collections.Generic;
using StirCrazy.Core.Domain;

namespace StirCrazy.Models.Carousel
{
    public class CarouselModel
    {
        public List<MediaFile> CarouselImages { get; set; }
    }
}