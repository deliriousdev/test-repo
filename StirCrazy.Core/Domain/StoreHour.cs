﻿using System;

namespace StirCrazy.Core.Domain
{
    public class StoreHour
    {
        public int Id { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public TimeSpan OpenTime { get; set; }
        public TimeSpan ClosedTime { get; set; }
        public bool IsClosed { get; set; }
    }
}
