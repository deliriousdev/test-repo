using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Services.Order
{
    public interface IPartyOrderService
    {
        IEnumerable<PartyOrder> Get();
        IEnumerable<PartyOrder> GetByStatus(OrderStatus status);
        Task<PartyOrder> GetAsync(Guid id);
        Task<PartyOrder> CreateAsync(PartyOrder partyOrder);
        Task UpdateAsync(PartyOrder partyOrder);
        Task UpdateStatusAsync(Guid id, OrderStatus status);
        decimal Calculate(PartyOrder partyOrder);
        Task SendEmailAsync(string baseUri, Guid id, string email);
        Task SendNotificationAsync(PartyOrder partyOrder);
        Task CreateOutlookEvent(PartyOrder partyOrder);
        Task RemoveAsync(Guid id);
        Task CancelAsync(Guid id);
        byte[] GetPdfBytes(string url);
    }
}