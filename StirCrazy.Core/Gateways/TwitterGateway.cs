﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Tweetinvi;
using Tweetinvi.Core.Interfaces;

namespace StirCrazy.Core.Gateways
{
    public class TwitterGateway : ITwitterGateway
    {
        private static readonly string OauthConsumerKey = ConfigurationManager.AppSettings["TwitterOauthConsumerKey"];
        private static readonly string OauthConsumerSecret = ConfigurationManager.AppSettings["TwitterOauthConsumerSecret"];
        private static readonly string OauthToken = ConfigurationManager.AppSettings["TwitterOauthToken"];
        private static readonly string OauthTokenSecret = ConfigurationManager.AppSettings["TwitterOauthTokenSecret"];

        private static readonly int CountRss = Convert.ToInt32(ConfigurationManager.AppSettings["TwitterFeedLimit"]);
        private static readonly string PageRssId = ConfigurationManager.AppSettings["TwitterUserId"];

        public List<ITweet> GetTweets()
        {
            TwitterCredentials.SetCredentials(OauthToken, OauthTokenSecret, OauthConsumerKey, OauthConsumerSecret);
            var timeLines = Timeline.GetUserTimeline(PageRssId, CountRss);

            return timeLines.ToList();
        }
    }
}
