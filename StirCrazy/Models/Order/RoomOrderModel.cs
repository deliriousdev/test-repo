﻿using System;
using StirCrazy.Core;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Models.Party;

namespace StirCrazy.Models.Order
{
    public class RoomOrderModel
    {
        public Guid Id { get; set; }
        public Guid PromotionCodeId { get; set; }
        public int RoomBookingId { get; set; }
        public int PayPalTransactionId { get; set; }
        public string Notes { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string Promotion { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }
        public ApplicationUser Customer { get; set; }
        public RoomBookingModel RoomBooking { get; set; }
        public PaymentTransaction PaymentTransaction { get; set; }

        public bool BypassPaypal { get; set; }
    }
}