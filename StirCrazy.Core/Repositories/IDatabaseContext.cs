﻿using System.Data.Entity;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Repositories
{
    public interface IDatabaseContext
    {
        IDbSet<PackageAddon> PackageOptions { get; set; }
        IDbSet<RoomBooking> PartyBookings { get; set; }
        IDbSet<PartyPackage> PartyPackages { get; set; }
        IDbSet<PartyRoom> PartyRooms { get; set; }
        IDbSet<MediaGallery> Gallery { get; set; }
        IDbSet<Addon> Addons { get; set; }
        IDbSet<TimeSlot> TimeSlots { get; set; }
    }
}