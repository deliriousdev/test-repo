﻿using System;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Domain.Orders
{
    public class GrabNGoOrderAddon
    {
        public int Id { get; set; }
        public Guid GrabNGoOrderId { get; set; }
        public int AddonId { get; set; }
        public int Quantity { get; set; }
        public virtual Addon Addon { get; set; }
    }
}