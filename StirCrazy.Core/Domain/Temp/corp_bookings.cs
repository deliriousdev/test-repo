﻿using System;

namespace StirCrazy.Core.Domain.Temp
{
    public class corp_bookings
    {
        public int corp_booking_id { get; set; }
        public DateTime booking_date { get; set; }
        public DateTime booking_start_time { get; set; }
        public DateTime booking_end_time { get; set; }
        public string booking_status { get; set; }
        public DateTime create_date { get; set; }
        public string pay_status { get; set; }
        public int? id { get; set; }
        public int? booking_id { get; set; }
        public string corp_name { get; set; }
        public string corp_email { get; set; }
        public string corp_phone { get; set; }
        public string corp_desc { get; set; }
        public DateTime? corp_date { get; set; }
        public string adm_name { get; set; }
    }
}