﻿$(document).ready(function () {

    //== JQUERY - LEGACY BINDING SUPPORT (.live) ==\\
    jQuery.fn.extend({
        live: function (types, data, fn) {
            jQuery(this.context).on(types, this.selector, data, fn);
            return this;
        }
    });

    // Get Holidays
    $.ajax({
        type: "POST",
        url: "/Configuration/GetHolidays",
        data: { },
        dataType: 'html',
        success: function (data) {

            $.each(data.items, function(item) {

                $('#holiday-list').append('<li class="list-group-item">'
                    + '<input type="hidden" name="Holidays[' + id + ']" value="' + item + '"/>'
                    + item + ' <button type="button" class="btn btn-default btn-sm btn-rounded holiday-remove pull-right">Remove</button>'
                    + '</li>');

                id++;
                $('#holiday-list').attr('data-id', id);
            });
        }
    });

    // Add Holiday
    $("#holiday-save").click(function () {

        var holidayDate = $('#HolidayDate').val();

        if (holidayDate != "") {

            $('#holiday-list').append('<li class="list-group-item">'
                + '<input type="hidden" name="Holidays[' + id + ']" value="' + holidayDate + '"/>'
                + holidayDate + ' <button type="button" class="btn btn-default btn-sm btn-rounded holiday-remove pull-right">Remove</button>'
                + '</li>');

            id++;
            $('#holiday-list').attr('data-id', id);
        }
    });


    // Remove Holiday
    $(".holiday-remove").live("click", function () {
        $(this).parent('li').remove();
        reindex();
    });

    function reindex() {
        $(".list-group-item").each(function (index) {
            var prefix = "Holidays[" + index + "]";
            $(this).find("input").each(function () {
                this.name = this.name.replace(/Holidays\[\d+\]/, prefix);
            });
        });
    }

});