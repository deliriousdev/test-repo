﻿using System;
using System.Linq;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Party;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Party;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class PartyBookingsController : Controller
    {
        private readonly IRoomBookingService _roomBookingService;
        private readonly IPartyRoomService _partyRoomService;

        public PartyBookingsController(IRoomBookingService roomBookingService, IPartyRoomService partyRoomService)
        {
            _roomBookingService = roomBookingService;
            _partyRoomService = partyRoomService;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PartyBookings(DateTime? partyBookingDate)
        {

            var model = new RoomBookingsModel
            {
                PartyRooms = _partyRoomService.Get().Select(y => new PartyRoom
                {
                    Capacity = y.Capacity,
                    Description = y.Description,
                    Id = y.Id,
                    IsAvailable = y.IsAvailable,
                    Name = y.Name
                }).ToList(),
                TimeSlots = _roomBookingService.GetTimeSlots().Select(y => new TimeSlot
                {
                    Id = y.Id,
                    CheckInTime = DateTime.Today.Add(y.CheckInTime).TimeOfDay,
                    CheckOutTime = DateTime.Today.Add(y.CheckOutTime).TimeOfDay,
                    EndTime = DateTime.Today.Add(y.EndTime).TimeOfDay,
                    StartTime = DateTime.Today.Add(y.StartTime).TimeOfDay
                }).ToList(),
            };

            model.Bookings = _roomBookingService.Get()
                .Where(x => x.Date == (partyBookingDate.HasValue ? partyBookingDate : DateTime.Today))
                .Select(x => new RoomBookingModel
            {
                Id = x.Id,
                PartyRoom = model.PartyRooms.Single(y => y.Id == x.PartyRoomId),
                Date = x.Date,
                TimeSlot = new TimeSlot
                {
                    Id = x.TimeSlotId,
                    CheckInTime = DateTime.Today.Add(x.TimeSlot.CheckInTime).TimeOfDay,
                    CheckOutTime = DateTime.Today.Add(x.TimeSlot.CheckOutTime).TimeOfDay,
                    EndTime = DateTime.Today.Add(x.TimeSlot.EndTime).TimeOfDay,
                    StartTime = DateTime.Today.Add(x.TimeSlot.StartTime).TimeOfDay
                }
            }).ToList();

            return PartialView("_PartyBookingsPartial", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create()
        {
            var checkBoxKeys = Request.Form.AllKeys.Where(key => key.StartsWith("checkbox"));
            var date = DateTime.Parse(Request.Form["PartyBookingDate"]);
            foreach (var checkBoxKey in checkBoxKeys)
            {
                var keyArray = checkBoxKey.Split('_');
                var partyRoomId = Convert.ToInt32(keyArray[2]);
                var timeSlotId = Convert.ToInt32(keyArray[4]);
                _roomBookingService.Add(new RoomBooking { PartyRoomId = partyRoomId, TimeSlotId = timeSlotId, Date = date });
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            _roomBookingService.Remove(id);
            return Content("");
        }
    }
}
