﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StirCrazy.Admin.Startup))]
namespace StirCrazy.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
