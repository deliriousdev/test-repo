﻿using System;
using StirCrazy.Core.Domain.Reports;

namespace StirCrazy.Admin.Models.Reporting
{
    public class ReportModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ReportType ReportType { get; set; }
    }
}