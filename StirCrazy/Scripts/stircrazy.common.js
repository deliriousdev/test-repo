﻿$(document).ready(function () {

    /* ==== Map Settings ===========================================================================
        
        Find the Latitude and Longitude of your address:
            - http://universimmedia.pagesperso-orange.fr/geo/loc.htm
            - http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/
            
    ============================================================================================= */

    // Map Markers
    var mapMarkers = [
        {
            address: '3355 Sunridge Way NE Calgary, AB T1Y 7H5',
            html: '<div style="color: #000"><strong>Stir Crazy Family Fun Centre</strong><br>3355 Sunridge Way NE<br>Calgary, AB T1Y 7H5</div>',
            icon: {
                image: "/img/map_pin.png",
                iconsize: [32, 30],
                iconanchor: [32, 30]
            },
            popup: true
        }
    ];

    // Map Initial Location
    var initLatitude = 51.0694602;
    var initLongitude = -113.9875102;

    // Map Extended Settings
    var mapSettings = {
        controls: {
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: true
        },
        scrollwheel: false,
        markers: mapMarkers,
        latitude: initLatitude,
        longitude: initLongitude,
        zoom: 14
    };

    $("#googlemaps").gMap(mapSettings);

    // Map Center At
    $("#googlemaps").gMap('centerAt', {
        latitude: initLatitude,
        longitude: initLongitude,
        zoom: 14
    });

    /* ==== GeoComplete Settings ================================================================

        Wraps the Google Maps API's Geocoding and Places Autocomplete services. 
        - http://ubilabs.github.io/geocomplete/
    
    ============================================================================================= */

    if ($('input[data-geo="search_address"]').length) {

        $('input[data-geo="search_address"]').geocomplete({
            details: "form",
            detailsAttribute: "data-geo"
        });


        $('input[data-geo="search_address_map"]').geocomplete({
            details: "form",
            detailsAttribute: "data-geo",
            map: "#google_map"
        });

        if ($('input[data-geo="search_address_map"]') != null) {
            $('input[data-geo="search_address_map"]').trigger("geocode");
        }
    }

    /* ==== Marquee =========================================================================
       ====================================================================================== */

    $.ajax({
        url: '/Home/Announcements',
        type: 'POST',
        data: {},
        success: function (data) {

            $.each(data, function (index, value) {
                $('#marquee').append('<li>' + value.Message + '</li>');
            });

            $('#marquee').marquee();
        }
    });

    //$(".panorama").panorama_viewer({
    //    repeat: true,              // The image will repeat when the user scroll reach the bounding box. The default value is false.
    //    direction: "horizontal",    // Let you define the direction of the scroll. Acceptable values are "horizontal" and "vertical". The default value is horizontal
    //    animationTime: 700,         // This allows you to set the easing time when the image is being dragged. Set this to 0 to make it instant. The default value is 700.
    //    easing: "ease-out",         // You can define the easing options here. This option accepts CSS easing options. Available options are "ease", "linear", "ease-in", "ease-out", "ease-in-out", and "cubic-bezier(...))". The default value is "ease-out".
    //    overlay: true               // Toggle this to false to hide the initial instruction overlay
    //});

    $('#accordion .panel-heading a').on('click', function (e) {
        if ($(this).parent('.panel').children('.panel-collapse').hasClass('in')) {
            e.stopPropagation();
        }
    });

    /* ==== Popup =========================================================================
       ====================================================================================== */

    if (noticeGetCookie("stircrazy-popup") != "done") {

        var open = $('#popup').attr("data-open");
        var close = $('#popup').attr("data-close");

        if (open == "" || close == "") {
            var today = moment();
            $('#popup').attr("data-open", today.format('YYYY/MM/DD'));
            $('#popup').attr("data-close", today.format('YYYY/MM/DD'));
        }

        open = $('#popup').attr("data-open");
        close = $('#popup').attr("data-close");

        var openDate = new Date(open);
        var closeDate = new Date(close);

        var curDate = new Date();
        if (openDate.getDate() <= curDate.getDate() && curDate.getDate() <= closeDate.getDate()) {
            $('#popup').bPopup();
        }
    }

    $('#dont-show').click(function () {
        setCookie("stircrazy-popup", "done", 1);
        $('#popup').bPopup().close();
    });

    /* Set Cookie*/
    function setCookie(name, value, expiredays) {
        var todayDate = new Date();
        todayDate.setDate(todayDate.getDate() + expiredays);
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";";
    }

    /* Get Cookie */
    function noticeGetCookie(name) {
        var nameOfCookie = name + "=";
        var x = 0;
        while (x <= document.cookie.length) {
            var y = (x + nameOfCookie.length);
            if (document.cookie.substring(x, y) == nameOfCookie) {
                if ((window.endOfCookie = document.cookie.indexOf(";", y)) == -1)
                    window.endOfCookie = document.cookie.length;
                return unescape(document.cookie.substring(y, window.endOfCookie));
            }
            x = document.cookie.indexOf(" ", x) + 1;
            if (x == 0) break;
        }

        return "";
    }

    /* ==== Slider Settings ===========================================================================

        Flexisel Responsive Carousel jQuery Plugin
            - http://9bitstudios.github.io/flexisel/

    ============================================================================================= */

    if ($(".flexisel-stircrazy-awards").length) {

        $('.flexisel-stircrazy-awards').hide();

        $(window).load(function () {
            $(".flexisel-stircrazy-awards").flexisel({
                visibleItems: 2,
                animationSpeed: 9500,
                autoPlay: true,
                autoPlaySpeed: 0,
                pauseOnHover: false,
                clone: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    portrait: {
                        changePoint: 480,
                        visibleItems: 2
                    },
                    landscape: {
                        changePoint: 640,
                        visibleItems: 3
                    },
                    tablet: {
                        changePoint: 768,
                        visibleItems: 4
                    }
                }
            });
        });

        $('.flexisel-stircrazy-awards').show();
    }

    // Reset Numeric Inputs
    $('form').on('focusin', ':not(#CustomerInformation :text)', function () {
        if ($(this).attr("pattern")) {
            $(this).val("");
        }
    });

    $('form').on('focusout', ':not(#CustomerInformation :text)', function () {
        if (($(this).val() == "" || $(this).val() == null) && $(this).attr("pattern")) {
            $(this).val("0");
        }
    });
});