using System.Collections.Generic;
using StirCrazy.Core.Domain;

namespace StirCrazy.Core.Services
{
    public interface IAnnouncementService
    {
        IEnumerable<Announcement> Get();
        Announcement Get(int id);
        Announcement Add(Announcement announcement);
        Announcement Update(Announcement announcement);
        void Remove(int id);
    }
}