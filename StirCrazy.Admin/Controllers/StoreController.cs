﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Store;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Store;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class StoreController : Controller
    {
        private readonly IStoreService _storeService;
        private readonly IAnnouncementService _announcementService;

        public StoreController(IStoreService storeService, IAnnouncementService announcementService)
        {
            _storeService = storeService;
            _announcementService = announcementService;
        }

        public ActionResult Index()
        {
            var model = new StoreModel()
            {
                StoreHolidays = _storeService.GetStoreHolidays().Select(x => new StoreHolidayModel()
                {
                    Id = x.Id,
                    Date = x.Date,
                    OpenTime = x.OpenTime.ToString(),
                    ClosedTime = x.ClosedTime.ToString(),
                    IsClosed = x.IsClosed
                }).ToList(),
                StoreHours = _storeService.GetStoreHours().Select(x => new StoreHourModel()
                {
                    Id = x.Id,
                    DayOfWeek = x.DayOfWeek,
                    OpenTime = x.OpenTime.ToString(),
                    ClosedTime = x.ClosedTime.ToString(),
                    IsClosed = x.IsClosed
                }).ToList()
            };

            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var storeHour = _storeService.GetStoreHour(id);
            if (storeHour == null) return HttpNotFound();

            var model = new StoreHourModel()
            {
                Id = storeHour.Id,
                DayOfWeek = storeHour.DayOfWeek,
                OpenTime = storeHour.OpenTime.ToString(),
                ClosedTime = storeHour.ClosedTime.ToString(),
                IsClosed = storeHour.IsClosed
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(StoreHourModel model)
        {
            if (ModelState.IsValid)
            {
                DateTime openTime = DateTime.Now;
                DateTime closeTime = DateTime.Now;

                if (model.OpenTime != null && model.ClosedTime != null)
                {
                    openTime = DateTime.ParseExact(model.OpenTime, "h:mm tt", CultureInfo.InvariantCulture);
                    closeTime = DateTime.ParseExact(model.ClosedTime, "h:mm tt", CultureInfo.InvariantCulture);
                }

                var storeHour = new StoreHour()
                {
                    Id = model.Id,
                    DayOfWeek = model.DayOfWeek,
                    OpenTime = openTime.TimeOfDay,
                    ClosedTime = closeTime.TimeOfDay
                };

                _storeService.Update(storeHour);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult CreateHoliday()
        {
            var model = new StoreHolidayModel()
            {
                Date = DateTime.Now
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateHoliday(StoreHolidayModel model)
        {
            if (ModelState.IsValid)
            {
                DateTime startTime = DateTime.Today;
                DateTime endTime = DateTime.Today;

                if (model.OpenTime != null && model.ClosedTime != null)
                {
                    startTime = DateTime.ParseExact(model.OpenTime, "h:mm tt", CultureInfo.InvariantCulture);
                    endTime = DateTime.ParseExact(model.ClosedTime, "h:mm tt", CultureInfo.InvariantCulture);
                }

                var storeHoliday = new StoreHoliday()
                {
                    ClosedTime = startTime.TimeOfDay,
                    OpenTime = endTime.TimeOfDay,
                    Date = model.Date,
                    Holiday = model.Holiday,
                    IsClosed = model.IsClosed,
                    AllowScheduling = model.AllowScheduling
                };

                _storeService.Add(storeHoliday);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteHoliday(int id)
        {
            _storeService.RemoveStoreHoliday(id);
            return null;
        }
    }
}
