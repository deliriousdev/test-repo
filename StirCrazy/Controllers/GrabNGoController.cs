﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using PayPal.Api.Payments;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Party;
using StirCrazy.Models.Addons;
using StirCrazy.Models.Order;

namespace StirCrazy.Controllers
{
    public class GrabNGoController : AsyncController
    {
        private readonly IPartyAddonService _partyAddonService;
        private readonly IGrabNGoOrderService _grabNGoOrderService;
        private readonly IPayPalService _payPalService;
        private readonly IPromotionCodeService _promotionCodeService;

        public GrabNGoController(IGrabNGoOrderService grabNGoOrderService,
            IPartyAddonService partyAddonService,
            IPayPalService payPalService,
            IPromotionCodeService promotionCodeService)
        {
            _partyAddonService = partyAddonService;
            _payPalService = payPalService;
            _promotionCodeService = promotionCodeService;
            _grabNGoOrderService = grabNGoOrderService;
        }

        public ActionResult Index()
        {
           return RedirectToAction("Index", "Home");
        }

        public ActionResult Create()
        {
            var model = new GrabNGoModel()
            {
                Addons = _partyAddonService.GetAddons().Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = new List<GrabNGoOrderAddon>(),
                PaymentTransaction = new PaymentTransaction(),
                AddonGroups = new List<AddonGroupModel>()
            };

            if (model.Addons.Any(x => x.AddonType == AddonType.EdibleArrangement))
            {
                var edibleAddOns = model.Addons.OrderBy(x => x.Id).Where(x => x.AddonType == AddonType.EdibleArrangement);
                var groupCount = 0;

                foreach (var addon in edibleAddOns)
                {
                    if (addon.AddonType == AddonType.EdibleArrangement)
                    {

                        if (addon.Name.Contains("(Small)"))
                        {
                            addon.Size = "Small";
                        }
                        else if (addon.Name.Contains("(Regular)"))
                        {
                            addon.Size = "Regular";
                        }
                        else if (addon.Name.Contains("(Large)"))
                        {
                            addon.Size = "Large";
                        }

                        var addonNoSuffix = addon.Name.Replace(" (Small)", "").Replace(" (Large)", "").Replace(" (Regular)", "");

                        var group = model.AddonGroups.FirstOrDefault(s => s.Name == addonNoSuffix);

                        if (group == null)
                        {
                            groupCount++;
                            group = new AddonGroupModel() { Name = addonNoSuffix, Id = groupCount };
                            model.AddonGroups.Add(group);
                        }

                        group.Addons.Add(addon);
                    }
                }
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(GrabNGoModel model)
        {
            var grabNGoOrder = new GrabNGoOrder()
            {
                PickUpDate = model.PickUpDate,
                PickUpTime = model.PickUpTime,
                Notes = model.Notes,
                Customer = model.Customer,
                OrderAddons = new List<GrabNGoOrderAddon>(),
                PaymentTransaction = new PaymentTransaction()
                {
                    Amount = model.Total,
                    PaymentDate = DateTime.Now
                },
                Status = OrderStatus.Created,
                SubTotal = model.SubTotal,
                Tax = model.Tax,
                Total = model.Total
            };

            if (model.Promotion != null)
            {
                var promoCode = _promotionCodeService.Get(model.Promotion);

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Percentage)
                {
                    grabNGoOrder.Discount = (promoCode.Adjustment / 100) * grabNGoOrder.Total;
                }

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Discount)
                {
                    grabNGoOrder.Discount = (grabNGoOrder.Total - promoCode.Adjustment);
                }
            }

            grabNGoOrder.Customer.UserName = model.Customer.Email;
            foreach (var orderAddon in model.OrderAddons)
            {
                if (orderAddon.Quantity > 0)
                {
                    grabNGoOrder.OrderAddons.Add(new GrabNGoOrderAddon()
                    {
                        AddonId = orderAddon.Id,
                        GrabNGoOrderId = grabNGoOrder.Id,
                        Addon = orderAddon.Addon,
                        Quantity = orderAddon.Quantity,
                    });
                }
            }

            // BYPASS PAYPAL PAYMENT PROCESSING
            if (model.BypassPaypal)
            {
                grabNGoOrder.PaymentTransaction.TransactionType = model.PaymentTransaction.TransactionType;
                grabNGoOrder.PaymentTransaction.TransactionNumber = model.PaymentTransaction.TransactionNumber ?? "Not Applicable";
                grabNGoOrder.PaymentTransaction.PaymentStatus = "Approved";

                // SAVE DATABASE
                grabNGoOrder.CreatedBy = "Administrator";
                grabNGoOrder.Status = OrderStatus.Paid;
                var order = await _grabNGoOrderService.CreateAsync(grabNGoOrder);

                // SEND NOTIFICATION
                try { await _grabNGoOrderService.SendOrderNotificationAsync(grabNGoOrder); }
                catch (Exception ex) { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                return RedirectToAction("Email", new RouteValueDictionary { { "Id", order.Id } });
            }

            // SAVE DATABASE
            grabNGoOrder.CreatedBy = "Customer";
            await _grabNGoOrderService.CreateAsync(grabNGoOrder);

            // PAYPAL PAYMENT PROCESSING
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/GrabNGo/";
            var returnUrl = baseUri + "Payment/" + grabNGoOrder.Id;
            var cancelUrl = baseUri + "Cancelled/" + grabNGoOrder.Id;

            return await ProcessPayment(grabNGoOrder.Id, (double)grabNGoOrder.Total, "Stir Crazy - Grab N Go Order", returnUrl, cancelUrl);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);
            if (grabNGoOrder == null) return HttpNotFound();

            var model = new GrabNGoModel()
            {
                Id = grabNGoOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = grabNGoOrder.OrderAddons.Select(x => new GrabNGoOrderAddon()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    GrabNGoOrderId = x.GrabNGoOrderId,
                    Quantity = x.Quantity
                }).ToList(),
                Notes = grabNGoOrder.Notes,
                Customer = grabNGoOrder.Customer,
                PaymentTransaction = grabNGoOrder.PaymentTransaction,
                PickUpDate = grabNGoOrder.PickUpDate,
                PickUpTime = grabNGoOrder.PickUpTime,
                SubTotal = grabNGoOrder.SubTotal,
                Discount = grabNGoOrder.Discount,
                Tax = grabNGoOrder.Tax,
                Total = grabNGoOrder.Total,
                Status = grabNGoOrder.Status,
                Promotion = (grabNGoOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", grabNGoOrder.PromotionCode.Code, grabNGoOrder.PromotionCode.Description) : "",
            };

            return View(model);
        }

        public async Task<ActionResult> Print(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);
            if (grabNGoOrder == null) return HttpNotFound();

            var model = new GrabNGoModel()
            {
                Id = grabNGoOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = grabNGoOrder.OrderAddons.Select(x => new GrabNGoOrderAddon()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    Quantity = x.Quantity
                }).ToList(),
                Notes = grabNGoOrder.Notes,
                Customer = grabNGoOrder.Customer,
                PaymentTransaction = grabNGoOrder.PaymentTransaction,
                PickUpDate = grabNGoOrder.PickUpDate,
                PickUpTime = grabNGoOrder.PickUpTime,
                SubTotal = grabNGoOrder.SubTotal,
                Discount = grabNGoOrder.Discount,
                Tax = grabNGoOrder.Tax,
                Total = grabNGoOrder.Total,
                Status = grabNGoOrder.Status,
                Promotion = (grabNGoOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", grabNGoOrder.PromotionCode.Code, grabNGoOrder.PromotionCode.Description) : "",
            };

            return View(model);
        }

        private async Task<RedirectResult> ProcessPayment(Guid id, double amount, string description, string returnUrl, string cancelUrl)
        {
            if (Request.Url != null)
            {
                var payment = _payPalService.CreatePayment(amount, description, returnUrl, cancelUrl);
                if (payment != null)
                {
                    var paymentId = payment.id;
                    var paymentState = payment.state;

                    var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);

                    if (grabNGoOrder != null)
                    {
                        grabNGoOrder.PaymentTransaction.Id = grabNGoOrder.PaymentTransaction.Id;
                        grabNGoOrder.PaymentTransaction.TransactionType = TransactionType.Paypal;
                        grabNGoOrder.PaymentTransaction.PaymentDate = DateTime.Now;
                        grabNGoOrder.PaymentTransaction.PaymentStatus = paymentState;
                        grabNGoOrder.PaymentTransaction.TransactionNumber = paymentId;
                        grabNGoOrder.PaymentTransaction.Amount = (decimal)amount;

                        await _grabNGoOrderService.UpdateAsync(grabNGoOrder);
                    }

                    var redirectUrl = string.Format("{0}{1}", GetApprovalUrl(payment), "&useraction=commit");
                    return new RedirectResult(redirectUrl);
                }
            }

            return null;
        }

        public async Task<RedirectToRouteResult> Payment(Guid id)
        {
            var payer = Request["PayerID"];
            var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);

            var payment = new Payment
            {
                id = grabNGoOrder.PaymentTransaction.TransactionNumber,
                state = grabNGoOrder.PaymentTransaction.PaymentStatus
            };

            var result = _payPalService.ExecutePayment(payment, payer);
            if (result.state == "approved")
            {
                grabNGoOrder.Status = OrderStatus.Paid;
            }

            grabNGoOrder.PaymentTransaction.PaymentStatus = result.state;

            await _grabNGoOrderService.UpdateAsync(grabNGoOrder);
            await _grabNGoOrderService.SendOrderNotificationAsync(grabNGoOrder);

            return RedirectToAction("Email", new RouteValueDictionary { { "Id", id } });
        }

        public async Task<RedirectToRouteResult> Cancelled(Guid id)
        {
            await _grabNGoOrderService.RemoveAsync(id);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Save(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/GrabNGo/";
            var detailsUrl = baseUri + "Print/" + id;

            var bytesAsPdf = _grabNGoOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        public async Task<ActionResult> Email(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);
            if (grabNGoOrder == null) return HttpNotFound();

            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/GrabNGo/";
            await _grabNGoOrderService.SendEmailAsync(baseUri, grabNGoOrder.Id, grabNGoOrder.Customer.Email);

            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        private string GetApprovalUrl(Payment payment)
        {
            var links = payment.links;
            return (from lnk in links where lnk.rel.ToLower().Equals("approval_url") select Server.UrlDecode(lnk.href)).FirstOrDefault();
        }
    }
}