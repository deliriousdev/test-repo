﻿using System.Diagnostics.CodeAnalysis;
using System.Web;
using StirCrazy.Core.Constants;

namespace StirCrazy.Core.Utilities
{
    [ExcludeFromCodeCoverage]
    public static class ApplicationPath
    {
        public static string GetSiteRoot()
        {
            string port = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            if (port == null || port == "80" || port == "443")
                port = "";
            else
                port = ":" + port;

            string protocol = HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            if (protocol == null || protocol == "0")
                protocol = "http://";
            else
                protocol = "https://";

            string sOut = protocol + HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + port +
                          HttpContext.Current.Request.ApplicationPath;

            if (sOut.EndsWith("/"))
            {
                sOut = sOut.Substring(0, sOut.Length - 1);
            }

            return sOut;
        }
    }

    public static class ServerPaths
    {
        public static string MediaPath = "Uploads";
        public static string CarouselImagesPath = "Uploads/Carousel";
        public static string CakesPath = "Uploads/Cakes";
        public static string FacilityPath = "Uploads/Facility";
        public static string PartyRoomPath = "Uploads/PartyRooms";
        public static string AnnouncementPath = "Uploads/Announcements";
        public static string DefaultPath = "Uploads";

        public static string GetUploadFolder(UploadFolder uploadFolder)
        {
            switch (uploadFolder)
            {
                case UploadFolder.Cakes:
                    return CakesPath;

                case UploadFolder.Default:
                    return DefaultPath;

                case UploadFolder.Facility:
                    return FacilityPath;

                case UploadFolder.PartyRooms:
                    return PartyRoomPath;

                case UploadFolder.Carousel:
                    return CarouselImagesPath;

                case UploadFolder.ParentTotLounge:
                    return FacilityPath;

                case UploadFolder.ConcessionCounter:
                    return FacilityPath;

                case UploadFolder.Announcement:
                    return AnnouncementPath;

                default:
                    return DefaultPath;
            }
        }
    }

}