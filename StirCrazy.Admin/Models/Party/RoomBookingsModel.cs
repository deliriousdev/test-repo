﻿using System.Collections.Generic;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Admin.Models.Party
{
    public class RoomBookingsModel
    {
        public List<PartyRoom> PartyRooms { get; set; }
        public List<TimeSlot> TimeSlots { get; set; }

        public List<RoomBookingModel> Bookings { get; set; }
    }
}