﻿using System;
using System.Linq;
using System.Web.Mvc;
using StirCrazy.Admin.Models;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Services;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class PromotionController : Controller
    {
        private readonly IPromotionCodeService _promotionCodeService;

        public PromotionController(IPromotionCodeService promotionCodeService)
        {
            _promotionCodeService = promotionCodeService;
        }

        public ActionResult Index()
        {
            var model = _promotionCodeService.Get().Select(x => new PromotionModel()
            {
                Id = x.Id,
                AdjustmentType = x.AdjustmentType,
                Adjustment = x.Adjustment,
                PromotionType = x.PromotionType,
                Description = x.Description,
                Created = x.Created,
                Code = x.Code,
                Expired = x.Expired
            }).ToList();

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new PromotionModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(PromotionModel model)
        {
            var promotion = new PromotionCode()
            {
                Adjustment = model.Adjustment,
                AdjustmentType = model.AdjustmentType,
                Code = model.Code,
                Created = model.Created,
                Description = model.Description,
                Expired = model.Expired,
                PromotionType = model.PromotionType
            };

            _promotionCodeService.Add(promotion);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            var promotion = _promotionCodeService.Get(id);
            if (promotion == null) return HttpNotFound();

            var model = new PromotionModel()
            {
                Id = promotion.Id,
                Adjustment = promotion.Adjustment,
                AdjustmentType = promotion.AdjustmentType,
                Code = promotion.Code,
                Created = promotion.Created,
                Description = promotion.Description,
                Expired = promotion.Expired,
                PromotionType = promotion.PromotionType
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PromotionModel model)
        {
            var promotion = new PromotionCode()
            {
                Id = model.Id,
                Adjustment = model.Adjustment,
                AdjustmentType = model.AdjustmentType,
                Code = model.Code,
                Created = model.Created,
                Description = model.Description,
                Expired = model.Expired,
                PromotionType = model.PromotionType
            };

            _promotionCodeService.Update(promotion);
            return RedirectToAction("Index");
        }

        public ActionResult Details(Guid id)
        {
            var promotion = _promotionCodeService.Get(id);
            if (promotion == null) return HttpNotFound();

            var model = new PromotionModel()
            {
                Id = promotion.Id,
                Adjustment = promotion.Adjustment,
                AdjustmentType = promotion.AdjustmentType,
                Code = promotion.Code,
                Created = promotion.Created,
                Description = promotion.Description,
                Expired = promotion.Expired,
                PromotionType = promotion.PromotionType
            };

            return View(model);
        }

        public ActionResult Delete(Guid id)
        {
            var promotion = _promotionCodeService.Get(id);
            if (promotion == null) return HttpNotFound();

            var model = new PromotionModel()
            {
                Id = promotion.Id,
                Adjustment = promotion.Adjustment,
                AdjustmentType = promotion.AdjustmentType,
                Code = promotion.Code,
                Created = promotion.Created,
                Description = promotion.Description,
                Expired = promotion.Expired,
                PromotionType = promotion.PromotionType
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(PromotionModel model)
        {
            _promotionCodeService.Remove(model.Id);
            return RedirectToAction("Index");
        }
    }
}
