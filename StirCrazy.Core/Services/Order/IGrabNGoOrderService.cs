using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Services.Order
{
    public interface IGrabNGoOrderService
    {
        IEnumerable<GrabNGoOrder> Get();
        IEnumerable<GrabNGoOrder> GetByStatus(OrderStatus status);
        Task<GrabNGoOrder> GetAsync(Guid id);
        Task<GrabNGoOrder> CreateAsync(GrabNGoOrder grabNGoOrder);
        System.Threading.Tasks.Task UpdateAsync(GrabNGoOrder grabNGoOrder);
        Task SendEmailAsync(string baseUri, Guid id, string email);
        Task SendOrderNotificationAsync(GrabNGoOrder grabNGoOrder);
        Task UpdateStatusAsync(Guid id, OrderStatus status);
        Task CancelAsync(Guid id);
        Task RemoveAsync(Guid id);
        byte[] GetPdfBytes(string url);
    }
}