﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace StirCrazy.Admin.Models.Carousel
{
    public class CarouselModel
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public string PhysicalPath { get; set; }
        public string Transition { get; set; }
        public bool IsActive { get; set; }

        public SelectList TransitionsList
        {
            get
            {
                return new SelectList(
                  new List<Object>
                {
                    new SelectListItem {Value = "sliceDown", Text = "Slice Down"},
                    new SelectListItem {Value = "sliceDownLeft", Text = "Slice Down/Left"},
                    new SelectListItem {Value = "sliceUp", Text = "Slice Up"},
                    new SelectListItem {Value = "sliceUpLeft", Text = "Slice Up/Left"},
                    new SelectListItem {Value = "sliceUpDown", Text = "Slice Up/Down"},
                    new SelectListItem {Value = "sliceUpDownLeft", Text = "Slice Up/Down/Left"},
                    new SelectListItem {Value = "fold", Text = "Fold"},
                    new SelectListItem {Value = "fade", Text = "Fade"},
                    new SelectListItem {Value = "slideInRight", Text = "Slide In Right"},
                    new SelectListItem {Value = "slideInLeft", Text = "Slide In Left"},
                    new SelectListItem {Value = "boxRandom", Text = "Box Random"},
                    new SelectListItem {Value = "boxRain", Text = "Box Rain"},
                    new SelectListItem {Value = "boxRainReverse", Text = "Box Rain Reverse"},
                    new SelectListItem {Value = "boxRainGrow", Text = "Box Rain Grow"},
                    new SelectListItem {Value = "boxRainGrowReverse", Text = "Box Rain Grow & Reverse"}
                }, "Value", "Text");
            }
        }
    }
}