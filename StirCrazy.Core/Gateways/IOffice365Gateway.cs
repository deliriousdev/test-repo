using System;
using System.Collections.Generic;
using StirCrazy.Core.Domain.Calendar;

namespace StirCrazy.Core.Gateways
{
    public interface IOffice365Gateway
    {
        List<CalendarItem> GetAppointments(DateTime start, DateTime end);
        List<CalendarItem> GetAppointmentsByCreationDate(DateTime startCreationTime, DateTime endCreationTime);
        string CreateAppointment(CalendarItem calendarItem);
        bool UpdateAppointment(CalendarItem calendarItem);
        bool DeleteAppointment(string itemId);
    }
}