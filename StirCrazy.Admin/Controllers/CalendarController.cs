﻿using System;
using System.Linq;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Calendar;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class CalendarController : Controller
    {
        private readonly IOffice365Service _office365Service;

        public CalendarController()
        {
            _office365Service = new Office365Service("events@stircrazy.ca");
        }

        public ActionResult Index()
        {
            var model = _office365Service.GetEvents();

            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CalendarEventModel model)
        {
            if (ModelState.IsValid)
            {
                _office365Service.CreateEvent(model.Start, model.End, model.Title, model.Description, model.IsAllDay);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Edit(string id)
        {
            var evnt = _office365Service.GetEvents().FirstOrDefault(x => x.ItemId == id);
            if (evnt == null) return HttpNotFound();

            var model = new CalendarEventModel()
            {
                ItemId = evnt.ItemId,
                Title = evnt.Subject,
                Description = evnt.Body,
                Start = evnt.StartTime,
                End = evnt.EndTime,
                IsAllDay = evnt.IsAllDay
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(CalendarEventModel model)
        {
            if (ModelState.IsValid)
            {
                _office365Service.UpdateEvent(model.ItemId, model.Start, model.End, model.Title, model.Description, model.IsAllDay);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Details(string id)
        {
            var evnt = _office365Service.GetEvents().FirstOrDefault(x => x.ItemId == id);
            if (evnt == null) return HttpNotFound();

            var model = new CalendarEventModel()
            {
                ItemId = evnt.ItemId,
                Title = evnt.Subject,
                Description = evnt.Body,
                Start = evnt.StartTime,
                End = evnt.EndTime,
                IsAllDay = evnt.IsAllDay
            };


            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            _office365Service.DeleteEvent(id);
            return null;
        }
    }
}
