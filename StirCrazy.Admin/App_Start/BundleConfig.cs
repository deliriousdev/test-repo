﻿using System.Web.Optimization;

namespace StirCrazy.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"));

            bundles.Add(new StyleBundle("~/Content/theme").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/plugins.css",
                      "~/Content/style.css",
                      "~/Content/custom.css"));

            bundles.Add(new StyleBundle("~/Content/calendar").Include(
                      "~/Scripts/plugins/fullcalendar/fullcalendar.css"));

            bundles.Add(new StyleBundle("~/Content/datetimepicker").Include(
                        "~/Scripts/plugins/datetimepicker/jquery.datetimepicker.css"));

            bundles.Add(new ScriptBundle("~/bundles/theme").Include(
                        "~/Scripts/plugins/jquery-migrate-1.2.1.js",
                        "~/Scripts/plugins/jquery-ui/jquery-ui-1.10.4.min.js",
                        "~/Scripts/plugins/bootstrap/bootstrap.min.js",
                        "~/Scripts/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js",
                        "~/Scripts/plugins/mmenu/js/jquery.mmenu.min.all.js",
                        "~/Scripts/plugins/fullcalendar/moment.min.js",
                        "~/Scripts/plugins/nprogress/nprogress.js",
                        "~/Scripts/plugins/charts-sparkline/sparkline.min.js",
                        "~/Scripts/plugins/breakpoints/breakpoints.js",
                        "~/Scripts/plugins/numerator/jquery-numerator.js",
                        "~/Scripts/plugins/jquery.cookie.min.js",
                        "~/Scripts/application.js",
                        "~/Scripts/stircrazy.common.js"));

            bundles.Add(new ScriptBundle("~/bundles/login").Include(
                        "~/Scripts/plugins/backstretch/backstretch.min.js",
                        "~/Scripts/plugins/bootstrap-loading/lada.min.js",
                        "~/Scripts/js/account.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
                        "~/Scripts/plugins/datatables/dynamic/jquery.dataTables.min.js",
                        "~/Scripts/plugins/datatables/dataTables.bootstrap.js",
                        "~/Scripts/plugins/datatables/dataTables.tableTools.js",
                        "~/Scripts/plugins/datatables/table.editable.js",
                        "~/Scripts/js/table_dynamic.js"));

            bundles.Add(new ScriptBundle("~/bundles/calendar").Include(
                        "~/Scripts/plugins/fullcalendar/moment.min.js",
                        "~/Scripts/plugins/fullcalendar/fullcalendar.js",
                        "~/Scripts/plugins/fullcalendar/gcal.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                        "~/Scripts/plugins/datetimepicker/jquery.datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-booking").Include(
            "~/Scripts/stircrazy.party-booking.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-room-booking").Include(
                        "~/Scripts/stircrazy.room-booking.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-corporate-booking").Include(
                        "~/Scripts/stircrazy.corporate-booking.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-party-grabngo").Include(
                        "~/Scripts/stircrazy.party-grabngo.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-order-bakery").Include(
                        "~/Scripts/stircrazy.order-bakery.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-order-admission").Include(
                        "~/Scripts/stircrazy.order-admission.js"));
                                    

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}
