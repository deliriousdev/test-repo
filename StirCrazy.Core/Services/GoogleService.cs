﻿using System;
using Google.Apis.Calendar.v3.Data;
using StirCrazy.Core.Gateways;

namespace StirCrazy.Core.Services
{
    public interface IGoogleService
    {
        Events GetEvents();
        Event GetEvent(string eventId);
        void InsertEvent(DateTime? start, DateTime? end, string summary, string description, string location);
        void UpdateEvent(string eventId, DateTime? start, DateTime? end, string summary, string description, string location);
        void DeleteEvent(string eventId);
    }

    public class GoogleService : IGoogleService
    {
        private readonly IGoogleGateway _googleGateway;

        public GoogleService(IGoogleGateway googleGateway)
        {
            _googleGateway = googleGateway;
        }

        public Events GetEvents()
        {
            return _googleGateway.GetEvents();
        }

        public Event GetEvent(string eventId)
        {
            return _googleGateway.GetEvent(eventId);
        }

        public void InsertEvent(DateTime? start, DateTime? end, string summary, string description, string location)
        {
            var Event = new Event()
            {
                Summary = summary,
                Description = description,
                Start = new EventDateTime()
                {
                    DateTime = start ?? DateTime.Now
                },
                End = new EventDateTime()
                {
                    DateTime = end ?? DateTime.Now
                }
            };

            _googleGateway.InsertEvent(Event);
        }

        public void UpdateEvent(string eventId, DateTime? start, DateTime? end, string summary, string description, string location)
        {
            var Event = new Event()
            {
                Summary = summary,
                Description = description,
                Start = new EventDateTime()
                {
                    DateTime = start ?? DateTime.Now
                },
                End = new EventDateTime()
                {
                    DateTime = end ?? DateTime.Now
                }
            };

            _googleGateway.UpdateEvent(Event, eventId);
        }

        public void DeleteEvent(string eventId)
        {
            if (eventId != null)
            {
                _googleGateway.RemoveEvent(eventId);
            }
        }
    }
}
