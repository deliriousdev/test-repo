﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Admin.Models.Reporting
{
    public class BakeryReportModel
    {
        public List<BakeryOrder> BakeryOrders { get; set; }
        public List<PartyOrder> PartyOrders { get; set; } 
    }
}