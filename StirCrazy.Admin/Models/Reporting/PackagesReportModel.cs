﻿using System.Collections.Generic;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Admin.Models.Reporting
{
    public class PackagesReportModel
    {
        public Month Month { get; set; }
        public List<PartyPackage> Packages { get; set; }
    }
}