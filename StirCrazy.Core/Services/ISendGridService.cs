﻿using System;
using System.Threading.Tasks;
using SendGrid;
using StirCrazy.Core.Domain;

namespace StirCrazy.Core.Services
{
    public interface ISendGridService
    {
        Task Send(SendGridMessage message);
        Task SendEmail(string baseUri, Guid id, string email, ConfirmationType type);
        Task SendAccountInformation(ApplicationUser user, string password);
        byte[] GetPdfBytes(string url);
    }
}