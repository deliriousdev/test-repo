﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using StirCrazy.Core.Constants;

namespace StirCrazy.Core.Domain
{
    public class MediaGallery
    {
        [Key]
        public int Id { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Index { get; set; }
        public int MediaFileId { get; set; }
        public UploadFolder UploadFolder { get; set; }
        public virtual MediaFile MediaFile { get; set; }
    }
}
