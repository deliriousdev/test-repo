﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Addons;
using StirCrazy.Admin.Models.Order;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Party;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class GrabNGoController : AsyncController
    {
        private readonly IPartyAddonService _partyAddonService;
        private readonly IGrabNGoOrderService _grabNGoOrderService;
        private readonly IPromotionCodeService _promotionCodeService;

        public GrabNGoController(IGrabNGoOrderService grabNGoOrderService, 
            IPartyAddonService partyAddonService, IPromotionCodeService promotionCodeService)
        {
            _partyAddonService = partyAddonService;
            _promotionCodeService = promotionCodeService;
            _grabNGoOrderService = grabNGoOrderService;
        }

        public ActionResult Index(OrderStatus status = OrderStatus.Paid)
        {
            var model = _grabNGoOrderService.GetByStatus(status).Select(x => new GrabNGoModel()
            {
                Id = x.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(y => new AddonModel()
                {
                    Id = y.Id,
                    AddonType = y.AddonType,
                    Name = y.Name,
                    Price = y.Price
                }).ToList(),
                OrderAddons = x.OrderAddons.Select(y => new GrabNGoOrderAddon()
                {
                    Id = y.Id,
                    Addon = y.Addon,
                    Quantity = y.Quantity
                }).ToList(),
                Notes = x.Notes,
                Customer = x.Customer,
                PaymentTransaction = x.PaymentTransaction,
                PickUpDate = x.PickUpDate,
                PickUpTime = x.PickUpTime,
                Total = x.Total,
                Status = x.Status,
                CreatedBy = x.CreatedBy

            }).ToList();

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new GrabNGoModel()
            {
                Addons = _partyAddonService.GetAddons().Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = new List<GrabNGoOrderAddon>(),
                PaymentTransaction = new PaymentTransaction(),
                AddonGroups = new List<AddonGroupModel>()
            };

            if (model.Addons.Any(x => x.AddonType == AddonType.EdibleArrangement))
            {
                var edibleAddOns = model.Addons.OrderBy(x => x.Id).Where(x => x.AddonType == AddonType.EdibleArrangement);
                var groupCount = 0;

                foreach (var addon in edibleAddOns)
                {
                    if (addon.AddonType == AddonType.EdibleArrangement)
                    {

                        if (addon.Name.Contains("(Small)"))
                        {
                            addon.Size = "Small";
                        }
                        else if (addon.Name.Contains("(Regular)"))
                        {
                            addon.Size = "Regular";
                        }
                        else if (addon.Name.Contains("(Large)"))
                        {
                            addon.Size = "Large";
                        }

                        var addonNoSuffix = addon.Name.Replace(" (Small)", "").Replace(" (Large)", "").Replace(" (Regular)", "");

                        var group = model.AddonGroups.FirstOrDefault(s => s.Name == addonNoSuffix);

                        if (group == null)
                        {
                            groupCount++;
                            group = new AddonGroupModel() { Name = addonNoSuffix, Id = groupCount };
                            model.AddonGroups.Add(group);
                        }

                        group.Addons.Add(addon);
                    }
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(GrabNGoModel model)
        {
            var grabNGoOrder = new GrabNGoOrder()
            {
                PickUpDate = model.PickUpDate,
                PickUpTime = model.PickUpTime,
                Notes = model.Notes,
                Customer = model.Customer,
                OrderAddons = new List<GrabNGoOrderAddon>(),
                PaymentTransaction = new PaymentTransaction()
                {
                    Amount = model.Total,
                    PaymentDate = DateTime.Now,
                    TransactionType = model.PaymentTransaction.TransactionType,
                    TransactionNumber = model.PaymentTransaction.TransactionNumber ?? "-- Not Applicable --",
                    PaymentStatus = "Paid"
                },
                Status = OrderStatus.Created,
                SubTotal = model.SubTotal,
                Tax = model.Tax,
                Total = model.Total
            };

            if (model.Promotion != null)
            {
                var promoCode = _promotionCodeService.Get(model.Promotion);

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Percentage)
                {
                    grabNGoOrder.Discount = (promoCode.Adjustment / 100) * grabNGoOrder.Total;
                }

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Discount)
                {
                    grabNGoOrder.Discount = (grabNGoOrder.Total - promoCode.Adjustment);
                }
            }

            grabNGoOrder.Customer.UserName = model.Customer.Email;
            foreach (var orderAddon in model.OrderAddons)
            {
                if (orderAddon.Quantity > 0)
                {
                    grabNGoOrder.OrderAddons.Add(new GrabNGoOrderAddon()
                    {
                        AddonId = orderAddon.Id,
                        GrabNGoOrderId = grabNGoOrder.Id,
                        Addon = orderAddon.Addon,
                        Quantity = orderAddon.Quantity,
                    });
                }
            }

            // SAVE DATABASE
            grabNGoOrder.CreatedBy = User.Identity.Name;
            _grabNGoOrderService.CreateAsync(grabNGoOrder);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);
            if (grabNGoOrder == null) return HttpNotFound();

            var model = new GrabNGoModel()
            {
                Id = grabNGoOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = grabNGoOrder.OrderAddons.Select(x => new GrabNGoOrderAddon()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    GrabNGoOrderId = x.GrabNGoOrderId,
                    Quantity = x.Quantity
                }).ToList(),
                Notes = grabNGoOrder.Notes,
                Customer = grabNGoOrder.Customer,
                PaymentTransaction = grabNGoOrder.PaymentTransaction,
                PickUpDate = grabNGoOrder.PickUpDate,
                PickUpTime = grabNGoOrder.PickUpTime,
                SubTotal = grabNGoOrder.SubTotal,
                Discount = grabNGoOrder.Discount,
                Tax = grabNGoOrder.Tax,
                Total = grabNGoOrder.Total,
                Status = grabNGoOrder.Status,
                Promotion = (grabNGoOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", grabNGoOrder.PromotionCode.Code, grabNGoOrder.PromotionCode.Description) : "",
            };

            return View(model);
        }

        public async Task<ActionResult> Print(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);
            if (grabNGoOrder == null) return HttpNotFound();

            var model = new GrabNGoModel()
            {
                Id = grabNGoOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = grabNGoOrder.OrderAddons.Select(x => new GrabNGoOrderAddon()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    Quantity = x.Quantity
                }).ToList(),
                Notes = grabNGoOrder.Notes,
                Customer = grabNGoOrder.Customer,
                PaymentTransaction = grabNGoOrder.PaymentTransaction,
                PickUpDate = grabNGoOrder.PickUpDate,
                PickUpTime = grabNGoOrder.PickUpTime,
                SubTotal = grabNGoOrder.SubTotal,
                Discount = grabNGoOrder.Discount,
                Tax = grabNGoOrder.Tax,
                Total = grabNGoOrder.Total,
                Status = grabNGoOrder.Status,
                Promotion = (grabNGoOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", grabNGoOrder.PromotionCode.Code, grabNGoOrder.PromotionCode.Description) : "",
            };

            return View(model);
        }

        public ActionResult Save(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/GrabNGo/";
            var detailsUrl = baseUri + "Print/" + id;

            byte[] bytesAsPdf = _grabNGoOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        public async Task<ActionResult> Email(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);
            if (grabNGoOrder == null) return HttpNotFound();

            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/GrabNGo/";
            await _grabNGoOrderService.SendEmailAsync(baseUri, grabNGoOrder.Id, grabNGoOrder.Customer.Email);

            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var grabNGoOrder = await _grabNGoOrderService.GetAsync(id);
            if (grabNGoOrder == null) return HttpNotFound();

            var model = new GrabNGoModel()
            {
                Id = grabNGoOrder.Id,
                Addons = _partyAddonService.GetAddons().OrderBy(y => y.Name).Select(x => new AddonModel()
                {
                    Id = x.Id,
                    AddonType = x.AddonType,
                    Name = x.Name,
                    Price = x.Price
                }).ToList(),
                OrderAddons = grabNGoOrder.OrderAddons.Select(x => new GrabNGoOrderAddon()
                {
                    Id = x.Id,
                    Addon = x.Addon,
                    Quantity = x.Quantity
                }).ToList(),
                Notes = grabNGoOrder.Notes,
                Customer = grabNGoOrder.Customer,
                PaymentTransaction = grabNGoOrder.PaymentTransaction,
                PickUpDate = grabNGoOrder.PickUpDate,
                PickUpTime = grabNGoOrder.PickUpTime,
                SubTotal = grabNGoOrder.SubTotal,
                Discount = grabNGoOrder.Discount,
                Tax = grabNGoOrder.Tax,
                Total = grabNGoOrder.Total,
                Status = grabNGoOrder.Status,
                Promotion = (grabNGoOrder.PromotionCode != null)
                ? string.Format("{0} - {1}", grabNGoOrder.PromotionCode.Code, grabNGoOrder.PromotionCode.Description) : "",
            };

            return View(model);
        }

        [HttpPost]
        public async Task<RedirectToRouteResult> Delete(RoomOrderModel model)
        {
            await _grabNGoOrderService.RemoveAsync(model.Id);
            return RedirectToAction("Index");
        }

        public async Task<RedirectToRouteResult> Cancelled(Guid id)
        {
            await _grabNGoOrderService.RemoveAsync(id);
            return RedirectToAction("Index", "Home");
        }
    }
}