﻿using System.Collections.Generic;
using System.Linq;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services.Party
{
    public class PartyPackageService : IPartyPackageService
    {
        private readonly IRepository<PartyPackage> _partyPackageRepository;
        private readonly IRepository<PackageAddon> _packageAddonRepository;

        public PartyPackageService(IRepository<PartyPackage> partyPackageRepository, IRepository<PackageAddon> packageAddonRepository)
        {
            _partyPackageRepository = partyPackageRepository;
            _packageAddonRepository = packageAddonRepository;
        }

        public IEnumerable<PartyPackage> Get()
        {
            return _partyPackageRepository.Get().ToList();
        }

        public PartyPackage Get(int id)
        {
            return _partyPackageRepository.Get().SingleOrDefault(x => x.Id == id);
        }

        public PartyPackage Add(PartyPackage partyPackage)
        {
            return _partyPackageRepository.Add(partyPackage);
        }

        public PartyPackage Update(PartyPackage partyPackage)
        {
            return _partyPackageRepository.Update(partyPackage, partyPackage.Id);
        }

        public void Remove(PartyPackage partyPackage)
        {
            _partyPackageRepository.Delete(partyPackage);
        }

        public void RemoveAddon(int id)
        {
            var addon = _packageAddonRepository.Get(id);
            _packageAddonRepository.Delete(addon);
        }
    }
}
