﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Models.Store
{
    public class StoreHolidayModel
    {
        public int Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public string Holiday { get; set; }
        [Required]
        public string OpenTime { get; set; }
        [Required]
        public string ClosedTime { get; set; }
        [Required]
        public bool IsClosed { get; set; }
    }
}