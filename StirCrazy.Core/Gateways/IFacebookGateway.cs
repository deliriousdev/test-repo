﻿namespace StirCrazy.Core.Gateways
{
    public interface IFacebookGateway
    {
        string GetPosts();
    }
}