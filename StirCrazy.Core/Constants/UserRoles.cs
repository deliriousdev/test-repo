﻿namespace StirCrazy.Core.Constants
{
    public class UserRoles
    {
        public const string Administrator = "Administrator";
        public const string Manager = "Manager";
        public const string Staff = "Staff";
        public const string Customer = "Customer";
    }
}
