﻿using System.Web.Optimization;

namespace StirCrazy
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-formhelpers.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy").Include(
                        "~/Scripts/jquery.gmap.js",
                        "~/Vendor/fullcalendar/moment.min.js",
                        "~/Vendor/jquery.marquee/lib/jquery.marquee.min.js",
                        "~/Scripts/stircrazy.common.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-formhelpers.min.css",
                      "~/Vendor/font-awesome-4.2.0/css/font-awesome.min.css",
                      "~/Vendor/jquery.marquee/css/jquery.marquee.min.css",
                      "~/Content/main.css"));


            bundles.Add(new ScriptBundle("~/bundles/stircrazy-booking").Include(
                        "~/Scripts/stircrazy.party-booking.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-room-booking").Include(
                        "~/Scripts/stircrazy.room-booking.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-party-grabngo").Include(
                        "~/Scripts/stircrazy.party-grabngo.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-order-bakery").Include(
                        "~/Scripts/stircrazy.order-bakery.js"));

            bundles.Add(new ScriptBundle("~/bundles/stircrazy-order-admission").Include(
                        "~/Scripts/stircrazy.order-admission.js"));

            // JQuery Calendar
            bundles.Add(new ScriptBundle("~/bundles/calendar").Include(
                        "~/Vendor/fullcalendar/moment.min.js",
                        "~/Vendor/fullcalendar/fullcalendar.min.js",
                        "~/Vendor/fullcalendar/gcal.js"));

            bundles.Add(new StyleBundle("~/Content/calendar").Include(
                        "~/Vendor/fullcalendar/fullcalendar.css"));

            // Nivo Lightbox
            bundles.Add(new StyleBundle("~/Content/nivo-lightbox").Include(
                        "~/Vendor/nivo-lightbox/nivo-lightbox.css"));

            bundles.Add(new ScriptBundle("~/bundles/nivo-lightbox").Include(
                        "~/Vendor/nivo-lightbox/nivo-lightbox.min.js"));

            // JQuery Datetimepicker
            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                        "~/Vendor/fullcalendar/moment.min.js",
                        "~/Vendor/datetimepicker/jquery.datetimepicker.js"));

            bundles.Add(new StyleBundle("~/Content/datetimepicker").Include(
                        "~/Vendor/datetimepicker/jquery.datetimepicker.css"));

            // JQuery Sortable
            bundles.Add(new ScriptBundle("~/bundles/sortable").Include(
                        "~/Vendor/jquery.sortable/jquery.sortable.min.js"));

            // JQuery BSPopup
            bundles.Add(new ScriptBundle("~/bundles/bpopup").Include(
                        "~/Vendor/jquery.bpopup/jquery.bpopup.min.js"));

            bundles.Add(new StyleBundle("~/Content/bpopup").Include(
                        "~/Vendor/jquery.bpopup/jquery.bpopup.min.css"));

            // Nivo Slider
            bundles.Add(new StyleBundle("~/Content/nivo-slider").Include(         
                        "~/Vendor/nivo-slider/nivo-slider.css",
                        "~/Vendor/nivo-slider/themes/default/default.css"));

            bundles.Add(new ScriptBundle("~/bundles/nivo-slider").Include(
                        "~/Vendor/nivo-slider/jquery.nivo.slider.pack.js"));

            // Flexisel
            bundles.Add(new StyleBundle("~/Content/flexisel").Include(
                        "~/Vendor/jquery.flexisel/jquery.flexisel.css"));

            bundles.Add(new ScriptBundle("~/bundles/flexisel").Include(
                        "~/Vendor/jquery.flexisel/jquery.flexisel.js"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
