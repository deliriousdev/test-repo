﻿using System.Collections.Generic;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Services.Party
{
    public interface IPartyPackageService
    {
        IEnumerable<PartyPackage> Get();
        PartyPackage Get(int id);
        PartyPackage Add(PartyPackage partyPackage);
        PartyPackage Update(PartyPackage partyPackage);
        void Remove(PartyPackage partyPackage);
        void RemoveAddon(int id);
    }
}