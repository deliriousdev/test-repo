﻿using System;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Core.Domain
{
    public class RoomBooking
    {
        public int Id { get; set; }
        public BookingType BookingType { get; set; }
        public int PartyRoomId { get; set; }
        public int TimeSlotId { get; set; }
        public DateTime Date { get; set; }
        public virtual PartyRoom PartyRoom { get; set; }
        public virtual TimeSlot TimeSlot { get; set; }
        public Status Status { get; set; }
    }

    public enum BookingType
    {
        PartyOrder,
        RoomRental,
        Corporate
    }

    public enum Status
    {
        Pending,
        Scheduled,
        Cancelled
    }
}