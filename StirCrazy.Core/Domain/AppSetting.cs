﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain
{
    public class AppSetting
    {
        [Key]
        public Guid Id { get; set; }
        public MetaType Type { get; set; }
        public string Meta { get; set; }
        public string Value { get; set; }
    }

    public enum MetaType
    {
        AdmissionRate,
        AdmissionGroupRate,
        GoogleCalendar,
        HoursOfOperation,
        Holiday
    }
}
