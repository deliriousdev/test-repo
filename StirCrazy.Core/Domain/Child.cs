﻿using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain
{
    public class Child
    {
        [Key]
        public int Id { get; set; }
        public string ParentId { get; set; }
        public Gender Gender { get; set; }
        public int Age { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        public virtual ApplicationUser Parent { get; set; }
    }

    public enum Gender
    {
        [Display(Name = "Male")]
        Male = 1,
        [Display(Name = "Female")]
        Female = 2
    }
}