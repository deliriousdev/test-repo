﻿using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using SendGrid;

namespace StirCrazy.Core.Gateways
{
    public class SendGridGateway : ISendGridGateway
    {
        private readonly string _sendGridUsername = ConfigurationManager.AppSettings["SendGridUsername"];
        private readonly string _sendGridPassword = ConfigurationManager.AppSettings["SendGridPassword"];

        public async Task Send(SendGridMessage message)
        {
            // Create network credentials to access your SendGrid account.
            var credentials = new NetworkCredential(_sendGridUsername, _sendGridPassword);

            // Create an Web transport for sending email.
            var transportWeb = new Web(credentials);

            // Disable Unsubsribe
            message.DisableUnsubscribe();

            // Send the email.
            await transportWeb.DeliverAsync(message);
        }
    }
}
