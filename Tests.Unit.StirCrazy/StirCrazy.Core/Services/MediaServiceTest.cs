﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Moq;
using NUnit.Framework;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services;

namespace Tests.Unit.StirCrazy.StirCrazy.Core.Services
{
    [TestFixture]
    public class MediaServiceTest
    {
        private Mock<IRepository<MediaFile>> _mediaFileRepositoryMock;
        private IMediaService _mediaService;

        [SetUp]
        public void SetUp()
        {
            _mediaFileRepositoryMock = new Mock<IRepository<MediaFile>>();
            _mediaService = new MediaService(_mediaFileRepositoryMock.Object);
        }

        [Test]
        public void GetShouldReturnListWhenPopulated()
        {
            var mediaFiles = new List<MediaFile>();

            var mediaFile = new MediaFile
            {
                Id = 1,
            };

            mediaFiles.Add(mediaFile);
            _mediaFileRepositoryMock.Setup(x => x.Get()).Returns(mediaFiles.AsQueryable());

            var result = _mediaService.Get();

            _mediaFileRepositoryMock.Verify(x => x.Get());
            Assert.AreEqual(1, result.Count());
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetShouldReturnMediaFileWhenProvidedWithId()
        {
            var mediaFile = new MediaFile
            {
                Id = 71,
            };

            _mediaFileRepositoryMock.Setup(x => x.Get(It.IsAny<int>())).Returns(mediaFile);
            var result = _mediaService.Get(71);

            _mediaFileRepositoryMock.Verify(x => x.Get(It.IsAny<int>()));

            Assert.IsNotNull(result);
            Assert.AreEqual(71, result.Id);
        }

        [Test]
        public void GetShouldReturnNullWhenProvidedWithNullId()
        {
            var mediaFiles = new List<MediaFile>();

            var mediaFile = new MediaFile
            {
                Id = 71,
            };

            mediaFiles.Add(mediaFile);

            var result = _mediaService.Get(0);

            Assert.IsNull(result);
        }

        [Test]
        public void ShouldDeleteMediaFileWhenUpdateAndFileDoesNotExist()
        {
            var mediaFile = new MediaFile
            {
                Id = 71,
                FilePath = "somepath"
            };
            var filesystem = new Mock<IFile>();
            filesystem.Setup(fs => fs.Exists("somepath")).Returns(true);
            _mediaFileRepositoryMock.Setup(x => x.Update(mediaFile, mediaFile.Id));

            _mediaService.Update(mediaFile);

            _mediaFileRepositoryMock.Verify(x => x.Delete(It.IsAny<MediaFile>()));
        }

        [Test]
        public void ShouldRemoveCarouselImage()
        {
            // TODO: Figure out how to test this
            //var carouselImage = new CarouselImage
            //{
            //    Id = 71,
            //};

            //_carouselImageRepository.Setup(x => x.Delete(carouselImage));
            //_carouselImageService.Remove(carouselImage.Id);

            //_carouselImageRepository.Verify(x => x.Delete(It.IsAny<CarouselImage>()));
        }


        public interface IFile
        {
            void Copy(string source, string dest);
            void Delete(string fn);
            bool Exists(string fn);
        }

        public class FileImpl : IFile
        {
            public virtual void Copy(string source, string dest) { File.Copy(source, dest); }
            public virtual void Delete(string fn) { File.Delete(fn); }
            public virtual bool Exists(string fn) { return File.Exists(fn); }
        }
    }
}
