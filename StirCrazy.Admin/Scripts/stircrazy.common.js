﻿$(function () {

    var now = moment();

    // Remove Item
    $(document).on('click', '.remove-item', function () {

        var self = this;
        var remove = confirm("WARNING: \r\nAre you sure you wish to delete this item?");

        if (remove) {

            var source = $(self);
            var id = source.data("id");
            var url = source.data("url");

            $.ajax({
                type: "POST",
                url: url,
                data: { id: id },
            });

            $('#' + id).remove();
        };
    });

    $('.datetimepicker').datetimepicker({
        timepicker: true, format: 'm/d/Y H:i', minDate: 0, step: 30
    });

    $('.datepicker').datetimepicker({
        timepicker: false, format: 'm/d/Y', minDate: 0, inline: true, startDate: now.format('YYYY/MM/DD')
    });

    $('.timepicker').datetimepicker({
        timepicker: true, datepicker: false, step: 30, format: 'H:i:s', minDate: 0, inline: true
    });

});