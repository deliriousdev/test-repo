﻿using System;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Gateways;
using TuesPechkin;

namespace StirCrazy.Core.Services
{
    public class SendGridService : ISendGridService
    {
        private readonly ISendGridGateway _sendGridGateway;

        public SendGridService(ISendGridGateway sendGridGateway)
        {
            _sendGridGateway = sendGridGateway;
        }

        public async Task Send(SendGridMessage message)
        {
            await _sendGridGateway.Send(message);
        }

        public async Task SendEmail(string baseUri, Guid id, string email, ConfirmationType type)
        {
            // Override Confirmations
            //email = "info@thinkmonkey.ca";

            switch (type)
            {
                case ConfirmationType.BakeryOrder:
                    await SendBakeryOrderConfirmation(baseUri, id, email);
                    break;

                case ConfirmationType.GrabNGoOrder:
                    await SendGrabNGoOrderConfirmation(baseUri, id, email);
                    break;

                case ConfirmationType.PartyOrder:
                    await SendPartyOrderConfirmation(baseUri, id, email);
                    break;

                case ConfirmationType.RoomOrder:
                    await SendRoomOrderConfirmation(baseUri, id, email);
                    break;

                case ConfirmationType.AnnualPassOrder:
                    await SendAnnualPassConfirmation(baseUri, id, email);
                    break;
            }
        }

        public async Task SendAccountInformation(ApplicationUser user, string password)
        {
            var email = user.Email;
            var mail = new SendGridMessage
            {
                Subject = "Account Information",
                Html = "<h2>Stir Crazy Family Fun Centre</h2>"
                + "<br /><br />"
                + "<p>Hello " + user.FirstName + ", "
                + "<br /><br />"
                + "Your account has been registered with Stir Crazy Family Fun Centre and is waiting activation.<br />"
                + "For your records please keep the following login information in a safe and secure location.<br />"
                + "<br /><br />"
                + "Username: <strong>" + user.UserName + "</strong><br />"
                + "Password: <strong>" + password + "</strong><br />"
                + "<br /><i>Please login with the above credentials to activate your account.</i>"
                + "<br /><br />"
                + "<p>Thank You, <br />Stir Crazy Team</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            await _sendGridGateway.Send(mail);
        }

        public byte[] GetPdfBytes(string url)
        {
            var pdfAsByte = new byte[0];
            if (!string.IsNullOrEmpty(url))
            {
                var tuesPechkin = Factory.Create();
                var htmlToPdfDoc = new HtmlToPdfDocument()
                {
                    GlobalSettings = new GlobalSettings()
                    {
                        DocumentTitle = "Order Confirmation",
                        Orientation = GlobalSettings.PaperOrientation.Portrait,
                        PaperSize = System.Drawing.Printing.PaperKind.Letter,
                        Margins =
                        {
                            All = 1.375,
                            Unit = Unit.Centimeters
                        }
                    },
                    Objects = 
                    {
                        new ObjectSettings
                        {
                            PageUrl = url
                        }
                    }
                };
                pdfAsByte = tuesPechkin.Convert(htmlToPdfDoc);
            }
            return pdfAsByte;
        }

        private async Task SendAnnualPassConfirmation(string baseUri, Guid id, string email)
        {
            string confirmationUri = baseUri + "Print/" + id;
            byte[] confirmationPdf = GetPdfBytes(confirmationUri);

            var mail = new SendGridMessage
            {
                Subject = "Stir Crazy - Annual Pass Confirmation",
                Html = "<h2>Annual Pass Confirmation from Stir Crazy Family Fun Centre</h2>"
                + "<br /><br />"
                + "<p>Hello,"
                + "<br /><br />"
                + "Thank you for ordering your annual pass with Stir Crazy, our team is busy reviewing and confirming your order.<br />"
                + "For your records we have attached a copy of your order confirmation and will contact you if any further information is required to process your order.<br />"
                + "Please print a copy of your order confirmation and present it at stir crazy to claim your order.<br /><br />"
                + "If you require any further assistance please email to partycoordinator@stircrazy.ca"
                + "<br /><br />"
                + "<p>Thank You, <br />Stir Crazy Team</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            using (var stream = new MemoryStream(confirmationPdf))
            {
                stream.Position = 0;
                mail.StreamedAttachments.Add("stircrazy_order-confirmation.pdf", stream);

                await _sendGridGateway.Send(mail);
            }
        }

        private async Task SendBakeryOrderConfirmation(string baseUri, Guid id, string email)
        {
            string confirmationUri = baseUri + "Print/" + id;
            byte[] confirmationPdf = GetPdfBytes(confirmationUri);

            var mail = new SendGridMessage
            {
                Subject = "Stir Crazy - Bakery Order Confirmation",
                Html = "<h2>Order Confirmation from Stir Crazy Family Fun Centre</h2>"
                + "<br /><br />"
                + "<p>Hello,"
                + "<br /><br />"
                + "Thank you for placing your bakery order with Stir Crazy, our team is busy reviewing and confirming your order.<br />"
                + "For your records we have attached a copy of your order confirmation and will contact you if any further information is required to process your order.<br />"
                + "Please print a copy of your order confirmation and present it at stir crazy to claim your order.<br /><br />"
                + "Please Note: Any special notes you entered at the time of your order will not be addressed until the week of your order date.<br />"
                + "If you require immediate assistance please email to partycoordinator@stircrazy.ca"
                + "<br /><br />"
                + "<p>Thank You, <br />Stir Crazy Team</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            using (var stream = new MemoryStream(confirmationPdf))
            {
                stream.Position = 0;
                mail.StreamedAttachments.Add("stircrazy_order-confirmation.pdf", stream);

                await _sendGridGateway.Send(mail);
            }
        }

        private async Task SendGrabNGoOrderConfirmation(string baseUri, Guid id, string email)
        {
            string confirmationUri = baseUri + "Print/" + id;
            byte[] confirmationPdf = GetPdfBytes(confirmationUri);

            var mail = new SendGridMessage()
            {
                Subject = "Stir Crazy - Grab N Go Confirmation",
                Html = "<h2>Order Confirmation from Stir Crazy Family Fun Centre</h2>"
                + "<br /><br />"
                + "<p>Hello,"
                + "<br /><br />"
                + "Thank you for placing your order with Stir Crazy, our team is busy reviewing and confirming your order.<br />"
                + "For your records we have attached a copy of your order confirmation and will contact you if any further information is required to process your order.<br />"
                + "Please print a copy of your order confirmation and present it at stir crazy to claim your order.<br /><br />"
                + "Please Note: Any special notes you entered at the time of your order will not be addressed until the week of your order date.<br />"
                + "If you require immediate assistance please email to partycoordinator@stircrazy.ca"
                + "<br /><br />"
                + "<p>Thank You, <br />Stir Crazy Team</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            using (var stream = new MemoryStream(confirmationPdf))
            {
                stream.Position = 0;
                mail.StreamedAttachments.Add("stircrazy_order-confirmation.pdf", stream);

                await _sendGridGateway.Send(mail);
            }
        }

        private async Task SendRoomOrderConfirmation(string baseUri, Guid id, string email)
        {
            string confirmationUri = baseUri + "Print/" + id;
            byte[] confirmationPdf = GetPdfBytes(confirmationUri);

            var mail = new SendGridMessage()
            {
                Subject = "Stir Crazy - Room Booking Confirmation",
                Html = "<h2>Order Confirmation from Stir Crazy Family Fun Centre</h2>"
                + "<br /><br />"
                + "<p>Hello,"
                + "<br /><br />"
                + "Thank you for placing your order with Stir Crazy, our team is busy reviewing and confirming your order.<br />"
                + "For your records we have attached a copy of your order confirmation and will contact you if any further information is required to process your order.<br />"
                + "Please print a copy of your order confirmation and present it at stir crazy to claim your order.<br /><br />"
                + "Please Note: Any special notes you entered at the time of your order will not be addressed until the week of your order date.<br />"
                + "If you require immediate assistance please email to partycoordinator@stircrazy.ca"
                + "<br /><br />"
                + "<p>Thank You, <br />Stir Crazy Team</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            using (var stream = new MemoryStream(confirmationPdf))
            {
                stream.Position = 0;
                mail.StreamedAttachments.Add("stircrazy_order-confirmation.pdf", stream);

                await _sendGridGateway.Send(mail);
            }
        }

        private async Task SendPartyOrderConfirmation(string baseUri, Guid id, string email)
        {
            string confirmationUri = baseUri + "Print/" + id;
            string invitesUri = baseUri + "Invites/" + id;
            string thanksUri = baseUri + "ThankYou/" + id;
            string mapUri = baseUri + "Map";

            byte[] confirmationPdf = GetPdfBytes(confirmationUri);
            byte[] invitesPdf = GetPdfBytes(invitesUri);
            byte[] thanksPdf = GetPdfBytes(thanksUri);
            byte[] mapPdf = GetPdfBytes(mapUri);

            var mail = new SendGridMessage()
            {
                Subject = "Stir Crazy - Party Order Confirmation",
                Html = "<h2>Order Confirmation from Stir Crazy Family Fun Centre</h2>"
                + "<br /><br />"
                + "<p>Hello,"
                + "<br /><br />"
                + "Thank you for booking your next birthday party with Stir Crazy, our team is busy reviewing and confirming your order.<br />"
                + "For your records we have attached a copy of your order confirmation and will contact you if any further information is required to process your order.<br />"
                + "Please print a copy of your order confirmation and present it at stir crazy to claim your order.<br /><br />"
                + "Please Note: Any special notes you entered at the time of your order will not be addressed until the week of your order date.<br />"
                + "If you require immediate assistance please email to partycoordinator@stircrazy.ca"
                + "<br /><br />"
                + "<p>Thank You, <br />Stir Crazy Team</p>",
            };

            var stream1 = new MemoryStream(confirmationPdf) { Position = 0 };
            var stream2 = new MemoryStream(invitesPdf) { Position = 0 };
            var stream3 = new MemoryStream(thanksPdf) { Position = 0 };
            var stream4 = new MemoryStream(mapPdf) { Position = 0 };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            mail.StreamedAttachments.Add("stircrazy_party-confirmation.pdf", stream1);
            mail.StreamedAttachments.Add("stircrazy_party-invites.pdf", stream2);
            mail.StreamedAttachments.Add("stircrazy_party-thankyou.pdf", stream3);
            mail.StreamedAttachments.Add("stircrazy_map.pdf", stream4);

            await _sendGridGateway.Send(mail);
        }
    }
}
