﻿using System;

namespace StirCrazy.Admin.Models.Calendar
{
    public class CalendarEventModel
    {
        public string ItemId { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public bool IsAllDay { get; set; }
    }
}