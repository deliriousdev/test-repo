﻿using PayPal;

namespace StirCrazy.Core.Gateways
{
    public interface IPayPalGateway
    {
        APIContext Api { get; }
    }
}