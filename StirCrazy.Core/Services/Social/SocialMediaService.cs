﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Gateways;
using Tweetinvi.Core.Interfaces;

namespace StirCrazy.Core.Services.Social
{
    public class SocialMediaService : ISocialMediaService
    {
        private readonly IFacebookGateway _facebookGateway;
        private readonly ITwitterGateway _twitterGateway;

        public SocialMediaService(IFacebookGateway facebookGateway, ITwitterGateway twitterGateway)
        {
            _facebookGateway = facebookGateway;
            _twitterGateway = twitterGateway;
        }

        public List<ITweet> GetTwitterTweets()
        {
            return _twitterGateway.GetTweets();
        }

        public List<FacebookPost> GetFacebookPosts()
        {
            var response = _facebookGateway.GetPosts();

            XDocument document = XDocument.Parse(response);

            var posts = document.Descendants("item").ToList();
            var pagePosts = new List<FacebookPost>();

            int index = 0;
            foreach (var post in posts)
            {

                pagePosts.Add(new FacebookPost
                {
                    Title = WebUtility.HtmlDecode(post.Element("title").Value),
                    Description = post.Element("description").Value.Replace("<![CDATA[", "").Replace("]]>", "").Trim(),
                    PubDate = post.Element("pubDate").Value,
                    Author = post.Element("author").Value,
                    Link = post.Element("link").Value
                });

                index++;
                if (index >= 10) break;
            }

            return pagePosts;
        }
    }
}
