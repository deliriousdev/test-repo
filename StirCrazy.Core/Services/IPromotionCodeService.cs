using System;
using System.Collections.Generic;
using StirCrazy.Core.Domain;

namespace StirCrazy.Core.Services
{
    public interface IPromotionCodeService
    {
        IEnumerable<PromotionCode> Get();
        PromotionCode Get(Guid id);
        PromotionCode Get(string promotionCode);
        PromotionCode Get(string promotionCode, PromotionType promotionType);
        PromotionCode Add(PromotionCode promotionCode);
        PromotionCode Update(PromotionCode promotionCode);
        void Remove(Guid id);
    }
}