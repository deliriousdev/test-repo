﻿using System;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Core.Domain
{
    public class CorporateEvent
    {
        public Guid Id { get; set; }
        public string EventId { get; set; }
        public string CustomerId { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public bool IsAllDay { get; set; }
        public string Notes { get; set; }
        public decimal Total { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }
        public virtual ApplicationUser Customer { get; set; }
    }
}
