﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Core.Domain
{
    public class PromotionCode
    {
        public Guid Id { get; set; }
        public PromotionType PromotionType { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Adjustment { get; set; }
        public AdjustmentType AdjustmentType { get; set; }
        public DateTime Created { get; set; }
        public DateTime Expired { get; set; }
    }

    public enum PromotionType
    {
        [Display(Name = "Party Bookings")]
        PartyBooking = 1,
        [Display(Name = "Room Rentals")]
        RoomRental = 2,
        [Display(Name = "Grab N Go's")]
        GrabNGo = 3,
        [Display(Name = "Bakery Orders")]
        Bakery = 4,
        [Display(Name = "Annual Passes")]
        AnnualPass = 5
    }

    public enum AdjustmentType
    {
        [Display(Name = "%")]
        Percentage = 1,
        [Display(Name = "$")]
        Discount = 2
    }
}
