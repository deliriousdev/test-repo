using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace StirCrazy.Core.Repositories
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> Get();
        Task<List<T>> GetAsync();
        Task<IEnumerable<T>> GetAllAsync();
        T Get(int id);
        Task<T> GetAsync(int id);
        Task<T> GetAsync(Guid id);
        T Get(Guid id);
        T Find(Expression<Func<T, bool>> match);
        Task<T> FindAsync(Expression<Func<T, bool>> match);
        IEnumerable<T> FindAll(Expression<Func<T, bool>> match);
        Task<List<T>> FindAllAsync(Expression<Func<T, bool>> match);
        T Add(T t);
        Task<T> AddAsync(T t);
        T Update(T updated,int key);
        Task<T> UpdateAsync(T updated, int key);
        T Update(T updated, Guid key);
        Task<T> UpdateAsync(T updated, Guid key);
        void Delete(T t);
        Task DeleteAsync(T t);
        int Count();
        Task<int> CountAsync();
    }
}