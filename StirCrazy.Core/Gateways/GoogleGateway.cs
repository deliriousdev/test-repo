﻿using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;

namespace StirCrazy.Core.Gateways
{
    public class GoogleGateway : IGoogleGateway
    {
        private readonly CalendarService _calendarService;

        private readonly string _googleServiceAccount = ConfigurationManager.AppSettings["GoogleServiceAccount"];
        private readonly string _googleApplicationName = ConfigurationManager.AppSettings["GoogleApplicationName"];
        private readonly string _googleCalendarId = ConfigurationManager.AppSettings["GoogleCalendarId"];

        public GoogleGateway()
        {
            var certPath = HttpContext.Current.Server.MapPath("/App_Data/Certs/StirCrazy-GoogleCert.p12");
            var certificate = new X509Certificate2(certPath, "notasecret", X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.DefaultKeySet);

            var credential = new ServiceAccountCredential(
               new ServiceAccountCredential.Initializer(_googleServiceAccount)
               {
                   Scopes = new[] { CalendarService.Scope.Calendar }
               }.FromCertificate(certificate));

            // Create the service
            var service = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = _googleApplicationName,
            });

            _calendarService = service;
        }

        public Events GetEvents()
        {
            return _calendarService.Events.List(_googleCalendarId).Execute();
        }

        public Event GetEvent(string eventId)
        {
            return _calendarService.Events.Get(_googleCalendarId, eventId).Execute();
        }

        public void InsertEvent(Event calendarEvent)
        {
            _calendarService.Events.Insert(calendarEvent, _googleCalendarId).Execute();
        }

        public void UpdateEvent(Event calendarEvent, string eventId)
        {
            _calendarService.Events.Update(calendarEvent, _googleCalendarId, eventId).Execute();
        }

        public void RemoveEvent(string eventId)
        {
            _calendarService.Events.Delete(_googleCalendarId, eventId).Execute();
        }
    }
}
