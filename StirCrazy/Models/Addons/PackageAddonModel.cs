﻿using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Models.Addons
{
    public class PackageAddonModel
    {
        public int Id { get; set; }
        public int PackageId { get; set; }
        public int AddonTypeId { get; set; }
        public AddonType AddonType { get; set; }
        public int? Quantity { get; set; }
        public int RequiredGuests { get; set; }
    }
}