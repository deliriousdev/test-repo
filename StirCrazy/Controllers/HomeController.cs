﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Social;
using StirCrazy.Core.Services.Store;
using StirCrazy.Models;
using StirCrazy.Models.Carousel;
using StirCrazy.Models.Store;

namespace StirCrazy.Controllers
{
    public class HomeController : AsyncController
    {
        private readonly IMediaService _mediaService;
        private readonly IAnnouncementService _announcementService;
        private readonly IStoreService _storeService;
        private readonly IPromotionCodeService _promotionCodeService;
        private readonly ISocialMediaService _socialMediaService;
        private readonly IPartyOrderService _partyOrderService;

        public HomeController(IAnnouncementService announcementService,
            IMediaService mediaService,
            IStoreService storeService, 
            IPromotionCodeService promotionCodeService, 
            ISocialMediaService socialMediaService, IPartyOrderService partyOrderService)
        {
            _announcementService = announcementService;
            _mediaService = mediaService;
            _storeService = storeService;
            _promotionCodeService = promotionCodeService;
            _socialMediaService = socialMediaService;
            _partyOrderService = partyOrderService;
        }

        public ActionResult Index()
        {
            var model = new CarouselModel
            {
                CarouselImages = _mediaService.Get(UploadFolder.Carousel).Where(x => x.IsActive).ToList()
            };

            return View(model);
        }

        public async Task<ActionResult> Test(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);
            await _partyOrderService.SendNotificationAsync(partyOrder);
            await _partyOrderService.CreateOutlookEvent(partyOrder);

            return View("Index");
        }

        public ActionResult Rules()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        [OutputCache(Duration = 3600)]
        public JsonResult Announcements()
        {
            var data = _announcementService.Get().Where(x => !x.IsPopup);
            return Json(data, JsonRequestBehavior.DenyGet);
        }

        //[OutputCache(Duration = 3600)]
        public ActionResult _PopupMessages()
        {
            var announcements = _announcementService.Get().Where(x => x.IsPopup && (x.StartDate <= DateTime.Now && x.EndDate >= DateTime.Now)).Select(x => new AnnouncementModel()
            {
                Id = x.Id,
                StartDate = x.StartDate.ToShortDateString(),
                EndDate = x.EndDate.ToShortDateString(),
                MediaFileId = x.MediaFileId,
                MediaFile = x.MediaFile,
                Message = x.Message,
                IsPopup = x.IsPopup,
                IsImage = x.IsImage
            });

            var model = new PopupModel()
            {
                Announcements = announcements.ToList()
            };

            return PartialView(model);
        }

        [OutputCache(Duration = 3600)]
        public ActionResult _BusinessHours()
        {
            var model = new StoreModel()
            {
                StoreHolidays = _storeService.GetStoreHolidays().Where(o => o.Date >= DateTime.Now && o.Date <= DateTime.Now.AddDays(30)).Select(x => new StoreHolidayModel()
                {
                    Id = x.Id,
                    Date = x.Date,
                    OpenTime = x.OpenTime.ToString(),
                    ClosedTime = x.ClosedTime.ToString(),
                    Holiday = x.Holiday,
                    IsClosed = x.IsClosed
                }).ToList(),
                StoreHours = _storeService.GetStoreHours().Select(x => new StoreHourModel()
                {
                    Id = x.Id,
                    DayOfWeek = x.DayOfWeek,
                    OpenTime = x.OpenTime,
                    ClosedTime = x.ClosedTime,
                    IsClosed = x.IsClosed
                }).ToList()
            };

            return PartialView(model);
        }

        [OutputCache(Duration = 3600)]
        public ActionResult _FacebookFeed()
        {
            var model = _socialMediaService.GetFacebookPosts().Select(x => new SocialFeedModel()
            {
                Author = x.Author,
                Description = x.Description,
                PubDate = x.PubDate,
                Link = x.Link,
                Title = x.Title
            }).ToList();

            return PartialView(model);
        }


        [HttpPost]
        public JsonResult PromotionCode(string promoCode, PromotionType promoType)
        {
            var response = _promotionCodeService.Get(promoCode.ToUpper(), promoType);

            // INVALID
            if (response == null)
            {
                return Json(new PromotionCode()
                {
                    Description = "(!) The promotional code you entered is not recognized."
                }, JsonRequestBehavior.DenyGet);
            }

            // EXPIRED
            if (response.Expired < DateTime.Now)
            {
                return Json(new PromotionCode()
                {
                    Description = "(!) The promotional code you entered is no longer available."
                }, JsonRequestBehavior.DenyGet);
            }

            return Json(response, JsonRequestBehavior.DenyGet);
        }
    }
}