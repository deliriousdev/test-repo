﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using PayPal.Api.Payments;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Services;
using StirCrazy.Core.Services.Order;
using StirCrazy.Models.Order;

namespace StirCrazy.Controllers
{
    public class AnnualPassController : AsyncController
    {
        private readonly IAnnualPassOrderService _annualPassOrderService;
        private readonly IPromotionCodeService _promotionCodeService;
        private readonly IPayPalService _payPalService;

        public AnnualPassController(IPromotionCodeService promotionCodeService,
            IAnnualPassOrderService annualPassOrderService,
            IPayPalService payPalService)
        {
            _promotionCodeService = promotionCodeService;
            _annualPassOrderService = annualPassOrderService;
            _payPalService = payPalService;
        }

        public ActionResult Create()
        {
            var model = new AnnualPassModel();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(AnnualPassModel model)
        {
            var admissionOrder = new AnnualPassOrder()
            {
                Parent = model.Customer,
                Child = model.Child,
                PickUpDate = model.PickUpDate,
                PickUpTime = model.PickUpTime,
                PaymentTransaction = new PaymentTransaction()
                {
                    Amount = model.Total,
                    PaymentDate = DateTime.Now
                },
                Notes = model.Notes,
                SubTotal = model.SubTotal,
                Tax = model.Tax,
                Total = model.Total,
                Status = OrderStatus.Created
            };

            if (model.Promotion != null)
            {
                var promoCode = _promotionCodeService.Get(model.Promotion);

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Percentage)
                {
                    admissionOrder.Discount = (promoCode.Adjustment / 100) * admissionOrder.Total;
                    admissionOrder.PromotionCode = promoCode;
                }

                if (promoCode != null && promoCode.AdjustmentType == AdjustmentType.Discount)
                {
                    admissionOrder.Discount = promoCode.Adjustment;
                    admissionOrder.PromotionCode = promoCode;
                }
            }

            // BYPASS PAYPAL PAYMENT PROCESSING
            if (model.BypassPaypal)
            {
                admissionOrder.PaymentTransaction.TransactionType = model.PaymentTransaction.TransactionType;
                admissionOrder.PaymentTransaction.TransactionNumber = model.PaymentTransaction.TransactionNumber ?? "Not Applicable";
                admissionOrder.PaymentTransaction.PaymentStatus = "Approved";

                // SAVE DATABASE
                admissionOrder.CreatedBy = "Administrator";
                admissionOrder.Status = OrderStatus.Paid;
                var order = await _annualPassOrderService.CreateAsync(admissionOrder);

                // SEND NOTIFICATION
                try { await _annualPassOrderService.SendOrderNotificationAsync(admissionOrder); }
                catch (Exception ex) { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                var redirectUrl = string.Format("{0}://{1}/AnnualPass/Email/{2}", Request.Url.Scheme, Request.Url.Authority, order.Id);
                return new RedirectResult(redirectUrl);
            }

            // SAVE DATABASE
            admissionOrder.CreatedBy = "Customer";
            await _annualPassOrderService.CreateAsync(admissionOrder);

            // PAYPAL PAYMENT PROCESSING
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/AnnualPass/";
            var returnUrl = baseUri + "Payment/" + admissionOrder.Id;
            var cancelUrl = baseUri + "Cancelled/" + admissionOrder.Id;

            return await ProcessPayment(admissionOrder.Id, (double)admissionOrder.Total, "Stir Crazy - Annual Pass", returnUrl, cancelUrl);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var admissionOrder = await _annualPassOrderService.GetAsync(id);
            if (admissionOrder == null) return HttpNotFound();

            var model = new AnnualPassModel()
            {
                Customer = admissionOrder.Parent,
                Child = admissionOrder.Child,
                Notes = admissionOrder.Notes,
                PickUpDate = admissionOrder.PickUpDate,
                PickUpTime = admissionOrder.PickUpTime,
                PaymentTransaction = admissionOrder.PaymentTransaction,
                SubTotal = admissionOrder.SubTotal,
                Tax = admissionOrder.Tax,
                Discount = admissionOrder.Discount,
                Total = admissionOrder.Total,
                Status = admissionOrder.Status,
                Promotion = (admissionOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", admissionOrder.PromotionCode.Code, admissionOrder.PromotionCode.Description) : "NONE"
            };

            return View(model);
        }

        public async Task<ActionResult> Print(Guid id)
        {
            var admissionOrder = await _annualPassOrderService.GetAsync(id);
            if (admissionOrder == null) return HttpNotFound();

            var model = new AnnualPassModel()
            {
                Customer = admissionOrder.Parent,
                Child = admissionOrder.Child,
                Notes = admissionOrder.Notes,
                PickUpDate = admissionOrder.PickUpDate,
                PickUpTime = admissionOrder.PickUpTime,
                PaymentTransaction = admissionOrder.PaymentTransaction,
                SubTotal = admissionOrder.SubTotal,
                Tax = admissionOrder.Tax,
                Discount = admissionOrder.Discount,
                Total = admissionOrder.Total,
                Status = admissionOrder.Status,
                Promotion = (admissionOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", admissionOrder.PromotionCode.Code, admissionOrder.PromotionCode.Description) : "NONE"
            };

            return View(model);
        }

        private async Task<RedirectResult> ProcessPayment(Guid id, double amount, string description, string returnUrl, string cancelUrl)
        {
            if (Request.Url != null)
            {
                var payment = _payPalService.CreatePayment(amount, description, returnUrl, cancelUrl);
                if (payment != null)
                {
                    var paymentId = payment.id;
                    var paymentState = payment.state;

                    var admissionOrder = await _annualPassOrderService.GetAsync(id);

                    if (admissionOrder != null)
                    {
                        admissionOrder.PaymentTransaction.Id = admissionOrder.PaymentTransaction.Id;
                        admissionOrder.PaymentTransaction.TransactionType = TransactionType.Paypal;
                        admissionOrder.PaymentTransaction.PaymentDate = DateTime.Now;
                        admissionOrder.PaymentTransaction.PaymentStatus = paymentState;
                        admissionOrder.PaymentTransaction.TransactionNumber = paymentId;
                        admissionOrder.PaymentTransaction.Amount = (decimal)amount;

                        await _annualPassOrderService.UpdateAsync(admissionOrder);
                    }

                    var redirectUrl = string.Format("{0}{1}", GetApprovalUrl(payment), "&useraction=commit");
                    return new RedirectResult(redirectUrl);
                }
            }

            return null;
        }

        public async Task<ActionResult> Payment(Guid id)
        {
            var payer = Request["PayerID"];
            var partyOrder = await _annualPassOrderService.GetAsync(id);

            var payment = new Payment
            {
                id = partyOrder.PaymentTransaction.TransactionNumber,
                state = partyOrder.PaymentTransaction.PaymentStatus
            };

            var result = _payPalService.ExecutePayment(payment, payer);
            if (result.state == "approved")
            {
                partyOrder.Status = OrderStatus.Paid;
            }

            partyOrder.PaymentTransaction.PaymentStatus = result.state;
            
            await _annualPassOrderService.UpdateAsync(partyOrder);
            await _annualPassOrderService.SendOrderNotificationAsync(partyOrder);

            return RedirectToAction("Email", new RouteValueDictionary { { "Id", id } });
        }

        [HttpPost]
        public ActionResult Cancelled(Guid id)
        {
            _annualPassOrderService.RemoveAsync(id);
            return RedirectToAction("Index", "Admission");
        }

        [AllowAnonymous]
        public ActionResult Save(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/AnnualPass/";
            var detailsUrl = baseUri + "Print/" + id;

            var bytesAsPdf = _annualPassOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Email(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/AnnualPass/";

            var partyOrder = await _annualPassOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            await _annualPassOrderService.SendEmailAsync(baseUri, id, partyOrder.Parent.Email);
            return RedirectToAction("Details", new RouteValueDictionary { { "Id", id } });
        }

        private string GetApprovalUrl(Payment payment)
        {
            var links = payment.links;
            return (from lnk in links where lnk.rel.ToLower().Equals("approval_url") select Server.UrlDecode(lnk.href)).FirstOrDefault();
        }
    }
}