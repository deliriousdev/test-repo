﻿using System;
using System.Collections.Generic;
using StirCrazy.Admin.Models.Addons;
using StirCrazy.Core;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Admin.Models.Order
{
    public class GrabNGoModel
    {
        public Guid Id { get; set; }
        public Guid? PromotionCodeId { get; set; }
        public int PayPalTransactionId { get; set; }
        public DateTime PickUpDate { get; set; }
        public PickUpTime PickUpTime { get; set; }
        public List<AddonModel> Addons { get; set; }
        public List<GrabNGoOrderAddon> OrderAddons { get; set; }
        public List<AddonGroupModel> AddonGroups { get; set; }
        public string Notes { get; set; }
        public decimal Discount { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public string Promotion { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }
        public ApplicationUser Customer { get; set; }
        public PaymentTransaction PaymentTransaction { get; set; }
        public PromotionCode PromotionCode { get; set; }
    }
}