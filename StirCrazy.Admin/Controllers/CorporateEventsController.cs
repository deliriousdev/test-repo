﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Party;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Services;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class CorporateEventsController : AsyncController
    {
        private readonly ICorporateEventService _corporateEventService;
        private readonly IRoomBookingService _roomBookingService;

        public CorporateEventsController(ICorporateEventService corporateEventService, IRoomBookingService roomBookingService)
        {
            _corporateEventService = corporateEventService;
            _roomBookingService = roomBookingService;
        }

        public ActionResult Index(OrderStatus status = OrderStatus.Created)
        {
            var model = _corporateEventService.GetByStatus(status).Select(x => new CorporateEventModel()
            {
                Id = x.Id,
                Date = x.Date,
                Customer = x.Customer,
                StartTime = x.StartTime.ToString(),
                EndTime = x.EndTime.ToString(),
                CreatedBy = x.CreatedBy,
                IsAllDay = x.IsAllDay,
                Status = x.Status
            });
            return View(model);
        }

        [HttpPost]
        public JsonResult Availability(DateTime searchDate, string startTime, string endTime)
        {
            DateTime openTime = DateTime.Now;
            DateTime closeTime = DateTime.Now;

            if (startTime != null && endTime != null)
            {
                openTime = DateTime.ParseExact(startTime, "h:mm tt", CultureInfo.InvariantCulture);
                closeTime = DateTime.ParseExact(endTime, "h:mm tt", CultureInfo.InvariantCulture);
            }

            var bookings = _roomBookingService.CheckAvailability(searchDate, openTime.TimeOfDay, closeTime.TimeOfDay);

            return Json(bookings, JsonRequestBehavior.DenyGet);
        }


        public ActionResult Create()
        {
            var model = new CorporateEventModel()
            {
                Date = DateTime.Now
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CorporateEventModel model)
        {
            if (ModelState.IsValid)
            {
                DateTime startTime = new DateTime();
                DateTime endTime = new DateTime();

                if (model.StartTime != null && model.EndTime != null)
                {
                    startTime = DateTime.ParseExact(model.StartTime, "h:mm tt", CultureInfo.InvariantCulture);
                    endTime = DateTime.ParseExact(model.EndTime, "h:mm tt", CultureInfo.InvariantCulture);
                }

                var corporateEvent = new CorporateEvent()
                {
                    Date = model.Date,
                    Customer = model.Customer,
                    StartTime = startTime.TimeOfDay,
                    EndTime = endTime.TimeOfDay,
                    IsAllDay = model.IsAllDay,
                    Notes = model.Notes,
                    CreatedBy = User.Identity.Name,
                    Total = model.Total,
                    Status = OrderStatus.Scheduled
                };

                _corporateEventService.Add(corporateEvent);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Details(Guid id)
        {
            var corporateEvent = _corporateEventService.Get(id);
            if (corporateEvent == null) return HttpNotFound();

            var model = new CorporateEventModel()
            {
                Id = corporateEvent.Id,
                IsAllDay = corporateEvent.IsAllDay,
                Date = corporateEvent.Date,
                StartTime = corporateEvent.StartTime.ToString(),
                EndTime = corporateEvent.EndTime.ToString(),
                Customer = corporateEvent.Customer,
                CreatedBy = corporateEvent.CreatedBy,
                CustomerId = corporateEvent.CustomerId,
                Notes = corporateEvent.Notes,
                Status = corporateEvent.Status
            };

            return View(model);
        }

        public ActionResult Print(Guid id)
        {
            var corporateEvent = _corporateEventService.Get(id);
            if (corporateEvent == null) return HttpNotFound();

            var model = new CorporateEventModel()
            {
                Id = corporateEvent.Id,
                IsAllDay = corporateEvent.IsAllDay,
                Date = corporateEvent.Date,
                StartTime = corporateEvent.StartTime.ToString(),
                EndTime = corporateEvent.EndTime.ToString(),
                Customer = corporateEvent.Customer,
                CreatedBy = corporateEvent.CreatedBy,
                CustomerId = corporateEvent.CustomerId,
                Notes = corporateEvent.Notes,
                Status = corporateEvent.Status
            };

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Save(Guid id)
        {
            string baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/CorporateEvents/";
            string detailsUrl = baseUri + "Print/" + id;

            byte[] bytesAsPdf = _corporateEventService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Event_Confirmation.pdf");

            return HttpNotFound();
        }

        [AllowAnonymous]
        public ActionResult Email(Guid id)
        {
            var partyOrder = _corporateEventService.Get(id);
            if (partyOrder == null) return HttpNotFound();

            string baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/CorporateEvents/";
            _corporateEventService.SendEmail(baseUri, partyOrder.Id, partyOrder.Customer.Email); ;

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Delete(Guid id)
        {
            var corporateEvent = _corporateEventService.Get(id);
            if (corporateEvent == null) return HttpNotFound();

            var model = new CorporateEventModel()
            {
                Id = corporateEvent.Id,
                IsAllDay = corporateEvent.IsAllDay,
                Date = corporateEvent.Date,
                StartTime = corporateEvent.StartTime.ToString(),
                EndTime = corporateEvent.EndTime.ToString(),
                Customer = corporateEvent.Customer,
                CreatedBy = corporateEvent.CreatedBy,
                CustomerId = corporateEvent.CustomerId,
                Notes = corporateEvent.Notes,
                Status = corporateEvent.Status
            };

            return View(model);
        }

        [HttpPost]
        public async Task<RedirectToRouteResult> Delete(CorporateEventModel model)
        {
            await _corporateEventService.CancelAsync(model.Id);
            return RedirectToAction("Index");
        }

        public async Task<RedirectToRouteResult> Cancelled(Guid id)
        {
            await _corporateEventService.CancelAsync(id);
            return RedirectToAction("Index", "Home");
        }
    }
}