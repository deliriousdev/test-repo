﻿using System.Linq;
using System.Web.Mvc;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Services;

namespace StirCrazy.Controllers
{
    public class AdmissionController : Controller
    {
        private readonly IAppSettingService _appSettingService;

        public AdmissionController(IAppSettingService appSettingService)
        {
            _appSettingService = appSettingService;
        }

        public ActionResult Index()
        {
            var admissionRates = _appSettingService.Get().Where(x => x.Type == MetaType.AdmissionRate).ToList();
            return View(admissionRates);
        }
    }
}