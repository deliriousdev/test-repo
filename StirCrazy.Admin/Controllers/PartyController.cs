﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Addons;
using StirCrazy.Admin.Models.Order;
using StirCrazy.Admin.Models.Party;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services.Order;
using StirCrazy.Core.Services.Party;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class PartyController : AsyncController
    {
        private readonly IPartyOrderService _partyOrderService;
        private readonly IPartyPackageService _partyPackageService;

        private readonly IRepository<PartyOrder> _repository;

        public PartyController(IPartyOrderService partyOrderService, IPartyPackageService partyPackageService, IRepository<PartyOrder> repository)
        {
            _partyOrderService = partyOrderService;
            _partyPackageService = partyPackageService;
            _repository = repository;
        }

        public ActionResult Sync()
        {
            var parties = _partyOrderService.Get().Where(x => x.Status == OrderStatus.Created);

            foreach (var partyOrder in parties)
            {
                partyOrder.Status = OrderStatus.Paid;
                _repository.Update(partyOrder, partyOrder.Id);
            }

            return RedirectToAction("Index");

        }

        public ActionResult Index(OrderStatus status = OrderStatus.Created)
        {
            var model = _partyOrderService.GetByStatus(status).Select(x => new PartyOrderModel()
            {
                Id = x.Id,
                AdditionalGuests = x.AdditionalGuests,
                PartyTheme = x.PartyTheme ?? "",
                CakeFlavour = x.CakeFlavour ?? "",
                CakeTheme = x.CakeTheme ?? "",
                HasOwnCake = x.HasOwnCake,
                HasOwnDecor = x.HasOwnDecor,
                HasCupcakes = x.HasCupcakes,
                SwapFoodPlatter = x.SwapFoodPlatter,
                UpgradeLootBags = x.UpgradeLootBags,
                UpgradeLatexBalloons = x.UpgradeLatexBalloons,
                Parent = x.Parent,
                Child = x.Child ?? new Child(),
                PartyPackageId = x.PartyPackage.Id,
                PartyPackage = new PartyPackageModel()
                {
                    Id = x.PartyPackage.Id,
                    Name = x.PartyPackage.Name,
                    Description = x.PartyPackage.Description,
                    Guests = x.PartyPackage.Guests,
                    MaxGuests = x.PartyPackage.MaxGuests,
                    HasCake = !x.HasCupcakes,
                    PackageAddons = x.PartyPackage.PackageAddons
                },
                RoomBooking = new RoomBookingModel
                {
                    Date = x.RoomBooking.Date,
                    PartyRoomId = x.RoomBooking.PartyRoomId,
                    PartyRoom = x.RoomBooking.PartyRoom,
                    TimeSlotId = x.RoomBooking.TimeSlotId,
                    TimeSlot = x.RoomBooking.TimeSlot
                },
                PartyAddons = x.PartyAddons.Select(o => new PartyAddonModel()
                {
                    Id = o.Id,
                    AddonType = o.Addon.AddonType,
                    Name = o.Addon.Name,
                    Price = o.Addon.Price,
                    Quantity = o.Quantity
                }).ToList(),
                Notes = x.Notes,
                Total = x.Total,
                Status = x.Status,
                CreatedBy = x.CreatedBy,
                DateCreated = x.DateCreated
            }).ToList();

            return View(model);
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            var package = _partyPackageService.Get(partyOrder.PartyPackageId);

            var partyAddons = partyOrder.PartyAddons.Select(o => new PartyAddonModel()
            {
                Id = o.Id,
                AddonType = o.Addon.AddonType,
                Name = o.Addon.Name,
                Price = o.Addon.Price,
                Quantity = o.Quantity,
                IsIncluded = o.IsIncluded
            }).ToList();

            var model = new PartyOrderModel()
            {
                Id = partyOrder.Id,
                AdditionalGuests = partyOrder.AdditionalGuests,
                PrepaidAdmissions = partyOrder.PrepaidAdmissions,
                PartyTheme = (partyOrder.PartyTheme != null)
                    ? string.Format("{0} THEME", partyOrder.PartyTheme.ToUpper()) : "",
                AddCake = partyOrder.AddCake,
                CakeTheme = partyOrder.CakeTheme,
                CakeFlavour = partyOrder.CakeFlavour,
                HasOwnCake = partyOrder.HasOwnCake,
                HasOwnDecor = partyOrder.HasOwnDecor,
                HasCupcakes = partyOrder.HasCupcakes,
                SwapFoodPlatter = partyOrder.SwapFoodPlatter,
                UpgradeLootBags = partyOrder.UpgradeLootBags,
                AddPlaceSetting = partyOrder.AddPlaceSetting,
                HasOwnPlaceSetting = partyOrder.HasOwnPlaceSetting,
                HasOwnBalloons = partyOrder.HasOwnBalloons,
                HasOwnLootBag = partyOrder.HasOwnLootBag,
                UpgradeLatexBalloons = partyOrder.UpgradeLatexBalloons,
                Parent = partyOrder.Parent,
                Child = partyOrder.Child,
                PartyPackageId = partyOrder.PartyPackage.Id,
                PartyPackage = new PartyPackageModel()
                {
                    Id = partyOrder.PartyPackage.Id,
                    Name = partyOrder.PartyPackage.Name,
                    Description = partyOrder.PartyPackage.Description,
                    Guests = partyOrder.PartyPackage.Guests,
                    GuestPrice = partyOrder.PartyPackage.GuestPrice,
                    Price = partyOrder.PartyPackage.Price,
                    MaxGuests = partyOrder.PartyPackage.MaxGuests,
                    HasCake = partyOrder.PartyPackage.HasCake,
                    PackageAddons = partyOrder.PartyPackage.PackageAddons
                },
                PackageAddons = package.PackageAddons.Select(x => new PackageAddonModel
                {
                    AddonType = x.AddonType,
                    Id = x.Id,
                    PackageId = x.PartyPackageId,
                    Quantity = x.Quantity,
                    RequiredGuests = x.RequiredGuests
                }).ToList(),
                RoomBooking = new RoomBookingModel
                {
                    Date = partyOrder.RoomBooking.Date,
                    PartyRoomId = partyOrder.RoomBooking.PartyRoomId,
                    PartyRoom = partyOrder.RoomBooking.PartyRoom,
                    TimeSlotId = partyOrder.RoomBooking.TimeSlotId,
                    TimeSlot = partyOrder.RoomBooking.TimeSlot
                },
                Promotion = (partyOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", partyOrder.PromotionCode.Code, partyOrder.PromotionCode.Description) : "",
                PaymentTransaction = partyOrder.PaymentTransaction,
                PartyAddons = partyAddons,
                Notes = partyOrder.Notes,
                Discount = partyOrder.Discount,
                SubTotal = partyOrder.SubTotal,
                Tax = partyOrder.Tax,
                Total = partyOrder.Total,
                Status = partyOrder.Status
            };

            if (partyOrder.PartyPackage.Id == 3)
            {
                decimal price;

                switch (partyOrder.RoomBooking.Date.DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        price = 119.99m;
                        break;
                    case DayOfWeek.Sunday:
                    case DayOfWeek.Saturday:
                        price = 249.99m;
                        break;
                    default:
                        price = partyOrder.PartyPackage.Price;
                        break;
                }

                model.PartyPackage.Price = Convert.ToDecimal(price);
            }

            return View(model);
        }

        public async Task<ActionResult> Print(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            var package = _partyPackageService.Get(partyOrder.PartyPackageId);

            var partyAddons = partyOrder.PartyAddons.Select(o => new PartyAddonModel()
            {
                Id = o.Id,
                AddonType = o.Addon.AddonType,
                Name = o.Addon.Name,
                Price = o.Addon.Price,
                Quantity = o.Quantity,
                IsIncluded = o.IsIncluded
            }).ToList();

            var model = new PartyOrderModel()
            {
                Id = partyOrder.Id,
                AdditionalGuests = partyOrder.AdditionalGuests,
                PrepaidAdmissions = partyOrder.PrepaidAdmissions,
                PartyTheme = (partyOrder.PartyTheme != null)
                    ? string.Format("{0} THEME", partyOrder.PartyTheme.ToUpper()) : "",
                AddCake = partyOrder.AddCake,
                CakeTheme = partyOrder.CakeTheme,
                CakeFlavour = partyOrder.CakeFlavour,
                HasOwnCake = partyOrder.HasOwnCake,
                HasOwnDecor = partyOrder.HasOwnDecor,
                HasCupcakes = partyOrder.HasCupcakes,
                SwapFoodPlatter = partyOrder.SwapFoodPlatter,
                UpgradeLootBags = partyOrder.UpgradeLootBags,
                AddPlaceSetting = partyOrder.AddPlaceSetting,
                HasOwnPlaceSetting = partyOrder.HasOwnPlaceSetting,
                HasOwnBalloons = partyOrder.HasOwnBalloons,
                HasOwnLootBag = partyOrder.HasOwnLootBag,
                UpgradeLatexBalloons = partyOrder.UpgradeLatexBalloons,
                Parent = partyOrder.Parent,
                Child = partyOrder.Child,
                PartyPackageId = partyOrder.PartyPackage.Id,
                PartyPackage = new PartyPackageModel()
                {
                    Id = partyOrder.PartyPackage.Id,
                    Name = partyOrder.PartyPackage.Name,
                    Description = partyOrder.PartyPackage.Description,
                    Guests = partyOrder.PartyPackage.Guests,
                    GuestPrice = partyOrder.PartyPackage.GuestPrice,
                    Price = partyOrder.PartyPackage.Price,
                    MaxGuests = partyOrder.PartyPackage.MaxGuests,
                    HasCake = partyOrder.PartyPackage.HasCake,
                    PackageAddons = partyOrder.PartyPackage.PackageAddons
                },
                PackageAddons = package.PackageAddons.Select(x => new PackageAddonModel
                {
                    AddonType = x.AddonType,
                    Id = x.Id,
                    PackageId = x.PartyPackageId,
                    Quantity = x.Quantity,
                    RequiredGuests = x.RequiredGuests
                }).ToList(),
                RoomBooking = new RoomBookingModel
                {
                    Date = partyOrder.RoomBooking.Date,
                    PartyRoomId = partyOrder.RoomBooking.PartyRoomId,
                    PartyRoom = partyOrder.RoomBooking.PartyRoom,
                    TimeSlotId = partyOrder.RoomBooking.TimeSlotId,
                    TimeSlot = partyOrder.RoomBooking.TimeSlot
                },
                Promotion = (partyOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", partyOrder.PromotionCode.Code, partyOrder.PromotionCode.Description) : "",
                PaymentTransaction = partyOrder.PaymentTransaction,
                PartyAddons = partyAddons,
                Notes = partyOrder.Notes,
                Discount = partyOrder.Discount,
                SubTotal = partyOrder.SubTotal,
                Tax = partyOrder.Tax,
                Total = partyOrder.Total,
                Status = partyOrder.Status
            };

            if (partyOrder.PartyPackage.Id == 3)
            {
                decimal price;

                switch (partyOrder.RoomBooking.Date.DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        price = 119.99m;
                        break;
                    case DayOfWeek.Sunday:
                    case DayOfWeek.Saturday:
                        price = 249.99m;
                        break;
                    default:
                        price = partyOrder.PartyPackage.Price;
                        break;
                }

                model.PartyPackage.Price = Convert.ToDecimal(price);
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Save(Guid id)
        {
            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Party/";
            var detailsUrl = baseUri + "Print/" + id;

            byte[] bytesAsPdf = _partyOrderService.GetPdfBytes(detailsUrl);
            if (bytesAsPdf.Any())
                return File(bytesAsPdf, "application/pdf", "Order_Confirmation.pdf");

            return HttpNotFound();
        }

        public async Task<ActionResult> Email(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            var baseUri = Request.Url.Scheme + "://" + Request.Url.Authority + "/Party/";
            await _partyOrderService.SendEmailAsync(baseUri, partyOrder.Id, partyOrder.Parent.Email); ;

            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Delete(Guid id)
        {
            var partyOrder = await _partyOrderService.GetAsync(id);
            if (partyOrder == null) return HttpNotFound();

            var package = _partyPackageService.Get(partyOrder.PartyPackageId);

            var partyAddons = partyOrder.PartyAddons.Select(o => new PartyAddonModel()
            {
                Id = o.Id,
                AddonType = o.Addon.AddonType,
                Name = o.Addon.Name,
                Price = o.Addon.Price,
                Quantity = o.Quantity,
                IsIncluded = o.IsIncluded
            }).ToList();

            var model = new PartyOrderModel()
            {
                Id = partyOrder.Id,
                AdditionalGuests = partyOrder.AdditionalGuests,
                PrepaidAdmissions = partyOrder.PrepaidAdmissions,
                PartyTheme = (partyOrder.PartyTheme != null)
                    ? string.Format("{0} THEME", partyOrder.PartyTheme.ToUpper()) : "",
                AddCake = partyOrder.AddCake,
                CakeTheme = partyOrder.CakeTheme,
                CakeFlavour = partyOrder.CakeFlavour,
                HasOwnCake = partyOrder.HasOwnCake,
                HasOwnDecor = partyOrder.HasOwnDecor,
                HasCupcakes = partyOrder.HasCupcakes,
                SwapFoodPlatter = partyOrder.SwapFoodPlatter,
                UpgradeLootBags = partyOrder.UpgradeLootBags,
                AddPlaceSetting = partyOrder.AddPlaceSetting,
                HasOwnPlaceSetting = partyOrder.HasOwnPlaceSetting,
                HasOwnBalloons = partyOrder.HasOwnBalloons,
                HasOwnLootBag = partyOrder.HasOwnLootBag,
                UpgradeLatexBalloons = partyOrder.UpgradeLatexBalloons,
                Parent = partyOrder.Parent,
                Child = partyOrder.Child,
                PartyPackageId = partyOrder.PartyPackage.Id,
                PartyPackage = new PartyPackageModel()
                {
                    Id = partyOrder.PartyPackage.Id,
                    Name = partyOrder.PartyPackage.Name,
                    Description = partyOrder.PartyPackage.Description,
                    Guests = partyOrder.PartyPackage.Guests,
                    GuestPrice = partyOrder.PartyPackage.GuestPrice,
                    Price = partyOrder.PartyPackage.Price,
                    MaxGuests = partyOrder.PartyPackage.MaxGuests,
                    HasCake = partyOrder.PartyPackage.HasCake,
                    PackageAddons = partyOrder.PartyPackage.PackageAddons
                },
                PackageAddons = package.PackageAddons.Select(x => new PackageAddonModel
                {
                    AddonType = x.AddonType,
                    Id = x.Id,
                    PackageId = x.PartyPackageId,
                    Quantity = x.Quantity,
                    RequiredGuests = x.RequiredGuests
                }).ToList(),
                RoomBooking = new RoomBookingModel
                {
                    Date = partyOrder.RoomBooking.Date,
                    PartyRoomId = partyOrder.RoomBooking.PartyRoomId,
                    PartyRoom = partyOrder.RoomBooking.PartyRoom,
                    TimeSlotId = partyOrder.RoomBooking.TimeSlotId,
                    TimeSlot = partyOrder.RoomBooking.TimeSlot
                },
                Promotion = (partyOrder.PromotionCode != null)
                    ? string.Format("{0} - {1}", partyOrder.PromotionCode.Code, partyOrder.PromotionCode.Description) : "",
                PaymentTransaction = partyOrder.PaymentTransaction,
                PartyAddons = partyAddons,
                Notes = partyOrder.Notes,
                Discount = partyOrder.Discount,
                SubTotal = partyOrder.SubTotal,
                Tax = partyOrder.Tax,
                Total = partyOrder.Total,
                Status = partyOrder.Status
            };

            if (partyOrder.PartyPackage.Id == 3)
            {
                decimal price;

                switch (partyOrder.RoomBooking.Date.DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        price = 119.99m;
                        break;
                    case DayOfWeek.Sunday:
                    case DayOfWeek.Saturday:
                        price = 249.99m;
                        break;
                    default:
                        price = partyOrder.PartyPackage.Price;
                        break;
                }

                model.PartyPackage.Price = Convert.ToDecimal(price);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<RedirectToRouteResult> Delete(RoomOrderModel model)
        {
            await _partyOrderService.CancelAsync(model.Id);
            return RedirectToAction("Index");
        }

        public async Task<RedirectToRouteResult> Cancelled(Guid id)
        {
            await _partyOrderService.CancelAsync(id);
            return RedirectToAction("Index");
        }

        public ActionResult Packages()
        {
            return View();
        }
    }
}