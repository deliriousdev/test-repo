﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StirCrazy.Models.Store
{
    public class StoreHourModel
    {
        public int Id { get; set; }
        [Required]
        public DayOfWeek DayOfWeek { get; set; }
        [Required]
        public TimeSpan OpenTime { get; set; }
        [Required]
        public TimeSpan ClosedTime { get; set; }
        [Required]
        public bool IsClosed { get; set; }
    }
}