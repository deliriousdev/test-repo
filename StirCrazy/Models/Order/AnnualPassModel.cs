﻿using System;
using StirCrazy.Core;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;

namespace StirCrazy.Models.Order
{
    public class AnnualPassModel
    {
        public Guid Id { get; set; }
        public int PayPalTransactionId { get; set; }
        public DateTime PickUpDate { get; set; }
        public PickUpTime PickUpTime { get; set; }
        public string CustomerId { get; set; }
        public int ChildId { get; set; }
        public string Promotion { get; set; }
        public string Notes { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public OrderStatus Status { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }

        public ApplicationUser Customer { get; set; }
        public Child Child { get; set; }
        public PaymentTransaction PaymentTransaction { get; set; }
        public PromotionCode PromotionCode { get; set; }
        public bool BypassPaypal { get; set; }
    }
}