﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Web;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Core.Services
{
    public class MediaService : IMediaService
    {
        private readonly IRepository<MediaFile> _mediaFileRepository;

        public MediaService(IRepository<MediaFile> mediaFileRepository)
        {
            _mediaFileRepository = mediaFileRepository;
        }

        public MediaFile Get(int id)
        {
            var file = _mediaFileRepository.Get(id);
            return file;
        }

        public MediaFile Get(string filepath)
        {
            var file = _mediaFileRepository.Get().FirstOrDefault(x => x.FilePath == filepath);
            return file;
        }

        public List<MediaFile> Get()
        {
            return _mediaFileRepository.Get().OrderBy(x => x.Index).ToList();
        }

        public IEnumerable<MediaFile> Get(UploadFolder uploadFolder)
        {
            return _mediaFileRepository.Get().Where(x => x.UploadFolder == uploadFolder).OrderBy(y => y.Index);
        }

        public int Save(HttpPostedFileBase file, string serverPath, UploadFolder uploadFolder)
        {
            var folderName = ServerPaths.GetUploadFolder(uploadFolder);
            var fileName = string.Format("{0}.png", Guid.NewGuid());

            // File Paths      
            var filePath = string.Format("https://{0}/{1}/{2}", HttpContext.Current.Request.Url.Host, folderName.Replace(@"\", @"/"), fileName);
            var folderPath = Path.Combine(serverPath, folderName);
            var physicalPath = Path.Combine(folderPath, fileName);

            // Save File (Server/Database)
            var mediaFile = new MediaFile()
            {
                Filename = fileName,
                ContentType = file.ContentType,
                FilePath = filePath,
                PhysicalPath = physicalPath,
                UploadFolder = uploadFolder
            };

            // Check Root Directory
            var rootFolder = ServerPaths.GetUploadFolder(UploadFolder.Default);
            var rootPath = Path.Combine(serverPath, rootFolder);
            CheckDirectory(rootPath);

            // Check Directory Exists & Permissions
            CheckDirectory(folderPath);

            file.SaveAs(physicalPath);
            var media = _mediaFileRepository.Add(mediaFile);

            // Resize Image
            ResizeImage(uploadFolder, serverPath, folderName, fileName);
            return media.Id;
        }

        public void Save(HttpPostedFileBase file, MediaFile media, string serverPath, UploadFolder uploadFolder)
        {
            var folderName = ServerPaths.GetUploadFolder(uploadFolder);
            var fileName = string.Format("{0}.png", Guid.NewGuid());

            // File Paths      
            var filePath = string.Format("https://{0}/{1}/{2}", HttpContext.Current.Request.Url.Host, folderName.Replace(@"\", @"/"), fileName);
            var folderPath = Path.Combine(serverPath, folderName);

            var physicalPath = Path.Combine(folderPath, fileName);

            // Save File (Server/Database)
            if (!File.Exists(physicalPath))
            {
                var mediaFile = new MediaFile()
                {
                    Filename = fileName,
                    ContentType = file.ContentType,
                    FilePath = filePath,
                    PhysicalPath = physicalPath,
                    UploadFolder = uploadFolder,
                    Transition = media.Transition,
                    IsActive = true
                };

                // Check Root Directory
                var rootFolder = ServerPaths.GetUploadFolder(UploadFolder.Default);
                var rootPath = Path.Combine(serverPath, rootFolder);
                CheckDirectory(rootPath);

                // Check Directory Exists & Permissions
                CheckDirectory(folderPath);

                file.SaveAs(physicalPath);
                _mediaFileRepository.Add(mediaFile);
            }

            ResizeImage(uploadFolder, serverPath, folderName, fileName);
        }

        public void Resize(int maxWidth, int maxHeight, string serverPath, string folderName, string fileName)
        {
            var imagePath = Path.Combine(serverPath, folderName, fileName);

            try
            {
                // Resize Image File
                var fileImage = Image.FromFile(imagePath);

                var resizedImage = ScaleImage(fileImage, maxWidth, maxHeight);
                var thumbImage = ScaleImage(fileImage, 200, 150);

                fileImage.Dispose();

                // Save Resized Image
                var filename = string.Format("{0}.png", Guid.NewGuid());
                var filepath = string.Format("https://{0}/{1}/{2}", HttpContext.Current.Request.Url.Host, folderName.Replace(@"\", @"/"), filename);
                var physicalpath = Path.Combine(serverPath, folderName.Replace(@"\", @"/"), filename);

                resizedImage.Save(physicalpath);

                // Save Resized Image
                var thumbFilename = string.Format("thumbnail_{0}", filename);
                var thumbFilepath = string.Format("https://{0}/{1}/{2}", HttpContext.Current.Request.Url.Host, folderName.Replace(@"\", @"/"), thumbFilename);
                var thumbPhysicalpath = Path.Combine(serverPath, folderName.Replace(@"\", @"/"), thumbFilename);

                thumbImage.Save(thumbPhysicalpath);

                var mediaFile = _mediaFileRepository.Find(x => x.PhysicalPath == imagePath);

                mediaFile.PhysicalPath = physicalpath;
                mediaFile.FilePath = filepath;
                mediaFile.Filename = filename;
                mediaFile.ThumbnailPath = thumbFilepath;
                mediaFile.ThumbnailPhysical = thumbPhysicalpath;

                _mediaFileRepository.Update(mediaFile, mediaFile.Id);

                // Remove Original Image
                File.Delete(imagePath);
            }
            catch (Exception exception)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }
        }

        private static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }

        private void ResizeImage(UploadFolder uploadFolder, string serverPath, string folderName, string fileName)
        {
            // Resize Image
            switch (uploadFolder)
            {
                case UploadFolder.Cakes:
                    Resize(500, 500, serverPath, folderName, fileName);
                    break;

                case UploadFolder.Facility:
                    Resize(1024, 768, serverPath, folderName, fileName);
                    break;

                case UploadFolder.ParentTotLounge:
                    Resize(1024, 768, serverPath, folderName, fileName);
                    break;

                case UploadFolder.ConcessionCounter:
                    Resize(1024, 768, serverPath, folderName, fileName);
                    break;

                case UploadFolder.Carousel:
                    Resize(968, 545, serverPath, folderName, fileName);
                    break;

                case UploadFolder.PartyRooms:
                    Resize(780, 780, serverPath, folderName, fileName);
                    break;

                case UploadFolder.Announcement:
                    Resize(480, 480, serverPath, folderName, fileName);
                    break;
            }
        }

        private void CheckDirectory(string folderPath)
        {
            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            var sec = Directory.GetAccessControl(folderPath);
            var everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            sec.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.Modify | FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
            Directory.SetAccessControl(folderPath, sec);
        }

        //private void CleanupDirectory(string folderPath)
        //{
        //    var serverPath = AppDomain.CurrentDomain.BaseDirectory;
        //    var rootPath = Path.Combine(serverPath, folderPath);

        //    var files = Directory.GetFiles(rootPath);

        //    foreach (var file in files)
        //    {
        //        var filePath = Path.GetFullPath(file);
        //        var mediaFiles = _mediaFileRepository.Get();

        //        if ((!mediaFiles.Any(x => x.PhysicalPath == filePath)) && (!mediaFiles.Any(x => x.ThumbnailPhysical == filePath)))
        //        {
        //            if (File.Exists(filePath)) { File.Delete(filePath); }
        //        }
        //    }
        //}

        public void Update(MediaFile mediaFile)
        {
            if (mediaFile == null) return;

            if (File.Exists(mediaFile.FilePath))
            {
                _mediaFileRepository.Update(mediaFile, mediaFile.Id);
            }
            else
            {
                _mediaFileRepository.Delete(mediaFile);
            }
        }

        public void ReIndex(int id, int order)
        {
            var mediaFile = Get(id);
            if (mediaFile == null) return;

            mediaFile.Index = order;
            _mediaFileRepository.Update(mediaFile, mediaFile.Id);
        }

        public void Remove(string physicalPath)
        {
            var mediaFile = _mediaFileRepository.Find(x => x.PhysicalPath == physicalPath);
            if (mediaFile == null) return;

            if (File.Exists(mediaFile.PhysicalPath))
            {
                File.Delete(mediaFile.PhysicalPath);
            }

            if (File.Exists(mediaFile.ThumbnailPhysical))
            {
                File.Delete(mediaFile.ThumbnailPhysical);
            }

            _mediaFileRepository.Delete(mediaFile);
        }

        public void Remove(int id)
        {
            var mediaFile = Get(id);
            if (mediaFile == null) return;

            if (File.Exists(mediaFile.PhysicalPath))
            {
                File.Delete(mediaFile.PhysicalPath);
            }

            if (File.Exists(mediaFile.ThumbnailPhysical))
            {
                File.Delete(mediaFile.ThumbnailPhysical);
            }

            _mediaFileRepository.Delete(mediaFile);
        }
    }
}