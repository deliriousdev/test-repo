﻿using System;
using System.Collections.Generic;
using StirCrazy.Core;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;

namespace StirCrazy.Admin.Models.Dashboard
{
    public class OverviewModel
    {
        public IEnumerable<PartyOrder> PartyOrders { get; set; }
        public IEnumerable<RoomOrder> RoomRentals { get; set; }
        public IEnumerable<GrabNGoOrder> GrabNGoOrders { get; set; }
        public IEnumerable<BakeryOrder> BakeryOrders { get; set; }
        public IEnumerable<CorporateEvent> CorporateEvents { get; set; }
        public IEnumerable<AnnualPassOrder> AnnualPassOrders { get; set; }
    }

    public class OrderViewModel
    {
        public string Id { get; set; }
        public OrderType Type { get; set; }
        public DateTime OrderDate { get; set; }
        public string ScheduledTime { get; set; }
        public string ScheduledDate { get; set; }
        public ApplicationUser Customer { get; set; }
        public RoomBooking Booking { get; set; }
        public PartyPackage Package { get; set; }
        public PaymentTransaction Transaction { get; set; }
        public decimal Total { get; set; }
        public OrderStatus Status { get; set; }

        public string cssClass { get; set; }
    }
}