﻿using System;
using System.Collections.Generic;
using System.Linq;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Repositories;

namespace StirCrazy.Core.Services
{
    public class PromotionCodeService : IPromotionCodeService
    {
        private readonly IRepository<PromotionCode> _repository;

        public PromotionCodeService(IRepository<PromotionCode> repository)
        {
            _repository = repository;
        }

        public IEnumerable<PromotionCode> Get()
        {
            return _repository.Get();
        }

        public PromotionCode Get(Guid id)
        {
            return _repository.Get(id);
        }

        public PromotionCode Get(string promotionCode)
        {
            return _repository.Get().FirstOrDefault(x => x.Code == promotionCode);
        }

        public PromotionCode Get(string promotionCode, PromotionType promotionType)
        {
            return _repository.Get().FirstOrDefault(x => x.Code == promotionCode && x.PromotionType == promotionType);
        }

        public PromotionCode Add(PromotionCode promotionCode)
        {
            promotionCode.Created = DateTime.Now;
            promotionCode.Code = promotionCode.Code.ToUpper();
            return _repository.Add(promotionCode);
        }

        public PromotionCode Update(PromotionCode promotionCode)
        {
            promotionCode.Code = promotionCode.Code.ToUpper();
            return _repository.Update(promotionCode, promotionCode.Id);
        }

        public void Remove(Guid id)
        {
            var promotion = _repository.Get(id);
            _repository.Delete(promotion);
        }
    }
}
