﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using StirCrazy.Admin.Models.Party;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Services.Party;

namespace StirCrazy.Admin.Controllers
{
    [Authorize]
    public class PartyPackageController : Controller
    {
        private readonly IPartyPackageService _partyPackageService;

        public PartyPackageController(IPartyPackageService partyPackageService)
        {
            _partyPackageService = partyPackageService;
        }

        public ActionResult Index()
        {
            var model = _partyPackageService.Get().Select(x => new PartyPackageModel()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Guests = x.Guests,
                MaxGuests = x.MaxGuests,
                Price = x.Price,
                PackageAddons = x.PackageAddons,
            }).ToList();

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new PartyPackageModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PartyPackageModel model)
        {
            if (ModelState.IsValid)
            {
                var partyPackage = new PartyPackage()
                {
                    Name = model.Name,
                    Description = model.Description,
                    Price = model.Price,
                    Guests = model.Guests,
                    MaxGuests = model.MaxGuests,
                    HasCake = model.HasCake,
                    PackageAddons = new List<PackageAddon>()
                };

                foreach (var addon in model.PackageAddons)
                {
                    if (addon.Quantity > 0)
                    {
                        partyPackage.PackageAddons.Add(new PackageAddon()
                        {
                            PartyPackageId = partyPackage.Id,
                            AddonType = addon.AddonType,
                            Quantity = addon.Quantity,
                            RequiredGuests = addon.RequiredGuests
                        });
                    }
                }

                _partyPackageService.Add(partyPackage);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var package = _partyPackageService.Get(id);
            if (package == null) return HttpNotFound();

            var model = new PartyPackageModel()
            {
                Id = package.Id,
                Name = package.Name,
                Description = package.Description,
                Price = package.Price,
                Guests = package.Guests,
                MaxGuests = package.MaxGuests,
                HasCake = package.HasCake,
                PackageAddons = package.PackageAddons
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PartyPackageModel model)
        {
            if (ModelState.IsValid)
            {
                var partyPackage = new PartyPackage()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Description = model.Description,
                    Price = model.Price,
                    Guests = model.Guests,
                    MaxGuests = model.MaxGuests,
                    HasCake = model.HasCake,
                    PackageAddons = new List<PackageAddon>()
                };

                foreach (var addon in model.PackageAddons)
                {
                    if (addon.Quantity > 0)
                    {
                        partyPackage.PackageAddons.Add(new PackageAddon()
                        {
                            PartyPackageId = partyPackage.Id,
                            AddonType = addon.AddonType,
                            Quantity = addon.Quantity,
                            RequiredGuests = addon.RequiredGuests
                        });
                    }
                }

                _partyPackageService.Update(partyPackage);
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Details(int id = 0)
        {
            var package = _partyPackageService.Get(id);
            if (package == null) return HttpNotFound();

            var model = new PartyPackageModel()
            {
                Id = package.Id,
                Name = package.Name,
                Description = package.Description,
                Price = package.Price,
                Guests = package.Guests,
                MaxGuests = package.MaxGuests,
                HasCake = package.HasCake,
                PackageAddons = package.PackageAddons
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            _partyPackageService.RemoveAddon(id);
            return Content("");
        }
    }
}
