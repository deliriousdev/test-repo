using System;
using System.Collections.Generic;
using StirCrazy.Core.Domain.Calendar;

namespace StirCrazy.Core.Services
{
    public interface IOffice365Service
    {
        List<CalendarItem> GetEvents();
        List<CalendarItem> GetEvents(DateTime startDateTime, DateTime endDateTime);
        string CreateEvent(DateTime? start, DateTime? end, string summary, string description, bool isAllDay);
        void UpdateEvent(string id, DateTime? start, DateTime? end, string summary, string description, bool isAllDay);
        void DeleteEvent(string id);
    }
}