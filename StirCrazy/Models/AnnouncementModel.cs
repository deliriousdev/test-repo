﻿
using System.Collections.Generic;
using StirCrazy.Core.Domain;

namespace StirCrazy.Models
{
    public class AnnouncementModel
    {
        public int Id { get; set; }
        public int? MediaFileId { get; set; }
        public MediaFile MediaFile { get; set; }
        public string Message { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool IsImage { get; set; }
        public bool IsPopup { get; set; }
    }

    public class PopupModel
    {
        public List<AnnouncementModel> Announcements { get; set; }
    }
}