﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Moq;
using NUnit.Framework;
using StirCrazy.Core;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Party;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services;

namespace Tests.Unit.StirCrazy.StirCrazy.Core.Services
{
    [TestFixture]
    public class RoomBookingServiceTest
    {
        private Mock<IRepository<RoomBooking>> _roomBookingRepositoryMock;
        private Mock<IRepository<TimeSlot>> _timeSlotRepositoryMock;
        private Mock<IRepository<PartyRoom>> _partyRoomRepositoryMock;
        private Mock<IRepository<StoreHour>> _storeHoursRepositoryMock;
        private Mock<IRepository<StoreHoliday>> _storeHolidayRepositoryMock;
        private Mock<IRepository<CorporateEvent>> _corporateEventsRepositoryMock;
        private IRoomBookingService _roomBookingService;

        private List<TimeSlot> _timeSlots;
        private List<StoreHour> _storeHours;
        private List<StoreHoliday> _storeHolidays;
        private List<PartyRoom> _partyRooms;

        [SetUp]
        public void Setup()
        {
            _roomBookingRepositoryMock = new Mock<IRepository<RoomBooking>>();
            _timeSlotRepositoryMock = new Mock<IRepository<TimeSlot>>();
            _partyRoomRepositoryMock = new Mock<IRepository<PartyRoom>>();
            _storeHoursRepositoryMock = new Mock<IRepository<StoreHour>>();
            _storeHolidayRepositoryMock = new Mock<IRepository<StoreHoliday>>();
            _corporateEventsRepositoryMock = new Mock<IRepository<CorporateEvent>>();

            _roomBookingService = new RoomBookingService(_roomBookingRepositoryMock.Object,
                _timeSlotRepositoryMock.Object,
                _partyRoomRepositoryMock.Object,
                _storeHoursRepositoryMock.Object,
                _storeHolidayRepositoryMock.Object,
                _corporateEventsRepositoryMock.Object);

            #region TimeSlots
            _timeSlots = new List<TimeSlot>
            {
                new TimeSlot
                {
                    Id = 1,
                    CheckInTime = new TimeSpan(10, 0, 0),
                    StartTime = new TimeSpan(10, 30, 0),
                    EndTime = new TimeSpan(12, 0, 0),
                    CheckOutTime = new TimeSpan(12, 30, 0)
                },
                new TimeSlot
                {
                    Id = 2,
                    CheckInTime = new TimeSpan(12, 0, 0),
                    StartTime = new TimeSpan(12, 30, 0),
                    EndTime = new TimeSpan(14, 0, 0),
                    CheckOutTime = new TimeSpan(14, 30, 0)
                },
                new TimeSlot
                {
                    Id = 3,
                    CheckInTime = new TimeSpan(14, 0, 0),
                    StartTime = new TimeSpan(14, 30, 0),
                    EndTime = new TimeSpan(16, 0, 0),
                    CheckOutTime = new TimeSpan(16, 30, 0)
                },
                new TimeSlot
                {
                    Id = 4,
                    CheckInTime = new TimeSpan(16, 0, 0),
                    StartTime = new TimeSpan(16, 30, 0),
                    EndTime = new TimeSpan(18, 0, 0),
                    CheckOutTime = new TimeSpan(18, 30, 0)
                },
                new TimeSlot
                {
                    Id = 5,
                    CheckInTime = new TimeSpan(18, 0, 0),
                    StartTime = new TimeSpan(18, 30, 0),
                    EndTime = new TimeSpan(20, 0, 0),
                    CheckOutTime = new TimeSpan(20, 30, 0)
                }
            };
            #endregion

            #region StoreHours
            _storeHours = new List<StoreHour>
            {
                new StoreHour()
                {
                    Id = 1,
                    DayOfWeek = DayOfWeek.Monday,
                    OpenTime = new TimeSpan(9, 30, 00),
                    ClosedTime = new TimeSpan(20, 30, 00),
                    IsClosed = false
                },
                new StoreHour()
                {
                    Id = 2,
                    DayOfWeek = DayOfWeek.Tuesday,
                    OpenTime = new TimeSpan(9, 30, 00),
                    ClosedTime = new TimeSpan(20, 30, 00),
                    IsClosed = false
                },
                new StoreHour()
                {
                    Id = 3,
                    DayOfWeek = DayOfWeek.Wednesday,
                    OpenTime = new TimeSpan(9, 30, 00),
                    ClosedTime = new TimeSpan(20, 30, 00),
                    IsClosed = false
                },
                new StoreHour()
                {
                    Id = 4,
                    DayOfWeek = DayOfWeek.Thursday,
                    OpenTime = new TimeSpan(9, 30, 00),
                    ClosedTime = new TimeSpan(20, 30, 00),
                    IsClosed = false
                },
                new StoreHour()
                {
                    Id = 5,
                    DayOfWeek = DayOfWeek.Friday,
                    OpenTime = new TimeSpan(9, 30, 00),
                    ClosedTime = new TimeSpan(20, 30, 00),
                    IsClosed = false
                },
                new StoreHour()
                {
                    Id = 6,
                    DayOfWeek = DayOfWeek.Saturday,
                    OpenTime = new TimeSpan(9, 30, 00),
                    ClosedTime = new TimeSpan(20, 30, 00),
                    IsClosed = false
                },
                new StoreHour()
                {
                    Id = 7,
                    DayOfWeek = DayOfWeek.Sunday,
                    OpenTime = new TimeSpan(10, 00, 00),
                    ClosedTime = new TimeSpan(18, 00, 00),
                    IsClosed = false
                }
            };
            #endregion

            #region StoreHolidays
            _storeHolidays = new List<StoreHoliday>
            {
                new StoreHoliday()
                {
                    Date = new DateTime(2014, 12, 25),
                    OpenTime = new TimeSpan(),
                    ClosedTime = new TimeSpan(),
                    Holiday = "Christmas Day",
                    IsClosed = true
                },
                new StoreHoliday()
                {
                    Date = new DateTime(2014, 12, 24),
                    OpenTime = new TimeSpan(8, 0, 0),
                    ClosedTime = new TimeSpan(14, 0, 0),
                    Holiday = "Christmas Eve",
                    IsClosed = false
                }
            };
            #endregion

            #region PartyRooms
            _partyRooms = new List<PartyRoom>
            {
                new PartyRoom()
                {
                    Id = 1,
                    Name = "Rock 'N' Roll Red",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 2,
                    Name = "Mellow Yellow",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 3,
                    Name = "Be Bop Blue",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 4,
                    Name = "Outrageous Orange",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 5,
                    Name = "Sublime Lime",
                    Capacity = 30,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 6,
                    Name = "Playful Purple",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                    {
                        new RoomBooking()
                        {
                            Id = 1,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 1,
                            PartyRoom = new Mock<PartyRoom>().Object,
                            TimeSlot = new Mock<TimeSlot>().Object
                        },
                        new RoomBooking()
                        {
                            Id = 3,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 2,
                            PartyRoom = new Mock<PartyRoom>().Object,
                            TimeSlot = new Mock<TimeSlot>().Object
                        }
                    }
                }
            };

            #endregion
        }

        [Test]
        public async void GetAvailabilityShouldReturnTimeSlotsWhenAvailable()
        {
            _storeHoursRepositoryMock.Setup(x => x.Get()).Returns(_storeHours.AsQueryable);
            _storeHolidayRepositoryMock.Setup(x => x.Get()).Returns(_storeHolidays.AsQueryable);
            _timeSlotRepositoryMock.Setup(x => x.Get()).Returns(_timeSlots.AsQueryable);
            _partyRoomRepositoryMock.Setup(x => x.Get()).Returns(_partyRooms.AsQueryable);

            var availableTimeSlots = _roomBookingService.GetAvailability(new DateTime(2014, 11, 15));

            Assert.AreEqual(30, availableTimeSlots.Count());
        }

        [Test]
        public void GetAvailabilityShouldReturnTimeSlotsWhenPartyRoomNotAvailable()
        {
            _storeHoursRepositoryMock.Setup(x => x.Get()).Returns(_storeHours.AsQueryable);
            _storeHolidayRepositoryMock.Setup(x => x.Get()).Returns(_storeHolidays.AsQueryable);
            _timeSlotRepositoryMock.Setup(x => x.Get()).Returns(_timeSlots.AsQueryable);

            #region PartyRooms
            var partyRooms = new List<PartyRoom>
            {
                new PartyRoom()
                {
                    Id = 1,
                    Name = "Rock 'N' Roll Red",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 2,
                    Name = "Mellow Yellow",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 3,
                    Name = "Be Bop Blue",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 4,
                    Name = "Outrageous Orange",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 5,
                    Name = "Sublime Lime",
                    Capacity = 30,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 6,
                    Name = "Playful Purple",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                    {
                        new RoomBooking()
                        {
                            Id = 1,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 1,
                            PartyRoom = new Mock<PartyRoom>().Object,
                            TimeSlot = new Mock<TimeSlot>().Object,
                            Status = Status.Cancelled
                        },
                        new RoomBooking()
                        {
                            Id = 2,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 2,
                            PartyRoom = new Mock<PartyRoom>().Object,
                            TimeSlot = new Mock<TimeSlot>().Object,
                            Status = Status.Scheduled
                        },
                        new RoomBooking()
                        {
                            Id = 3,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 3,
                            PartyRoom = new Mock<PartyRoom>().Object,
                            TimeSlot = new Mock<TimeSlot>().Object,
                            Status = Status.Scheduled
                        },
                        new RoomBooking()
                        {
                            Id = 4,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 4,
                            PartyRoom = new Mock<PartyRoom>().Object,
                            TimeSlot = new Mock<TimeSlot>().Object,
                            Status = Status.Scheduled
                        },
                        new RoomBooking()
                        {
                            Id = 5,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 5,
                            PartyRoom = new Mock<PartyRoom>().Object,
                            TimeSlot = new Mock<TimeSlot>().Object,
                            Status = Status.Scheduled
                        }
                    }
                }
            };
            #endregion

            _partyRoomRepositoryMock.Setup(x => x.Get()).Returns(partyRooms.AsQueryable);

            var availableTimeSlots = _roomBookingService.GetAvailability(new DateTime(2014, 11, 15));
            var partyRoom = availableTimeSlots.Where(x => x.Item1.Id == 6);

            Assert.AreEqual(30, availableTimeSlots.Count());
            Assert.AreEqual(5, partyRoom.Count());
        }

        [Test]
        public void CheckAvialabilityShouldReturnFalseWhenPartyRoomNotAvailable()
        {
            _storeHoursRepositoryMock.Setup(x => x.Get()).Returns(_storeHours.AsQueryable);
            _storeHolidayRepositoryMock.Setup(x => x.Get()).Returns(_storeHolidays.AsQueryable);
            _timeSlotRepositoryMock.Setup(x => x.Get()).Returns(_timeSlots.AsQueryable);

            #region PartyRooms

            var partyRooms = new List<PartyRoom>
            {
                new PartyRoom()
                {
                    Id = 1,
                    Name = "Rock 'N' Roll Red",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 2,
                    Name = "Mellow Yellow",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 3,
                    Name = "Be Bop Blue",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 4,
                    Name = "Outrageous Orange",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 5,
                    Name = "Sublime Lime",
                    Capacity = 30,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 6,
                    Name = "Playful Purple",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                    {
                        new RoomBooking()
                        {
                            Id = 1,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 1,
                        },
                        new RoomBooking()
                        {
                            Id = 2,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 2,
                        },
                        new RoomBooking()
                        {
                            Id = 3,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 3,
                        },
                        new RoomBooking()
                        {
                            Id = 4,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 4,
                        },
                        new RoomBooking()
                        {
                            Id = 5,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 5,
                        }
                    }
                }
            };

            #endregion

            #region RoomBookings

            var roomBookings = new List<RoomBooking>()
            { 
                    new RoomBooking()
                {
                    Id = 1,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 1,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 1),
                    Status = Status.Scheduled
                },        
                new RoomBooking()
                {
                    Id = 2,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 2,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 2),
                    Status = Status.Scheduled
                },
                new RoomBooking()
                {
                    Id = 3,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 3,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 3),
                    Status = Status.Scheduled
                },
                new RoomBooking()
                {
                    Id = 4,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 4,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 4),
                    Status = Status.Scheduled
                },
                new RoomBooking()
                {
                    Id = 5,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 5,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 5),
                    Status = Status.Scheduled
                }
            };
            #endregion

            _partyRoomRepositoryMock.Setup(x => x.Get()).Returns(partyRooms.AsQueryable);
            _roomBookingRepositoryMock.Setup(x => x.Get()).Returns(roomBookings.AsQueryable);

            var isAvailable = _roomBookingService.CheckAvailability(new DateTime(2014, 11, 15), new TimeSpan(8, 30, 0), new TimeSpan(14, 0, 0));
            Assert.IsFalse(isAvailable);
        }

        [Test]
        public void CheckAvialabilityShouldReturnFalseEvenWhenPartyRoomsAvailable()
        {
            _storeHoursRepositoryMock.Setup(x => x.Get()).Returns(_storeHours.AsQueryable);
            _storeHolidayRepositoryMock.Setup(x => x.Get()).Returns(_storeHolidays.AsQueryable);
            _timeSlotRepositoryMock.Setup(x => x.Get()).Returns(_timeSlots.AsQueryable);

            #region PartyRooms

            var partyRooms = new List<PartyRoom>
            {
                new PartyRoom()
                {
                    Id = 1,
                    Name = "Rock 'N' Roll Red",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 2,
                    Name = "Mellow Yellow",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 3,
                    Name = "Be Bop Blue",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 4,
                    Name = "Outrageous Orange",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 5,
                    Name = "Sublime Lime",
                    Capacity = 30,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 6,
                    Name = "Playful Purple",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                    {
                        new RoomBooking()
                        {
                            Id = 1,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 1,
                        },
                        new RoomBooking()
                        {
                            Id = 2,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 2,
                        },
                        new RoomBooking()
                        {
                            Id = 3,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 3,
                        },
                        new RoomBooking()
                        {
                            Id = 4,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 4,
                        },
                        new RoomBooking()
                        {
                            Id = 5,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 5,
                        }
                    }
                }
            };

            #endregion

            #region RoomBookings

            var roomBookings = new List<RoomBooking>()
            { 
                    new RoomBooking()
                {
                    Id = 1,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 1,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 1),
                    Status = Status.Scheduled
                },        
                new RoomBooking()
                {
                    Id = 2,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 2,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 2),
                    Status = Status.Scheduled
                },
                new RoomBooking()
                {
                    Id = 3,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 3,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 3),
                    Status = Status.Scheduled
                },
                new RoomBooking()
                {
                    Id = 4,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 4,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 4),
                    Status = Status.Scheduled
                },
                new RoomBooking()
                {
                    Id = 5,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 5,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 5),
                    Status = Status.Scheduled
                }
            };
            #endregion

            _partyRoomRepositoryMock.Setup(x => x.Get()).Returns(partyRooms.AsQueryable);
            _roomBookingRepositoryMock.Setup(x => x.Get()).Returns(roomBookings.AsQueryable);

            var isAvailable = _roomBookingService.CheckAvailability(new DateTime(2014, 11, 15), new TimeSpan(18, 30, 0), new TimeSpan(19, 0, 0));
            Assert.IsFalse(isAvailable);
        }

        [Test]
        public void CheckAvailabilityShouldReturnTimeSlotsWhenPartyRoomsAvailableWithCorporateEvent()
        {
            _storeHoursRepositoryMock.Setup(x => x.Get()).Returns(_storeHours.AsQueryable);
            _storeHolidayRepositoryMock.Setup(x => x.Get()).Returns(_storeHolidays.AsQueryable);
            _timeSlotRepositoryMock.Setup(x => x.Get()).Returns(_timeSlots.AsQueryable);
            _partyRoomRepositoryMock.Setup(x => x.Get()).Returns(_partyRooms.AsQueryable);

            #region CorporateEvents
            var corporateEvents = new List<CorporateEvent>
            {
                new CorporateEvent()
                {
                    Id = Guid.NewGuid(),
                    Customer = new Mock<ApplicationUser>().Object,
                    CustomerId = It.IsAny<string>(),
                    Date = new DateTime(2014, 11, 15),
                    StartTime = new TimeSpan(8, 30, 0),
                    EndTime = new TimeSpan(11, 30, 0),
                    IsAllDay = false
                }
            };

            #endregion
            _corporateEventsRepositoryMock.Setup(x => x.Get()).Returns(corporateEvents.AsQueryable);

            var availableTimeSlots = _roomBookingService.CheckAvailability(new DateTime(2014, 11, 15), new TimeSpan(8, 30, 0), new TimeSpan(11, 30, 0));
            Assert.IsFalse(availableTimeSlots);
        }

        [Test]
        public void CheckAvailabilityShouldNotReturnTimeSlotsWhenNoPartyRoomsAvailableWithCorporateEvent()
        {
            _storeHoursRepositoryMock.Setup(x => x.Get()).Returns(_storeHours.AsQueryable);
            _storeHolidayRepositoryMock.Setup(x => x.Get()).Returns(_storeHolidays.AsQueryable);
            _timeSlotRepositoryMock.Setup(x => x.Get()).Returns(_timeSlots.AsQueryable);

            #region PartyRooms
            var partyRooms = new List<PartyRoom>
            {
                new PartyRoom()
                {
                    Id = 1,
                    Name = "Rock 'N' Roll Red",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 2,
                    Name = "Mellow Yellow",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 3,
                    Name = "Be Bop Blue",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 4,
                    Name = "Outrageous Orange",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 5,
                    Name = "Sublime Lime",
                    Capacity = 30,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                },
                new PartyRoom()
                {
                    Id = 6,
                    Name = "Playful Purple",
                    Capacity = 25,
                    IsAvailable = true,
                    Bookings = new Collection<RoomBooking>()
                    {
                        new RoomBooking()
                        {
                            Id = 1,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 1,
                        },
                        new RoomBooking()
                        {
                            Id = 2,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 2,
                        },
                        new RoomBooking()
                        {
                            Id = 3,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 3,
                        },
                        new RoomBooking()
                        {
                            Id = 4,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 4,
                        },
                        new RoomBooking()
                        {
                            Id = 5,
                            BookingType = BookingType.PartyOrder,
                            Date = new DateTime(2014, 11, 15),
                            PartyRoomId = 6,
                            TimeSlotId = 5,
                        }
                    }
                }
            };
            #endregion
            #region RoomBookings

            var roomBookings = new List<RoomBooking>()
            { 
                    new RoomBooking()
                {
                    Id = 1,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 1,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 1),
                    Status = Status.Cancelled
                },        
                new RoomBooking()
                {
                    Id = 2,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 2,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 2),
                    Status = Status.Pending
                },
                new RoomBooking()
                {
                    Id = 3,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 3,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 3),
                    Status = Status.Pending
                },
                new RoomBooking()
                {
                    Id = 4,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 4,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 4),
                    Status = Status.Scheduled
                },
                new RoomBooking()
                {
                    Id = 5,
                    BookingType = BookingType.PartyOrder,
                    Date = new DateTime(2014, 11, 15),
                    PartyRoomId = 6,
                    PartyRoom = _partyRooms.FirstOrDefault(x => x.Id == 6),
                    TimeSlotId = 5,
                    TimeSlot = _timeSlots.FirstOrDefault(x => x.Id == 5),
                    Status = Status.Scheduled
                }
            };
            #endregion

            _partyRoomRepositoryMock.Setup(x => x.Get()).Returns(partyRooms.AsQueryable);
            _roomBookingRepositoryMock.Setup(x => x.Get()).Returns(roomBookings.AsQueryable);

            var availableTimeSlots = _roomBookingService.CheckAvailability(new DateTime(2014, 11, 15), new TimeSpan(18, 30, 0), new TimeSpan(19, 30, 0));
            Assert.IsFalse(availableTimeSlots);
        }

    }
}
