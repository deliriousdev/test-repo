﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using SendGrid;
using StirCrazy.Core.Constants;
using StirCrazy.Core.Domain;
using StirCrazy.Core.Domain.Orders;
using StirCrazy.Core.Repositories;
using StirCrazy.Core.Services.Accounts;
using StirCrazy.Core.Utilities;

namespace StirCrazy.Core.Services.Order
{
    public class BakeryOrderService : IBakeryOrderService
    {
        private readonly IRepository<BakeryOrder> _bakeryOrderRepository;
        private readonly IAccountService _accountService;
        private readonly ISendGridService _sendGridService;

        public BakeryOrderService(IRepository<BakeryOrder> bakeryOrderRepository, 
            IAccountService accountService, 
            ISendGridService sendGridService)
        {
            _bakeryOrderRepository = bakeryOrderRepository;
            _accountService = accountService;
            _sendGridService = sendGridService;
        }

        public IEnumerable<BakeryOrder> Get()
        {
            return _bakeryOrderRepository.Get();
        }

        public BakeryOrder Get(Guid id)
        {
            return _bakeryOrderRepository.Get(id);
        }

        public async Task<BakeryOrder> GetAsync(Guid id)
        {
            return await _bakeryOrderRepository.GetAsync(id);
        }

        public IEnumerable<BakeryOrder> GetByStatus(OrderStatus status)
        {
            if (status == OrderStatus.Created)
                return _bakeryOrderRepository.Get().Where(x => x.PickUpDate.Date >= DateTime.Today);

            if (status == OrderStatus.Cancelled)
                return _bakeryOrderRepository.Get().Where(x => x.Status == OrderStatus.Cancelled);

            return _bakeryOrderRepository.Get();
        }

        public async Task<BakeryOrder> CreateAsync(BakeryOrder bakeryOrder)
        {
            var account = await _accountService.FindByEmailAsync(bakeryOrder.Customer.Email);

            // Format Data
            bakeryOrder.Customer.UserName = bakeryOrder.Customer.Email.ToLower();
            bakeryOrder.Customer.FirstName = HtmlHelperExtensions.ToTitleCase(bakeryOrder.Customer.FirstName);
            bakeryOrder.Customer.LastName = HtmlHelperExtensions.ToTitleCase(bakeryOrder.Customer.LastName);
            bakeryOrder.Customer.Address = HtmlHelperExtensions.ToTitleCase(bakeryOrder.Customer.Address);
            bakeryOrder.Customer.City = HtmlHelperExtensions.ToTitleCase(bakeryOrder.Customer.City);
            bakeryOrder.Customer.PostalCode = bakeryOrder.Customer.PostalCode.ToUpper();
            bakeryOrder.Customer.Province = bakeryOrder.Customer.Province;
            bakeryOrder.Customer.PhoneNumber = HtmlHelperExtensions.PhoneNumber(bakeryOrder.Customer.PhoneNumber);
            bakeryOrder.Customer.Email = bakeryOrder.Customer.Email.ToLower();

            bakeryOrder.Child.FirstName = HtmlHelperExtensions.ToTitleCase(bakeryOrder.Child.FirstName);
            bakeryOrder.Child.LastName = HtmlHelperExtensions.ToTitleCase(bakeryOrder.Child.LastName);

            if (account != null)
            {
                // Update Account Information
                account.UserName = bakeryOrder.Customer.Email;
                account.FirstName = bakeryOrder.Customer.FirstName;
                account.LastName = bakeryOrder.Customer.LastName;
                account.Address = bakeryOrder.Customer.Address;
                account.City = bakeryOrder.Customer.City;
                account.PostalCode = bakeryOrder.Customer.PostalCode;
                account.Province = bakeryOrder.Customer.Province;
                account.PhoneNumber = bakeryOrder.Customer.PhoneNumber;
                account.Email = bakeryOrder.Customer.Email;

                bakeryOrder.CustomerId = account.Id;
                bakeryOrder.Customer = null;

                await _accountService.UpdateAsync(account);
            }

            bakeryOrder.DateCreated = DateTime.Now;
            bakeryOrder.CreatedBy = bakeryOrder.CreatedBy ?? "Customer";

            var order = await _bakeryOrderRepository.AddAsync(bakeryOrder);

            // Setup Account Access
            if (account == null)
            {
                _accountService.SetPassword(order.Customer.Id);
                _accountService.AddToRole(order.Customer.Id, UserRoles.Customer);
            }

            return order;
        }

        public async Task UpdateAsync(BakeryOrder bakeryOrder)
        {
            if (bakeryOrder != null)
            {
                await _bakeryOrderRepository.UpdateAsync(bakeryOrder, bakeryOrder.Id);
            }
        }

        public async Task SendEmailAsync(string baseUri, Guid id, string email)
        {
            await _sendGridService.SendEmail(baseUri, id, email, ConfirmationType.BakeryOrder);
        }

        public async Task CancelAsync(Guid id)
        {
            var bakeryOrder = await _bakeryOrderRepository.GetAsync(id);
            if (bakeryOrder != null)
            {
                bakeryOrder.Status = OrderStatus.Cancelled;
                await _bakeryOrderRepository.UpdateAsync(bakeryOrder, bakeryOrder.Id);
            }
        }

        public async Task UpdateStatusAsync(Guid id, OrderStatus status)
        {
            var bakeryOrder = await _bakeryOrderRepository.GetAsync(id);
            if (bakeryOrder != null)
            {
                bakeryOrder.Status = status;
                await _bakeryOrderRepository.UpdateAsync(bakeryOrder, bakeryOrder.Id);
            }
        }

        public byte[] GetPdfBytes(string url)
        {
            return _sendGridService.GetPdfBytes(url);
        }

        public async Task SendOrderNotificationAsync(BakeryOrder bakeryOrder)
        {
            if (bakeryOrder == null) return;

            var customer = _accountService.FindById(bakeryOrder.CustomerId);
            bakeryOrder.Customer = customer;

            var email = ConfigurationManager.AppSettings["BakeryOrderNotifications"];

            var pickupDate = bakeryOrder.PickUpDate.Date.ToString("ddd MMM dd, yyyy");
            var pickupTime = bakeryOrder.PickUpTime.GetDisplayName();
            var formattedPhoneNumber = HtmlHelperExtensions.PhoneNumber(bakeryOrder.Customer.PhoneNumber);
            var promotionCode = (bakeryOrder.PromotionCode != null) ? bakeryOrder.PromotionCode.Code : "NONE";

            if (bakeryOrder.PromotionCode != null)
            {
                promotionCode = string.Format("{0} - {1}", bakeryOrder.PromotionCode.Code, bakeryOrder.PromotionCode.Description);
            }

            var mail = new SendGridMessage
            {
                Subject = "Bakery Order Notification",
                Html = "<p>"
                + "<h2>Order Information</h2>"
                + "Pickup Date: <strong>" + pickupDate + "</strong><br />"
                + "Pickup Time: <strong>" + pickupTime + "</strong><br />"
                + "<br /><h2>Customer Information</h2>"
                + "Full Name: <strong>" + bakeryOrder.Customer.FullName + "</strong><br />"
                + "Address: <strong>" + bakeryOrder.Customer.FormattedAddress + "</strong><br />"
                + "Phone: <strong>" + formattedPhoneNumber + "</strong><br />"
                + "Email: <strong>" + bakeryOrder.Customer.Email + "</strong><br />"
                + "<br /><h2>Payment Information</h2>"
                + "Transaction: <strong>" + bakeryOrder.PaymentTransaction.TransactionNumber + "</strong><br />"
                + "Amount: <strong>$" + bakeryOrder.PaymentTransaction.Amount + "</strong><br />"
                + "Promotion: <strong>" + promotionCode + "</strong><br />"
                + "Status: <strong>" + bakeryOrder.PaymentTransaction.PaymentStatus.ToUpper() + "</strong>"
                + "</p>",
            };

            mail.AddTo(email);
            mail.From = new MailAddress("no-reply@stircrazy.ca", "Stir Crazy Family Fun Centre");

            try { await _sendGridService.Send(mail); }
            catch (Exception exception) { Elmah.ErrorSignal.FromCurrentContext().Raise(exception); }
        }

        public async Task RemoveAsync(Guid id)
        {
            var bakeryOrder = await _bakeryOrderRepository.GetAsync(id);
            if (bakeryOrder == null) return;

            bakeryOrder.Status = OrderStatus.Cancelled;
            await _bakeryOrderRepository.UpdateAsync(bakeryOrder, bakeryOrder.Id);
        }

    }
}